﻿using AutoMapper;
using TTCMS.Data.Entities;
using TTCMS.Web.Models;
using System;
using TTCMS.Web.Areas.TT_Admin.Models;
using PagedList;
using TTCMS.Core.AutoMapperConverters;
using TTCMS.Web.Models.Product;
using TTCMS.Web.Models.News;
using TTCMS.Web.ViewModels;
using TTCMS.Data.Entities.WebsiteNews;

namespace TTCMS.Web.Mappings
{
    public class EntityToViewModelMappingProfile : Profile
    {
        public override string ProfileName
        {
            get { return "DomainToViewModelMappings"; }
        }

        protected override void Configure()
        {
            ////Config
            Mapper.CreateMap<ConfigViewModel, ConfigurationViewModel>();
            Mapper.CreateMap<ConfigurationViewModel, ConfigViewModel>();
            //home single page
            Mapper.CreateMap<SinglePage, SinglePageHomeViewModel>();
            Mapper.CreateMap<IPagedList<SinglePage>, IPagedList<SinglePageHomeViewModel>>()
                .ConvertUsing<PagedListConverter<SinglePage, SinglePageHomeViewModel>>();

            Mapper.CreateMap<Tuple<SinglePage, Language_SinglePage>, SinglePageHomeViewModel>()
                .ForMember(d => d.Id, opt => opt.MapFrom(s => s.Item1.Id))
                .ForMember(d => d.Views, opt => opt.MapFrom(s => s.Item1.Views))
                .ForMember(d => d.Img_Thumbnail, opt => opt.MapFrom(s => s.Item1.Img_Thumbnail))
                .ForMember(d => d.CssClass, opt => opt.MapFrom(s => s.Item1.CssClass))
                .ForMember(d => d.IsActive, opt => opt.MapFrom(s => s.Item1.IsActive))
                .ForMember(d => d.IsHot, opt => opt.MapFrom(s => s.Item1.IsHot))
                .ForMember(d => d.IsHome, opt => opt.MapFrom(s => s.Item1.IsHome))
                .ForMember(d => d.Order, opt => opt.MapFrom(s => s.Item1.Order))
                .ForMember(d => d.CreatedById, opt => opt.MapFrom(s => s.Item1.CreatedById))
                .ForMember(d => d.CreatedDate, opt => opt.MapFrom(s => s.Item1.CreatedDate))
                .ForMember(d => d.SinglePage_Id, opt => opt.MapFrom(s => s.Item2.Id))
                .ForMember(d => d.Title, opt => opt.MapFrom(s => s.Item2.Title))
                .ForMember(d => d.Summary, opt => opt.MapFrom(s => s.Item2.Summary))
                .ForMember(d => d.Body, opt => opt.MapFrom(s => s.Item2.Body))
                .ForMember(d => d.Route, opt => opt.MapFrom(s => s.Item2.Slug))
                .ForMember(d => d.Keywords, opt => opt.MapFrom(s => s.Item2.Keywords))
                .ForMember(d => d.Tag, opt => opt.MapFrom(s => s.Item2.Tag))
                .ForMember(d => d.LanguageId, opt => opt.MapFrom(s => s.Item2.LanguageId))
                .ForMember(d => d.Description, opt => opt.MapFrom(s => s.Item2.Description));
            ////category
            Mapper.CreateMap<Category, CateHomeViewModel>();
            Mapper.CreateMap<Category, CategoryViewModel>()
                .ForMember(d => d.CategoryId, opt => opt.MapFrom(s => s.Id));
            Mapper.CreateMap<CategoryItem, CategoryViewModel>();
            Mapper.CreateMap<CategoryItem, CategoryItemViewModel>();
            Mapper.CreateMap<Category, CategoryViewModel>();
            Mapper.CreateMap<IPagedList<Category>, IPagedList<CategoryViewModel>>().ConvertUsing<PagedListConverter<Category, CategoryViewModel>>();
            ////media
            Mapper.CreateMap<Media, MediaViewModel>();
            Mapper.CreateMap<FAQ_Media, MediaViewModel>();
            Mapper.CreateMap<IPagedList<Media>, IPagedList<MediaViewModel>>().ConvertUsing<PagedListConverter<Media, MediaViewModel>>();
            Mapper.CreateMap<IPagedList<FAQ_Media>, IPagedList<MediaViewModel>>().ConvertUsing<PagedListConverter<FAQ_Media, MediaViewModel>>();

            ////slide
            Mapper.CreateMap<Slide, SlideViewModel>();
            Mapper.CreateMap<IPagedList<Slide>, IPagedList<SlideViewModel>>().ConvertUsing<PagedListConverter<Slide, SlideViewModel>>();
            //config
            Mapper.CreateMap<Setting, ConfigurationViewModel>();
            //news
            Mapper.CreateMap<News, NewsViewModel>();
            Mapper.CreateMap<NewsItem, NewsItemViewModel>();
            Mapper.CreateMap<IPagedList<News>, IPagedList<NewsViewModel>>().ConvertUsing<PagedListConverter<News, NewsViewModel>>();
            Mapper.CreateMap<stock_News, StockNewsViewModel>();
            Mapper.CreateMap<IPagedList<stock_News>, IPagedList<StockNewsViewModel>>().ConvertUsing<PagedListConverter<stock_News, StockNewsViewModel>>();
            Mapper.CreateMap<Slide, SlideViewModel>();
            Mapper.CreateMap<IPagedList<Slide>, IPagedList<SlideViewModel>>().ConvertUsing<PagedListConverter<Slide, SlideViewModel>>();
            //setting
            Mapper.CreateMap<Language_Setting, LangSettingViewModel>();
            Mapper.CreateMap<Setting, SettingViewModel>();
            Mapper.CreateMap<Language, ListLang>();

            //menu
            Mapper.CreateMap<Menu, MenuManagerViewModel>();
            Mapper.CreateMap<Menu, MenuViewModel>()
                .ForMember(d => d.Text, opt => opt.MapFrom(s => s.Name));

            //single page
            Mapper.CreateMap<SinglePage, SinglePageViewModel>();
            Mapper.CreateMap<IPagedList<SinglePage>, IPagedList<SinglePageViewModel>>().ConvertUsing<PagedListConverter<SinglePage, SinglePageViewModel>>();
            Mapper.CreateMap<Tuple<SinglePage, Language_SinglePage>, SinglePageViewModel>()
                .ForMember(d => d.Id, opt => opt.MapFrom(s => s.Item1.Id))
                .ForMember(d => d.Views, opt => opt.MapFrom(s => s.Item1.Views))
                .ForMember(d => d.Img_Thumbnail, opt => opt.MapFrom(s => s.Item1.Img_Thumbnail))
                .ForMember(d => d.CssClass, opt => opt.MapFrom(s => s.Item1.CssClass))
                .ForMember(d => d.IsActive, opt => opt.MapFrom(s => s.Item1.IsActive))
                .ForMember(d => d.IsHot, opt => opt.MapFrom(s => s.Item1.IsHot))
                .ForMember(d => d.IsHome, opt => opt.MapFrom(s => s.Item1.IsHome))
                .ForMember(d => d.Order, opt => opt.MapFrom(s => s.Item1.Order))
                .ForMember(d => d.CreatedById, opt => opt.MapFrom(s => s.Item1.CreatedById))
                .ForMember(d => d.CreatedDate, opt => opt.MapFrom(s => s.Item1.CreatedDate))
                .ForMember(d => d.SinglePage_Id, opt => opt.MapFrom(s => s.Item2.Id))
                .ForMember(d => d.Title, opt => opt.MapFrom(s => s.Item2.Title))
                .ForMember(d => d.Summary, opt => opt.MapFrom(s => s.Item2.Summary))
                .ForMember(d => d.Body, opt => opt.MapFrom(s => s.Item2.Body))
                .ForMember(d => d.Route, opt => opt.MapFrom(s => s.Item2.Slug))
                .ForMember(d => d.Keywords, opt => opt.MapFrom(s => s.Item2.Keywords))
                .ForMember(d => d.Tag, opt => opt.MapFrom(s => s.Item2.Tag))
                .ForMember(d => d.LanguageId, opt => opt.MapFrom(s => s.Item2.LanguageId))
                .ForMember(d => d.Description, opt => opt.MapFrom(s => s.Item2.Description));

            Mapper.CreateMap<Language_SinglePage, Language_SinglePageViewModel>();

            //Language
            Mapper.CreateMap<Language, LanguageItemViewModel>();
            Mapper.CreateMap<Language, LanguageViewModel>();
            Mapper.CreateMap<IPagedList<Language>, IPagedList<LanguageViewModel>>().ConvertUsing<PagedListConverter<Language, LanguageViewModel>>();
            //user manager
            Mapper.CreateMap<ApplicationUser, UserFormModel>();
            Mapper.CreateMap<ApplicationUser, UserEditFormModel>();
            Mapper.CreateMap<ApplicationUser, UserProfileViewModel>();
            //role manager
            Mapper.CreateMap<ApplicationRole, RoleViewModel>();
            Mapper.CreateMap<ApplicationRole, RoleOptiViewModel>();
            //Log manager
            Mapper.CreateMap<ActivityLog, LogActionViewModel>();
            Mapper.CreateMap<ActivityLog, LogViewModel>();
            Mapper.CreateMap<IPagedList<ActivityLog>, IPagedList<LogViewModel>>().ConvertUsing<PagedListConverter<ActivityLog, LogViewModel>>();
            //gaction
            Mapper.CreateMap<GAction, ActionLangViewModel>();
            Mapper.CreateMap<IPagedList<GAction>, IPagedList<ActionLangViewModel>>().ConvertUsing<PagedListConverter<GAction, ActionLangViewModel>>();
            Mapper.CreateMap<IPagedList<Language_GAction>, IPagedList<ActionLangViewModel>>().ConvertUsing<PagedListConverter<Language_GAction, ActionLangViewModel>>();
            Mapper.CreateMap<GAction, ActionLangViewModel>();

            //Group  map
            Mapper.CreateMap<Tuple<ApplicationRole, Language_Role>, RoleViewModel>()
                .ForMember(d => d.Id, opt => opt.MapFrom(s => s.Item1.Id))
                .ForMember(d => d.Name, opt => opt.MapFrom(s => s.Item1.Name))
                .ForMember(d => d.IsActived, opt => opt.MapFrom(s => s.Item1.IsActived))
                .ForMember(d => d.CreatedDate, opt => opt.MapFrom(s => s.Item1.CreatedDate))
                .ForMember(d => d.CreatedBy, opt => opt.MapFrom(s => s.Item1.CreatedBy))
                .ForMember(d => d.IsDeleted, opt => opt.MapFrom(s => s.Item1.IsDeleted))
                .ForMember(d => d.Language_RoleId, opt => opt.MapFrom(s => s.Item2.Language_RoleId))
                .ForMember(d => d.LanguageId, opt => opt.MapFrom(s => s.Item2.LanguageId))
                .ForMember(d => d.Description, opt => opt.MapFrom(s => s.Item2.Description));

            Mapper.CreateMap<Tuple<GAction, Language_GAction>, ActionLangViewModel>()
                .ForMember(d => d.Id, opt => opt.MapFrom(s => s.Item1.Id))
                .ForMember(d => d.IsActived, opt => opt.MapFrom(s => s.Item1.IsActived))
                .ForMember(d => d.CreatedDate, opt => opt.MapFrom(s => s.Item1.CreatedDate))
                .ForMember(d => d.CreatedBy, opt => opt.MapFrom(s => s.Item1.CreatedBy))
                .ForMember(d => d.IsDeleted, opt => opt.MapFrom(s => s.Item1.IsDeleted))
                .ForMember(d => d.Lang_ActionId, opt => opt.MapFrom(s => s.Item2.Id))
                .ForMember(d => d.LanguageId, opt => opt.MapFrom(s => s.Item2.LanguageId))
                .ForMember(d => d.Name, opt => opt.MapFrom(s => s.Item2.Name))
                .ForMember(d => d.Description, opt => opt.MapFrom(s => s.Item2.Description));

            Mapper.CreateMap<Tuple<Function, Language_Function>, FunctionFormModel>()
                .ForMember(d => d.Id, opt => opt.MapFrom(s => s.Item1.Id))
                .ForMember(d => d.LanguageId, opt => opt.MapFrom(s => s.Item2.LanguageId))
                .ForMember(d => d.Link, opt => opt.MapFrom(s => s.Item1.Link))
                .ForMember(d => d.Target, opt => opt.MapFrom(s => s.Item1.Target))
                .ForMember(d => d.Order, opt => opt.MapFrom(s => s.Item1.Order))
                .ForMember(d => d.CssClass, opt => opt.MapFrom(s => s.Item1.CssClass))
                .ForMember(d => d.IsLocked, opt => opt.MapFrom(s => s.Item1.IsLocked))
                .ForMember(d => d.IsDeleted, opt => opt.MapFrom(s => s.Item1.IsDeleted))
                .ForMember(d => d.IsFavorited, opt => opt.MapFrom(s => s.Item1.IsFavorited))
                .ForMember(d => d.CreatedDate, opt => opt.MapFrom(s => s.Item1.CreatedDate))
                .ForMember(d => d.CreatedBy, opt => opt.MapFrom(s => s.Item1.CreatedBy))
                .ForMember(d => d.UpdatedDate, opt => opt.MapFrom(s => s.Item1.UpdatedDate))
                .ForMember(d => d.UpdatedBy, opt => opt.MapFrom(s => s.Item1.UpdatedBy))
                .ForMember(d => d.Name, opt => opt.MapFrom(s => s.Item2.Name))
                .ForMember(d => d.Description, opt => opt.MapFrom(s => s.Item2.Description))
                .ForMember(d => d.Text, opt => opt.MapFrom(s => s.Item2.Text))
                .ForMember(d => d.ParentID, opt => opt.MapFrom(s => s.Item1.ParentID));
            Mapper.CreateMap<ApplicationUser, UserViewModel>();

            Mapper.CreateMap<Topic, TopicViewModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.QuickSearch, opt => opt.MapFrom(src => src.QuickSearch))
                .ForMember(dest => dest.QuickAction, opt => opt.MapFrom(src => src.QuickAction))
                .ForMember(dest => dest.CreatedBy, opt => opt.MapFrom(src => src.CreatedBy))
                .ForMember(dest => dest.CreatedDate, opt => opt.MapFrom(src => src.CreatedDate))
                .ForMember(dest => dest.UpdatedBy, opt => opt.MapFrom(src => src.UpdatedBy))
                .ForMember(dest => dest.UpdatedDate, opt => opt.MapFrom(src => src.UpdatedDate))
                .ForMember(dest => dest.ParentTopicId, opt => opt.MapFrom(src => src.ParentTopicId))
                .ForMember(dest => dest.Order, opt => opt.MapFrom(src => src.Order))
                .ForMember(dest => dest.ImageUrl, opt => opt.MapFrom(src => src.Image != null ? src.Image.FullPath : null))
                .ForMember(dest => dest.Language_Topic, opt => opt.Ignore())
                .ForMember(dest => dest.Active, opt => opt.MapFrom(src => src.Active));
            
            Mapper.CreateMap<Language_Topic, Language_TopicViewModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.TopicName, opt => opt.MapFrom(src => src.TopicName))
                .ForMember(dest => dest.TopicDescription, opt => opt.MapFrom(src => src.TopicDescription))
                .ForMember(dest => dest.CreatedBy, opt => opt.MapFrom(src => src.CreatedBy))
                .ForMember(dest => dest.CreatedDate, opt => opt.MapFrom(src => src.CreatedDate))
                .ForMember(dest => dest.UpdatedBy, opt => opt.MapFrom(src => src.UpdatedBy))
                .ForMember(dest => dest.UpdatedDate, opt => opt.MapFrom(src => src.UpdatedDate))
                .ForMember(dest => dest.Language, opt => opt.Ignore());

            Mapper.CreateMap<Language_Topic, TopicViewModel>()
                .ForMember(dest => dest.LanguageTopicId, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.TopicName, opt => opt.MapFrom(src => src.TopicName))
                .ForMember(dest => dest.TopicDescription, opt => opt.MapFrom(src => src.TopicDescription))
                .ForMember(dest => dest.QuickAction, opt => opt.MapFrom(src => src.Topic.QuickAction))
                .ForMember(dest => dest.QuickSearch, opt => opt.MapFrom(src => src.Topic.QuickSearch))
                .ForMember(dest => dest.Order, opt => opt.MapFrom(src => src.Topic.Order))
                .ForMember(dest => dest.Active, opt => opt.MapFrom(src => src.Topic.Active))
                .ForMember(dest => dest.CreatedBy, opt => opt.MapFrom(src => src.CreatedBy))
                .ForMember(dest => dest.CreatedDate, opt => opt.MapFrom(src => src.CreatedDate))
                .ForMember(dest => dest.UpdatedBy, opt => opt.MapFrom(src => src.UpdatedBy))
                .ForMember(dest => dest.UpdatedDate, opt => opt.MapFrom(src => src.UpdatedDate))
                .ForMember(dest => dest.LanguageId, opt => opt.MapFrom(src => src.Language.Id))
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Topic.Id));
            
            Mapper.CreateMap<Language_Topic, EditLanguageTopicViewModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.TopicName, opt => opt.MapFrom(src => src.TopicName))
                .ForMember(dest => dest.TopicDescription, opt => opt.MapFrom(src => src.TopicDescription))
                .ForMember(dest => dest.CreatedBy, opt => opt.MapFrom(src => src.CreatedBy))
                .ForMember(dest => dest.CreatedDate, opt => opt.MapFrom(src => src.CreatedDate))
                .ForMember(dest => dest.UpdatedBy, opt => opt.MapFrom(src => src.UpdatedBy))
                .ForMember(dest => dest.UpdatedDate, opt => opt.MapFrom(src => src.UpdatedDate))
                .ForMember(dest => dest.LanguageId, opt => opt.MapFrom(src => src.Language.Id));

            Mapper.CreateMap<Question, QuestionViewModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Active, opt => opt.MapFrom(src => src.Active))
                .ForMember(dest => dest.Priority, opt => opt.MapFrom(src => src.Priority))
                .ForMember(dest => dest.Latest, opt => opt.MapFrom(src => src.Latest))
                .ForMember(dest => dest.CreatedBy, opt => opt.MapFrom(src => src.CreatedBy))
                .ForMember(dest => dest.CreatedDate, opt => opt.MapFrom(src => src.CreatedDate));

            Mapper.CreateMap<Language_Question, QuestionEditViewModel>()
                .ForMember(dest => dest.Active, opt => opt.Ignore())
                .ForMember(dest => dest.Latest, opt => opt.Ignore())
                .ForMember(dest => dest.IsWebsiteShown, opt => opt.Ignore())
                .ForMember(dest => dest.LanguageQuestionId, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.QuestionName, opt => opt.MapFrom(src => src.QuestionName))
                .ForMember(dest => dest.Content, opt => opt.MapFrom(src => src.Content))
                .ForMember(dest => dest.ContentBrief, opt => opt.MapFrom(src => src.ContentBrief))
                .ForMember(dest => dest.CreatedBy, opt => opt.MapFrom(src => src.CreatedBy))
                .ForMember(dest => dest.CreatedDate, opt => opt.MapFrom(src => src.CreatedDate));

            Mapper.CreateMap<Tuple<Language_Question, Question>, QuestionViewModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Item2.Id))
                .ForMember(dest => dest.Active, opt => opt.MapFrom(src => src.Item2.Active))
                .ForMember(dest => dest.Latest, opt => opt.MapFrom(src => src.Item2.Latest))
                .ForMember(dest => dest.QuestionName, opt => opt.MapFrom(src => src.Item1.QuestionName))
                .ForMember(dest => dest.Content, opt => opt.MapFrom(src => src.Item1.Content))
                .ForMember(dest => dest.ContentBrief, opt => opt.MapFrom(src => src.Item1.ContentBrief))
                .ForMember(dest => dest.Priority, opt => opt.MapFrom(src => src.Item2.Priority))
                .ForMember(dest => dest.CreatedBy, opt => opt.MapFrom(src => src.Item2.CreatedBy))
                .ForMember(dest => dest.CreatedDate, opt => opt.MapFrom(src => src.Item2.CreatedDate));

            Mapper.CreateMap<Question, QuestionEditViewModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.QuestionName, opt => opt.Ignore())
                .ForMember(dest => dest.Content, opt => opt.Ignore())
                .ForMember(dest => dest.ContentBrief, opt => opt.Ignore())
                .ForMember(dest => dest.Active, opt => opt.MapFrom(src => src.Active))
                .ForMember(dest => dest.Priority, opt => opt.MapFrom(src => src.Priority))
                .ForMember(dest => dest.Latest, opt => opt.MapFrom(src => src.Latest))
                .ForMember(dest => dest.CreatedBy, opt => opt.MapFrom(src => src.CreatedBy))
                .ForMember(dest => dest.CreatedDate, opt => opt.MapFrom(src => src.CreatedDate));

            Mapper.CreateMap<QuestionAttachment, QuestionAttachmentViewModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.QuestionId, opt => opt.MapFrom(src => src.Question.Id))
                .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Description))
                .ForMember(dest => dest.Platform, opt => opt.MapFrom(src => src.Platform))
                .ForMember(dest => dest.ImageUrl, opt => opt.MapFrom(src => src.Image != null ? src.Image.FullPath : ""))
                .ForMember(dest => dest.Priority, opt => opt.MapFrom(src => src.Priority));

            Mapper.CreateMap<Comment, CommentViewModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Content, opt => opt.MapFrom(src => src.Content))
                .ForMember(dest => dest.AttachmentURL, opt => opt.MapFrom(src => src.AttachmentURL))
                .ForMember(dest => dest.ParentCommentId, opt => opt.MapFrom(src => src.ParentCommentId))
                .ForMember(dest => dest.PhoneNumber, opt => opt.MapFrom(src => src.PhoneNumber))
                .ForMember(dest => dest.CreatedBy, opt => opt.MapFrom(src => src.CreatedBy));

            Mapper.CreateMap<Comment, FAQCommentViewModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.QuestionId, opt => opt.MapFrom(src => src.QuestionId))
                .ForMember(dest => dest.Content, opt => opt.MapFrom(src => src.Content))
                .ForMember(dest => dest.Approved, opt => opt.MapFrom(src => src.Approved))
                .ForMember(dest => dest.ParentCommentId, opt => opt.MapFrom(src => src.ParentCommentId))
                .ForMember(dest => dest.AttachmentURL, opt => opt.MapFrom(src => src.AttachmentURL))
                .ForMember(dest => dest.Question, opt => opt.Ignore())
                .ForMember(dest => dest.ParentComment, opt => opt.Ignore())
                .ForMember(dest => dest.CreatedBy, opt => opt.MapFrom(src => src.CreatedBy))
                .ForMember(dest => dest.CreatedDate, opt => opt.MapFrom(src => src.CreatedDate));

            Mapper.CreateMap<PendingQuestion, FAQPendingQuestionViewModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Content, opt => opt.MapFrom(src => src.Content))
                .ForMember(dest => dest.PendingQuestionTitle, opt => opt.MapFrom(src => src.PendingQuestionTitle))
                .ForMember(dest => dest.PhoneNumber, opt => opt.MapFrom(src => src.PhoneNumber))
                .ForMember(dest => dest.Seen, opt => opt.MapFrom(src => src.Seen))
                .ForMember(dest => dest.AccountNumber, opt => opt.MapFrom(src => src.AccountNumber))
                .ForMember(dest => dest.AttachmentURL, opt => opt.MapFrom(src => src.AttachmentURL))
                .ForMember(dest => dest.FullName, opt => opt.MapFrom(src => src.FullName))
                .ForMember(dest => dest.CreatedDate, opt => opt.MapFrom(src => src.CreatedDate));

            Mapper.CreateMap<Tag, TagViewModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.TagName, opt => opt.MapFrom(src => src.TagName))
                .ForMember(dest => dest.QuickSearch, opt => opt.MapFrom(src => src.QuickSearch))
                .ForMember(dest => dest.Priority, opt => opt.MapFrom(src => src.Priority))
                .ForMember(dest => dest.CreatedDate, opt => opt.MapFrom(src => src.CreatedDate))
                .ForMember(dest => dest.CreatedBy, opt => opt.MapFrom(src => src.CreatedBy))
                .ForMember(dest => dest.UpdatedDate, opt => opt.MapFrom(src => src.UpdatedDate))
                .ForMember(dest => dest.UpdatedBy, opt => opt.MapFrom(src => src.UpdatedBy));

        }
    }
}