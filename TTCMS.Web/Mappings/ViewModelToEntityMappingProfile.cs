﻿using AutoMapper;
using TTCMS.Data.Entities;
using TTCMS.Data.Entities.WebsiteNews;
using TTCMS.Web.Areas.TT_Admin.Models;
using TTCMS.Web.ViewModels;

namespace TTCMS.Web.Mappings
{

    public class ViewModelToEntityMappingProfile : Profile
    {
        public override string ProfileName
        {
            get { return "ViewModelToDomainMappings"; }
        }

        protected override void Configure()
        {
            //slide
            Mapper.CreateMap<SlideViewModel, Slide>();
            
            //news
            Mapper.CreateMap<NewsViewModel, News>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.Category, opt => opt.Ignore())
                .ForMember(dest => dest.CreatedDate, opt => opt.Ignore());

            Mapper.CreateMap<StockNewsViewModel, stock_News>()
                .ForMember(dest => dest.NewsID, opt => opt.MapFrom(src => src.NewsID))
                .ForMember(dest => dest.GroupID, opt => opt.MapFrom(src => src.GroupID))
                .ForMember(dest => dest.LanguageID, opt => opt.MapFrom(src => src.LanguageID))
                .ForMember(dest => dest.ImageUrl, opt => opt.MapFrom(src => src.ImageUrl))
                .ForMember(dest => dest.IsHotNews, opt => opt.MapFrom(src => src.IsHotNews))
                .ForMember(dest => dest.CreatedDate, opt => opt.MapFrom(src => src.CreatedDate))
                .ForMember(dest => dest.LastModifiedDate, opt => opt.MapFrom(src => src.LastModifiedDate))
                .ForMember(dest => dest.Title, opt => opt.MapFrom(src => src.Title))
                .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Description))
                .ForMember(dest => dest.Content, opt => opt.MapFrom(src => src.Content));

            Mapper.CreateMap<TopicViewModel, Topic>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.QuickSearch, opt => opt.MapFrom(src => src.QuickSearch))
                .ForMember(dest => dest.QuickAction, opt => opt.MapFrom(src => src.QuickAction))
                .ForMember(dest => dest.CreatedBy, opt => opt.MapFrom(src => src.CreatedBy))
                .ForMember(dest => dest.CreatedDate, opt => opt.MapFrom(src => src.CreatedDate))
                .ForMember(dest => dest.UpdatedBy, opt => opt.MapFrom(src => src.UpdatedBy))
                .ForMember(dest => dest.UpdatedDate, opt => opt.MapFrom(src => src.UpdatedDate))
                .ForMember(dest => dest.ParentTopicId, opt => opt.MapFrom(src => src.ParentTopicId))
                .ForMember(dest => dest.Order, opt => opt.MapFrom(src => src.Order))
                .ForMember(dest => dest.Image, opt => opt.Ignore())
                .ForMember(dest => dest.Active, opt => opt.MapFrom(src => src.Active));
            
            Mapper.CreateMap<Language_TopicViewModel, Language_Topic>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.TopicName, opt => opt.MapFrom(src => src.TopicName))
                .ForMember(dest => dest.TopicDescription, opt => opt.MapFrom(src => src.TopicDescription))
                .ForMember(dest => dest.CreatedBy, opt => opt.MapFrom(src => src.CreatedBy))
                .ForMember(dest => dest.CreatedDate, opt => opt.MapFrom(src => src.CreatedDate))
                .ForMember(dest => dest.UpdatedBy, opt => opt.MapFrom(src => src.UpdatedBy))
                .ForMember(dest => dest.UpdatedDate, opt => opt.MapFrom(src => src.UpdatedDate));

            Mapper.CreateMap<EditLanguageTopicViewModel, Language_Topic>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.TopicName, opt => opt.MapFrom(src => src.TopicName))
                .ForMember(dest => dest.TopicDescription, opt => opt.MapFrom(src => src.TopicDescription))
                .ForMember(dest => dest.Topic, opt => opt.Ignore())
                .ForMember(dest => dest.TopicId, opt => opt.Ignore())
                .ForMember(dest => dest.CreatedBy, opt => opt.Ignore())
                .ForMember(dest => dest.CreatedDate, opt => opt.Ignore())
                .ForMember(dest => dest.UpdatedBy, opt => opt.MapFrom(src => src.UpdatedBy))
                .ForMember(dest => dest.UpdatedDate, opt => opt.MapFrom(src => src.UpdatedDate));

            Mapper.CreateMap<EditLanguageTopicViewModel, Topic>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.Image, opt => opt.Ignore())
                .ForMember(dest => dest.Language_Topic, opt => opt.Ignore())
                .ForMember(dest => dest.ParentTopic, opt => opt.Ignore())
                .ForMember(dest => dest.ParentTopicId, opt => opt.Ignore())
                .ForMember(dest => dest.SubTopics, opt => opt.Ignore())
                .ForMember(dest => dest.QuickAction, opt => opt.MapFrom(src => src.Topic.QuickAction))
                .ForMember(dest => dest.QuickSearch, opt => opt.MapFrom(src => src.Topic.QuickSearch))
                .ForMember(dest => dest.Order, opt => opt.MapFrom(src => src.Topic.Order))
                .ForMember(dest => dest.Active, opt => opt.MapFrom(src => src.Topic.Active))
                .ForMember(dest => dest.ParentTopic, opt => opt.MapFrom(src => src.Topic.Parent))
                .ForMember(dest => dest.CreatedBy, opt => opt.Ignore())
                .ForMember(dest => dest.CreatedDate, opt => opt.Ignore())
                .ForMember(dest => dest.UpdatedBy, opt => opt.MapFrom(src => src.UpdatedBy))
                .ForMember(dest => dest.UpdatedDate, opt => opt.MapFrom(src => src.UpdatedDate));

            Mapper.CreateMap<TopicViewModel, Language_Topic>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.TopicId, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.TopicName, opt => opt.MapFrom(src => src.TopicName))
                .ForMember(dest => dest.TopicDescription, opt => opt.MapFrom(src => src.TopicDescription))
                .ForMember(dest => dest.CreatedBy, opt => opt.MapFrom(src => src.CreatedBy))
                .ForMember(dest => dest.Topic, opt => opt.Ignore())
                .ForMember(dest => dest.CreatedDate, opt => opt.MapFrom(src => src.CreatedDate))
                .ForMember(dest => dest.UpdatedBy, opt => opt.MapFrom(src => src.UpdatedBy))
                .ForMember(dest => dest.UpdatedDate, opt => opt.MapFrom(src => src.UpdatedDate));

            Mapper.CreateMap<QuestionEditViewModel, Question>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.Active, opt => opt.MapFrom(src => src.Active))
                .ForMember(dest => dest.Latest, opt => opt.MapFrom(src => src.Latest))
                .ForMember(dest => dest.IsWebsiteShown, opt => opt.MapFrom(src => src.IsWebsiteShown))
                .ForMember(dest => dest.Priority, opt => opt.MapFrom(src => src.Priority))
                .ForMember(dest => dest.Language_Questions, opt => opt.Ignore())
                .ForMember(dest => dest.NumberOfLiked, opt => opt.Ignore())
                .ForMember(dest => dest.NumberOfShared, opt => opt.Ignore())
                .ForMember(dest => dest.QuestionAttachments, opt => opt.Ignore())
                .ForMember(dest => dest.Topic, opt => opt.Ignore())
                .ForMember(dest => dest.CreatedBy, opt => opt.Ignore())
                .ForMember(dest => dest.CreatedDate, opt => opt.Ignore())
                .ForMember(dest => dest.UpdatedBy, opt => opt.Ignore())
                .ForMember(dest => dest.UpdatedDate, opt => opt.Ignore());
            
            Mapper.CreateMap<QuestionEditViewModel, Language_Question>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.QuestionName, opt => opt.MapFrom(src => src.QuestionName))
                .ForMember(dest => dest.Content, opt => opt.MapFrom(src => src.Content))
                .ForMember(dest => dest.ContentBrief, opt => opt.MapFrom(src => src.ContentBrief))
                .ForMember(dest => dest.CreatedBy, opt => opt.Ignore())
                .ForMember(dest => dest.CreatedDate, opt => opt.Ignore())
                .ForMember(dest => dest.UpdatedBy, opt => opt.Ignore())
                .ForMember(dest => dest.UpdatedDate, opt => opt.Ignore());

            Mapper.CreateMap<NewsItemViewModel, NewsItem>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.CreatedBy, opt => opt.Ignore())
                .ForMember(dest => dest.News, opt => opt.Ignore())
                .ForMember(dest => dest.Language, opt => opt.Ignore())
                .ForMember(dest => dest.CreatedDate, opt => opt.Ignore());

            Mapper.CreateMap<SlideViewModel, Slide>();
            //setting
            Mapper.CreateMap<SettingViewModel, Setting>();
            Mapper.CreateMap<LangSettingViewModel, Language_Setting>();
            //menu
            Mapper.CreateMap<MenuManagerViewModel, Menu>();

            //category
            Mapper.CreateMap<CategoryViewModel, Category>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.Image, opt => opt.Ignore())
                .ForMember(dest => dest.News, opt => opt.Ignore())
                .ForMember(dest => dest.CategoryItems, opt => opt.Ignore())
                .ForMember(dest => dest.CreatedDate, opt => opt.Ignore());

            Mapper.CreateMap<CategoryViewModel, CategoryItem>();

            Mapper.CreateMap<CategoryItemViewModel, CategoryItem>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.Language, opt => opt.Ignore())
                .ForMember(dest => dest.Category, opt => opt.Ignore())
                .ForMember(dest => dest.CreatedDate, opt => opt.Ignore());

            //media
            Mapper.CreateMap<MediaViewModel, Media>();
            Mapper.CreateMap<MediaViewModel, FAQ_Media>();

            //single page
            Mapper.CreateMap<SinglePageViewModel, SinglePage>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.Language_SinglePages, opt => opt.Ignore())
                .ForMember(dest => dest.CreatedBy, opt => opt.Ignore())
                .ForMember(dest => dest.CreatedById, opt => opt.Ignore())
                .ForMember(dest => dest.CreatedDate, opt => opt.Ignore());

            Mapper.CreateMap<SinglePageViewModel, Language_SinglePage>()
                .ForMember(dest => dest.Id, opts => opts.MapFrom(src => src.SinglePage_Id));

            Mapper.CreateMap<Language_SinglePageViewModel, Language_SinglePage>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.SinglePageId, opt => opt.Ignore())
                .ForMember(dest => dest.SinglePage, opt => opt.Ignore())
                .ForMember(dest => dest.Language, opt => opt.Ignore())
                .ForMember(dest => dest.CreatedDate, opt => opt.Ignore());

            //lang
            Mapper.CreateMap<LanguageViewModel, Language>();
            Mapper.CreateMap<FunctionFormModel, Function>();
            Mapper.CreateMap<ActionLangViewModel, GAction>();
            Mapper.CreateMap<RoleViewModel, ApplicationRole>();
            Mapper.CreateMap<RoleViewModel, Language_Role>();
            Mapper.CreateMap<ActionLangViewModel, Language_GAction>().ForMember(dest => dest.Id,
             opts => opts.MapFrom(src => src.Lang_ActionId));
            Mapper.CreateMap<FunctionFormModel, Language_Function>().ForMember(dest => dest.Id,
               opts => opts.MapFrom(src => src.Lang_FunId));
            Mapper.CreateMap<ActionLangViewModel, Language_GAction>().ForMember(dest => dest.Id,
              opts => opts.MapFrom(src => src.Lang_ActionId));

            Mapper.CreateMap<QuestionAttachmentViewModel, QuestionAttachment>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Description))
                .ForMember(dest => dest.Platform, opt => opt.MapFrom(src => src.Platform))
                .ForMember(dest => dest.Priority, opt => opt.MapFrom(src => src.Priority))
                .ForMember(dest => dest.CreatedDate, opt => opt.Ignore())
                .ForMember(dest => dest.CreatedBy, opt => opt.Ignore())
                .ForMember(dest => dest.UpdatedDate, opt => opt.Ignore())
                .ForMember(dest => dest.UpdatedBy, opt => opt.Ignore());

            Mapper.CreateMap<FAQCommentViewModel, Comment>()
                .ForMember(dest => dest.Content, opt => opt.MapFrom(src => src.Content))
                .ForMember(dest => dest.Approved, opt => opt.MapFrom(src => src.Approved))
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.QuestionId, opt => opt.Ignore())
                .ForMember(dest => dest.CreatedBy, opt => opt.Ignore())
                .ForMember(dest => dest.CreatedDate, opt => opt.Ignore())
                .ForMember(dest => dest.UpdatedBy, opt => opt.Ignore())
                .ForMember(dest => dest.UpdatedDate, opt => opt.Ignore());

            Mapper.CreateMap<TagViewModel, Tag>()
                .ForMember(dest => dest.TagName, opt => opt.MapFrom(src => src.TagName))
                .ForMember(dest => dest.QuickSearch, opt => opt.MapFrom(src => src.QuickSearch))
                .ForMember(dest => dest.Priority, opt => opt.MapFrom(src => src.Priority))
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.CreatedBy, opt => opt.Ignore())
                .ForMember(dest => dest.CreatedDate, opt => opt.Ignore())
                .ForMember(dest => dest.UpdatedBy, opt => opt.Ignore())
                .ForMember(dest => dest.UpdatedDate, opt => opt.Ignore());
        }
    }
}