﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TTCMS.Web.ResourceString
{
    public enum ActivityNewsMenuKey
    {
        TSVIForComunity = 30,
        PhotoMaterial = 31,
        TSVIWithPress = 32,
        InternalActivities = 33
    }
}