﻿$(document).ready(function () {
    $("#frmQuestion").submit(function (event) {
        event.preventDefault()
        var rcres = grecaptcha.getResponse($('#recaptcha-questionForm').attr('data-widget-id'));
        if(rcres.length === 0){
            $(".captchaError").show();
            return;
        } 
        var phoneVal = $("input[name=PhoneNumber]").val();
        var filter = /(0[3|5|7|8|9])+([0-9]{8})\b/g;
        if ($("input[name=FullName]").val().trim() == "") {
            document.querySelector("#FullName").reportValidity()
            return;
        }
        if (!filter.test(phoneVal)) {
            document.getElementById("PhoneNumber").reportValidity();
            return;
        }
        if ($("#Content").val().trim() == "") {
            document.querySelector("#Content").reportValidity()
            return;
        }
        var file = $("#file_Uploader").get(0).files[0];
        if (file){
            var extension = file.name.substr((file.name.lastIndexOf('.') + 1));
            if(file.size > 2097152) {
                document.querySelector("#file_Uploader").reportValidity()
                return
            }
            if(allowedExtensions.indexOf(extension) < 0) {
                document.querySelector("#file_Uploader").reportValidity()
                return
            }
        }
        var formData = new FormData();
        formData.append("FullName", $("#FullName").val());
        formData.append("PhoneNumber", $("#PhoneNumber").val());
        formData.append("Content", $("#Content").val());
        if($("#AccountNumber").val() !== undefined)
        {
            formData.append("AccountNumber", $("#AccountNumber").val());
        }
        formData.append("PendingQuestionTitle", $("#PendingQuestionTitle").val());
        formData.append("FilePath", $("#fileLabel").val());
        formData.append("File", file);
        $.ajax({
            url: "/publicFormQuestion",
            data: formData,
            dataType: "json",
            type: "post",
            contentType: false,
            cache : false,
            processData: false,
            success: function(result){
                AjaxAlert(result.Code, result.Msg);
                $('#frmQuestion').trigger("reset");
                $(".captchaError").hide();
                grecaptcha.reset();
            }
        })
    })
    $("#fileLabel").on("click touchstart", function(){
        $("#file_Uploader").click();
    })
    $('#file_Uploader').change(function(event) {
        if ($(this).val() === "") return
        var split = $(this).val().split("\\")
        var value = split[split.length - 1]
        var file = event.currentTarget.files[0];
        $("#FilePath").val(split[split.length - 1]);
        $("#fileLabel").text(split[split.length - 1]);
    })

    function AjaxAlert(Code, Msg) {
        console.log("ajax alert")
        $.get("/AjaxAlert?code=" + Code + "&&msg=" + Msg).done(function (html) {
            $("#AjaxAlert").html(html);
        });
    };
})