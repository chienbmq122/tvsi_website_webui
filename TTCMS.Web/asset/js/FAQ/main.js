﻿(function (i, s, o, g, r, a, m) {
    i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
        (i[r].q = i[r].q || []).push(arguments)
    }, i[r].l = 1 * new Date(); a = s.createElement(o),
        m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
ga('create', '4108529835', 'auto');
ga('send', 'pageview');

var allowedExtensions = ["jpg", "jpeg", "png", "xlsx", "xls", "doc", "docx", "ppt", "pptx", "pdf"]
$(window).on('load', function () {
    let offsetTop = $("#banner").height()
    $(".floatingContainer").css("top", `-${offsetTop}px`)
    if ($(".floatingContainer").hasClass("dp-none")) {
        $(".floatingContainer").removeClass("dp-none")
    }
    $(".bannerContainer").css("height", `${offsetTop}px`)
    if ($(window).width() < 1440) {
        let menuContainerHeight = $(".menuContainer").height()
        $(".quickActionsContainer").css("transform", `translate(46px, -${menuContainerHeight}px)`)
    }
})
$(window).resize(function () {
    let offsetTop = $("#banner").height()
    $(".floatingContainer").css("top", `-${offsetTop}px`)
    $(".bannerContainer").css("height", `${offsetTop}px`)
})
$(document).ready(function () {
    $("button.close").click(function () {
        if ($("#AjaxAlert").length) $("#AjaxAlert").hide();
    })
})
$(document).on("click", "#createQuestionForm", function () {
    $([document.documentElement, document.body]).animate({
        scrollTop: $(".questionFormContainer").offset().top
    });
})
$(document).on("click", "#chatWithUs", function () {
    $("#stringeeChatIframe").click()
})
function onAskingQuestionClick() {
    $([document.documentElement, document.body]).animate({
        scrollTop: $(".questionFormContainer").offset().top
    });
}
var CaptchaCallback = function () {
    if ($("#recaptcha-questionForm").length) {
        let widgetIdQuestionForm = grecaptcha.render('recaptcha-questionForm', { 'sitekey': '6LdPOU0iAAAAAHQrMdMS0wexNcSA76YAmbqzwMxm' });
        $("#recaptcha-questionForm").attr('data-widget-id', widgetIdQuestionForm)
    }
    if ($("#recaptcha-comment").length) {
        let widgetIdComment = grecaptcha.render('recaptcha-comment', { 'sitekey': '6LdPOU0iAAAAAHQrMdMS0wexNcSA76YAmbqzwMxm' });
        $("#recaptcha-comment").attr('data-widget-id', widgetIdComment)
    }
};

$(".question.write").on("click touchstart", function(){
    $([document.documentElement, document.body]).animate({
        scrollTop: $(".questionFormContainer").offset().top
    });
})
$(document).on("click touchstart", "button.close", function(){
    $("#AjaxAlert").hide();
})

$(".blockContent").click(function(event){
    let id = $(this)[0].id.split("-")[1]
    window.location.href = `/SearchCategory?topicId=${id}`
})