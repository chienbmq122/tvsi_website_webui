﻿function AjaxAlert(Code, Msg) {
    $.get("/AjaxAlert?code=" + Code + "&&msg=" + Msg).done(function (html) {
        $("#AjaxAlert").html(html);
    });
};
var nameException = ["Tân Việt", "Tan Viet", "TVSI", "tanviet", "TânViệt"]
$(document).ready(function () {
    $([document.documentElement, document.body]).animate({
        scrollTop: $("section").offset().top
    });
})
$(window).on("load", function () {
    addMarginTopSwiper()
    positionDesktopMobileViewNav()
})
function addMarginTopSwiper() {
    let offsetBorder = 20;
    if ($(window).width() < 480) offsetBorder = 10;
    else if ($(window).width() < 550) offsetBorder = 15;
    let maxHeightLimit = 50;
    if ($(window).width() >= 1000) maxHeightLimit = 125;
    if ($(window).width() >= 768 && $(window).width() < 1000) maxHeightLimit = 100;
    if ($(window).width() >= 600 && $(window).width() <= 767) maxHeightLimit = 90;
    if ($(window).width() >= 480 && $(window).width() <= 599) maxHeightLimit = 70;
    let offset = $(".pc-container img").height() - offsetBorder;
    let maxHeight = $(".pc-container img").height() - maxHeightLimit;
    $(".desktopSwiperMobile").css("margin-top", `-${offset}px`)
    $(".desktopSwiper").css("margin-top", `-${offset}px`)
    if (maxHeight != 0) {
        $(".desktopSwiperMobile .swiper-wrapper img").css("max-height", `${maxHeight}px`)
        $(".swiper-slide img.img-pc").css("height", `${maxHeight}px`)
    }
}
function positionDesktopMobileViewNav() {
    let marginTop = $(".pc-container img").height() / 2
    $(".swiper-button-groups.swiper-button-groups-pc").css("width", `${$(".pc-container img").width()}px`)
    $(".swiper-button-groups.swiper-button-groups-pc").css("margin-top", `-${marginTop}px`)
}

$(".pc-container img").change(function () {
    addMarginTopSwiper()
    positionDesktopMobileViewNav()
})
$("img.img-pc").change(function () {
    positionDesktopMobileViewNav()
})
$(window).resize(function () {
    addMarginTopSwiper()
    positionDesktopMobileViewNav()
})

$("#textComment").click(function () {
    $("#questionCommentInput").focus()
})

var swiper1 = new Swiper(".desktopSwiper", {
    initialSlide: 0,
    pagination: {
        el: ".swiper-pagination",
        clickable: true,
        renderBullet: function (index, className) {
            return `<span class="${className} ${className}-pc">${index + 1}</span>`;
        },
    },
    navigation: {
        nextEl: ".swiper-button-desktop-next",
        prevEl: ".swiper-button-desktop-prev"
    }
});

var swiper2 = new Swiper(".mobileSwiper", {
    initialSlide: 0,
    slidesPerView: 1,
    navigation: {
        nextEl: ".swiper-button-mobile-next",
        prevEl: ".swiper-button-mobile-prev"
    }
});

swiper2.on("slideChange", function () {
    var index = swiper2.activeIndex
    if ($(window).width() < 1024) {
        mapButton(".mobilePaginationContainer.lg-d-none .pagination-button-mobile", index)
    } else {
        mapButton(".mobilePaginationContainer.mb-d-none .pagination-button-mobile", index)
    }
})

var swiper3 = new Swiper(".desktopSwiperMobile", {
    initialSlide: 0,
    slidesPerView: 1,
    navigation: {
        nextEl: ".swiper-button-desktopMobile-next",
        prevEl: ".swiper-button-desktopMobile-prev"
    }
});

swiper3.on("slideChange", function () {
    var index = swiper3.activeIndex
    mapButton(".pagination-button-desktop", index)
})

function slideTo2(index) {
    swiper2.slideTo(index)
}

function slideTo3(index) {
    swiper3.slideTo(index)
}

function mapButton(classname, index) {
    $.map($(classname).slice(0, index + 1), function (activeButton) {
        $(activeButton).addClass("active")
    })
    $.map($(classname).slice(index + 1), function (inactiveButton) {
        $(inactiveButton).removeClass("active")
    })
}

$(document).ready(function () {
    if ($("#mobileCheckWrapper").length) {
        $("input[name=platform][value=mobile]").prop('checked', true)
        ToggleMobileView()
    }
    else if ($("#desktopCheckWrapper").length) {
        $("input[name=platform][value=pc]").prop('checked', true)
        TogglePcView()
        addMarginTopSwiper()
    }
    $("input[name=platform]").change(function () {
        if (this.value === "mobile") {
            ToggleMobileView()
        }
        if (this.value === "pc") {
            TogglePcView()
            addMarginTopSwiper()
        }
    })
})
function ToggleMobileView() {
    $("#slidePc").hide();
    $(".desktopPaginationContainer").removeClass("paginationContainer");
    $(".desktopPaginationContainer").hide();
    $("#slideMobile").show();
    if ($(window).width() >= 1024) {
        $(".mobilePaginationContainer.mb-d-none").show();
    } else {
        $(".mobilePaginationContainer.lg-d-none").show();
    }
}
function TogglePcView() {
    $("#slideMobile").hide();
    $(".mobilePaginationContainer").hide();
    $("#slidePc").show();
    if ($(window).width() >= 1024) {
        $(".swiper.desktopSwiper").show();
        $(".swiper.desktopSwiperMobile").hide();
        $(".desktopPaginationContainer").hide();
    } else {
        $(".swiper.desktopSwiper").hide()
        $(".swiper.desktopSwiperMobile").show()
        $(".desktopPaginationContainer").show();
        $(".desktopPaginationContainer").addClass("paginationContainer");
        positionDesktopMobileViewNav()
    }
}

var typeOfComment = "comment"
var inputValue = ""
var commentId = 0
$("#questionCommentInput").keypress(function (event) {
    var key = event.which;
    if (key == 13) {
        var input = $(this).val().trim();
        if (input === "") $(".commentInputError").show();
        else {
            commentId = 0
            typeOfComment = "comment"
            inputValue = input
            if ($(".commentInputError").is(":visible")) $(".commentInputError").hide();
            $("html").addClass("noscroll")
            $("#overlay").addClass("active");
            $("#commentModal").show();
            $("#shareModal").hide();
        }
    }
})
$(".modalTitle i.fa-close").on("click touchstart", function () {
    $("#overlay").removeClass("active");
    $("html").removeClass("noscroll")
})
function onShareClick() {
    $("#shareModal").show();
    $("#commentModal").hide();
}

function SendComment(questionId) {
    var name = $("#commentName").val().trim();
    var phoneNumber = $("#commentPhoneNumber").val().trim();
    var filter = /(0[3|5|7|8|9])+([0-9]{8})\b/g;
    if (phoneNumber && phoneNumber.length > 0 && !filter.test(phoneNumber)) {
        $(".phoneInputError").text("Vui lòng nhập đúng số điện thoại")
        $(".phoneInputError").show();
        return;
    }
    var content = inputValue
    if (name === "") {
        $(".nameInputError").text("Vui lòng không để trống tên");
        $(".nameInputError").show();
        return;
    }
    var nameError = false;
    nameException.map((item) => {
        if (name.toLowerCase().indexOf(item.toLowerCase()) > -1) {
            $(".nameInputError").text("Tên nằm trong danh sách bị hạn chế. Vui lòng nhập tên khác");
            $(".nameInputError").show();
            nameError = true;
            return;
        }
    })
    if (nameError) return;
    var rcres = grecaptcha.getResponse($('#recaptcha-comment').attr('data-widget-id'));
    if (rcres.length === 0) {
        $(".wrongCaptchaError").show();
        return;
    }
    else {
        var file = $(`#Comment_File_Uploader_${questionId}`).get(0).files[0];
        if (file) {
            var extension = file.name.substr((file.name.lastIndexOf('.') + 1));
            if (file.size > 2097152) {
                return
            }
            if (allowedExtensions.indexOf(extension) < 0) {
                return
            }
        }
        var formData = new FormData();
        formData.append("name", name);
        formData.append("PhoneNumber", phoneNumber);
        formData.append("Content", content);
        formData.append("QuestionId", questionId);
        if (commentId != 0) {
            formData.append("ParentId", commentId);
        }

        if (file) {
            formData.append("FilePath", file.name);
            formData.append("File", file);
        }
        $.ajax({
            url: '/SendComment',
            data: formData,
            dataType: "json",
            type: "post",
            contentType: false,
            cache: false,
            processData: false,
            success: function (data, status, xhr) {
                if (data.success) {
                    AjaxAlert(data.Code, data.Msg);
                    var page = parseInt($('input[name="CurrentCommentPage"]').val())
                    $(`#CommentFileLabel-${questionId}`).text("");
                    $(`#CommentFileLabel-${questionId}`).val("");
                    $.ajax({
                        url: '/LoadMoreComments ',
                        method: "POST",
                        dataType: "html",
                        data: JSON.stringify({ questionId, limit: page * 4 + 1 }),
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            $("#commentsList").html(data)
                            if ($(".nameInputError").is(":visible")) $(".nameInputError").hide();
                            $("#overlay").removeClass("active");
                            $("html").removeClass("noscroll")
                            $("#commentModal").hide();
                            $("#shareModal").hide();
                            $("#commentName").val("");
                            $("input#questionCommentInput").val("");
                            $(".wrongCaptchaError").hide();
                            grecaptcha.reset();
                        }
                    })
                }
            }
        });
    }
}

function onQuestionLikeClick(id) {
    $.ajax({
        url: "/LikePost",
        type: "post",
        data: JSON.stringify({
            questionId: id
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data.Code == "alert-success") {
                var numberOfLikes = parseInt($(".likeDetailsContainer p").text().split(" ")) + 1
                $(".likeDetailsContainer p").text(`${numberOfLikes} Thích`)
                $(".likeDetailsContainer .reactionIcon image").addClass("liked")
                $("#likeIcon").attr("src", "/webroot/images/faq/heart-liked.png")
            }
        }
    })
}

function onLoadMoreCommentClick(questionId) {
    var page = parseInt($('input[name="CurrentCommentPage"]').val()) + 1
    $.ajax({
        url: '/LoadMoreComments ',
        method: "POST",
        dataType: "html",
        data: JSON.stringify({ questionId, limit: page * 4 }),
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            $("#commentsList").html(data)
            $('input[name="CurrentCommentPage"]').val(page)
        }
    })
}

function onCommentIconClick() {
    $("#questionCommentInput").focus()
}

$("#inputSend").click(function (event) {
    var input = $("#questionCommentInput").val().trim();
    if (input === "") $(".commentInputError").show();
    else {
        inputValue = input
        if ($(".commentInputError").is(":visible")) $(".commentInputError").hide();
        $("#overlay").addClass("active");
        $("#commentModal").show();
        $("#shareModal").hide();
        $("html").addClass("noscroll")
    }
})
$(document).click(function (event) {
    if (!($('#shareModal').is(event.target) ||
        $("#shareModal").find(event.target).length ||
        $('.shareButtonHolder button').find(event.target).length ||
        $('.shareButtonHolder button').is(event.target)
    ) && $("#shareModal").is(":visible")) {
        $("#shareModal").hide();
    }
})

$(document).on("click touchstart", "a[role=button]", function (event) {
    let id = event.target.id.split("-")[1];
    if ($(`#replyInput-${id}`).length) {
        $(`#replyInput-${id}`).focus();
        return;
    }
    var newInput = `
                <div class="commentInput replyInput">
                    <div class="inputContainer">
                        <input class="reply" id="replyInput-${id}" type="text" placeholder="Viết phản hồi">
                    </div>
                    <div class="inputActions replyAction">
                        <button class="replyButton" onclick=SendReply(${id})>
                            <i class="fa fa-chevron-right" class="replyClickIcon" id="replySend-${id}"></i>
                        </button>
                    </div>
                </div>
                <small class="commentInputError inputError" id="replyError-${id}">Vui lòng nhập bình luận của Bạn vào đây</small>
            `
    $(this).after(newInput)
    $(`#replyInput-${id}`).focus()
})

function SendReply(id) {
    var input = $(`#replyInput-${id}`).val().trim();
    if (input === "") $(`#replyError-${id}`).show();
    else {
        typeOfComment = "reply"
        inputValue = input
        commentId = id
        if ($(`#reply-${id}`).is(":visible")) $(`#reply-${id}`).hide();
        $("html").addClass("noscroll")
        $("#overlay").addClass("active");
        $("#commentModal").show();
        $("#shareModal").hide();
    }
}

$(document).on("keypress", ".replyInput .inputContainer input.reply", function (event) {
    var key = event.which;
    var elemId = event.target.id;
    var elem = event.target.id.split("-")[0]
    var id = elemId.split("-")[1]
    if (elem === "replyInput" && key == 13) {
        var input = $(event.target).val().trim();
        if (input === "") $(`#reply-${id}`).show();
        else {
            typeOfComment = "reply"
            inputValue = input
            commentId = id
            if ($(`#reply-${id}`).is(":visible")) $(`#reply-${id}`).hide();
            $("html").addClass("noscroll")
            $("#overlay").addClass("active");
            $("#commentModal").show();
            $("#shareModal").hide();
        }
    }
})