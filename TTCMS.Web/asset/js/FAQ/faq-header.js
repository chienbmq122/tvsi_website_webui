﻿function SearchKeyword() {
    var input = $('#faqSearchInput').val().trim()
    if (input === "") {
        $(".input-error").show();
        return;
    }
    else {
        window.location = `/SearchQuestion?keyword=${input}`;
    }
}
$(function () {
    var cache = {};
    $("#faqSearchInput").autocomplete({
        minLength: 2,
        source: function (request, response) {
            var input = $('#faqSearchInput').val().trim()
            if (input in cache) {
                response(cache[input]);
                return;
            }

            $.ajax({
                url: `/AutocompleteSearchQuestion?keyword=${input}`,
                type: "GET",
                contentType: 'application/json; charset=utf-8',
                async: true,
                processData: false,
                cache: true,
                success: function (data) {
                    cache[input] = data;
                    response(data);
                },
                error: function (xhr) {
                    console.log('error');
                }
            });
        },
        focus: function (event, ui) {
                return false;
        },
        select: function (event, ui) {
            window.location.href = `/QuestionPage?id=${ui.item.QuestionId}`
        }
    }).autocomplete("instance")._renderItem = function (ul, item) {
        return $("<li>").append(`<p style="border-bottom: 1px solid black">${item.QuestionName}</p>`).appendTo(ul);
    };
});