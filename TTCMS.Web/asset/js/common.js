function actionHeader() {
	const header = $('header');
	let canChange = false;
	let changeMarginTopBanner = function () {
		if (canChange) {
			const header = $('header');
			let headerHeight = header.height();

			$('section:first-of-type').css('margin-top', headerHeight + 'px');
		}
	};

	this.fixed = function () {
		const headerPc = header.find('.pc');

		$(window).scroll(function (event) {
			let scroll = $(window).scrollTop();

			if (scroll > 200) {
				headerPc.addClass('scroll');
			} else {
				headerPc.removeClass('scroll');
			}
		});
	};

	this.listMenuPc = function () {
		const listMenu = header.find('.bottom .left ul li');
		const listMenuDetail = header.find('.list-menu-default-detail');

		// Open list menu
		listMenu.on('mouseover', function () {
			let self = $(this);

			listMenu.removeClass('active');
			self.addClass('active');

			let type = self.attr('data-type');

			listMenuDetail.addClass('d-none');
			listMenuDetail.each(function () {
				if ($(this).attr('data-type') === type) {
					$(this).removeClass('d-none');
				}
			});

			canChange = false;

			changeMarginTopBanner();
		});

		listMenu.on('mouseleave', function () {
			listMenuDetail.on('mouseleave', function () {
				listMenu.removeClass('active');
				listMenuDetail.addClass('d-none');
				canChange = true
				changeMarginTopBanner();
			})
		})

		header.find('.top').on('mouseover', function () {
			listMenu.removeClass('active');
			listMenuDetail.addClass('d-none');
			changeMarginTopBanner();
		})
	};

	this.listMenuSp = function () {
		const link = header.find('.sp .right a');
		const detail = header.find('.detail');

		link.on('click', function () {
			let self = $(this);
			let type = self.attr('data-type');

			if (self.hasClass('active')) {
				self.removeClass('active');
				detail.addClass('d-none');

				return;
			}

			link.removeClass('active');

			if (self)
				self.addClass('active');

			detail.each(function () {
				let selfDetail = $(this);

				selfDetail.attr('data-type') === type ? selfDetail.removeClass('d-none') : selfDetail.addClass('d-none');
			});
		});

		const detailList = detail.find('.bottom .list');
		detailList.on('click', function () {
			let self = $(this);

			if (self.hasClass('active')) {
				self.removeClass('active');
			} else {
				self.addClass('active');
			}
		});
	};

	this.marginHeader = function () {
		$(window).on('resize load scroll', function () {
			canChange = true
			changeMarginTopBanner();
		});
	};

	this.changeLanguage = function () {
		const languageSelect = $('.language-select');

		const display = languageSelect.find('.display');
		const listLanguageUl = languageSelect.find('ul');
		const listLanguage = listLanguageUl.find('li');

		languageSelect.on('mouseover', function () {
			listLanguageUl.removeClass('d-none');
			display.removeClass('d-none');

			// let dataTypeCurrent = languageSelect.find('.display.selected').attr('data-type');

			// listLanguage.each(function () {
			// 	let self = $(this);
			// 	if (self.attr('data-type') === dataTypeCurrent) {
			// 		self.addClass('d-none');
			// 	}
			// });
		});

		languageSelect.on('mouseleave', function () {
			listLanguageUl.addClass('d-none');
			listLanguage.removeClass('d-none');
		})

		listLanguage.on('click', function (e) {
			// $(this).addClass('d-none');
			// display.removeClass('selected');
			// let dataType = $(this).attr('data-type');
			//
			// display.each(function () {
			// 	let self = $(this)
			// 	if (self.attr('data-type') === dataType) {
			// 		self.addClass('selected')
			// 	}
			// })
			// languageSelect.css('pointer-events', 'none')
			// setTimeout(function () {
			// 	languageSelect.css('pointer-events', 'auto')
			// }, 600)

			listLanguageUl.addClass('d-none')
		});
	}

	this.openQr = function () {
		const buttonQr = $('.open-overlay-qr');
		const modalQr = $('.modal-qr');

		buttonQr.on('click', function () {
			modalQr.addClass('active')
		})

		let modalContent = modalQr.find('.background');
		$(window).click(function (e) {
			if (modalContent.has(e.target).length === 0 && !modalContent.is(e.target)
				&& buttonQr.has(e.target).length === 0 && !buttonQr.is(e.target)
			) {
				modalQr.removeClass('active');
			}
		});
	}

	this.openQrZalo = function () {
		const footer = $('footer')
		const zalo = footer.find('.zalo')
		const qr = zalo.find('.qr')

		zalo.mouseenter(function () {
			qr.fadeIn()
		})

		zalo.mouseleave(function () {
			qr.fadeOut()
		})
	}

	this.fixed();
	this.listMenuPc();
	this.listMenuSp();
	this.marginHeader();
	this.changeLanguage();
	this.openQr();
	this.openQrZalo();

	new WOW().init({
		mobile: false,
	});
}

function actionHome() {
	// this.sectionSwiperBanner = function () {
	// 	let swiper = new Swiper('.banner', {
	// 		loop: true,
	// 		slidesPerView: 1,
	// 		spaceBetween: 0,
	// 		centeredSlides: true,
	// 		autoplay: {
	// 			delay: 3000,
	// 			disableOnInteraction: false,
	// 		},
	// 		pagination: {
	// 			el: '.swiper-pagination',
	// 			clickable: true,
	// 			// renderBullet: function (index, className) {
	// 			// 	return '<span class="' + className + '">' + ([1,2,3][index]) + '</span>';
	// 			// },
	// 		},
	// 		navigation: {
	// 			nextEl: '.swiper-button-next',
	// 			prevEl: '.swiper-button-prev',
	// 		},
	// 		grabCursor: true,
	// 	});
	// };

	this.sectionSwiperMenu = function () {
		$(window).on('resize load', function () {
			if (window.innerWidth < 768) {
				let swiper1 = new Swiper('.menu', {
					loop: true,
					spaceBetween: 0,
					centeredSlides: false,
					slidesPerView: 3,
					navigation: {
						nextEl: '.swiper-button-next.for-menu',
						prevEl: '.swiper-button-prev.form-menu',
					},
					grabCursor: true,
				});
			} else {
				let swiper1 = new Swiper('.menu', {
					loop: true,
					spaceBetween: 0,
					centeredSlides: false,
					slidesPerView: 5,
					grabCursor: true,
					allowTouchMove: false,
				});
			}
		});
	};

	this.sectionSwiperInvest = function () {
		let swiper = new Swiper('.swiper-container.invest', {
			loop: false,
			spaceBetween: 0,
			centeredSlides: false,
			autoplay: false,
			pagination: {
				el: '.swiper-pagination',
				clickable: true,
				renderBullet: function (index, className) {
					return '<span class="' + className + '">' + ([1, 2, 3][index]) + '</span>';
				},
			},
			grabCursor: false,
			allowTouchMove: false,
		});
	};

	this.question = function () {
		const question = $('.home-question');
		const listQuestion = question.find('.content ul li');
		const listAnswer = question.find('.right p');

		let changeAnswer = function (type) {
			listAnswer.removeClass('active')
			listAnswer.each(function () {
				let self = $(this);
				if (self.attr('data-type') === type) {
					self.addClass('active')
				}
			})
		}

		listQuestion.on('click', function () {
			listQuestion.removeClass('active');
			let self = $(this);
			self.addClass('active')

			let dataType = self.attr('data-type')
			changeAnswer(dataType)

			clearInterval(interval)
			interval = setInterval(changeQuestionAuto, 4000)
		});

		let changeQuestionAuto = function () {
			let flag = true
			listQuestion.each(function () {
				if (!flag) {
					return
				}
				let selfQuestion = $(this);
				if (selfQuestion.attr('data-type') === '1' && selfQuestion.hasClass('active')) {
					selfQuestion.removeClass('active')
					listQuestion.each(function () {
						if ($(this).attr('data-type') === '2') {
							$(this).addClass('active')
							changeAnswer('2')
							flag = false
						}
					});
				}
				if (selfQuestion.attr('data-type') === '2' && selfQuestion.hasClass('active')) {
					selfQuestion.removeClass('active')
					listQuestion.each(function () {
						if ($(this).attr('data-type') === '3') {
							$(this).addClass('active')
							changeAnswer('3')
							flag = false
						}
					});
				}
				if (selfQuestion.attr('data-type') === '3' && selfQuestion.hasClass('active')) {
					selfQuestion.removeClass('active')
					listQuestion.each(function () {
						if ($(this).attr('data-type') === '1') {
							$(this).addClass('active')
							changeAnswer('1')
							flag = false
						}
					});
				}
			});
		};

		let interval = setInterval(changeQuestionAuto, 4000)
	}

	this.sectionSwiperMenu();
	this.sectionSwiperInvest();
	this.question();
}

function actionHomeNew() {
	this.sectionSwiperBanner = function () {
		var swiper = new Swiper('.home-new-banner', {
			loop: true,
			slidesPerView: 1,
			spaceBetween: 0,
			centeredSlides: true,
			autoplay: {
				delay: 5000,
				disableOnInteraction: false,
				pauseOnMouseEnter: true
			},
			pagination: {
				el: '.home-new-banner .swiper-pagination',
				clickable: true,
			},
			navigation: {
				nextEl: '.home-new-banner .swiper-button-next',
				prevEl: '.home-new-banner .swiper-button-prev',
			},
			grabCursor: true,
		});
	};
	this.sectionSwiperMenu = function () {
		$(window).on('resize load', function () {
			if (window.innerWidth > 768) {
				let swiper1 = new Swiper('.home-new-menu', {
					loop: true,
					slidesPerView: 5,
					spaceBetween: 15,
					centeredSlides: false,
					autoplay: false,
					pagination: false,
					allowTouchMove: false,
					grabCursor: false,
				});
			} else {
				let swiper1 = new Swiper('.home-new-menu', {
					loop: false,
					slidesPerView: 3,
					spaceBetween: 10,
					centeredSlides: false,
					autoplay: false,
					pagination: false,
					navigation: {
						nextEl: '.swiper-button-next',
						prevEl: '.swiper-button-prev',
					},
					allowTouchMove: true,
					grabCursor: true,
				});
			}
		});
	};

	this.sectionSwiperProductService = function () {
		$(window).on('resize load', function () {
			if (window.innerWidth > 768) {
				let swiper2 = new Swiper('.home-new-product-service', {
					loop: false,
					slidesPerView: 3,
					spaceBetween: 30,
					centeredSlides: false,
					autoplay: false,
					pagination: false,
					allowTouchMove: true,
					grabCursor: true,
					navigation: {
						nextEl: '.swiper-button-next-menu',
						prevEl: '.swiper-button-prev-menu',
					},
				});
			} else if (window.innerWidth > 576) {
				let swiper2 = new Swiper('.home-new-product-service', {
					loop: false,
					slidesPerView: 2,
					spaceBetween: 20,
					centeredSlides: false,
					autoplay: false,
					pagination: false,
					allowTouchMove: true,
					grabCursor: true,
				});
			} else {
				let swiper2 = new Swiper('.home-new-product-service', {
					loop: false,
					slidesPerView: 1,
					spaceBetween: 20,
					centeredSlides: false,
					autoplay: false,
					pagination: false,
					allowTouchMove: true,
					grabCursor: true,
				});
			}
		});
	};

	this.sectionSwiperAnalyseSwiper = function () {
		$(window).on('resize load', function () {
			if (window.innerWidth > 992) {
				let swiper3 = new Swiper('.home-new-analyse-swiper', {
					loop: false,
					slidesPerView: 3,
					spaceBetween: 30,
					centeredSlides: false,
					autoplay: false,
					pagination: false,
					allowTouchMove: true,
					grabCursor: true,
				});
			} else if (window.innerWidth > 767) {
				let swiper3 = new Swiper('.home-new-analyse-swiper', {
					loop: false,
					slidesPerView: 2,
					spaceBetween: 20,
					centeredSlides: false,
					autoplay: false,
					pagination: false,
					allowTouchMove: true,
					grabCursor: true,
				});
			} else {
				let swiper3 = new Swiper('.home-new-analyse-swiper', {
					loop: false,
					slidesPerView: 1,
					spaceBetween: 20,
					centeredSlides: false,
					autoplay: false,
					pagination: false,
					allowTouchMove: true,
					grabCursor: true,
				});
			}
		});
	};

	this.sectionSwiperNews = function () {
		$(window).on('resize load', function () {
			if (window.innerWidth > 992) {
				let swiper3 = new Swiper('.home-new-news', {
					loop: false,
					slidesPerView: 3,
					spaceBetween: 30,
					centeredSlides: false,
					autoplay: false,
					pagination: false,
					allowTouchMove: true,
					grabCursor: true,
					navigation: {
						nextEl: '.swiper-button-next-analyse',
						prevEl: '.swiper-button-prev-analyse',
					},
				});
			} else if (window.innerWidth > 767) {
				let swiper3 = new Swiper('.home-new-news', {
					loop: false,
					slidesPerView: 2,
					spaceBetween: 20,
					centeredSlides: false,
					autoplay: false,
					pagination: false,
					allowTouchMove: true,
					grabCursor: true,
					navigation: {
						nextEl: '.swiper-button-next-analyse',
						prevEl: '.swiper-button-prev-analyse',
					},
				});
			} else {
				let swiper3 = new Swiper('.home-new-news', {
					loop: false,
					slidesPerView: 1,
					spaceBetween: 20,
					centeredSlides: false,
					autoplay: false,
					pagination: false,
					allowTouchMove: true,
					grabCursor: true,
				});
			}
		});
	};

	this.question = function () {
		$(window).on('resize load', function () {
			if (window.innerWidth < 768) {
				clearInterval(interval)
			}
		})

		const question = $('.home-new-question');
		const listQuestion = question.find('.content ul li');
		const listAnswer = question.find('.right .answer');

		let changeAnswer = function (type) {
			listAnswer.removeClass('active')
			listAnswer.each(function () {
				let self = $(this);
				if (self.attr('data-type') === type) {
					self.addClass('active')
				}
			})
		}

		listQuestion.on('click', function () {
			listQuestion.removeClass('active');
			let self = $(this);
			self.addClass('active')

			let dataType = self.attr('data-type')
			changeAnswer(dataType)

			clearInterval(interval)
			interval = setInterval(changeQuestionAuto, 4000)
		});

		let changeQuestionAuto = function () {
			let flag = true
			listQuestion.each(function () {
				if (!flag) {
					return
				}
				let selfQuestion = $(this);
				if (selfQuestion.attr('data-type') === '1' && selfQuestion.hasClass('active')) {
					selfQuestion.removeClass('active')
					listQuestion.each(function () {
						if ($(this).attr('data-type') === '2') {
							$(this).addClass('active')
							changeAnswer('2')
							flag = false
						}
					});
				}
				if (selfQuestion.attr('data-type') === '2' && selfQuestion.hasClass('active')) {
					selfQuestion.removeClass('active')
					listQuestion.each(function () {
						if ($(this).attr('data-type') === '3') {
							$(this).addClass('active')
							changeAnswer('3')
							flag = false
						}
					});
				}
				if (selfQuestion.attr('data-type') === '3' && selfQuestion.hasClass('active')) {
					selfQuestion.removeClass('active')
					listQuestion.each(function () {
						if ($(this).attr('data-type') === '1') {
							$(this).addClass('active')
							changeAnswer('1')
							flag = false
						}
					});
				}
			});
		};

		let interval = setInterval(changeQuestionAuto, 4000)
	}

	this.sectionSwiperBanner();
	this.sectionSwiperMenu();
	this.sectionSwiperProductService();
	this.sectionSwiperAnalyseSwiper();
	this.sectionSwiperNews();
	this.question();
}

function sectionMenu() {
	this.menu = function () {
		// SP
		let swiper = new Swiper('.menu-default.sp', {
			loop: true,
			spaceBetween: 0,
			centeredSlides: false,
			slidesPerView: 2,
			navigation: {
				nextEl: '.swiper-button-next.for-menu',
				prevEl: '.swiper-button-prev.form-menu',
			},
			grabCursor: true,
		});

		// PC
		let menuType = $('.section-menu .menu');
		let menuDetail = $('.section-menu .menu-detail');
		menuType.on('click', function () {
			let self = $(this)

			let type = self.attr('data-type')

			menuType.removeClass('active')

			self.addClass('active')

			menuDetail.addClass('d-none')
			menuDetail.each(function () {
				if ($(this).attr('data-type') === type) {
					$(this).removeClass('d-none')
				}
			})
		})

	};

	this.menu();
}

function sectionAbout() {
	this.menu = function () {
		// SP
		let swiper = new Swiper('.menu-default.sp', {
			loop: true,
			spaceBetween: 0,
			centeredSlides: false,
			slidesPerView: 2,
			navigation: {
				nextEl: '.swiper-button-next.for-menu',
				prevEl: '.swiper-button-prev.form-menu',
			},
			grabCursor: true,
		});

		// PC
		let menuType = $('.section-menu .menu');
		let menuDetail = $('.section-menu .about');
		menuType.on('click', function () {
			let self = $(this)

			let type = self.attr('data-type')

			menuType.removeClass('active')

			self.addClass('active')

			menuDetail.addClass('d-none')
			menuDetail.each(function () {
				if ($(this).attr('data-type') === type) {
					$(this).removeClass('d-none')
				}
			})
		})

	};

	this.menu();
}

function actionForm() {
	this.select = function () {
		let select = $('.form-input select')
		select.on('change', function () {
			let value = this.value

			value === '' ? $(this).removeClass('selected') : $(this).addClass('selected');
		})
	}

	this.openAccount = function () {
		const step1 = $('[data-open-account=1]')
		const step2 = $('[data-open-account=2]')

		$('.step-form .continue-next').on('click', function () {
			step1.addClass('d-none')
			step2.removeClass('d-none')
			$("html, body").animate({ scrollTop: 250 }, "fast");
		})

		$('.step-form .continue-back').on('click', function () {
			step2.addClass('d-none')
			step1.removeClass('d-none')
			$("html, body").animate({ scrollTop: 250 }, "fast");
		})
	}

	this.changeRelationUS = function () {
		const understandMe = $('.understand-me');
		$('input[type=radio][name=national]').change(function () {
			$(this).val() === '0' ? understandMe.removeClass('d-none') : understandMe.addClass('d-none');
		});
	};

	this.digitCode = function () {
		const selector = $('.authentication-digit-code input.visible');
		this.validateNumber = function () {
			$(selector).on('input', function () {
				this.value = this.value.replace(/[^0-9]/g, '');
			});
		};
		this.actionInput = function () {
			$(selector).on('click', function () {
				if (!$(selector).first().val()) {
					$(selector).first().focus();
				}
				let numberInput = 1;
				while (numberInput <= 6) {
					let numberChild = $(selector).filter(':nth-of-type(' + numberInput + ')');
					if (!numberChild.val()) {
						numberChild.focus();
						break;
					}
					numberInput++;
				}
			});

			$(selector).on('keyup', function (e) {
				if (this.value.length) {
					$(this).next().focus();
				}
				if (e.keyCode === 8) {
					let prevSelector = $(this).prev();
					prevSelector.focus();
					prevSelector.val('');
				}
			});

			$('#fail-modal').on('hidden.bs.modal', function () {
				$('input.visible:first-of-type').focus();
			});

			if (navigator.userAgent.search("Safari") >= 0 && navigator.userAgent.search("Chrome") < 0) {
				$('.visible').addClass('safari');
			}
		};
		this.paste = function () {
			let inputVisible = $('input.visible');
			inputVisible.bind('paste', function (e) {
				let pastedData = '';
				let ua = window.navigator.userAgent;
				let isIE = /MSIE|Trident/.test(ua);
				if (isIE) {
					pastedData = window.clipboardData.getData('Text');
				} else {
					pastedData = e.originalEvent.clipboardData.getData('text');
				}
				let data = pastedData.replace(/[^0-9]/g, '').split('');
				inputVisible.each(function () {
					$(this).val('');
				});
				$.each(data, function (key, value) {
					inputVisible.filter(':nth-of-type(' + (key + 1) + ')').val(value);
				});
				inputVisible.filter(':nth-of-type(' + (data.length || 1) + ')').focus();
			});
		};
		this.init = function () {
			this.validateNumber();
			this.paste();
			this.actionInput();
		};
		this.init();
	}

	this.countdown = function () {
		const parent = $('#otpModal')
		const NUMBER = 60
		let countdownNumber = NUMBER
		const countdownEle = parent.find('.countdown-second')
		countdownEle.text(countdownNumber)
		setInterval(function () {
			countdownEle.text(countdownNumber)
			if (countdownNumber <= NUMBER / 2) {
				parent.find('.re-send').removeClass('opacity-0')
			}
			if (countdownNumber > 0) {
				countdownNumber = countdownNumber - 1
			}
		}, 1000)

		parent.find('.re-send').on('click', function () {
			countdownNumber = NUMBER
		})
	}

	this.nextStepModal = function () {
		const parent = $('#otpModal')
		const modal1 = parent.find('.modal-body-1')
		const modal2 = parent.find('.modal-body-2')
		const buttonNext = modal1.find('button')
		buttonNext.on('click', function () {
			modal1.addClass('d-none')
			modal2.removeClass('d-none')
			parent.find('img.close').removeClass('d-none')
		})
	}

	this.select()
	this.openAccount()
	// this.changeRelationUS()
	this.digitCode()
	this.countdown()
	this.nextStepModal()
}

function actionRegister() {
	this.selectType = function () {
		let menuType = $('.ekyc-form .desc .select-form a');
		let form = $('.ekyc-form .confirm');
		menuType.on('click', function () {
			let self = $(this)

			let type = self.attr('data-type')

			menuType.removeClass('active')

			self.addClass('active')

			form.addClass('d-none')
			form.each(function () {
				if ($(this).attr('data-type') === type) {
					$(this).removeClass('d-none')
				}
			})
		})
	}

	this.selectType()
}

function takePhoto() {
	const photo = $('.take-photo .photo')

	this.take = function () {
		const button = photo.find('button')

		button.on('click', function () {
			let self = $(this)

			self.parents('.photo').find('input').click()
		})
	}

	this.display = function () {
		const input = photo.find('input')

		input.on('change', function (event) {
			let self = $(this)
			let file = event.target.files[0]

			let src = URL.createObjectURL(file)

			let parent = self.parent()
			let img = parent.find('.input-upload');
			img.attr('src', src)
			img.removeClass('d-none')

			parent.find('.close').on('click', function () {
				self.val('')
				img.attr('src', '')
			})
		})
	}

	this.take()
	this.display()
}
