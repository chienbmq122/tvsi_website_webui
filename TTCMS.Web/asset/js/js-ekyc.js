﻿
$(document).ready(function () {
	$("#BankNo_01").flexselect();
	
	$("#tinhtptxt").flexselect();
	$("#quanhuyentxt").flexselect();
	$("#xaphuongtxt").flexselect();
	
	$("#noicaptxt").flexselect();
	$("#SubBranchNo_01").flexselect();
	$("#BankNo_02").flexselect();
	$("#SubBranchNo_02").flexselect();
	$("#sale_id").flexselect();
	let paramsUrl = new URLSearchParams(window.location.search)
	var saleValue = paramsUrl.has('SaleID');
	if (saleValue){
		var saleID = paramsUrl.get('SaleID');
		$("#saletxt").val(saleID);
		getSaleName(saleID);
		$('#saletxt').attr('readonly', true);
	}
	
	
	$('.ekyc-step-2-2').prop('disabled', true);
	$('.btn-firm-otp').prop('disabled', true);
	$("#CustomerRelaUsaNo").prop("checked", true);
	$("#CustomerUSAYes").prop("checked", true);
	$("#Text_PlaceOfBirthTxtYes").prop("checked", true);
	$("#Text_depositoryUSAYes").prop("checked", true);
	$("#Text_PhoneNumberUSAYes").prop("checked", true);
	$("#Text_PaymentAddressUSAYes").prop("checked", true);
	$("#Text_AuthorizationLetterUSAYes").prop("checked", true);
	$("#Text_CustomerReceiveMailYes").prop("checked", true);
	$('.show-us').hide();
	$("#face-to-ekyc").hide();
	var next_ekyc = $(".ekyc-step-2-1");
	var previous_ekyc = $(".previous-step-2-1");
	var previous_hide = $(".previous-step-2-1-1");
	var next_hide = $(".ekyc-step-2-1-1");
	
	var current_fs, next_fs, previous_fs; //fieldsets
	var opacity;
	var current = 1;
	var steps = $("fieldset").length;
	var error_messenger = ("error-messenger");
	var step_ekyc_first = ("step-ekyc-1");
	var step_ekyc_second = ("ekyc-step-2-1");
	var step_next_ekyc = ("ekyc-step-2-1-1");
	var check_success = false;
	
	previous_ekyc.hide();
	next_hide.hide();
	$(".next").click(function () {
		if ($(this).hasClass(step_ekyc_first)) {
			var valid = false;
			if ($("#hotentxt").val() == null || $("#hotentxt").val() === "") {
				$("#hotentxt").focus();
				$("#hotentxt").addClass(error_messenger);
				$("#hotentxt").closest(".col-md-9").find(".text-error-messenger").show();
				valid = true;
			}
			if ($("#phonetxt").val() == null || $("#phonetxt").val() === "") {
				$("#phonetxt").focus();
				$("#phonetxt").addClass(error_messenger);
				$("#phonetxt").closest(".col-md-9").find(".text-error-messenger").show();
				valid = true;
			}
			if ($("#emailtxt").val() == null || $("#emailtxt").val() === "") {
				$("#emailtxt").focus();
				$("#emailtxt").addClass(error_messenger);
				$("#emailtxt").closest(".col-md-9").find(".text-error-messenger").show();
				valid = true;
			}
			if ($("#emailtxt").val() != null || $("#emailtxt").val() != "") {
				if (!validateEmail($("#emailtxt").val())){
					swal("Thông báo!!", "Email không đúng định dạng, vui lòng nhập lại.", "error");
					valid = true;
				}
				if (validCharacters($("#emailtxt").val())){
					swal("Thông báo!!", "Email không đúng định dạng, vui lòng nhập lại.", "error");
					valid = true;
				}
			}
			
			if (valid === true)
				return;
			
			var phone = $("#phonetxt").val();
			var email = $("#emailtxt").val();
			
			$("#Phone_01").val(phone);
			$("#email").val(email);
		}
		if ($(this).hasClass(step_next_ekyc)) {
			var face_Image = $(".face-to-ekyc #results_front_camera").find("img");
			if (face_Image.length > 0) {
				var imageFace = $(".face-to-ekyc #results_front_camera img").attr('src');
				var base64imFace = imageFace.split(',')[1];
				var data = UploadImageWebCam(base64imFace);
				var image_Face = $(".face-to-ekyc #results_front_camera img").attr('src');
				var image_Attr = $("#thongtincanhan #imagecmnd img");
				if (data === true) {
					
					var sale_id = $("#saletxt").val();
					if (sale_id != ''){
						$(".QA").hide();
					}else{
						$(".QA").show();
					}
					image_Attr.attr('src', image_Face);
				} else {
					return;
				}
				
			} else {
				swal("Thiếu ảnh!", "Không có hình ảnh chân dung, vui lòng chụp lại.", "error")
				return;
			}
		}
		if ($(this).hasClass("ekyc-step-2-2")) {
			var valid = false;
			if ($("#hotentxtt").val() == null || $("#hotentxtt").val() === "") {
				$("#hotentxtt").focus();
				$("#hotentxtt").addClass(error_messenger);
				$("#hotentxtt").closest(".col-sm-8").find(".text-error-messenger").show();
				valid = true;
			}
			if ($("#ngaysinhtxt").val() == null || $("#ngaysinhtxt").val() === "") {
				$("#ngaysinhtxt").focus();
				$("#ngaysinhtxt").addClass(error_messenger);
				$("#ngaysinhtxt").closest(".col-sm-8").find(".text-error-messenger").show();
				valid = true;
			}
			if ($("#ngaycaptxt").val() == null || $("#ngaycaptxt").val() === "") {
				$("#ngaycaptxt").focus();
				$("#ngaycaptxt").addClass(error_messenger);
				$("#ngaycaptxt").closest(".col-sm-8").find(".text-error-messenger").show();
				valid = true;
			}
			if ($("#noicaptxt").val() == null || $("#noicaptxt").val() === "") {
				$("#noicaptxt").focus();
				$("#noicaptxt").addClass(error_messenger);
				$("#noicaptxt").closest(".col-sm-8").find(".text-error-messenger").show();
				$("#noicaptxt_flexselect").addClass(error_messenger);
				valid = true;
			}
			if ($("#dkhtttxt").val() == null || $("#dkhtttxt").val() === "") {
				$("#dkhtttxt").focus();
				$("#dkhtttxt").addClass(error_messenger);
				$("#dkhtttxt").closest(".col-sm-8").find(".text-error-messenger").show();
				valid = true;
			}
			if ($("#tinhtptxt").val() == null || $("#tinhtptxt").val() === "") {
				$("#tinhtptxt_flexselect").focus();
				$("#tinhtptxt").addClass(error_messenger);
				$("#tinhtptxt").closest(".col-sm-4").find(".text-error-messenger").show();
				$("#tinhtptxt_flexselect").addClass(error_messenger);
				valid = true;
			}
			if ($("#quanhuyentxt").val() == null || $("#quanhuyentxt").val() === "") {
				$("#quanhuyentxt_flexselect").focus();
				$("#quanhuyentxt").addClass(error_messenger);
				$("#quanhuyentxt").closest(".col-sm-4").find(".text-error-messenger").show();
				$("#quanhuyentxt_flexselect").addClass(error_messenger);
				valid = true;
			}
			if ($("#xaphuongtxt").val() == null || $("#xaphuongtxt").val() === "") {
				$("#xaphuongtxt__flexselect").focus();
				$("#xaphuongtxt").addClass(error_messenger);
				$("#xaphuongtxt").closest(".col-sm-4").find(".text-error-messenger").show();
				$("#xaphuongtxt_flexselect").addClass(error_messenger);
				valid = true;
			}
			if ($("#diachichitiettxt").val() == null || $("#diachichitiettxt").val() === "") {
				$("#diachichitiettxt").focus();
				$("#diachichitiettxt").addClass(error_messenger);
				$("#diachichitiettxt").closest(".col-sm-8").find(".text-error-messenger").show();
				valid = true;
			}
			var full_name = $("#hotentxtt").val();
			$("#BankAccName_01").val(full_name);
			if ($("#ngaysinhtxt").val() != ''){
				var birthDay = $("#ngaysinhtxt").val();
				
				var dateofBirth = fvalidBirthDay(birthDay);
				if (dateofBirth != null){
					swal("Thông báo!!!", dateofBirth, "error")
					valid = true;
				}
			}
			if ($("#ngaycaptxt").val() != ''){
				var issueDate = $("#ngaycaptxt").val();
				var birthDay = $("#ngaysinhtxt").val();
				var cardID = $("#cmndtxt").val();
				var type = $("#type").val();
				var validIssueDate = fvalidIssueDate(issueDate,birthDay,cardID,type);
				if (validIssueDate != null){
					swal("Thông báo!!!", validIssueDate, "error")
					valid = true;
				}
			}
			console.log(valid);
			if (valid === true){
				return;
			}
		}
		
		if ($(this).hasClass("btn-firm-otp")) {
			var valid = false;
			if ($('input[name="BankReg"]:checked').val() == 1) {
				if ($("#BankAccNo_01").val() == null || $("#BankAccNo_01").val() === "") {
					$("#BankAccNo_01").focus();
					$("#BankAccNo_01").addClass(error_messenger);
					$("#BankAccNo_01").closest(".col-md-9").find(".text-error-messenger").show();
					valid = true;
				}
				if ($("#BankNo_01").val() == null || $("#BankNo_01").val() === "") {
					$("#BankNo_01_flexselect").focus();
					$("#BankNo_01_flexselect").addClass(error_messenger);
					$("#BankNo_01").closest(".col-md-9").find(".text-error-messenger").show();
					valid = true;
				}
				if ($("#SubBranchNo_01").val() == null || $("#SubBranchNo_01").val() === "" || $("#SubBranchNo_01_flexselect").val() == null || $("#SubBranchNo_01_flexselect").val() === "" ) {
					$("#SubBranchNo_01_flexselect").focus();
					$("#SubBranchNo_01_flexselect").addClass(error_messenger);
					$("#SubBranchNo_01").closest(".col-md-9").find(".text-error-messenger").show();
					valid = true;
				}
				
				if ($("#BankAccNo_02").val() != "") {
					if ($("#BankNo_02").val() == null || $("#BankNo_02").val() === "") {
						$("#BankNo_02").focus();
						$("#BankNo_02_flexselect").addClass(error_messenger);
						$("#BankNo_02").closest(".col-md-9").find(".text-error-messenger").show();
						valid = true;
					}
				}
				if ($("#BankNo_02").val() != "") {
					if ($("#BankAccNo_02").val() == null || $("#BankAccNo_02").val() == "") {
						$("#BankAccNo_02").focus();
						$("#BankAccNo_02").addClass(error_messenger);
						$("#BankAccNo_02").closest(".col-md-9").find(".text-error-messenger").show();
						valid = true;
					}
				}
				if ($("#SubBranchNo_02").val() != "" ) {
					if ($("#SubBranchNo_02").val() == null || $("#SubBranchNo_02").val() === "" || $("#SubBranchNo_02_flexselect").val() == null || $("#SubBranchNo_02_flexselect").val() === "") {
						$("#SubBranchNo_02_flexselect").focus();
						$("#SubBranchNo_02_flexselect").addClass(error_messenger);
						$("#SubBranchNo_02").closest(".col-md-9").find(".text-error-messenger").show();
						valid = true;
					}
				}
			}
			if ($("#Phone_01").val() == null || $("#Phone_01").val() === "") {
				$("#Phone_01").focus();
				$("#Phone_01").addClass(error_messenger);
				$("#Phone_01").closest(".col-md-7").find(".text-error-messenger").show();
				valid = true;
			}
			if ($("#email").val() == null || $("#email").val() === "") {
				$("#email").focus();
				$("#email").addClass(error_messenger);
				$("#email").closest(".col-md-7").find(".text-error-messenger").show();
				valid = true;
			}
			let searchParams = new URLSearchParams(window.location.search)
			var sale_value = searchParams.has('SaleID');
			
			if (sale_value === false){
				if ($("#saletxt").val() == null || $("#saletxt").val() == "") {
					var tvsiPack = $("input[name='QA1txt']:checked").val();
					if (tvsiPack === "" || tvsiPack == null) {
						$("input[name='QA1txt']").focus();
						$(".QA").find(".text-error-messenger").show();
						valid = true;
					}
				}	
			}
			
			if ($("#email").val() != null || $("#email").val() != "") {
				if (!validateEmail($("#email").val())) {
					$("#email").focus();
					swal("Thông báo!!", "Email không đúng định dạng, vui lòng nhập lại.", "error");
					valid = true;
				}
				if (validCharacters($("#email").val())) {
					$("#email").focus();
					swal("Thông báo!!", "Email không đúng định dạng, vui lòng nhập lại.", "error");
					valid = true;
				}
			}
			
			
			if (valid === true) {
				return;
			}
			var cardNumber = $("#cmndtxt").val();
			var sale_id = $("#saletxt").val();
			var fullName = $("#hotentxtt").val();
			var gender = $("#gioitinhtxt").val();
			var birthday = $("#ngaysinhtxt").val();
			var issueDate = $("#ngaycaptxt").val();
			var note = $("#note").val();
			var issuePlace = $("#noicaptxt option:selected").text();
			var provinceValue = $("#tinhtptxt").val();
			var address = $("#dkhtttxt").val();
			var addressDetail = $("#diachichitiettxt").val();
			var province = $("#tinhtptxt option:selected").text();
			var distric = $("#quanhuyentxt option:selected").text();
			var ward = $("#xaphuongtxt option:selected").text();
			
			$("#hoten").val(fullName);
			$("#sale_id").val(sale_id);
			$("#ngaysinh").val(birthday);
			$("#gioitinh").val(gender);
			$("#cmnd").val(cardNumber);
			$("#ngaycapcmnd").val(issueDate);
			$("#noicap").val(issuePlace);
			$("#diachi").val();
			$("#Province").val(province);
			$("#Distric").val(distric);
			$("#Ward").val(ward);
			$("#ProvinceValue").val(provinceValue);
			$("#AddressDetail").val(addressDetail);
			$("#Note").val(note);
			
			let phone = $("input[name='Phone_01']").val();
			$.ajax({
				url: '/OpenAccount/GenerateOTP',
				type: "POST",
				dataType: "JSON",
				data: {phone: phone},
				success: function (result) {
					if (result.Status == true) {
						console.log(result.Data);
						$("#modal-confirm-code-ekyc").modal("show");
					}
				}
			});
			
			
			$("#modal-confirm-code-ekyc").modal("show");
			current_fs = $(this).parent();
			next_fs = $(this).parent().next();
			
			$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
			return;
		}
		
		
		next_ekyc.show();
		previous_ekyc.hide();
		next_hide.hide();
		previous_hide.show();
		
		current_fs = $(this).parent();
		next_fs = $(this).parent().next();
		
		$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
		next_fs.show();
		current_fs.animate({opacity: 0}, {
			step: function (now) {

				opacity = 1 - now;
				current_fs.css({
					'display': 'none',
					'position': 'relative'
				});
				next_fs.css({'opacity': opacity});
			},
			duration: 500
		});
	});
	$(next_ekyc).click(function () {

		var file_front_check = $(".sRegister #results_front_camera").find("img");
		var file_back_check = $(".sRegister #results_backside_camera").find("img");
		
		if ($(this).hasClass(step_ekyc_second)) {
			if (file_front_check.length > 0 && file_back_check.length > 0) {
				var imgfront = $("#thongtincanhan #imagefront img");
				var imgback = $("#thongtincanhan #imageback img");
				
				var err_text = $(".wrap__two__content__error");
				var er_front = $(".error_front");
				var er_back = $(".error_back");
				var file_front = $(".sRegister #results_front_camera img").attr('src');
				var file_back = $(".sRegister #results_backside_camera img").attr('src');
				var imageFrontBase64 = file_front;
				var imageBackBase64 = file_back;
				var paramFront = file_front.split(',')[1];
				var paramBack = file_back.split(',')[1];
				$.ajax({
					url: '/OpenAccount/UploadImageEKYC',
					data: '{"front" : "' + paramFront + '","back" : "' + paramBack + '"}',
					contentType: 'application/json; charset=utf-8',
					dataType: 'json',
					type: 'POST',
					beforeSend: function () {
						$('.spinner-loader').show();
					},
					complete: function () {
						$('.spinner-loader').hide();
					},
					success: function (response) {
						console.log(response);
						if (response.Status == true){
							$('.spinner-loader').hide();
							var retData = response.Data.DataEkyc.object;
							if (retData.gender != null && retData.gender != "-") {
								if (retData.gender == "Nam") {
									$("#gioitinhtxt").val(1);
								} else {
									$("#gioitinhtxt").val(2);
								}
							}
							$("#face-to-ekyc").show();
							$(".ocr-card").hide();
							$("#cmndtxt").val(retData.id);
							$("#hotentxtt").val(capitalize(retData.name,true));
							$("#ngaysinhtxt").val(retData.birth_day);
							$("#dkhtttxt").val(retData.recent_location.replace("\n", ", "));
							$("#ngaycaptxt").val(retData.issue_date);
							$("#type").val(retData.type_id);
							if (retData.valuePlace != 0 || retData.valuePlace != "" || retData.valuePlace != null){
								$("#noicaptxt").val(retData.valuePlace).change();
							}else{
								$("#noicaptxt").val(titleCase(retData.issue_place));
							}
							imgfront.attr('src', imageFrontBase64);
							imgback.attr('src', imageBackBase64);
							
							/* show interface */
							$("#face-to-ekyc").show();
							$(".ocr-card").hide();
							next_ekyc.hide();
							next_hide.show();
							previous_hide.hide();
							previous_ekyc.show();
						}else{
							err_text.show();
							if (response.Type == 1) {
								er_front.text(response.Message);
								return;
							} else if (response.Type == 2) {
								er_back.text(response.Message);
								return;
								
							} else if (response.Type == 0) {
								er_front.text(response.Message);
								er_back.text(response.Message);
								return;
							}
							next_ekyc.show();
							next_hide.hide();
							previous_hide.show();
							previous_ekyc.hide();
							return;
						}

					},
					failure: function (response) {
						console.log(response.responseText);
					},
					error: function (response) {
						console.log(response.responseText);
						
					}
				});
				
			} else {
				swal("Thiếu ảnh!", "Vui lòng tải lên 2 mặt CMND/CCCD!", "error")
				return;
			}
		}
		
		
	});
	$("input[name='exampleCheck1']").change(function (){
		if (this.checked){
			$('.ekyc-step-2-2').prop('disabled', false);
		}else{
			$('.ekyc-step-2-2').prop('disabled', true);
		}
	});
	$("input[name='agreepolicy']").change(function (){
		if (this.checked){
			$('.btn-firm-otp').prop('disabled', false);
		}else{
			$('.btn-firm-otp').prop('disabled', true);
		}
	});
	
	
	
	$("input[name='CustomerRelaUsa']").change(function () {
		var value = $(this).val();
		if (value == 1){
			$(".show-us").show();
		}else{
			$(".show-us").hide();
		}
	});
	
	$(previous_ekyc).click(function () {
		previous_hide.show();
		$(this).hide();
		$("#face-to-ekyc").hide();
		$(".ocr-card").show();
	});
	
	$(".previous").click(function () {
		$("#face-to-ekyc").hide();
		$(".ocr-card").show();
		previous_ekyc.hide();
		next_hide.hide();
		
		next_ekyc.show();
		previous_hide.show();
		if ($(this).hasClass("btn-firm-back")){
			$("#confirm").removeClass("active");
			$("#profile").removeClass("active");
		}
		
		current_fs = $(this).parent();
		previous_fs = $(this).parent().prev();
		
		console.log(current_fs);
		console.log(previous_fs);

//Remove class active
		$("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

//show the previous fieldset
		previous_fs.show();

//hide the current fieldset with style
		current_fs.animate({opacity: 0}, {
			step: function (now) {
// for making fielset appear animation
				opacity = 1 - now;
				
				current_fs.css({
					'display': 'none',
					'position': 'relative'
				});
				previous_fs.css({'opacity': opacity});
			},
			duration: 500
		});
		
	});
	$("#saletxt").change(function (){
		var value = $(this).val();
		if (value != '' && value != null){
			getSaleName(value);
		}
	});
	
	$("input[name='QA1txt']").change(function (){
		if ($(this).prop("checked") === true){
			var value = $(this).val();
			if (value == 1){
				$("#QA2txt").closest(".QA").find(".detail-customer").hide();
				$("#QA3txt").closest(".QA").find(".detail-customer").hide();
			}else if (value == 2){
				$("#QA1txt").closest(".QA").find(".detail-customer").hide();
				$("#QA3txt").closest(".QA").find(".detail-customer").hide();
			}else{
				$("#QA1txt").closest(".QA").find(".detail-customer").hide();
				$("#QA2txt").closest(".QA").find(".detail-customer").hide();
			}
			
			$(this).closest(".QA").find(".detail-customer").show();
		}else{
			$(this).closest(".QA").find(".detail-customer").hide();
			
		}
	});
	$("input[name='BankReg']").change(function (){
		var value = $(this).val();
		if (value == 2){
			$(".tab-bank").hide();
		}else{
			$(".tab-bank").show();
			
		}
	});
	
	function getSaleName(saleID) {
		$.ajax({
			url: '/OpenAccount/GetSaleName',
			data: '{"saleID" : "' + saleID + '"}',
			contentType: 'application/json; charset=utf-8',
			dataType: 'json',
			type: 'POST',
			success: function (response) {
				console.log(response);
				if (response.Status == true) {
					$("#saletinfoxt").val(response.data)
				} else {
					$("#saletinfoxt").val('');
					$("#saletxt").val('');
					swal("Không có thông tin!", "Không có thông tin nhân viên môi giới.", "error")
					
				}
			},
			failure: function (response) {
				console.log(response.responseText);
			},
			error: function (response) {
				console.log(response.responseText);
				
			}
		});
	}
	function UploadImageWebCam(imageface) {
			var value = false;
			$.ajax({
				url: '/OpenAccount/UploadImageFace',
				data: '{"face" : "' + imageface + '"}',
				type: 'POST',
				contentType: 'application/json; charset=utf-8',
				dataType: 'json',
				async:false,
				beforeSend: function () {
					getSpinner();
				},
				complete: function () {
					hideSpinner();
				},
				success: function (response) {
					console.log(response);
					if (response.Status == true) {
						value = true;
						var data = response.Data.DataEkyc.object;
						if (data != null){
							var prob_face = data.prob;
							if (prob_face >= 80.00) {
								$(".sosanh").text("Khuôn mặt khớp").css("font-weight", "700").css("color", "#5bd210");
							} else {
								$(".sosanh").text("Khuôn mặt không khớp với CMND/CCCD vui lòng thử lại").css("color", "red").css("font-weight", "700");
								$("#modal-error-ekyc").modal("show");
								$("#cmndtxt").val('');
								$("#hotentxtt").val('');
								$("#ngaysinhtxt").val('');
								$("#dkhtttxt").val('');
								$("#ngaycaptxt").val('');
								$("#noicaptxt").val('');
								value = false;
							}
							
						}
					} else {
						$('.error_face').show();
						$('.error_face').text(response.Message);
						value = false;
						
					}
				},
				failure: function (response) {
					value = false;
					console.log(response);
				},
				error: function (response) {
					value = false;
					console.log(response);
				}
			});
			return value;
	}
	
	function getSpinner() {
		var loadingimage = $('.spinner-loader').show();
		return loadingimage;
	}
	function hideSpinner() {
		var loadingimage = $('.spinner-loader').hide();
		return loadingimage;
	}
	
	$(".submit").click(function () {
		return false;
	})
	
	$.datepicker.regional["vi-VN"] =
		{
			closeText: "Đóng",
			prevText: "Trước",
			nextText: "Sau",
			currentText: "Hôm nay",
			monthNames: ["Tháng một", "Tháng hai", "Tháng ba", "Tháng tư", "Tháng năm", "Tháng sáu", "Tháng bảy", "Tháng tám", "Tháng chín", "Tháng mười", "Tháng mười một", "Tháng mười hai"],
			monthNamesShort: ["Một", "Hai", "Ba", "Bốn", "Năm", "Sáu", "Bảy", "Tám", "Chín", "Mười", "Mười một", "Mười hai"],
			dayNames: ["Chủ nhật", "Thứ hai", "Thứ ba", "Thứ tư", "Thứ năm", "Thứ sáu", "Thứ bảy"],
			dayNamesShort: ["CN", "Hai", "Ba", "Tư", "Năm", "Sáu", "Bảy"],
			dayNamesMin: ["CN", "T2", "T3", "T4", "T5", "T6", "T7"],
			weekHeader: "Tuần",
			dateFormat: "dd/mm/yy",
			firstDay: 1,
			isRTL: false,
			showMonthAfterYear: false,
			yearSuffix: ""
		};
	$.datepicker.setDefaults($.datepicker.regional['vi-VN']);
	$("#ngaysinhtxt").datepicker({
		dateFormat: "dd/mm/yy",
		changeMonth: true,
		changeYear: true
	});
	$("#ngaycaptxt").datepicker({
		dateFormat: "dd/mm/yy",
		changeMonth: true,
		changeYear: true
	});
	
	function capitalize(string, a) {
		var tempstr = string.toLowerCase();
		if (a == false || a == undefined)
			return tempstr.replace(tempstr[0], tempstr[0].toUpperCase());
		else {
			return tempstr.split(" ").map(function (i) { return i[0].toUpperCase() + i.substring(1) }).join(" ");
		}
	}
	function titleCase(string) {
		return string[0].toUpperCase() + string.slice(1).toLowerCase();
	}
	$("#tinhtptxt").on("change", function () {
		var selectedVal = $(this).find(':selected').val();
		$("#quanhuyentxt").html("");
		if (selectedVal != ''){
			GetDistrict(selectedVal);
		}else{
			$("#quanhuyentxt").append(
				$('<option></option>').val('').html('Chọn Tỉnh/TP...'));
			$("#quanhuyentxt").flexselect();
		}
	});
	$("#quanhuyentxt").on("change", function () {
		var selectedVal = $(this).find(':selected').val();
		$("#xaphuongtxt").html("");
		if (selectedVal != ''){
			GetWardList(selectedVal);
		}else{
			$("#xaphuongtxt").append(
				$('<option></option>').val('').html('Chọn Xã/Phường...'));
			$("#xaphuongtxt").flexselect();
		}
	});

	
	$("#btn-confirmOTP").click(function (){
		let codeOTP_1 = $("#otp-form #ist").val()
		let codeOTP_2 = $("#otp-form #sec").val()
		let codeOTP_3 = $("#otp-form #third").val()
		let codeOTP_4= $("#otp-form #fourth").val()
		let codeOTP_5 = $("#otp-form #fifth").val()
		let codeOTP_6 = $("#otp-form #sixth").val()
		let phone = $("input[name='Phone_01']").val();
		let cardId = $("input[name='cmndtxt']").val();
		if (codeOTP_1 != '' && codeOTP_2 != '' && codeOTP_3 != '' && codeOTP_4 != ''
			&& codeOTP_5 != '' && codeOTP_6 != '' )
		{
			let otp = (codeOTP_1 + codeOTP_2 + codeOTP_3 + codeOTP_4 + codeOTP_5 + codeOTP_6);
			if (otp.length >= 6){
				$.ajax({
					url: '/OpenAccount/ConfirmOTP',
					type: "POST",
					dataType: "JSON",
					data: { otp : otp},
					success: function (result) {
						if (result.Status === true){
							$("#signupform").submit();
						}else{
							$(".confirm-fail-otp").text(result.Message).show();
						}
					}
				});
			}
		}else{
			$(".confirm-fail-otp").text("Vui lòng nhập đầy đủ mã OTP").show();
		}
		
	});
	$("#btn-resendOTP").click(function (){
		let phone = $("input[name='Phone_01']").val();
		if (phone != null && phone != ""){
			begin(phone)
		}
	});
	function begin(phone) {
		$("#ist").val('');
		$("#sec").val('');
		$("#third").val('');
		$("#fourth").val('');
		$("#fifth").val('');
		$("#sixth").val('');
		$("#btn-resendOTP").prop('disabled', true);
		$(".countdown-otp-resend").show();
		var myTimer, timing = 30;
		$('#timing').html(timing);
		$('#begin').prop('disabled', true);
		myTimer = setInterval(function() {
			--timing;
			$('#timing').html(timing);
			if (timing === 0) {
				fResendOTP(phone);
				$("#btn-resendOTP").prop('disabled', false);
				$(".countdown-otp-resend").hide();
				clearInterval(myTimer);
				$('#begin').prop('disabled', false);
			}
		}, 1000);
	}
	
	
	
	function GetDistrict(provinceCode){
		if (provinceCode != null || provinceCode != ''){
			$.ajax({
				url: '/OpenAccount/GetDistrictList',
				type: "POST",
				dataType: "JSON",
				data: { provinceCode: provinceCode },
				success: function (result) {
					$.each(result.Data, function (i, branch) {
						$("#quanhuyentxt").append(
							$('<option></option>').val(branch.DistrictCode).html(branch.DistrictName));
					});
					$("#quanhuyentxt").flexselect();
					
				}
			});
		}
	}
	
	function GetWardList(DistrictCode){
		if (DistrictCode != null || DistrictCode != ''){
			$.ajax({
				url: '/OpenAccount/GetWardList',
				type: "POST",
				dataType: "JSON",
				data: { districtCode: DistrictCode },
				success: function (result) {
					$.each(result.Data, function (i, branch) {
						$("#xaphuongtxt").append(
							$('<option></option>').val(branch.WardCode).html(branch.WardName));
					});
					$("#xaphuongtxt").flexselect();
				}
			});
		}
	}
	function isNumber(evt) {
		evt = (evt) ? evt : window.event;
		var charCode = (evt.which) ? evt.which : evt.keyCode;
		if (charCode > 31 && (charCode < 48 || charCode > 57)) {
			return false;
		}
		return true;
	}
	$("#ist").keypress(function (e){
		var charCode = (e.which) ? e.which : e.keyCode;
		if (charCode > 31 && (charCode < 48 || charCode > 57)) {
			return false;
		}
	});
	$("#hotentxt").change(function () {
		var text = $(this).val();
		text = text.toLowerCase().replace(/^[\u00C0-\u1FFF\u2C00-\uD7FF\w]|\s[\u00C0-\u1FFF\u2C00-\uD7FF\w]/g, function (letter) {
			return letter.toUpperCase();
		});
		$(this).val(text);
	});
	$("#hotentxtt").change(function () {
		var text = $(this).val();
		text = text.toLowerCase().replace(/^[\u00C0-\u1FFF\u2C00-\uD7FF\w]|\s[\u00C0-\u1FFF\u2C00-\uD7FF\w]/g, function (letter) {
			return letter.toUpperCase();
		});
		$(this).val(text);
	});
	
	function validateEmail(email) {
		var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
		return emailReg.test(email);
	}
	
	function validCharacters(string){
		var specialChars = "<>!#$%^&*()_+[]{}?:;|'\"\\,/~`-=";
		for(i = 0; i < specialChars.length;i++){
			if(string.indexOf(specialChars[i]) > -1){
				return true;
			}
		}
		return false;
	}
	function fvalidBirthDay(birthday) {
		var value = null;
		$.ajax({
			url: '/OpenAccount/DateTimeIsValidBirthDay',
			type: "POST",
			dataType: "JSON",
			async: false,
			data: { datetime: birthday},
			success: function (result) {
				if (result.Status == false) {
					value = result.Message;
				} else {
					value = null;
				}
			}
		});
		return value;
	}
	
	function fvalidIssueDate(issueDate,birthDay,cardID,type){
		var value = null;
		$.ajax({
			url: '/OpenAccount/DateTimeIsValidIssueDate',
			type: "POST",
			dataType: "JSON",
			async: false,
			data: { dateIssue: issueDate , dateBirth : birthDay, cardID : cardID ,type : type},
			success: function (result) {
				if (result.status === false){
					value = result.Message;
				}
			},
			error: function (er) {
				
			}
		});
		return value;
	}
	
	function fResendOTP(phone) {
		$.ajax({
			url: '/OpenAccount/ResendOTP',
			type: "POST",
			dataType: "JSON",
			data: {phone: phone},
			success: function (result) {
				if (result.Status == true) {
					console.log(result.Data);
				} else {
					$(".confirm-fail-otp").text(result.Message).show();
				}
			}
		});
	}
  function fGetUrlParam(name){
		var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
		return results[1] || 0;
	}
	
	$("#BankNo_01").on("change", function () {
		var selectedVal = $(this).find(':selected').val();
		GetBankInfo(selectedVal, 1);
	});
	$("#BankNo_02").on("change", function () {
		var selectedVal = $(this).find(':selected').val();
		GetBankInfo(selectedVal, 2);
	});
	function GetBankInfo(bankNo, order) {
		if (order == 1) {
			// Clear list chi nhánh
			$("#SubBranchNo_01").html("");
			if (bankNo != '') {
				$.ajax({
					url: '/Openaccount/GetBranchList',
					type: "POST",
					dataType: "JSON",
					data: { bankNo: bankNo },
					success: function (result) {
						console.log(result);
						$.each(result.Data, function (i, branch) {
							$("#SubBranchNo_01").append(
								$('<option></option>').val(branch.BranchNo).html(branch.BranchName));
						});
						$("#SubBranchNo_01").flexselect();
					}
				});
			} else if ($("#BankNo_01").val() == '') {
				
				$("#SubBranchNo_01").append(
					$('<option></option>').val('').html('Chọn Chi nhánh...'));
				$("#SubBranchNo_01").flexselect();
			}
		}
		else if (order == 2) {
			// Clear list chi nhánh
			$("#SubBranchNo_02").html("");
			if (bankNo != '') {
				$.ajax({
					url: '/Openaccount/GetBranchList',
					type: "POST",
					dataType: "JSON",
					data: { bankNo: bankNo },
					success: function (result) {
						console.log(result);
						$.each(result.Data, function (i, branch) {
							$("#SubBranchNo_02").append(
								$('<option></option>').val(branch.BranchNo).html(branch.BranchName));
						});
						//$("#SubBranchNo_02").selectpicker('refresh');
						$("#SubBranchNo_02").flexselect();
					}
				});
			} else {
				$("#SubBranchNo_02").append(
					$('<option></option>').val('').html('Chọn Chi nhánh...'));
				$("#SubBranchNo_02").flexselect();
			}
		}
	}
});