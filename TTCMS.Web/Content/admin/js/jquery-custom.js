﻿
function Ajax_pagination() {
    $(document).on("click", "#ajax_paging .pagination  a", function () {
        var url = $(this).attr("href");
        $.get(url).done(function (html) {
            $("#TableContainer").html(html);
        });
        return false;
    });
};
function LoadFunction(URLs) {
    var url = URLs + "?show=" + $('#ShowId').val() + "&&page=" + $('#Page').val() + "&&search=" + $('#Search').val();
    $.get(url).done(function (html) {
        $("#TableContainer").html(html);
    });
};
function SearchAjax(URLs) {
    
    var url = URLs + "?show=" + $('#ShowId').val() + "&&search=" + $('#Search').val();
    $.get(url).done(function (html) {
        $("#TableContainer").html(html);
    });
};
function LoadClickAjaxModal() {
    $('[data-toggle="modal"]').click(function () {
        var url = $(this).attr("href");
        $('#modal-show').load(url, function (result) {
            $('#modal-form').modal({
                show: true,
                backdrop: 'static',
                keyboard: false
            });
        });
    });
};
function CompleteAjaxRequest(result) {
    var returnObj = eval('(' + result.responseText + ')');
    $("#AjaxAlert").html("");
    if (returnObj.RedirectUrl != null && returnObj.Success == true) {
        $('#modal-form').modal('hide');
        $('#TableContainer').load(returnObj.RedirectUrl, function (resulturl) {
            AjaxAlert(returnObj.Code, returnObj.Msg);
        });
    }
    else {
        AjaxAlert(returnObj.Code, returnObj.Msg);
    }
};
function LoadClickAjaxModalJson() {
    $('[data-ajax="modal-json"]').click(function () {
        $("#AjaxAlert").html("");
        var url = $(this).attr("href");
        $.ajax({
            type: "GET",
            url: url,
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                if (result.RedirectUrl == null) {
                    if (result.Success == true) {
                        $("#modal-show").html(result.PartialViewHtml);
                        $('#modal-form').modal({
                            show: true,
                            backdrop: 'static',
                            keyboard: false
                        });
                    }
                    else {
                        AjaxAlert(result.Code, result.Msg);
                    }
                }
                else {
                    $('#TableContainer').load(result.RedirectUrl, function (result) {
                        AjaxAlert(result.Code, result.Msg);
                    });
                }
            },
            error: function (e) {
                alert(e.Message);
            }
        });
    });
};

function LoadButonClickAjaxModalJson() {
    $('[data-ajax="modal-json"]').click(function () {
        $("#AjaxAlert").html("");
        var url = $(this).attr("data-href");
        $.ajax({
            type: "GET",
            url: url,
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                if (result.RedirectUrl == null) {
                    if (result.Success == true) {
                        $("#modal-show").html(result.PartialViewHtml);
                        $('#modal-form').modal({
                            show: true,
                            backdrop: 'static',
                            keyboard: false
                        });
                    }
                    else {
                        AjaxAlert(result.Code, result.Msg);
                    }
                }
                else {
                    $('#TableContainer').load(result.RedirectUrl, function (result) {
                        AjaxAlert(result.Code, result.Msg);
                    });
                }
            },
            error: function (e) {
                alert(e.Message);
            }
        });
    });
};

function onSuccess(result) {
    AjaxAlert(result.Code, result.Msg);
    if (result.Code == "alert-success") {
        $("#item_" + result.Id).remove();
    }
};

function AjaxAlert(Code, Msg) {
    $.get("/TT_Admin/Base/AjaxAlert?code=" + Code + "&&msg=" + Msg).done(function (html) {
        $("#AjaxAlert").html(html);
    });
};

function show_confirm(obj) {
    var r = confirm(obj.attr('data-alert'));
    if (r == true)
        window.location = obj.attr('href');
}

function BrowseServer(onSelectActionFunction) {
    var finder = new CKFinder();
    //finder.basePath = '../';
    finder.selectActionFunction
        = typeof onSelectActionFunction === "function" ? onSelectActionFunction : SetFileField;
    finder.popup();
}

function SetFileField(fileUrl) {
    $("#Img_Thumbnail").val(fileUrl);
    document.getElementById('Image').src = fileUrl;
}

function initTinyMce(onKeyup, onInit) {
    tinymce.init({
        selector: '.tinymce-title',
        height: 160,
        menubar: false,
        plugins: [],
        toolbar: 'formatselect | bold italic forecolor',
        setup: function (editor) {
            editor.on('init', function (e) {
                typeof onInit === "function" && onInit();
            });
            editor.on('keyup', function (e) {
                typeof onKeyup === "function" && onKeyup(e);
            });
        }
    });
}


const SlugModule = (function () {

    function getSlugOnInput(value) {
        if (!value) return '';

        const lastChar = value[value.length - 1];
        const semiLastChar = value[value.length - 2];

        if (!_isSpaceOrDash(lastChar)) {
            return getSlug(value);
        }

        let result = getSlug(value.substring(0, value.length - 1));
        if (!_isSpaceOrDash(semiLastChar)) {
            result += ' ';
        }

        return result;
    }

    function _isSpaceOrDash(char) {
        return char === ' ' || char === '-';
    }

    function getSlug(title) {
        //Đổi chữ hoa thành chữ thường
        slug = title.toLowerCase();

        //Đổi ký tự có dấu thành không dấu
        slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
        slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
        slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
        slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
        slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
        slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
        slug = slug.replace(/đ/gi, 'd');
        //Xóa các ký tự đặt biệt
        slug = slug.replace(/\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|_/gi, '');
        //Đổi khoảng trắng thành ký tự gạch ngang
        slug = slug.replace(/ /gi, "-");
        //Đổi nhiều ký tự gạch ngang liên tiếp thành 1 ký tự gạch ngang
        //Phòng trường hợp người nhập vào quá nhiều ký tự trắng
        slug = slug.replace(/\-\-\-\-\-/gi, '-');
        slug = slug.replace(/\-\-\-\-/gi, '-');
        slug = slug.replace(/\-\-\-/gi, '-');
        slug = slug.replace(/\-\-/gi, '-');
        //Xóa các ký tự gạch ngang ở đầu và cuối
        slug = '@' + slug + '@';
        slug = slug.replace(/\@\-|\-\@|\@/gi, '');
        return slug;
    }

    return {
        getSlugOnInput,
        getSlug
    }
})()