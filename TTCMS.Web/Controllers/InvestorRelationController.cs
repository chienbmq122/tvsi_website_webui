﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using TTCMS.Service;
using TTCMS.Web.ViewModels;

namespace TTCMS.Web.Controllers
{
    public class InvestorRelationController : BaseController
    {
        private readonly IUATWebsiteDBService _uatWebsiteDbService;
        public InvestorRelationController(IUATWebsiteDBService uatWebsiteDbService)
        {
            _uatWebsiteDbService = uatWebsiteDbService;
        }

        // GET: InvestorRelation
        public async Task<ActionResult> FinancialReport(int page = 0)
        {
            ViewBag.Meta = Config;
            ViewBag.Title = Resources.Resources.FinancialReport;
            var languageCode = Cache.GetLang();
            var categoryId = languageCode == "vi-VN" ? 58 : 66;
            var _uatLanguageId = languageCode == "vi-VN" ? 1 : 2;
            var data = await _uatWebsiteDbService.GetByCategory(categoryId, _uatLanguageId, 10, page);
            var totalNumberOfRecords = await _uatWebsiteDbService.GetTotalNumberOfRecords(categoryId, _uatLanguageId);
            var model = new InvestorRelationViewModel()
            {
                Data = data.ToList(),
                TotalNumberOfPage = (int)Math.Ceiling((decimal)(totalNumberOfRecords) / 10),
                CurrentPage = page
            };
            return View(model);
        }

        public async Task<ActionResult> PublicInformation(int page = 0)
        {
            ViewBag.Meta = Config;
            ViewBag.Title = Resources.Resources.FinancialReport;
            var languageCode = Cache.GetLang();
            //var languageId = languageCode != null && languageCode != "" ? _languageService.GetByCode(languageCode).Id : 1;
            //var categoryId = languageId == 1 ? 58 : 66;
            var languageId = 1;
            var categoryId = 59;
            var data = await _uatWebsiteDbService.GetByCategory(categoryId, languageId, 10, page);
            var totalNumberOfRecords = await _uatWebsiteDbService.GetTotalNumberOfRecords(categoryId, languageId);
            var model = new InvestorRelationViewModel()
            {
                Data = data.ToList(),
                TotalNumberOfPage = (int)Math.Ceiling((decimal)(totalNumberOfRecords) / 10),
                CurrentPage = page
            };
            return View(model);
        }

        public async Task<ActionResult> EnterpriseManagement()
        {
            ViewBag.Meta = Config;
            ViewBag.Title = Resources.Resources.EnterpriseManagement;
            var languageCode = Cache.GetLang();
            var categoryId = languageCode == "vi-VN" ? 60 : 68;
            var _uatLanguageId = languageCode == "vi-VN" ? 1 : 2;
            var data = await _uatWebsiteDbService.GetByCategory(categoryId, _uatLanguageId, 10, 0);
            var model = new EnterpriseManagementViewModel()
            {
                Data = data.FirstOrDefault()
            };
            return View(model);
        }

        public async Task<ActionResult> GeneralStakeholderMeeting()
        {
            ViewBag.Meta = Config;
            ViewBag.Title = Resources.Resources.GeneralStakeholderMeeting;
            var languageCode = Cache.GetLang();
            var categoryId = languageCode == "vi-VN" ? 61 : 69;
            var _uatLanguageId = languageCode == "vi-VN" ? 1 : 2;
            var data = await _uatWebsiteDbService.GetByCategory(categoryId, _uatLanguageId, 10, 0);
            var model = new EnterpriseManagementViewModel()
            {
                Data = data.FirstOrDefault(),
            };
            return View(model);
        }

        public async Task<ActionResult> AnnualReport(int page = 0)
        {
            ViewBag.Meta = Config;
            ViewBag.Title = Resources.Resources.AnnualReport;
            var languageCode = Cache.GetLang();
            var _uatLanguageId = languageCode == "vi-VN" ? 1 : 2;
            var categoryId = languageCode == "vi-VN" ? 62 : 70;
            var data = await _uatWebsiteDbService.GetByCategory(categoryId, _uatLanguageId, 6, page);
            var totalNumberOfRecords = await _uatWebsiteDbService.GetTotalNumberOfRecords(categoryId, _uatLanguageId);
            var model = new InvestorRelationViewModel()
            {
                Data = data.ToList(),
                TotalNumberOfPage = (int)Math.Ceiling((decimal)(totalNumberOfRecords) / 6),
                CurrentPage = page
            };
            return View(model);
        }
    }
}