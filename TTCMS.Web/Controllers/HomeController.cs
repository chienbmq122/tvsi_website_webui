﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TTCMS.Service;
using TTCMS.Web.Helper;
using TTCMS.Web.ViewModels;
using System.Threading.Tasks;
using System.Web.Http.Cors;

namespace TTCMS.Web.Controllers
{
    public class HomeController : BaseController
    {
        private readonly ICategoryService _category;
        private readonly ISiteSettingService siteSettingService;
        private readonly IFAQService FAQService;
        private readonly ILangugage_FAQService langugage_FAQService;
        private readonly IQuestionService _questionService;
        private readonly ILanguage_QuestionService _language_QuestionService;
        private readonly INewsItemService newsItemService;
        private readonly INewsService newsService;
        private readonly ILanguageService languageService;
        private readonly ITVSIWebsiteNewsService _tvsiWebsiteNewsService;
        private readonly string[] dummyNewsImages = {
            "/Content/upload/files/news-1.jpg",
            "/Content/upload/files/news-2.jpg",
            "/Content/upload/files/news-3.jpeg",
            "/Content/upload/files/news-4.jpg",
            "/Content/upload/files/report-1.jpg",
            "/Content/upload/files/report-2.jpg",
            "/Content/upload/files/report-3.jpg",
            "/Content/upload/files/report-4.jpg",
            "/Content/upload/files/tthd-bg.png",
            "/Content/upload/files/work-1.jpg",
            "/Content/upload/files/work-2.jpg",
            "/Content/upload/files/meeting-1.jpg",
            "/Content/upload/files/meeting-2.jpg"
        };
        private readonly List<FAQObject> dummyFAQsEn = new List<FAQObject>();

        public HomeController(ICategoryService _category,
            ISiteSettingService siteSettingService, IFAQService fAQService, ILangugage_FAQService langugage_FAQService,
            INewsItemService newsItemService, INewsService newsService, ILanguageService languageService, ITVSIWebsiteNewsService tvsiWebsiteNewsService,
            IQuestionService questionService, ILanguage_QuestionService language_QuestionService)
        {
            this._category = _category;
            this.siteSettingService = siteSettingService;
            this.FAQService = fAQService;
            this.langugage_FAQService = langugage_FAQService;
            this.newsItemService = newsItemService;
            this.newsService = newsService;
            this.languageService = languageService;
            _tvsiWebsiteNewsService = tvsiWebsiteNewsService;
            _questionService = questionService;
            dummyFAQsEn.Add(new FAQObject() { Question = "Which system is available for customers to trade online?", Answer = "<p>At TVSI, you can online trade through: webtrading iTrade Home, TVSI Mobile online trading application, and iTrade Pro online trading application.</p>" });
            dummyFAQsEn.Add(new FAQObject() { Question = "When can I place an off-market order for the next trading day?", Answer = "<p>TVSI Mobile system allows placing off-market orders for the next trading day from 15:30 of the current trading day.</p>" });
            dummyFAQsEn.Add(new FAQObject() { Question = "How to open a margin trading account?", Answer = @"<p>To register for a margin account, you can choose 1 of 3 ways:</p>
                        <p><strong>Option 1</strong>: You can arrive at TVSI headquarters or branches for support.</p>
                        <p><strong>Option 2</strong>: You can complete the account opening contract through the account manager.</p>
                        <p><strong>Option 3</strong>: You can log in to the TVSI iTrade-Mobile App and follow the instructions.</p>
                        Note: Please complete if the account has a net asset of over 500 million or uses a deposit of over VND 1 billion to complete the contract." });
            dummyFAQsEn.Add(new FAQObject() { Question = "How to check order history?", Answer = @"<p>To look up the order history, you can choose one of the following ways:</p>
                <p><strong>Option 1</strong>: Access website <a href=""https://itrade-home.tvsi.com.vn/"">https://itrade-home.tvsi.com.vn/</a> and go to Portfolio Management/Order History</p>
                <p><strong>Option 2</strong>: Access TVSI mobile application section Trading - Order history</p>
                <p><strong>Option 3</strong>: Contact the hotline 19001885 for support</p>
            " });
            _language_QuestionService = language_QuestionService;
        }

        // Starting page
        public async Task<ActionResult> Index()
        {
            ViewBag.Meta = Config;
            ViewBag.Title = Config.Title;
            var languageCode = Cache.GetLang();
            var languageId = languageCode != null && languageCode != "" ? languageService.GetByCode(languageCode).Id : 2;
            var model = HomeHelper.GetHomeViewModel(languageId, siteSettingService, languageService);
            model.FAQs = new List<FAQObject>();
            var faq_questions = _questionService
                .GetList()
                .Where(x => x.IsWebsiteShown && x.DeletedAt == null)
                .OrderByDescending(x => x.CreatedDate);
            if (faq_questions.ToList().Count() > 4) {
                foreach (var item in faq_questions)
                {
                    if (model.FAQs.Count >= 4) break;
                    var faqItem = _language_QuestionService.GetById(languageCode, item.Id);
                    if (faqItem != null)
                    {
                        model.FAQs.Add(new FAQObject { Question = faqItem.QuestionName, Answer = faqItem.ContentBrief });
                    }
                }
            } else
            {
                var faqs = FAQService.GetListForActive();
                if (languageCode == "vi-VN")
                {
                    foreach (var item in faqs)
                    {
                        if (model.FAQs.Count >= 4) break;
                        var faqItem = langugage_FAQService.GetById(item.Id, languageId).FirstOrDefault();
                        if (faqItem != null && model.FAQs.Count < 4)
                        {
                            model.FAQs.Add(new FAQObject { Question = faqItem.Question, Answer = faqItem.Answer });
                        }
                    }
                }
                else
                {
                    model.FAQs = dummyFAQsEn;
                }
            }
            // var faqs = FAQService.GetListForActive();
        
            
            var news = await _tvsiWebsiteNewsService.GetHotest(new List<int>(), languageId, 29);
            var hotest = new List<NewsInfo>();
            foreach (var item in news)
            {
                var category = "";
                switch (item.GroupID)
                {
                    case 29:
                        category = Resources.Resources.TVSINews;
                        break;
                    case 30:
                        category = Resources.Resources.InternalActivity;
                        break;
                    case 35:
                        category = Resources.Resources.TVSIWithPress;
                        break;
                    case 36:
                        category = Resources.Resources.TVSIForCommunity;
                        break;
                    default:
                        category = Resources.Resources.TVSINews;
                        break;
                }
                hotest.Add(new NewsInfo
                {
                    Title = item.Title,
                    ImageUrl = item.ImageUrl != null ? $"https://fp.tvsi.com.vn//NewsImages/{item.ImageUrl}.jpg" : dummyNewsImages[item.NewsID % dummyNewsImages.Length],
                    Slug = item.NewsID.ToString(),
                    CreatedDate = item.CreatedDate,
                    category = category
                });
            }
            model.HotestNews = hotest;
            model.InvestmentFormItems = model.InvestmentFormItems.Where(i => i.ImageUrl != null &&
            i.ImageUrl.Value != null && i.ImageUrl.Value != "").ToList();
            return View(model);
        }
    
        public ActionResult Margin()
        {
            return View();
        }
    }
}