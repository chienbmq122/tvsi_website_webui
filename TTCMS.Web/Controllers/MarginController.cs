﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TTCMS.Core.Infrastructure.Alerts;
using TTCMS.Web.ViewModels;
using Dapper;
using TTCMS.Service;
using System.Threading.Tasks;

namespace TTCMS.Web.Controllers
{
    public class MarginController : BaseController
    {
        private readonly IEkycService _ekycService;
        public MarginController(IEkycService ekycService)
        {
            _ekycService = ekycService;
        }
        // GET: Margin
        [HttpGet]
        public ActionResult Index()
        {
            var viewModel = new MarginViewModel
            {
            };
            return View(viewModel);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Index(MarginViewModel model)
        {
            try
            {
                //SqlParameter para_hoten = new SqlParameter("@CustName", model.CustName);
                //SqlParameter para_email = new SqlParameter("@Email", model.Email);
                //SqlParameter para_sdt = new SqlParameter("@Mobile", model.Mobile);
                //SqlParameter para_yeucau = new SqlParameter("@Content", model.Content);
                var result = await _ekycService.RegisterConsultant(model.CustName, model.Mobile, model.Email, model.Description);
                ViewBag.Success = result;
            }
            catch (Exception ex)
            {
                HandleException("Message ", ex);
                ViewBag.Success = false;
            }
            return View(model);
        }
    }
}