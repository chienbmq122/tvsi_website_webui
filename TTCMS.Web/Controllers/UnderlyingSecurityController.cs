﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TTCMS.Web.Controllers
{
    public class UnderlyingSecurityController : BaseController
    {
        // GET: UnderlyingSecurity
        public ActionResult UnderlyingSecurityIndex()
        {
            ViewBag.Meta = Config;
            ViewBag.Title = Resources.Resources.UnderlyingSecurity;
            return View();
        }
    }
}