﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TTCMS.Core.Infrastructure.Alerts;
using TTCMS.Service;
using TTCMS.Web.Areas.TT_Admin.Models;

namespace TTCMS.Web.Controllers
{
    [RoutePrefix("Category")]
    public class CategoryController : BaseController
    {
        private readonly ISinglePageService singlePageService;
        private readonly ILanguage_SinglePageService language_SinglePageService;
        public CategoryController(ISinglePageService singlePageService, ILanguage_SinglePageService language_SinglePageService)
        {
            this.singlePageService = singlePageService;
            this.language_SinglePageService = language_SinglePageService;
        }

        // GET: SinglePage
        public ActionResult Index(string slug)
        {
            var languageCode = Cache.GetLang();
            var a = language_SinglePageService.GetByRoute(slug).ToList();
            var model = language_SinglePageService.GetByRoute(slug)?.Where(s => s.Language.Code == languageCode)?.FirstOrDefault();
            var viewModel = new Language_SinglePageViewModel();

            if (model != null)
            {
                Mapper.Map(model, viewModel);
            }

            ViewBag.Meta = Config;
            ViewBag.Title = model?.Title;
            return View(viewModel);
        }
    }
}