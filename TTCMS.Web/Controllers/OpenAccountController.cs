﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Dapper;
using log4net;
using TTCMS.Data.Entities;
using TTCMS.Data.Repositories;
using TTCMS.Service;
using TTCMS.Service.Common;
using TTCMS.Web.Models.OpenAccount;
using TTCMS.Web.ViewModels;
using TTCMS.Web.ViewModels.OpenAccount;

namespace TTCMS.Web.Controllers
{
    public class OpenAccountController : BaseController
    {
        private readonly ILog log = LogManager.GetLogger(typeof(OpenAccountController));
        private IBankService _bankService;
        private IOpenAccountService _accountService;
        private IEkycService _ekycService;

        public OpenAccountController(IBankService bankService,
            IOpenAccountService openAccountService, IEkycService ekycService)
        {
            _bankService = bankService;
            _accountService = openAccountService;
            _ekycService = ekycService;
        }

        // GET: OpenAccount
        public async Task<ActionResult> ConfirmIdentityIndex()
        {
            ViewBag.Meta = Config;
            ViewBag.Title = Resources.Resources.eKYC;
            return View();
        }

        [HttpPost]
        public ActionResult ConfirmIdentityIndex(string id)
        {
            ViewBag.Meta = Config;
            ViewBag.Title = Resources.Resources.eKYC;
            return RedirectToAction("ConfirmIdentityIndex");
        }

        [HttpGet]
        public async Task<ActionResult> RegisterOpenAccountEKYC()
        {
            await GetCommonInfo();
            var viewModel = new OpenAccountViewModel();
            if (Session["check_canhan_ekyc"] != null && Session["email_ekyc"] != null)
            {
                ViewBag.success = Session["check_canhan_ekyc"];
                ViewBag.email = Session["email_ekyc"];
                Session["check_canhan_ekyc"] = null;
                Session["email_ekyc"] = null;
            }
            else
            {
                ViewBag.email = null;
                ViewBag.success = false;
            }
            viewModel.TradeMethodPhone = true;
            viewModel.TradeMethodInternet = true;
            viewModel.TradeResultEmail = true;
            viewModel.TradeResultSMS = true;
            viewModel.RegistChannelInfo = true;
            viewModel.BankTransfer = true;
            viewModel.RegistChannelDirect = true;
            viewModel.RegistItradeHome = true;
            viewModel.MonthlyReportEMail = true;
            return View(viewModel);
        }


        public async Task<ActionResult> RegisterOpenAccountEKYC(OpenAccountViewModel model)
        {
            try
            {
                await GetCommonInfo(model.BankNo_01, model.BankNo_02);
                var confirmOtp = DateTime.Now;
                if (string.IsNullOrEmpty(model.sale_id))
                {
                    var provinceCode = !string.IsNullOrEmpty(model.ProvinceValue) ? model.ProvinceValue : "01";
                    model.sale_id = await _ekycService.ProcessGenSaleID(provinceCode, model.QA1txt);
                }

                model.noicap = !string.IsNullOrEmpty(await _ekycService.CutPlaceIssueCardID(model.noicap))
                    ? await _ekycService.CutPlaceIssueCardID(model.noicap)
                    : model.noicap;

                if (!string.IsNullOrEmpty(model.AddressDetail))
                    model.diachi += model.AddressDetail + ", ";
                if (!string.IsNullOrEmpty(model.Ward))
                    model.diachi += model.Ward + ", ";
                if (!string.IsNullOrEmpty(model.Distric))
                    model.diachi += model.Distric + ", ";
                if (!string.IsNullOrEmpty(model.Province))
                    model.diachi += model.Province;
                var manName = string.Empty;
                var postionName = string.Empty;
                var manNo = string.Empty;
                DateTime? manDate = null;
                var manCardID = string.Empty;
                var manIssuePlace = string.Empty;
                DateTime? manIssueDate = null;
                var isValid = true;

                var errors = await _ekycService.ValidBank(model.BankAccNo_01, model.BankAccNo_02, model.BankNo_01,
                    model.BankNo_02, model.BankAccName_01, model.BankAccName_02, model.SubBranchNo_01,
                    model.SubBranchNo_02);
                if (errors.Any())
                {
                    foreach (string errMsg in errors)
                    {
                        ModelState.AddModelError("", errMsg);
                    }

                    isValid = false;
                }

                if (!isValid)
                {
                    await GetCommonInfo(model.BankNo_01, model.BankNo_02);
                    return View(model);
                }

                if (!await _ekycService.IsValidDatetime(model.ngaysinh))
                {
                    ViewBag.Message = "Ngày sinh không đúng, Quý khác vui lòng nhập lại.";
                    return View(model);
                }

                if (!await _ekycService.IsValidDatetime(model.ngaycapcmnd))
                {
                    ViewBag.Message = "Ngày cấp CMT/CCCD không đúng, Quý khác vui lòng nhập lại.";
                    return View(model);
                }

                if (!await _ekycService.IsValidEmail(model.email))
                {
                    ViewBag.Message = "Email không đúng,Quý khác vui lòng nhập lại.";
                    return View(model);
                }

                var ngaysinh = DateTime.ParseExact(model.ngaysinh, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                var ngaycapcmnd =
                    DateTime.ParseExact(model.ngaycapcmnd, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                var validDateOfBirth = await _ekycService.ValidDateOfBirth(ngaysinh);

                if (!string.IsNullOrEmpty(validDateOfBirth))
                {
                    ViewBag.Message = validDateOfBirth;
                    return View(model);
                }

                if (!string.IsNullOrEmpty(model.sale_id))
                {
                    if (!await _ekycService.CheckSaleInfo(model.sale_id))
                    {
                        ViewBag.Message = "Mã nhân viên tư vấn không đúng, Quý khác vui lòng nhập đúng hoặc để trống";
                        return View(model);
                    }
                }

                if (!await _ekycService.CheckCardID(model.cmnd))
                {
                    ViewBag.Message = "CMT/CCCD đã tồn tại.";
                    return View(model);
                }

                var saleid = string.Empty;
                string paramSaleID = Request.QueryString["SaleID"];

                if (!string.IsNullOrEmpty(paramSaleID))
                {
                    model.sale_id = paramSaleID;
                    saleid = await _ekycService.GetSaleID(paramSaleID);
                }
                else
                    saleid = !string.IsNullOrEmpty(model.sale_id)
                        ? await _ekycService.GetSaleID(model.sale_id)
                        : model.Branch_Transaction;

                if (!string.IsNullOrEmpty(model.BankNo_01))
                {
                    model.BankName_01 = await _ekycService.GetBankName(model.BankNo_01);
                    model.SubBranchName_01 = await _ekycService.GetSubBranchName(model.BankNo_01,
                        model.SubBranchNo_01);
                }

                if (!string.IsNullOrEmpty(model.BankNo_02))
                {
                    model.BankName_02 = await _ekycService.GetBankName(model.BankNo_01);
                    model.SubBranchName_02 = await _ekycService.GetSubBranchName(model.BankNo_02,
                        model.SubBranchNo_02);
                }

                if (model.CustomerRelaUsa == 0)
                {
                    model.CustomerUSA = 0;
                    model.PlaceOfBirthUSA = 0;
                    model.DepositoryUSA = 0;
                    model.CustomerPhoneNumberUSA = 0;
                    model.CustomerTransferBankUSA = 0;
                    model.CustomerAuthorizationUSA = 0;
                    model.CustomerReceiveMailUSA = 0;
                }

                var mansql = await _ekycService.GetManInfo();
                var manModel = new ManDetail();
                ManInfo maninfo = null;

                foreach (var item in mansql)
                {
                    if (!string.IsNullOrEmpty(item.ma_chi_nhanh) &&
                        item.ma_chi_nhanh.Contains(await _ekycService.GetBranchID(saleid.Substring(0, 4))))
                    {
                        maninfo = item;
                        break;
                    }
                }

                if (maninfo != null)
                {
                    maninfo = mansql.FirstOrDefault(x => x.ma_chi_nhanh == "All");
                }
                if (maninfo != null)
                {
                    var arrPara = maninfo.gia_tri.Split('|');
                    if (!string.IsNullOrEmpty(arrPara[0]))
                        manName = arrPara[0];
                    if (!string.IsNullOrEmpty(arrPara[1]))
                        postionName = arrPara[1];
                    if (!string.IsNullOrEmpty(arrPara[2]))
                        manNo = arrPara[2];
                    if (!string.IsNullOrEmpty(arrPara[3]))
                        manDate = DateTime.ParseExact(arrPara[3],
                            "dd/MM/yyyy", CultureInfo.CurrentCulture);
                    if (!string.IsNullOrEmpty(arrPara[4]))
                        manCardID = arrPara[4];
                    if (!string.IsNullOrEmpty(arrPara[5]))
                        manIssueDate = DateTime.ParseExact(arrPara[5],
                            "dd/MM/yyyy", CultureInfo.CurrentCulture);
                    if (!string.IsNullOrEmpty(arrPara[6]))
                        manIssuePlace = arrPara[6];
                }
                var transferType = "";
                if (!string.IsNullOrEmpty(model.BankAccName_01) || !string.IsNullOrEmpty(model.BankAccName_02))
                {
                    transferType += "1";
                }

                var confirmCode = await _ekycService.EncriptMd5(model.email + await _ekycService.RandomNumber(6));
                if (confirmCode.Length > 50)
                    confirmCode = confirmCode.Substring(0, 50);

                var confirmSms = await _ekycService.RandomNumber(6);
                var regisID = 0;
                string crm = ConfigurationManager.ConnectionStrings["TTCRMEntities"].ToString();
                using (var conn = new SqlConnection(crm))
                {
                    var parameter = new DynamicParameters();
                    parameter.Add("@hoten", model.hoten);
                    parameter.Add("@ngaysinh", ngaysinh);
                    parameter.Add("@gioitinh", model.gioitinh);
                    parameter.Add("@cmnd", model.cmnd);
                    parameter.Add("@ngaycapcmnd", ngaycapcmnd);
                    parameter.Add("@noicap", model.noicap);
                    parameter.Add("@sdt", model.Phone_01);
                    parameter.Add("@email", model.email);
                    parameter.Add("@diachi", model.diachi);
                    parameter.Add("@saleid", saleid);
                    parameter.Add("@branch_id", await _ekycService.GetBranchID(saleid.Substring(0, 4)));
                    parameter.Add("@branch_name", await _ekycService.GetBranchName(saleid.Substring(0, 4)));
                    parameter.Add("@tvsi_nguoi_dai_dien", manName);
                    parameter.Add("@tvsi_chuc_vu", postionName);
                    parameter.Add("@tvsi_giay_uq", manNo);
                    parameter.Add("@tvsi_ngay_uq", manDate);
                    parameter.Add("@yeucau", model.Note ?? "");
                    parameter.Add("@phuong_thuc_gd", model.TradeMethod);
                    parameter.Add("@phuong_thuc_nhan_kqgd", model.TradeResult);
                    parameter.Add("@phuong_thuc_sao_ke", model.MonthlyReport);
                    parameter.Add("@dv_chuyen_tien", transferType);
                    parameter.Add("@so_dien_thoai_02", model.Phone_02);
                    parameter.Add("@chu_tai_khoan_nh_01", model.BankAccName_01);
                    parameter.Add("@so_tai_khoan_nh_01", model.BankAccNo_01);
                    parameter.Add("@ma_ngan_hang_01", model.BankNo_01);
                    parameter.Add("@ngan_hang_01", model.BankName_01);
                    parameter.Add("@chi_nhanh_nh_01", model.SubBranchName_01);
                    parameter.Add("@ma_chi_nhanh_01", model.SubBranchNo_01);
                    parameter.Add("@tinh_tp_nh_01", model.City_01);
                    parameter.Add("@chu_tai_khoan_nh_02", model.BankAccName_02);
                    parameter.Add("@so_tai_khoan_nh_02", model.BankAccNo_02);
                    parameter.Add("@ngan_hang_02", model.BankName_02);
                    parameter.Add("@ma_ngan_hang_02", model.BankNo_02);
                    parameter.Add("@ma_chi_nhanh_02", model.SubBranchNo_02);
                    parameter.Add("@chi_nhanh_nh_02", model.SubBranchName_02);
                    parameter.Add("@tinh_tp_nh_02", model.City_02);
                    parameter.Add("@kenh_trao_doi", model.RegistChannelInfo);
                    parameter.Add("@phuong_thuc_kenh_td", model.ChannelInfoMethod);
                    parameter.Add("@confirmCode", confirmCode);
                    parameter.Add("@confirmSms", confirmSms);
                    parameter.Add("@fatca_kh_usa", model.CustomerUSA);
                    parameter.Add("@fatca_noi_sinh_usa", model.PlaceOfBirthUSA);
                    parameter.Add("@fatca_noi_cu_tru_usa", model.DepositoryUSA);
                    parameter.Add("@fatca_sdt_usa", model.CustomerPhoneNumberUSA);
                    parameter.Add("@fatca_lenh_ck_usa", model.CustomerTransferBankUSA);
                    parameter.Add("@fatca_giay_uy_quyen_usa", model.CustomerAuthorizationUSA);
                    parameter.Add("@fatca_dia_chi_nhan_thu_usa", model.CustomerReceiveMailUSA);
                    parameter.Add("@marginAccount", model.MarginAccount);
                    parameter.Add("@confirmOTP", confirmOtp);
                    parameter.Add("@manCardID", manCardID);
                    parameter.Add("@manIssuePlace", manIssuePlace);
                    parameter.Add("@manIssueDate", manIssueDate);
                    parameter.Add("@dangkyid", dbType: DbType.Int32, direction: ParameterDirection.Output);
                    await conn.ExecuteAsync("TVSI_sDANG_KY_MO_TAI_KHOAN_EKYC", parameter,
                        commandType: CommandType.StoredProcedure);
                    regisID = parameter.Get<int>("@dangkyid");
                }

                if (regisID > 0)
                {
                    var path = Request.Url.GetLeftPart(UriPartial.Path).Replace(Request.Url.LocalPath, Utils.Path);
                    var sendMailCustomer = await _ekycService.SendEmailOpenAccount(model.hoten, model.email, confirmCode,
                        confirmSms, path);
                    if (!sendMailCustomer)
                    {
                        log.Error("loi gui mail sendMailCustomer");
                    }
                    if (!string.IsNullOrEmpty(model.sale_id))
                    {
                        var emailSubject = Utils.EmailSubject + "Đăng ký mở tài khoản trực tuyến" + "";
                        var saleInfo = await _ekycService.GetEmailSale(model.sale_id.Substring(0, 4));
                        if (saleid != null)
                        {
                            var department = await _ekycService.GetBranchName(saleid.Substring(0, 4));
                            var sendMailSale = await _ekycService.SendEmailSaleEKYC(saleInfo.SaleName, model.hoten, model.ngaysinh,
                                model.cmnd
                                , model.ngaycapcmnd, model.Phone_01, model.email, model.diachi, model.QA1txt
                                , model.Note ?? model.yeucau, "eKYC Web", department, emailSubject, saleInfo.Email);
                            if (!sendMailSale)
                            {
                                log.Error("loi gui mail sendMailSale");
                            }
                        }

                        if (!Directory.Exists(Server.MapPath(Utils.FolderKYC + regisID + "")))
                            Directory.CreateDirectory(Server.MapPath(Utils.FolderKYC + regisID + "/"));

                        var fullpath1 = Server.MapPath(Utils.FolderKYC + regisID + "/");
                        var sourceImageF = Session["imagefront"];
                        var sourceImageB = Session["imageback"];
                        var sourceImageFace = Session["path_image_face"];
                        System.IO.File.Copy(Convert.ToString(sourceImageF),
                            fullpath1 + regisID + Utils.Front + ".jpg");
                        System.IO.File.Copy(Convert.ToString(sourceImageB), fullpath1 + regisID + Utils.Back + ".jpg");
                        System.IO.File.Copy(Convert.ToString(sourceImageFace),
                            fullpath1 + regisID + Utils.Face + ".jpg");
                        string pathPicFront = regisID + Utils.Front + ".jpg";
                        string pathPicBack = regisID + Utils.Back + ".jpg";
                        string pathPicFace = regisID + Utils.Face + ".jpg";
                        var updatePathEKYC =
                            await _ekycService.UpdateImageEKYC(regisID, pathPicFront, pathPicBack, pathPicFace);
                        if (!updatePathEKYC)
                        {
                            ViewBag.Message = "Có lỗi xảy ra vui lòng thao tác lại từ đầu.";
                            return View(model);
                        }

                        await _ekycService.DeleteFileEKYC(Convert.ToString(sourceImageF));
                        await _ekycService.DeleteFileEKYC(Convert.ToString(sourceImageB));
                        await _ekycService.DeleteFileEKYC(Convert.ToString(sourceImageFace));
                        Session["check_canhan_ekyc"] = true;
                        Session["email_ekyc"] = model.email;
                        return RedirectToAction("RegisterOpenAccountEKYC", "OpenAccount");
                    }
                }
                ViewBag.Message = "Có lỗi xảy ra vui lòng thao tác lại từ đầu.";
                return View(model);
            }
            catch (Exception ex)
            {
                log.Error( string.Format("loi method RegisterOpenAccountEKYC - {0}", ex.Message));
                log.Error(string.Format("loi method RegisterOpenAccountEKYC - {0}", ex.InnerException));
                Session["check_canhan_ekyc"] = false;
                ViewBag.Message = "Đăng ký tài khoản không thành công";
                return View(model);
            }
        }

        public async Task<ActionResult> GenerateOTP(string phone)
        {
            try
            {
                Session[EKYCType.GenOTP] = await _ekycService.EncriptMd5(phone + await _ekycService.RandomNumber(6));
                var otp = await _ekycService.GenerateTOTP(Convert.ToString(Session[EKYCType.GenOTP]), DateTime.Now, 120,
                    6);
                if (!string.IsNullOrEmpty(otp))
                {
                    var smsMessage = ConfigSettings.ReadSetting("SMSMessage");
                    if (await _ekycService.QueueSMSOTP(phone, otp + smsMessage, 0))
                        return Json(new ResultDataViewModel<object>()
                        {
                            Status = true,
                            Data = otp,
                            Type = 0
                        }, JsonRequestBehavior.AllowGet);
                }

                return Json(new ResultViewModel()
                {
                    Status = false,
                    Message = Resources.Resources.ActonEKYCError,
                    Type = 0
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                log.Error(string.Format("Loi METHOD GenerateOTP - {0}", e.Message));
                log.Error(string.Format("Loi METHOD GenerateOTP - {0}", e.InnerException));
                return Json(new ResultViewModel()
                {
                    Status = false,
                    Message = Resources.Resources.ActonEKYCError,
                    Type = 0
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public async Task<ActionResult> ConfirmOTP(string otp)
        {
            try
            {
                var otpCompare = await _ekycService.VerifyTOTP(otp, DateTime.Now,
                    Convert.ToString(Session[EKYCType.GenOTP]), 120, 6);
                if (otpCompare)
                {
                    return Json(new ResultDataViewModel<object>()
                    {
                        Status = true,
                        Data = true,
                        Type = 0
                    }, JsonRequestBehavior.AllowGet);
                }

                return Json(new ResultViewModel()
                {
                    Status = false,
                    Message = Resources.Resources.ActonEKYCError,
                    Type = 0
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                log.Error(string.Format("Loi METHOD ConfirmOTP - {0}", e.Message));
                log.Error(string.Format("Loi METHOD ConfirmOTP - {0}", e.InnerException));
                return Json(new ResultViewModel()
                {
                    Status = false,
                    Message = Resources.Resources.ActonEKYCError,
                    Type = 0
                }, JsonRequestBehavior.AllowGet);
            }
        }


        public async Task<ActionResult> ResendOTP(string phone)
        {
            try
            {
                if (!string.IsNullOrEmpty(phone))
                {
                    Session[EKYCType.GenOTP] =
                        await _ekycService.EncriptMd5(phone + await _ekycService.RandomNumber(6));
                    var otpCode = await _ekycService.GenerateTOTP(Convert.ToString(Session[EKYCType.GenOTP]),
                        DateTime.Now,
                        120, 6);
                    if (otpCode != null)
                    {
                        string smsMessage = ConfigurationManager.AppSettings["SMSMessage"];
                        if (await _ekycService.QueueSMSOTP(phone, otpCode + smsMessage, 0))
                            return Json(new ResultDataViewModel<object>()
                            {
                                Status = true,
                                Data = otpCode,
                                Type = 0
                            }, JsonRequestBehavior.AllowGet);
                    }
                }

                return Json(new ResultViewModel()
                {
                    Status = false,
                    Message = Resources.Resources.ActonEKYCError,
                    Type = 0
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                log.Error(string.Format("Loi METHOD ResendOTP - {0}", e.Message));
                log.Error(string.Format("Loi METHOD ResendOTP - {0}", e.InnerException));
                return Json(new ResultViewModel()
                {
                    Status = false,
                    Message = Resources.Resources.ActonEKYCError,
                    Type = 0
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ConfirmFaceIndex()
        {
            ViewBag.Meta = Config;
            ViewBag.Title = Resources.Resources.eKYC;
            return View();
        }

        public ActionResult ConfirmFaceAndIdentityIndex()
        {
            ViewBag.Meta = Config;
            ViewBag.Title = Resources.Resources.eKYC;
            return View();
        }

        public ActionResult PersonalInforIndex()
        {
            ViewBag.Meta = Config;
            ViewBag.Title = Resources.Resources.eKYC;
            return View();
        }

        public ActionResult OTPConfirmIndex()
        {
            ViewBag.Meta = Config;
            ViewBag.Title = Resources.Resources.eKYC;
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> GetSaleName(string saleID)
        {
            if (!string.IsNullOrEmpty(saleID))
            {
                var data = await _accountService.GetSaleName(saleID);
                if (!string.IsNullOrEmpty(data))
                {
                    return Json(new {status = true, data = data}, JsonRequestBehavior.AllowGet);
                }
            }

            return Json(new {status = false}, JsonRequestBehavior.DenyGet);
        }

        [HttpPost]
        public async Task<ActionResult> UploadImageEKYC(string front, string back)
        {
            try
            {
                var byteFront = Convert.FromBase64String(front);
                var byteBack = Convert.FromBase64String(back);

                var textRandom = Guid.NewGuid().ToString().Substring(0, 8) + ".png";
                if (!Directory.Exists(Server.MapPath("~/Ekyc/CMND/")))
                    Directory.CreateDirectory(Server.MapPath("~/Ekyc/CMND/"));

                var fullPathFront = Server.MapPath("~/Ekyc/CMND/webcam_front_") + textRandom;
                var fullPathBack = Server.MapPath("~/Ekyc/CMND/webcam_back_") + textRandom;

                System.IO.File.WriteAllBytes(fullPathFront, byteFront);
                System.IO.File.WriteAllBytes(fullPathBack, byteBack);

                log.Info(string.Format("FullPathFront --- {0}", fullPathFront));
                log.Info(string.Format("FullPathBack --- {0}", fullPathBack));

                // Upload CMND mat truoc
                var hashDetailFront = await _ekycService.GetHashImage(fullPathFront);
                var hashDetailBack = await _ekycService.GetHashImage(fullPathBack);

                var hashImageFront = hashDetailFront.hash;
                var hashImageBack = hashDetailBack.hash;
                Session[EKYCType.TokenID] = hashDetailFront.tokenId;
                Session[EKYCType.ClientSession] = hashDetailFront.hash;
                if (!string.IsNullOrEmpty(hashImageFront) && !string.IsNullOrEmpty(hashImageBack))
                {
                    Session[EKYCType.HashFrontImage] = hashImageFront;
                    log.InfoFormat("upload CMT/CCCD mat truoc --- {0}", hashImageFront);
                    log.InfoFormat("upload CMT/CCCD mat sau ---- {0}", hashImageBack);

                    var checkCardLiveness = await _ekycService.CheckCardLiveness(hashImageFront, hashDetailFront.hash);

                    log.Info(string.Format("check card liveness  --- {0}", checkCardLiveness));

                    if (checkCardLiveness.liveness.Equals("success") && !checkCardLiveness.fake_liveness)
                    {
                        var cardType = await _ekycService.CheckClassifyId(hashImageFront, hashImageFront,
                            hashDetailFront.tokenId);
                        log.Info(string.Format("lAY LOAI CMT/CCCD --- {0}", cardType));
                        if (cardType >= 0)
                        {
                            var custInfo =
                                await _ekycService.GetOcrCardId(hashImageFront, hashImageBack, cardType, hashImageFront,
                                    hashDetailFront.tokenId);

                            if (custInfo != null)
                            {
                                var checkWarningTamper = custInfo.@object.tampering.warning;
                                if (checkWarningTamper.Count > 0)
                                {
                                    string cardIdValidLength = "id_invalid_length";
                                    string cardIdEdit = "id_sua_xoa";
                                    string cardIdNotTrue = "id_ko_hop_le";
                                    string cardIdNotJoint = "id_dob_ko_khop";
                                    string cardIdNotDob = "invalid_dob";
                                    string cardIdNotValid = "den_trang_ko_hop_le";
                                    string cardIdFakePrinting = "fake_printing";
                                    var caseValid = new List<string>();
                                    caseValid.Add(cardIdValidLength);
                                    caseValid.Add(cardIdEdit);
                                    caseValid.Add(cardIdNotTrue);
                                    caseValid.Add(cardIdNotJoint);
                                    caseValid.Add(cardIdNotDob);
                                    caseValid.Add(cardIdNotValid);
                                    caseValid.Add(cardIdFakePrinting);
                                    var compareTamper = false;

                                    var compareWarningFirst = caseValid.Except(checkWarningTamper);
                                    var compareWarningSecond = checkWarningTamper.Except(caseValid);
                                    if (compareWarningFirst.Any() || compareWarningSecond.Any())
                                    {
                                        compareTamper = true;
                                    }

                                    if (compareTamper)
                                    {
                                        if (System.IO.File.Exists(fullPathFront))
                                        {
                                            await _ekycService.DeleteFileEKYC(fullPathFront);
                                        }

                                        if (System.IO.File.Exists(fullPathBack))
                                        {
                                            await _ekycService.DeleteFileEKYC(fullPathBack);

                                        }
                                        return Json(new ResultViewModel()
                                        {
                                            Status = false,
                                            Message = Resources.Resources.ActionEKYCNoti1,
                                            Type = 0
                                        });
                                    }
                                }

                                if (!await _ekycService.CheckUnder18(custInfo.@object.birth_day))
                                {
                                    return Json(new ResultViewModel()
                                    {
                                        Status = false,
                                        Message = Resources.Resources.ActionEKYCNoti2,
                                        Type = 0
                                    });
                                }

                                if (!string.IsNullOrEmpty(custInfo.@object.valid_date) &&
                                    custInfo.@object.valid_date != "-" &&
                                    !custInfo.@object.valid_date.Contains("Không thời hạn"))
                                {
                                    if (!await _ekycService.CheckValidDate(custInfo.@object.valid_date))
                                    {
                                        return Json(new ResultViewModel()
                                        {
                                            Status = false,
                                            Message = Resources.Resources.ActionEKYCNoti2,
                                            Type = 0
                                        });
                                    }
                                }

                                if (!await _ekycService.ValidCardId(custInfo.@object.issue_date))
                                {
                                    return Json(new ResultViewModel()
                                    {
                                        Status = false,
                                        Message = Resources.Resources.ActionEKYCNoti2,
                                        Type = 0
                                    });
                                }

                                var cardId = custInfo.@object.id;
                                if (! await _ekycService.CheckCardIdExist(cardId))
                                {
                                    if (System.IO.File.Exists(fullPathFront))
                                    {
                                       await _ekycService.DeleteFileEKYC(fullPathFront);
                                    }

                                    if (System.IO.File.Exists(fullPathBack))
                                    {
                                        await _ekycService.DeleteFileEKYC(fullPathBack);

                                    }
                                    
                                    return Json(new ResultViewModel()
                                    {
                                        Status = false,
                                        Message = Resources.Resources.ActionEKYCNoti3,
                                        Type = 0
                                    });
                                }

                                Session[EKYCType.CardId] = cardId;
                                Session[EKYCType.FrontId] = fullPathFront;
                                Session[EKYCType.BackId] = fullPathBack;
                                var issuePlace = custInfo.@object.issue_place;
                                var valuePlace = await _ekycService.GetValueIssuePlace(cardId, issuePlace);
                                custInfo.@object.valuePlace = valuePlace;

                                if (!custInfo.@object.valid_date.Contains("Không thời hạn"))
                                    custInfo.@object.valid_date = "31/12/9999";

                                var result = new
                                {
                                    dataSessionFront = Session[EKYCType.FrontId],
                                    dataSessionBack = Session[EKYCType.BackId],
                                    DataEkyc = custInfo
                                };
                                return Json(new ResultDataViewModel<object>()
                                {
                                    Status = true,
                                    Data = result
                                }, JsonRequestBehavior.AllowGet);
                            }

                            if (System.IO.File.Exists(fullPathFront))
                            {
                                await _ekycService.DeleteFileEKYC(fullPathFront);
                            }

                            if (System.IO.File.Exists(fullPathBack))
                            {
                                await _ekycService.DeleteFileEKYC(fullPathBack);

                            }
                            return Json(new ResultViewModel()
                            {
                                Status = false,
                                Message = Resources.Resources.ActionEKYCNoti1,
                                Type = 0
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        if (System.IO.File.Exists(fullPathFront))
                        {
                            await _ekycService.DeleteFileEKYC(fullPathFront);
                        }

                        if (System.IO.File.Exists(fullPathBack))
                        {
                            await _ekycService.DeleteFileEKYC(fullPathBack);

                        }
                        return Json(
                            new ResultViewModel()
                            {
                                Status = false,
                                Message = Resources.Resources.ActionEKYCNoti1,
                                Type = 1
                            }, JsonRequestBehavior.AllowGet);
                    }
                }

                return Json(new ResultViewModel()
                {
                    Status = false
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                log.Error(string.Format("Loi METHOD UploadImageEKYC - {0}", e.Message));
                log.Error(string.Format("Loi METHOD UploadImageEKYC - {0}", e.InnerException));
                throw;
            }
        }

        [HttpPost]
        public async Task<ActionResult> UploadImageFace(string face)
        {
            try
            {
                string session = DateTime.Now.ToString().Replace("/", "-").Replace(" ", "_").Replace(":", "") + ".jpg";
                if (!Directory.Exists(Server.MapPath("~/Ekyc/Face/")))
                    Directory.CreateDirectory(Server.MapPath("~/Ekyc/Face/"));

                string fileName = Server.MapPath("~/Ekyc/Face/webcam_") + session;
                string url = Url.Content("~/Ekyc/Face/webcam_" + session);
                string path = HttpContext.Request.Url.GetLeftPart(UriPartial.Authority) + url;
                using (FileStream fs = new FileStream(fileName, FileMode.Create))
                {
                    using (BinaryWriter bw = new BinaryWriter(fs))
                    {
                        byte[] data = Convert.FromBase64String(face);
                        bw.Write(data);
                        bw.Close();
                    }
                }

                Session[EKYCType.ImageFace] = session;
                Session[EKYCType.PathFaceEkyc] = path;
                Session[EKYCType.PathFileName] = fileName;

                var hashImageFace = await _ekycService.GetHashFace(fileName);
                var hashImageFront = Session[EKYCType.HashFrontImage];
                var tokenId = Session[EKYCType.TokenID];
                var clientSession = Session[EKYCType.ClientSession];

                var frontImage = Session[EKYCType.FrontId];
                var backImage = Session[EKYCType.BackId];
                if (hashImageFront != null)
                {
                    var checkLiveness = await _ekycService.CheckFaceLiveness(hashImageFace.@object.hash,
                        Convert.ToString(clientSession), Convert.ToString(tokenId));
                    if (checkLiveness.Equals("success"))
                    {
                        var checkFaceCompare = await _ekycService.CheckFaceCompare(Convert.ToString(hashImageFront)
                            , hashImageFace.@object.hash, Convert.ToString(clientSession), Convert.ToString(tokenId));
                        var result = new
                        {
                            DataImageFace = path,
                            DataEkyc = checkFaceCompare
                        };
                        if (checkFaceCompare.@object.prob < 80)
                        {
                            if (System.IO.File.Exists(path))
                                await _ekycService.DeleteFileEKYC(Convert.ToString(fileName));

                            if (System.IO.File.Exists(Convert.ToString(frontImage)))
                                await _ekycService.DeleteFileEKYC(Convert.ToString(frontImage));
                            if (System.IO.File.Exists(Convert.ToString(backImage)))
                                await _ekycService.DeleteFileEKYC(Convert.ToString(backImage));
                        }

                        return Json(new ResultDataViewModel<object>()
                        {
                            Status = true,
                            Data = result
                        }, JsonRequestBehavior.AllowGet);
                    }

                    if (System.IO.File.Exists(path))
                        await _ekycService.DeleteFileEKYC(Convert.ToString(fileName));

                    return Json(new ResultViewModel()
                    {
                        Status = false,
                        Message = Resources.Resources.ActionEKYCNoti4
                    });
                }

                if (System.IO.File.Exists(path))
                    await _ekycService.DeleteFileEKYC(Convert.ToString(fileName));

                return Json(new ResultViewModel()
                {
                    Status = false,
                    Message = Resources.Resources.ActonEKYCError
                });
            }
            catch (Exception e)
            {
                log.Error(string.Format("Loi METHOD UploadImageFace {0}", e.Message));
                return Json(new ResultViewModel()
                {
                    Status = false,
                    Message = Resources.Resources.ActonEKYCError
                });
            }
        }

        [HttpPost]
        public async Task<ActionResult> GetDistrictList(string provinceCode)
        {
            try
            {
                if (!string.IsNullOrEmpty(provinceCode))
                {
                    var data = await _ekycService.GetDistrictList(provinceCode);
                    return Json(new ResultDataViewModel<object>()
                    {
                        Status = false,
                        Data = data
                    });
                }

                return Json(new ResultViewModel()
                {
                    Status = false,
                    Message = Resources.Resources.ActonEKYCError
                });
            }
            catch (Exception e)
            {
                log.Error(string.Format("Loi METHOD GetDistrictList {0}", e.Message));
                return Json(new ResultViewModel()
                {
                    Status = false,
                    Message = Resources.Resources.ActonEKYCError
                });
            }
        }

        [HttpPost]
        public async Task<ActionResult> GetWardList(string districtCode)
        {
            try
            {
                if (!string.IsNullOrEmpty(districtCode))
                {
                    var data = await _ekycService.GetWardList(districtCode);
                    return Json(new ResultDataViewModel<object>()
                    {
                        Status = false,
                        Data = data
                    });
                }

                return Json(new ResultViewModel()
                {
                    Status = false,
                    Message = Resources.Resources.ActonEKYCError
                });
            }
            catch (Exception e)
            {
                log.Error(string.Format("Loi METHOD GetWardList {0}", e.Message));
                return Json(new ResultViewModel()
                {
                    Status = false,
                    Message = Resources.Resources.ActonEKYCError
                });
            }
        }

        [HttpPost]
        public async Task<ActionResult> GetBranchList(string bankNo)
        {
            try
            {
                if (!string.IsNullOrEmpty(bankNo))
                {
                    var result = await _bankService.GetSubBranchList(bankNo);
                    return Json(new ResultDataViewModel<object>()
                    {
                        Status = true,
                        Data = result
                    }, JsonRequestBehavior.AllowGet);
                }

                return Json(new ResultViewModel()
                {
                    Status = false,
                    Message = Resources.Resources.ActonEKYCError
                });
            }
            catch (Exception e)
            {
                log.Error(string.Format("Loi METHOD GetBranchList {0}", e.Message));
                return Json(new ResultViewModel()
                {
                    Status = false,
                    Message = Resources.Resources.ActonEKYCError
                });
            }
        }

        [HttpPost]
        public async Task<ActionResult> DateTimeIsValidBirthDay(string datetime)
        {
            try
            {
                if (await _ekycService.IsValidDatetime(datetime) && datetime.Length >= 10)
                {
                    var dateOfBirth =
                        DateTime.ParseExact(datetime, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    var validDateOfBirth = await _ekycService.ValidDateOfBirth(dateOfBirth);
                    if (!string.IsNullOrEmpty(validDateOfBirth))
                    {
                        return Json(new ResultViewModel()
                        {
                            Status = false,
                            Message = validDateOfBirth
                        }, JsonRequestBehavior.AllowGet);
                    }

                    if (await _ekycService.CheckUnder18(datetime))
                    {
                        return Json(new ResultDataViewModel<object>()
                        {
                            Status = true,
                            Data = true,
                            Type = 0
                        }, JsonRequestBehavior.AllowGet);
                    }

                    return Json(new ResultViewModel()
                    {
                        Status = false,
                        Message = "Ngày sinh không hợp lệ"
                    }, JsonRequestBehavior.AllowGet);
                }

                return Json(new ResultViewModel()
                {
                    Status = false,
                    Message = "Định dạng ngày sinh vui lòng nhập lại"
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                log.Error(string.Format("Loi METHOD DateTimeIsValidBirthDay {0}", e.Message));
                return Json(new ResultViewModel()
                {
                    Status = false,
                    Message = Resources.Resources.ActonEKYCError
                });
            }
        }

        private async Task GetCommonInfo(string bankNo_01 = "", string bankNo_02 = "", string provinceCode = "",
            string district = "")
        {
            var bankList = await _bankService.GetBankList();
            ViewBag.BankList = bankList != null
                ? new SelectList(bankList, "BankNo", "ShortName")
                : new SelectList(new List<BankModel>(), "BankNo", "ShortName");
            ViewBag.SubBranchList_01 = new SelectList(new List<SubBranchModel>(), "BranchNo", "BranchName");
            ViewBag.SubBranchList_02 = new SelectList(new List<SubBranchModel>(), "BranchNo", "BranchName");
            if (!string.IsNullOrEmpty(bankNo_01))
            {
                ViewBag.SubBranchList_01 =
                    new SelectList(await _bankService.GetSubBranchList(bankNo_01), "BranchNo", "BranchName");
            }

            if (!string.IsNullOrEmpty(bankNo_02))
            {
                ViewBag.SubBranchList_02 = new SelectList(await _bankService.GetSubBranchList(bankNo_02), "BranchNo",
                    "BranchName");
            }

            var provinceList = await _bankService.GetProvinceList();
            ViewBag.ProvinceList = provinceList != null
                ? new SelectList(provinceList, "ProvinceID", "ProvinceName")
                : new SelectList(new List<Province>(), "ProvinceID", "ProvinceName");
            var placeOfIssueList = await _accountService.GetPlaceOfIssueList();
            ViewBag.placeOfIssueList = placeOfIssueList.Count() > 0
                ? new SelectList(placeOfIssueList, "Category", "CategoryName")
                : new SelectList(new List<Other>(), "Category", "CategoryName");
            var listGender = new List<Other>();
            listGender.Add(new Other()
            {
                Category = 1,
                CategoryName = "Nam"
            });
            listGender.Add(new Other()
            {
                Category = 2,
                CategoryName = "Nữ"
            });
            ViewBag.GenderList = listGender.Count() > 0
                ? new SelectList(listGender, "Category", "CategoryName")
                : new SelectList(new List<Other>(), "Category", "CategoryName");
            var provinceLists = await _accountService.GetProvinceList();

            ViewBag.GetProvinceList = provinceLists.Count() > 0
                ? new SelectList(provinceLists, "ProvinceCode", "ProvinceName")
                : new SelectList(new List<Province>(), "ProvinceCode", "ProvinceName");
            var districList = await _accountService.GetDistrictList(provinceCode);

            ViewBag.District = districList.Count() > 0
                ? new SelectList(districList, "DistrictCode", "DistrictName")
                : new SelectList(new List<Distric>(), "DistrictCode", "DistrictName");
            var wardList = await _accountService.GetWardList(district);

            ViewBag.Ward = wardList.Count() > 0
                ? new SelectList(wardList, "WardCode", "WardName")
                : new SelectList(new List<Ward>(), "WardCode", "WardName");
        }
    }
}