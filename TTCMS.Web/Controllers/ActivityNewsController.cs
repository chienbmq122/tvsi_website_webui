﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TTCMS.Service;
using TTCMS.Web.ViewModels;

namespace TTCMS.Web.Controllers
{
    [RoutePrefix("ActivityNews")]
    public class ActivityNewsController : BaseController
    {
        private readonly ICategoryService categoryService;
        private readonly ICategoryItemService categoryItemService;
        private readonly INewsService newsService;
        private readonly INewsItemService newsItemService;
        private readonly ILanguageService languageService;
        public ActivityNewsController(ICategoryService categoryService,
            INewsService newsService, INewsItemService newsItemService,
            ICategoryItemService categoryItemService,
            ILanguageService languageService)
        {
            this.categoryService = categoryService;
            this.newsService = newsService;
            this.newsItemService = newsItemService;
            this.categoryItemService = categoryItemService;
            this.languageService = languageService;
        }
        // GET: ActivityNews
        public ActionResult Index(string slug = "")
        {
            var languageCode = Cache.GetLang();
            var model = new ActivityNewsViewModel();
            var cateItemBySlug = categoryItemService.GetBySlug(slug);

            ViewBag.Title = cateItemBySlug?.Name;
            ViewBag.Meta = Config;

            if (cateItemBySlug == null)
            {
                return View(model);
            }

            if (cateItemBySlug.IsRedirect)
            {
                return Redirect(cateItemBySlug.RedirectUrl);
            }

            var parentId = cateItemBySlug.Category.ParentID;
            var listCate
                = categoryService
                    .GetByParentId(parentId ?? cateItemBySlug.Category.Id)
                    .Select(s => s.CategoryItems.Where(
                        c => c.Language.Code == languageCode
                    ).FirstOrDefault())
                    .Where(l => l != null)
                    .ToList();

            var listCateInfo = new List<CategoryInfo>();
            if (listCate != null && listCate.Any() && slug != "")
            {
                listCateInfo = listCate.Select(s => new CategoryInfo
                {
                    Name = s.Name,
                    Slug = s.Slug,
                    isActived = s.Slug == slug
                }).ToList();
            }

            parentId = parentId.HasValue ? cateItemBySlug.Category.Id : listCate != null && listCate.Count > 0 ? listCate[0].Category.Id : 0;
            var listCategoriesInGrid = categoryService.GetByParentId(parentId ?? 0).Where(n => n.CategoryItems.Any(i => i.Language.Code == languageCode));
            var listNewInfo = new List<NewsInfo>();
            var layout = (int)cateItemBySlug.Category.Layout;

            foreach (var item in listCategoriesInGrid)
            {
                var categoryItem = item.CategoryItems.FirstOrDefault(ci => ci.Language.Code == languageCode);
                listNewInfo.Add(
                    new NewsInfo
                    {
                        Title = categoryItem.Name,
                        ImageUrl = item.Image?.FullPath ?? "/asset/tvsi/images/dummy-image.jpg",
                        CreatedDate = item.CreatedDate,
                        Slug = categoryItem.RedirectUrl,
                        category = categoryItem.Name,
                        Desc = categoryItem.Description
                    }
                );
            }

            var pageSize = 6;
            model = new ActivityNewsViewModel()
            {
                CategoriesMenu = listCateInfo,
                News = listNewInfo,
                layout = layout,
                TotalPageNum = (listNewInfo.Count + pageSize - 1) / pageSize,
                ImagePath = cateItemBySlug.Category.Image != null ? cateItemBySlug.Category.Image.FullPath : "/webroot/images/banner/tthd-bg.png"
            };

            return View(model);
        }
        
        // GET: ActivityNews
        public ActionResult InternalActivity(string slug = "")
        {
            var languageCode = Cache.GetLang();
            ViewBag.Meta = Config;
            var model = new ActivityNewsViewModel();
            var cateBySlug = categoryItemService.GetBySlug(slug);
            ViewBag.Title = cateBySlug?.Name;
            if (cateBySlug == null)
                return View(model);
            if(cateBySlug.IsRedirect)
                return Redirect(cateBySlug.RedirectUrl);
            var listCate = cateBySlug.Category.ParentID.HasValue ? categoryService.GetByParentId(cateBySlug.Category.ParentID.Value).Select(s => s.CategoryItems
             .Where(c => c.Language.Code == languageCode).FirstOrDefault()).Where(l => l != null).ToList() : null;
            var layout = 0;
            var listCateInfo = new List<CategoryInfo>();
            if (listCate != null && listCate.Any() && slug != "")
            {
                listCateInfo = listCate.Select(s => new CategoryInfo
                {
                    Name = s.Name,
                    Slug = s.Slug,
                    isActived = s.Slug == slug
                }).ToList();
            }
            layout = (int)cateBySlug.Category.Layout;
            var listNewsId = newsService.GetByCategory(cateBySlug.Category.Id).Select(s => s.Id);
            var listNewInfo = new List<NewsInfo>();
            foreach (var item in listNewsId)
            {
                listNewInfo.Add(newsItemService.GetByNewsId(item).Where(n => n.Language.Code == languageCode)
                    .Select(s => new NewsInfo
                    {
                        Title = s.Title,
                        ImageUrl = s.News.ImageUrl,
                        CreatedDate = s.CreatedDate,
                        Slug = s.Slug,
                        category = s.News.Category?.CategoryItems.Where(c => c.Language.Code == languageCode).FirstOrDefault()?.Name,
                        Desc = s.Description
                    }).FirstOrDefault());
            }
            model = new ActivityNewsViewModel()
            {
                CategoriesMenu = listCateInfo,
                News = listNewInfo.Where(l => l != null).ToList(),
                layout = layout,
                TotalPageNum = int.Parse(Math.Round(listNewInfo.Count / (float)3).ToString())
            };
            if (listNewInfo.Any())
            {
                listNewInfo.Clear();
                var lisHotestId = newsService.GetHotest(listNewsId.ToList(), 3).Select(s => s.Id);
                foreach (var item in lisHotestId)
                {
                    listNewInfo.Add(newsItemService.GetByNewsId(item).Where(n => n.Language.Code == languageCode)
                        .Select(s => new NewsInfo
                        {
                            Title = s.Title,
                            ImageUrl = s.News.ImageUrl,
                            CreatedDate = s.CreatedDate,
                            Slug = s.Slug,
                            category = s.News.Category?.CategoryItems.Where(c => c.Language.Code == languageCode).FirstOrDefault()?.Name,
                            Desc = s.Description
                        }).FirstOrDefault());
                }
                model.HotestNews = listNewInfo.Where(l => l != null)?.ToList();
            }
            return View(model);
        }
        
        public ActionResult TVSIForCommunity()
        {
            ViewBag.Meta = Config;
            ViewBag.Title = Resources.Resources.TVSIForCommunity;
            return View();
        }
        public ActionResult TVSIWithPress()
        {
            ViewBag.Meta = Config;
            ViewBag.Title = Resources.Resources.TVSIWithPress;
            return View();
        }
        public ActionResult DocumentaryImage()
        {
            ViewBag.Meta = Config;
            ViewBag.Title = Resources.Resources.DocumentaryImage;
            return View();
        }
    }
}