﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TTCMS.Web.Controllers
{
    public class AboutUsController : BaseController
    {
        // GET: AboutUs
        public ActionResult AboutUsIndex()
        {
            ViewBag.Meta = Config;
            ViewBag.Title = Resources.Resources.AboutUs;
            return View();
        }
        public ActionResult EnterpriseValue()
        {
            ViewBag.Meta = Config;
            ViewBag.Title = Resources.Resources.AboutUs;
            return View();
        }
        public ActionResult DevelopmentMilestones()
        {
            ViewBag.Meta = Config;
            ViewBag.Title = Resources.Resources.AboutUs;
            return View();
        }
        public ActionResult Teams()
        {
            ViewBag.Meta = Config;
            ViewBag.Title = Resources.Resources.AboutUs;
            return View();
        }
        public ActionResult BranchNetwork()
        {
            ViewBag.Meta = Config;
            ViewBag.Title = Resources.Resources.AboutUs;
            return View();
        }
        public ActionResult ProfileTVSI()
        {
            ViewBag.Meta = Config;
            ViewBag.Title = Resources.Resources.AboutUs;
            return View();
        }
    }
}