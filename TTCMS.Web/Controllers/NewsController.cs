﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TTCMS.Data.Entities;
using TTCMS.Service;
using TTCMS.Web.ViewModels;
using System.Text.RegularExpressions;
using AutoMapper;
using TTCMS.Data.Entities.WebsiteNews;
using System.Threading.Tasks;
using System.IO;
using TTCMS.Web.Models;
using PagedList;
using System.Net.Http;
using Newtonsoft.Json;
using System.Net;
using System.Web;
using System.Collections.Concurrent;
using TTCMS.Service.Common;

namespace TTCMS.Web.Controllers
{
    [RoutePrefix("News")]
    public class NewsController : BaseController
    {
        private readonly INewsService newsService;
        
        static HttpClient client = new HttpClient();
        private readonly INewsItemService newsItemService;
        private readonly ICategoryService categoryService;
        private readonly ICategoryItemService categoryItemService;
        private readonly ILanguageService _languageService;
        private readonly ITVSIWebsiteNewsService _tvsiWebsiteNewsService;
        private readonly string ifinDataAPI = ConfigSettings.ReadSetting("iFinDataAPI");
        private readonly string ifinURL = ConfigSettings.ReadSetting("iFinRedirectURL");
        private readonly string[] dummyNewsImages = {
            "/Content/upload/files/news-1.jpg",
            "/Content/upload/files/news-2.jpg",
            "/Content/upload/files/news-3.jpeg",
            "/Content/upload/files/news-4.jpg",
            "/Content/upload/files/report-1.jpg",
            "/Content/upload/files/report-2.jpg",
            "/Content/upload/files/report-3.jpg",
            "/Content/upload/files/report-4.jpg",
            "/Content/upload/files/work-1.jpg",
            "/Content/upload/files/work-2.jpg",
            "/Content/upload/files/work-3.png",
            "/Content/upload/files/meeting-1.jpg",
            "/Content/upload/files/meeting-2.jpg"
        };
        private const string activitiesImagePath = "/Content/gallery/hoat-dong-tvsi/";
        private const string activitiesImagePath_EN = "/Content/gallery/activities-tvsi/";
        private const string documentaryImagePath = "/Content/gallery/anh-tu-lieu/";
        private const string documentaryImagePath_EN = "/Content/gallery/documentary-images/";
        private const int LIMIT = 6;
        private const string WHITELIST =
            @"</?p>|<brs?/?>|</?b>|</?strong>|</?i>|</?em>|
            </?s>|</?strike>|</?blockquote>|</?sub>|</?super>|
            </?h(1|2|3)>|</?pre>|<hrs?/?>|</?code>|</?ul>|
            </?ol>|</?li>|</a>|<a[^>]+>|<img[^>]+/?>";

        public NewsController(
            INewsService newsService, 
            INewsItemService newsItemService,
            ICategoryItemService categoryItemService,
            ICategoryService categoryService, 
            ITVSIWebsiteNewsService tvsiWebsiteNewsService, 
            ILanguageService languageService
        )
        {
            this.categoryItemService = categoryItemService;
            this.newsItemService = newsItemService;
            this.newsService = newsService;
            this.categoryService = categoryService;
            _tvsiWebsiteNewsService = tvsiWebsiteNewsService;
            _languageService = languageService;
        }

        // GET: News
        public async Task<ActionResult> Index(string slug)
        {
            var languageCode = Cache.GetLang();
            var languageId = languageCode != null && languageCode != "" ? _languageService.GetByCode(languageCode).Id : 2;
            ViewBag.Meta = Config;
            var news = await _tvsiWebsiteNewsService.GetNewsById(slug, languageId);
            if (news == null)
            {
                return RedirectToAction("Index", "Home");
            }
            var categoryId = languageId == 2 ? 1063 : 1138;
            switch (news.GroupID)
            {
                case 36:
                    categoryId = languageId == 2 ? 1064 : 1139;
                    break;
                case 35:
                    categoryId = languageId == 2 ? 1065 : 1140;
                    break;
            }
            var categoryName = categoryItemService.GetById(categoryId).Name;
            var model = new NewsViewItemModel
            {
                News = news,
                RedirectPageUrl = $"/News/Danh-sach-tin-tuc?categoryItemId={categoryId}",
                RedirectPage = categoryName
            };

            var skipIds = new List<int>
            {
                news.NewsID
            };

            var newestRaw = await _tvsiWebsiteNewsService.GetNewest(skipIds, languageId, 3);
            if (newestRaw != null && newestRaw.Count() > 0)
            {
                var newest = GetNewsInfoListFromStockNews(newestRaw, languageCode, 3);
                model.Newest = newest.ToList();
            }
            var hotestRaw = await _tvsiWebsiteNewsService.GetHotest(skipIds, languageId, 3);
            if (hotestRaw != null && hotestRaw.Count() > 0)
            {
                var hotest = GetNewsInfoListFromStockNews(hotestRaw, languageCode, 3);
                model.Hotest = hotest.ToList();
            }
            ViewBag.Title = news.Title;
            
            return View(model);
        }

        public async Task<ActionResult> Search(string keyWord = "", int categoryItemId = 0, int page = 0, int searchType = 0)
        {
            ViewBag.Meta = Config;
            ViewBag.Title = keyWord != "" ? Resources.Resources.SearchNews : Resources.Resources.ListNewsText;
            var languageCode = Cache.GetLang();
            var languageId = languageCode != null && languageCode != "" ? _languageService.GetByCode(languageCode).Id : 2;
            var groupId = -1;
            var totalRecords = 0;
            var cateItemBySlug = categoryItemService.GetById(categoryItemId);
            if (categoryItemId == 1066) {
                cateItemBySlug = categoryItemService.GetById(1039);
            }
            else if (categoryItemId == 1066){
                cateItemBySlug = categoryItemService.GetById(1037);
            }
            var layout = -1;
            if (cateItemBySlug != null && cateItemBySlug.Category != null)
            {
                layout = (int)cateItemBySlug.Category.Layout;
            }

            switch(categoryItemId) {
                case 1064:
                case 1139:
                    groupId = 36;
                    layout = -1;
                    break;
                case 1065:
                case 1140:
                    groupId = 35;
                    layout = -1;
                    break;
                case 1063:
                case 1138:
                    groupId = 29;
                    layout = -1;
                    break;
            }
            
            var stockNews = Enumerable.Empty<stock_News>();
            var categoryItemsSearch = Enumerable.Empty<CategoryItem>();
            var symbols = new List<NewsInfo>();

            if (groupId != -1 && keyWord == "")
            {
                stockNews = await _tvsiWebsiteNewsService.GetNewsByGroupID(groupId, languageId, LIMIT, page);
                totalRecords = await _tvsiWebsiteNewsService.GetNumberOfData(groupId, null, languageId);
            }

            if (keyWord != "")
            {
                keyWord = Regex.Replace(keyWord, WHITELIST, string.Empty);
                if (searchType == 1)
                {
                    symbols = await GetSymbol(keyWord, languageId);
                }
                else
                {
                    if (page == 0) categoryItemsSearch = categoryItemService.GetByTitle(keyWord, languageId);
                    stockNews = await _tvsiWebsiteNewsService.GetNewsByTitle(keyWord, languageId, LIMIT, page);
                    totalRecords = await _tvsiWebsiteNewsService.GetNumberOfData(null, keyWord, languageId);
                }
            }

            if (categoryItemId == 0 && keyWord == "")
            {
                stockNews = await _tvsiWebsiteNewsService.GetNews(LIMIT, languageId, page);
                totalRecords = await _tvsiWebsiteNewsService.GetNumberOfData(null, null, languageId);
            }

            var model = new SearchNewsInfo()
            {
                CurrentKeyWord = keyWord,
                TotalPageNum = 0,
                CurrentPage = page,
                CategoryItemId = categoryItemId,
                Layout = layout,
                PageURL = string.IsNullOrEmpty(keyWord) ? $"?categoryItemId={categoryItemId}" : $"?keyWord={keyWord}"
            };
            var imagePath = activitiesImagePath;
            if (categoryItemId == 1066 || categoryItemId == 1141)
            {
                if (categoryItemId == 1141) imagePath = activitiesImagePath_EN;
                List<ImageModel> imageFolder = GetFirstImagefolders(imagePath, null, false);
                var news = GetNewsInfoFromImageFolders(imageFolder, "Hoạt động nội bộ");
                model.TotalPageNum = (int)Math.Ceiling((decimal)(imageFolder.Count) / 6);
                model.News = news?.Skip(page * 6).Take(6).ToList();
                model.DisplayImageContent = true;
            }
            
            if (categoryItemId == 1162 || categoryItemId == 1163)
            {
                if (categoryItemId == 1162) imagePath = documentaryImagePath;
                if (categoryItemId == 1163) imagePath = documentaryImagePath_EN;
                List<ImageModel> imageFolder = GetFirstImagefolders(imagePath, null, false);
                var news = GetNewsInfoFromImageFolders(imageFolder, "Ảnh tư liệu");
                model.TotalPageNum = (int)Math.Ceiling((decimal)(imageFolder.Count) / 6);
                model.News = news?.Skip(page * 6).Take(6).ToList();
                model.DisplayImageContent = true;
            }
            if (stockNews != null && stockNews.Count() > 0)
            {
                var news = GetNewsInfoListFromStockNews(stockNews, languageCode, categoryItemId);
                model.TotalPageNum = (int)Math.Ceiling((decimal)(totalRecords) / LIMIT);
                model.News = news?.ToList();
            }
            var bool1 = categoryItemsSearch != null;
            var bool2 = categoryItemsSearch.Count() > 0;
            if (categoryItemsSearch != null && categoryItemsSearch.ToList().Count() > 0)
            {
                var news = GetNewsInfoFromCategoryItemsSearch(categoryItemsSearch).ToList();
                news.AddRange(model.News);
                model.News = news;
            }

            if (searchType == 1 && symbols != null && symbols.Count > 0)
            {
                model.News = symbols;
            }
            var categoriesInNews = categoryService.GetCategoriesInNews(languageCode);
            if (categoriesInNews != null)
            {
                var categoryItems = new List<CategoryItemViewModel>();

                foreach (var item in categoriesInNews)
                {
                    var categoryItem = item.CategoryItems.FirstOrDefault(x => x.Language.Code == languageCode && x.DeletedAt == null);
                    if (categoryItem == null) continue;
                    categoryItems.Add(Mapper.Map<CategoryItem, CategoryItemViewModel>(categoryItem));
                }
                model.CategoryItems = categoryItems;
            }

            return View(model);
        }

        public ActionResult Gallery(String folder, int page = 0, int categoryItemId = 1066)
        {
            var languageCode = Cache.GetLang();
            var imagePath = "";
            var pageName = categoryItemService.GetById(categoryItemId).Name;
            switch(categoryItemId)
            {
                case 1162:
                    imagePath = documentaryImagePath;
                    break;
                case 1163:
                    imagePath = documentaryImagePath_EN;
                    break;
                case 1141:
                    imagePath = activitiesImagePath_EN;
                    break;
                default:
                    imagePath = activitiesImagePath;
                    break;
            }
            var images = Directory.EnumerateFiles(Server.MapPath(imagePath + folder))
                    .Select(fn => imagePath + folder + "/" + Path.GetFileName(fn))
                    .Where(fn => !fn.EndsWith(".db"));
            var imageModel = new GalleryViewModel()
            {
                Images = images.Skip(page * 10).Take(10),
                RedirectPage = pageName,
                RedirectPageURL = $"/News/Danh-sach-tin-tuc?categoryItemId={categoryItemId}"
            };
            List<ImageModel> imageFolder = GetFirstImagefolders(imagePath, folder, true);
            var category = categoryItemId == 1066 || categoryItemId == 1141 ? Resources.Resources.InternalActivity : Resources.Resources.DocumentaryImage;
            var newest = GetNewsInfoFromImageFolders(imageFolder, category);
            imageModel.Newest = newest.ToList();
            imageModel.TotalPageNum = (int)Math.Ceiling((decimal)(images.Count()) / 10);
            imageModel.CurrentPage = page;
            imageModel.FolderName = folder;
            imageModel.CategoryItemId = categoryItemId;
            imageModel.Title = GetStringFirstIndex(folder, ".");
            return View(imageModel);
        }
        public ActionResult GetAllTitle()
        {
            var result = newsItemService.GetAllNewsTitle().Select(s => new { imgsrc = s.News.ImageUrl, label = s.Title, url = "/News/" + s.Slug + ".html" });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        private async Task<List<NewsInfo>> GetSymbol(string keyword, int languageId = 1)
        {
            // ServicePointManager.Expect100Continue = true;
            // ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            // ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
            var ticker = keyword.ToUpper();
            var uriBuilder = new UriBuilder(ifinDataAPI);
            var query = HttpUtility.ParseQueryString(uriBuilder.Query);
            query["code"] = ticker;
            query["langId"] = languageId.ToString();
            uriBuilder.Query = query.ToString();
            var results = new List<NewsInfo>();
            var response = await client.GetAsync(uriBuilder.Uri);
            var content = await response.Content.ReadAsStreamAsync();
            var streamReader = new StreamReader(content);
            var jsonReader = new JsonTextReader(streamReader);
            var serializer = new JsonSerializer();
            var items = serializer.Deserialize<List<Symbol>>(jsonReader);
            if (items != null && items.Count > 0)
            {
                foreach(var item in items)
                {
                    results.Add(new NewsInfo()
                    {
                        Title = $"{item.CODE} - {item.COMPANYNAME}",
                        RedirectUrl = $"{ifinURL}/{item.CODE}",
                        ImageUrl = dummyNewsImages[Math.Abs(item.CODE.GetHashCode()) % dummyNewsImages.Length]
                    });
                }
            }
            return results;
        }

        private IEnumerable<NewsInfo> GetNewsInfoList(IEnumerable<NewsItem> newsItems, string languageCode, int categoryItemId = 0)
        {
            if (newsItems == null || string.IsNullOrEmpty(languageCode)) return null;

            var filteredList = newsItems.Where(
                n => n.Language.Code == languageCode
                && (categoryItemId == 0
                    || (categoryItemId != 0
                        && (n.News.Category?.CategoryItems?.Any(x => x.Id == categoryItemId) ?? false))));

            return filteredList?.Select(s => new NewsInfo
            {
                Title = s.Title == "" || s.Title == null ?"" : s.Title,
                Slug = s.Slug == "" || s.Slug == null ? "" : s.Slug,
                ImageUrl = s.News.ImageUrl,
                category = s.News?.Category?.CategoryItems.Where(c => c.Language.Code == languageCode).FirstOrDefault()?.Name,
                Desc = s.Description ==  "" || s.Description ==  null ? "": s.Description,
                CreatedDate = s.CreatedDate
            });
        }

        private IEnumerable<NewsInfo> GetNewsInfoListFromStockNews(IEnumerable<stock_News> newsItems, string languageCode, int categoryItemId = 0)
        {
            if (newsItems == null || string.IsNullOrEmpty(languageCode)) return null;
            var category = "";
            switch (categoryItemId)
            {
                case 1063:
                case 1138:
                    category = Resources.Resources.TVSINews;
                    break;
                case 1066:
                case 1141:
                    category = Resources.Resources.InternalActivity;
                    break;
                case 1065:
                case 1140:
                    category = Resources.Resources.TVSIWithPress;
                    break;
                case 1064:
                case 1139:
                    category = Resources.Resources.TVSIForCommunity;
                    break;
                default:
                    category = Resources.Resources.TVSINews;
                    break;
            }

            return newsItems?.Select(s => new NewsInfo
            {
                Title = s.Title == "" || s.Title == null ? "" : s.Title,
                Slug = s.NewsID.ToString(),
                ImageUrl = s.ImageUrl != null ? $"https://fp.tvsi.com.vn//NewsImages/{s.ImageUrl}.jpg" : dummyNewsImages[s.NewsID % dummyNewsImages.Length],
                category = category,
                Desc = s.Description == "" || s.Description == null ? "" : s.Description,
                CreatedDate = s.CreatedDate
            });
        }

        private IEnumerable<NewsInfo> GetNewsInfoFromCategoryItemsSearch(IEnumerable<CategoryItem> list)
        {
            return list?.Select(s => new NewsInfo
            {
                Title = s.Name,
                Slug = s.Slug,
                ImageUrl = s.Category.Image != null ? s.Category.Image.FullPath : dummyNewsImages[s.Id % dummyNewsImages.Length],
                RedirectUrl = s.IsRedirect ? s.RedirectUrl : s.Category != null ? $"/category/{s.Slug}" : $"/page/{s.Slug}"
            });
        }

        private IEnumerable<NewsInfo> GetNewsInfoFromImageFolders(List<ImageModel> imageList, string category)
        {
            if (imageList == null) return null;
            var newsItems = new List<NewsInfo>();

            return imageList?.Select(s => new NewsInfo
            {
                Title = GetStringFirstIndex(s.FolderImage, "."),
                Slug = s.FolderImage,
                ImageUrl = s.PathImage ?? dummyNewsImages[s.GetHashCode() % dummyNewsImages.Length],
                category = category,
            });
        }
        private List<ImageModel> GetFirstImagefolders(String _pathImage, string skipFolder, bool newest)
        {
            string path = Server.MapPath(_pathImage);
            string[] dirs = Directory.GetDirectories(path, "*", SearchOption.AllDirectories);
            List<String> folders = new List<string>();
            for (int i = 0; i < dirs.Length; i++)
            {
                if (newest && folders.Count == 5) break;
                int index = dirs[i].LastIndexOf("\\");
                var folderName = dirs[i].Substring(index + 1, dirs[i].Length - index - 1);
                if (folderName == skipFolder) continue;
                folders.Add(folderName);
            }

            var myviewmodle = new ImageNewsViewModel();
            List<ImageModel> imageList = new List<ImageModel>();
            for (int i = 0; i < folders.Count; i++)
            {
                myviewmodle.Images = Directory.EnumerateFiles(Server.MapPath(_pathImage + folders[i]))
                    .Select(fn => _pathImage + folders[i] + "/" + Path.GetFileName(fn));
                int count = 0;
                foreach (String image in myviewmodle.Images)
                {
                    count++;
                }
                if (count > 0)
                {
                    imageList.Add(new ImageModel()
                    {
                        PathImage = myviewmodle.Images.First().ToString(),
                        FolderImage = folders[i]
                    });
                }
            }

            return imageList;
        }
        private string GetStringFirstIndex(string str, string str_index)
        {

            string str_temp = str;
            int index = str.IndexOf(str_index);
            if (index > 0)
            {
                str_temp = str.Substring(index + 1);
            }
            return str_temp;
        }
    }
}