﻿using AutoMapper;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using TTCMS.Data.Entities;
using TTCMS.Service;
using TTCMS.Web.Models;
using TTCMS.Web.Models.Product;
using TTCMS.Core.Infrastructure.Alerts;
using TTCMS.Web.Helper;
using TTCMS.Web.ViewModels;
using TTCMS.Web.ResourceString;
using TTCMS.Web.Areas.TT_Admin.Models;

namespace TTCMS.Web.Controllers
{
    public class SinglePageController : BaseController
    {
        private ISinglePageService singlePageService;
        private ILanguage_SinglePageService language_SinglePageService;
        public SinglePageController(ISinglePageService singlePageService , ILanguage_SinglePageService language_SinglePageService) {
            this.singlePageService = singlePageService;
            this.language_SinglePageService = language_SinglePageService;
        }

        // GET: SinglePage
        public ActionResult Index(string slug)
        {
            //var languageCode = (Cache.Get(CacheLanguageCode) as string)!= null && (Cache.Get(CacheLanguageCode) as string) != "" ? (Cache.Get(CacheLanguageCode) as string) : "vi-VN";
            var languageCode = Cache.GetLang();
            var a = language_SinglePageService.GetByRoute(slug).ToList() ;
            var model = language_SinglePageService.GetByRoute(slug)?.Where(s=>s.Language.Code == languageCode)?.FirstOrDefault();
            var viewModel = new Language_SinglePageViewModel();

            if (model != null)
            {
                Mapper.Map(model, viewModel);
            }

            ViewBag.Meta = Config;
            ViewBag.Title =  model?.Title;
            return View(viewModel);
        }

        public ActionResult Preview(int id)
        {
            var model = language_SinglePageService.GetById(id);
            var viewModel = new Language_SinglePageViewModel();

            if (model != null)
            {
                Mapper.Map(model, viewModel);
            }

            ViewBag.Meta = Config;
            ViewBag.Title = model.Title;
            return View("Index", viewModel);
        }

        public ActionResult ErrorPage(string slug)
        {
            return View();
        }
    }
}