﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TTCMS.Web.Controllers
{
    public class CorporateBondController : BaseController
    {
        // GET: CorporateBond
        public ActionResult CorporateBondIndex()
        {
            ViewBag.Meta = Config;
            ViewBag.Title = Resources.Resources.CorporateBond;
            return View();
        }
    }
}