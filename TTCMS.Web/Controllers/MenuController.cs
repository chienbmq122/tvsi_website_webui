﻿using AutoMapper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Cors;
using System.Web.Mvc;
using TTCMS.Data.Entities;
using TTCMS.Service;
using TTCMS.Service.Common;
using TTCMS.Web.ViewModels;

namespace TTCMS.Web.Controllers
{
    public class MenuController : BaseController
    {
        static HttpClient client = new HttpClient();
        private readonly IMenuService menuService;
        private readonly ICategoryService categoryService;
        private readonly ILanguageService languageService;
        private readonly ICategoryItemService categoryItemService;
        private readonly string ifinDataAPI = ConfigSettings.ReadSetting("iFinDataAPI");
        private readonly string ifinURL = ConfigSettings.ReadSetting("iFinRedirectURL");
        public MenuController(
            ICategoryService categoryService,
            IMenuService menuService,
            ILanguageService languageService,
            ICategoryItemService categoryItemService)
        {
            this.menuService = menuService;
            this.categoryService = categoryService;
            this.categoryItemService = categoryItemService;
            this.languageService = languageService;
        }

        [ChildActionOnly]
        public ActionResult HeaderMenu()
        {
            var languageCode = Cache.GetLang();
            var viewModel = new HeaderMenuViewModel() { LanguageCode = languageCode };
            var topMenus = menuService.GetActivatedMenu(MenuGroupType.TopMenu, languageCode);
            var headerMenus = menuService.GetActivatedMenu(MenuGroupType.HeaderMenu, languageCode);
            var languages = languageService.GetList().Where(item => item.Code != "*");

            viewModel.HeaderTopMenu = GetMenu(topMenus).OrderBy(x => x.Order).ToList();
            viewModel.HeaderMenu = GetMenu(headerMenus).OrderBy(x => x.Order).ToList();
            viewModel.Languages = Mapper.Map<IEnumerable<Language>, IEnumerable<LanguageViewModel>>(languages).ToList();
            viewModel.SearchLanguageId = languages.Where(x => x.Code == languageCode).FirstOrDefault().Id == 1 ? 2 : 1;
            viewModel.iFinDataAPI = ifinDataAPI;
            return PartialView(viewModel);
        }

        public async Task<ActionResult> GetSymbol(string code, int langId = 1)
        {
            var uriBuilder = new UriBuilder(ifinDataAPI);
            var query = HttpUtility.ParseQueryString(uriBuilder.Query);
            query["code"] = code;
            query["langId"] = langId.ToString();
            uriBuilder.Query = query.ToString();
            var results = new List<NewsInfo>();
            var response = await client.GetAsync(uriBuilder.Uri);
            var content = await response.Content.ReadAsStreamAsync();
            var streamReader = new StreamReader(content);
            var jsonReader = new JsonTextReader(streamReader);
            var serializer = new JsonSerializer();
            var items = serializer.Deserialize<List<Symbol>>(jsonReader);
            if (items != null && items.Count > 0)
            {
                foreach (var item in items)
                {
                    results.Add(new NewsInfo()
                    {
                        Title = $"{item.CODE} - {item.COMPANYNAME}",
                        RedirectUrl = $"{ifinURL}/{item.CODE}",
                    });
                }
            }
            return Json(results, JsonRequestBehavior.AllowGet);
        }
        private List<MenuViewModel> GetMenu(IEnumerable<Menu> menus)
        {
            var result = new List<MenuViewModel>();
            var childrenHeaderMenuViewModels = new List<MenuViewModel>();

            foreach (var item in menus)
            {
                var itemViewModel = Mapper.Map<Menu, MenuViewModel>(item);

                if (item.CategoryItem != null)
                {
                    // itemViewModel.Link = "/category/" + item.CategoryItem.Slug;
                    itemViewModel.Link = item.CategoryItem.RedirectUrl ?? "/category/" + item.CategoryItem.Slug;
                    itemViewModel.Icon = item.CategoryItem.Category.Icon;
                }
                else if (item.Language_SinglePage != null)
                {
                    itemViewModel.Link = "/page/" + item.Language_SinglePage.Slug;
                }

                if (item.ParentID == 0)
                {
                    result.Add(itemViewModel);
                }
                else
                {
                    childrenHeaderMenuViewModels.Add(itemViewModel);
                }
            }

            foreach (var item in childrenHeaderMenuViewModels)
            {
                var root = result.FirstOrDefault(x => x.Id == item.ParentID);

                if (root != null)
                {
                    root.Children.Add(item);
                    continue;
                }

                root = childrenHeaderMenuViewModels.FirstOrDefault(x => x.Id == item.ParentID);

                if (root != null)
                {
                    root.Children.Add(item);
                }

            }

            return result;
        }

        private List<MenuViewModel> GetMenuChildren(int categoryId, int languageId)
        {
            var result = new List<MenuViewModel>();
            var categories = categoryService.GetByParentId(categoryId);

            if (categories == null) return result;

            foreach (var category in categories)
            {
                var categoryItem = category.CategoryItems.FirstOrDefault(c => c.Language.Id == languageId);
                if (categoryItem != null)
                {
                    var menuViewModel = new MenuViewModel()
                    {
                        Icon = categoryItem.Category.Icon,
                        Link = categoryItem.Slug,
                        Text = categoryItem.Name,
                        Children = GetMenuChildren(categoryItem.Category.Id, languageId)
                    };
                    result.Add(menuViewModel);
                }
            }

            return result;
        }

        [ChildActionOnly]
        public ActionResult MainMenu()
        {
            string _menuMain = "MenuMain";
            IList<Menu> m_menu = new List<Menu>();

            if (!Cache.IsSet(_menuMain))
            {
                m_menu = menuService.GetList(MenuGroupType.MainMenu, cultureName).Where(x => x.IsActived).OrderBy(x => x.Order).ToArray();
                foreach (var item in m_menu)
                {
                    item.Link = getLinkMenu(item.TextType, item.Link, item.WithId ?? 0, item.Action, item.Controller);
                }
                Cache.Set(_menuMain, m_menu);
            }
            else
            {
                m_menu = Cache.Get(_menuMain) as Menu[];
            }
            ViewBag.HotLine = Config.HotLine;
            return PartialView(m_menu);
        }

        [ChildActionOnly]
        public ActionResult Category()
        {
            string _Cache_BotMenu = "Category_Menu";
            IList<Menu> m_menu = new List<Menu>();

            if (!Cache.IsSet(_Cache_BotMenu))
            {
                m_menu = menuService.GetList(MenuGroupType.Category, cultureName).Where(x => x.IsActived).OrderBy(x => x.Order).ToArray();
                foreach (var item in m_menu)
                {
                    item.Link = getLinkMenu(item.TextType, item.Link, item.WithId ?? 0, item.Action, item.Controller);
                }
                Cache.Set(_Cache_BotMenu, m_menu);
            }
            else
            {
                m_menu = Cache.Get(_Cache_BotMenu) as Menu[];
            }
            return PartialView(m_menu);
        }
        [ChildActionOnly]
        public ActionResult Category_Mobile()
        {
            string _Cache_BotMenu = "Category_Mobile";
            IList<Menu> m_menu = new List<Menu>();

            if (!Cache.IsSet(_Cache_BotMenu))
            {
                m_menu = menuService.GetList(MenuGroupType.Category_Mobile, cultureName).Where(x => x.IsActived).OrderBy(x => x.Order).ToArray();
                foreach (var item in m_menu)
                {
                    item.Link = getLinkMenu(item.TextType, item.Link, item.WithId ?? 0, item.Action, item.Controller);
                }
                Cache.Set(_Cache_BotMenu, m_menu);
            }
            else
            {
                m_menu = Cache.Get(_Cache_BotMenu) as Menu[];
            }
            return PartialView(m_menu);
        }
        [ChildActionOnly]
        public ActionResult BotMenu()
        {
            var languageCode = Cache.GetLang();
            string _Cache_BotMenu = "BotMenu";
            IList<Menu> m_menu = new List<Menu>();

            if (!Cache.IsSet(_Cache_BotMenu))
            {
                m_menu = menuService.GetList(MenuGroupType.BotMenu, languageCode).Where(x => x.IsActived).OrderBy(x => x.Order).ToArray();
                foreach (var item in m_menu)
                {
                    item.Link = getLinkMenu(item.TextType, item.Link, item.WithId ?? 0, item.Action, item.Controller);
                }
                Cache.Set(_Cache_BotMenu, m_menu);
            }
            else
            {
                m_menu = Cache.Get(_Cache_BotMenu) as Menu[];
            }
            ViewBag.Contact = Config.contactus_setting;
            return PartialView(m_menu);
        }
        [ChildActionOnly]
        public ActionResult PageMenu(int Id = 0)
        {
            string _Cache_BotMenu = "PageMenu";
            IList<Menu> m_menu = new List<Menu>();

            if (!Cache.IsSet(_Cache_BotMenu))
            {
                m_menu = menuService.GetList(MenuGroupType.BotMenu, cultureName).Where(x => x.IsActived).OrderBy(x => x.Order).ToArray();
                foreach (var item in m_menu)
                {
                    item.Link = getLinkMenu(item.TextType, item.Link, item.WithId ?? 0, item.Action, item.Controller);
                }
                Cache.Set(_Cache_BotMenu, m_menu);
            }
            else
            {
                m_menu = Cache.Get(_Cache_BotMenu) as Menu[];
            }
            ViewBag.Id = Id;
            return PartialView(m_menu);
        }

        [HttpPost]
        public JsonResult UpdateLanguage(string code)
        {
            try
            {
                if (languageService.GetByCode(code) == null)
                {
                    return Json(
                        new { isSuccess = false, error = "language code is not valid" },
                        JsonRequestBehavior.AllowGet
                    );
                }
                Session["Language"] = code;
                cultureName = code;
                Resources.Resources.Culture = Session["Language"].ToString() == "en-US" ? new System.Globalization.CultureInfo("en-US") : new System.Globalization.CultureInfo("vi-VN");
                return Json(
                    new { isSuccess = true, error = "" },
                    JsonRequestBehavior.AllowGet
                );
            }
            catch (System.Exception ex)
            {
                return Json(
                    new { isSuccess = false, error = ex.Message },
                    JsonRequestBehavior.AllowGet
                );
            }
        }
    }
}