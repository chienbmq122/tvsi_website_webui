﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TTCMS.Service;
using TTCMS.Web.ViewModels;
using AutoMapper;
using TTCMS.Data.Entities;
using TTCMS.Web.Areas.TT_Admin.Models;
using System;
using TTCMS.Core.Infrastructure.Alerts;
using System.Web;
using System.IO;
using log4net;
using System.Configuration;
using System.Reflection;

namespace TTCMS.Web.Controllers
{
    public class FAQController : BaseController
    {
        private readonly IMenuService menuService;
        private readonly ITagService _tagService;
        private readonly IFAQService FAQService;
        private readonly ILangugage_FAQService language_FAQ;
        private readonly ITopicService _topicService;
        private readonly ILanguage_TopicService _language_TopicService;
        private readonly List<FAQObject> dummyFAQsEn = new List<FAQObject>();
        private readonly IQuestionService _questionService;
        private readonly IQuestionAttachmentService _questionAttachmentService;
        private readonly IPendingQuestionService _pendingquestionService;
        private readonly ILanguage_QuestionService _language_QuestionService;
        private readonly ICommentService _commentService;
        private static readonly ILog logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private static readonly string templateMailFolderPath = ConfigurationManager.AppSettings["TemplateMailFolderPath"].ToString();
                
        private readonly List<string> allowedFileExtensions = new List<string>(new string[]
        {
            ".jpg",".jpeg",".png", ".doc", ".docx", ".pdf", ".xlsx", ".xls", ".ppt", ".pptx"
        });
        public FAQController(
            IFAQService fAQService, 
            ITagService tagService,
            ILangugage_FAQService language_FAQ,
            ITopicService topicService,
            ILanguage_TopicService language_TopicService,
            ILanguage_QuestionService language_QuestionService,
            IPendingQuestionService pendingquestionService,
            IQuestionService questionService,
            IQuestionAttachmentService questionAttachmentService,
            ICommentService commentService,
            IMenuService menuService)
        {
            this.FAQService = fAQService;
            _tagService = tagService;
            this.language_FAQ = language_FAQ;
            _topicService = topicService;
            _language_TopicService = language_TopicService;
            _pendingquestionService = pendingquestionService;
            _questionAttachmentService = questionAttachmentService;
            dummyFAQsEn.Add(new FAQObject() { Question = "Which system is available for customers to trade online?", Answer = "<p>At TVSI, you can online trade through: webtrading iTrade Home, TVSI Mobile online trading application, and iTrade Pro online trading application.</p>" });
            dummyFAQsEn.Add(new FAQObject() { Question = "When can I place an off-market order for the next trading day?", Answer = "<p>TVSI Mobile system allows placing off-market orders for the next trading day from 15:30 of the current trading day.</p>" });
            dummyFAQsEn.Add(new FAQObject() { Question = "How to open a margin trading account?", Answer = @"<p>To register for a margin account, you can choose 1 of 3 ways:</p>
                        <p><strong>Option 1</strong>: You can arrive at TVSI headquarters or branches for support.</p>
                        <p><strong>Option 2</strong>: You can complete the account opening contract through the account manager.</p>
                        <p><strong>Option 3</strong>: You can log in to the TVSI iTrade-Mobile App and follow the instructions.</p>
                        Note: Please complete if the account has a net asset of over 500 million or uses a deposit of over VND 1 billion to complete the contract." });
            dummyFAQsEn.Add(new FAQObject() { Question = "How to check order history?", Answer = @"<p>To look up the order history, you can choose one of the following ways:</p>
                <p><strong>Option 1</strong>: Access website <a href=""https://itrade-home.tvsi.com.vn/"">https://itrade-home.tvsi.com.vn/</a> and go to Portfolio Management/Order History</p>
                <p><strong>Option 2</strong>: Access TVSI mobile application section Trading - Order history</p>
                <p><strong>Option 3</strong>: Contact the hotline 19001885 for support</p>
            " });
            _language_QuestionService = language_QuestionService;
            _questionService = questionService;
            _commentService = commentService;
            this.menuService = menuService;
        }
        public ActionResult IndexV1(int langId = 2)
        {
            ViewBag.Meta = Config;
            ViewBag.Title = Resources.Resources.FAQ;
            var languageCode = Cache.GetLang();
            var model = new FAQHomeViewModel();
            var faqs = FAQService.GetListForActive();
            model.FAQs = new List<FAQObject>();
            model.TotalFAQ = faqs.Count();
            if (languageCode == "vi-VN") 
            {
                foreach (var item in faqs)
                {
                    var itemViewModel = language_FAQ.GetById(item.Id, langId).Select(q => new FAQObject()
                    {
                        Question = q.Question,
                        Answer = q.Answer
                    }).FirstOrDefault();

                    if (itemViewModel != null)
                    {
                        model.FAQs.Add(itemViewModel);
                    }
                }
            }
            else {
                model.FAQs = dummyFAQsEn;
                model.TotalFAQ = dummyFAQsEn.Count();
            }
            return View(model);
        }

        // GET: FAQ
        public ActionResult Index(int langId = 2)
        {
            var languageCode = Cache.GetLang();
            ViewBag.Meta = Config;
            ViewBag.Title = Resources.Resources.FAQ;
            var model = new FAQHomeViewModel();
            var faqs = FAQService.GetListForActive();
            var topics = _topicService
                .GetListForActive()
                .OrderBy(x => x.Order);
            var parents = topics.Where(x => x.ParentTopic == null);
            foreach(var item in parents)
            {
                var vm = new FAQTopicViewModel
                {
                    Id = item.Id,
                    ImageUrl = item.Image?.FullPath ?? "/webroot/images/faq/questions/pana.png",
                    Order = item.Order
                };
                var language_topic = _language_TopicService.GetByTopic(item.Id, "vi-VN");
                if (language_topic != null)
                {
                    vm.TopicName = language_topic.TopicName;
                    if(!string.IsNullOrEmpty(language_topic.TopicDescription)) vm.TopicDescription = language_topic.TopicDescription;
                }
                model.HomeTopics.Add(vm);
            }
            var latest = _questionService.GetListForActive().Where(x => x.Latest).OrderByDescending(x => x.CreatedDate);
            if (latest != null && latest.Count() > 0)
            {
                foreach(var question in latest)
                {
                    var languageQuestion = question.Language_Questions.Where(x => x.Language.Code == languageCode).FirstOrDefault();
                    if (languageQuestion != null)
                    {
                        var questionVM = new FAQQuestionPageViewModel()
                        {
                            Id = question.Id,
                            QuestionName = languageQuestion.QuestionName,
                            ContentBrief = !string.IsNullOrEmpty(languageQuestion.ContentBrief) ? languageQuestion.ContentBrief : "",
                            CreatedDate = question.CreatedDate
                        };
                        model.Latest.Add(questionVM);
                    }
                }
            }
            
            return View(model);
        }

        public ActionResult SearchIndex(int langId = 2)
        {
            ViewBag.Meta = Config;
            ViewBag.Title = Resources.Resources.FAQ;
            var model = new FAQTopicSearchViewModel();
            return View(model);
        }

        public ActionResult SearchCategory(int topicId = 0, int langId = 2, string keyword="")
        {
            var languageCode = Cache.GetLang();
            ViewBag.Meta = Config;
            ViewBag.Title = Resources.Resources.FAQ;
            var model = new FAQTopicViewModel();
            var quickActions = _topicService.GetListForActive().Where(x => x.QuickAction).OrderBy(x => x.Order);
            if (topicId!= 0)
            {
                model.Id = topicId;
                var topic = _topicService.GetById(topicId);
                if (topic != null)
                {
                    var languageTopic = _language_TopicService.GetByTopic(topicId).Where(x => x.Language.Code == languageCode).FirstOrDefault();
                    if (languageTopic != null)
                    {
                        model.TopicName = languageTopic.TopicName;
                        model.TopicDescription = !string.IsNullOrEmpty(languageTopic.TopicDescription) ? languageTopic.TopicDescription : "";
                    }
                    if (topic.SubTopics != null && topic.SubTopics.Count > 0)
                    {
                        model.SubTopics = new List<FAQTopicViewModel>();
                        var topicList = topic.SubTopics.Where(x => x.DeletedAt == null && x.Active).OrderBy(x => x.Order);
                        foreach (var subTopic in topicList)
                        {
                            var languageSubTopic = subTopic.Language_Topic.Where(x => x.Language.Code == languageCode).FirstOrDefault();
                            var subTopicVM = new FAQTopicViewModel();
                            var questionList = subTopic.Questions.Where(x => x.DeletedAt == null && x.Active).OrderBy(x => x.Priority);
                            subTopicVM.TopicName = languageSubTopic.TopicName;
                            subTopicVM.Id = subTopic.Id;
                            if (questionList != null && questionList.ToList().Count > 0)
                            {
                                foreach (var question in questionList.Take(5))
                                {
                                    var language_question =
                                        question.Language_Questions
                                        .Where(x => x.Language.Code == languageCode)
                                        .FirstOrDefault();
                                    var questionVM =
                                        Mapper.Map<Tuple<Language_Question, Question>, QuestionViewModel>
                                        (new Tuple<Language_Question, Question>(language_question, question));
                                    subTopicVM.Questions.Add(questionVM);
                                }
                            }
                            if (subTopic.Image != null && subTopic.Image.FullPath != null)
                            {
                                subTopicVM.ImageUrl = subTopic.Image.FullPath;
                            }
                            model.SubTopics.Add(subTopicVM);
                        }
                    }
                    else
                    {
                        if (topic.Image != null && topic.Image.FullPath != null)
                        {
                            model.ImageUrl = topic.Image.FullPath;
                        }
                        if (topic.ParentTopic != null)
                        {
                            var languageParentTopic = topic.ParentTopic.Language_Topic.Where(x => x.Language.Code == languageCode).FirstOrDefault();
                            model.ParentTopic = new FAQTopicViewModel()
                            {
                                Id = topic.ParentTopic.Id,
                                TopicName = languageParentTopic.TopicName,
                                TopicDescription = !string.IsNullOrEmpty(languageParentTopic.TopicDescription) ? languageParentTopic.TopicDescription : ""
                            };
                        }
                        if (topic.Questions != null && topic.Questions.Count > 0)
                        {
                            var questionList = topic.Questions.Where(x => x.DeletedAt == null && x.Active).OrderBy(x => x.Priority);
                            model.QuestionList = new FAQSearchQuestionViewModel();
                            model.QuestionList.TotalNumberOfRecords = questionList.Count();
                            model.QuestionList.TotalPage = questionList.Count() % 4 == 0 ? questionList.Count() / 4 : questionList.Count() / 4 + 1;
                            model.QuestionList.CurrentPage = 1;
                            model.QuestionList.TopicId = topic.Id;
                            var questionPageListVM = new List<FAQQuestionPageViewModel>();
                            foreach (var question in questionList.Take(4))
                            {
                                var language_question =
                                    question.Language_Questions
                                    .Where(x => x.Language.Code == languageCode)
                                    .FirstOrDefault();
                                if(language_question != null)
                                {
                                    var questionPageVM = new FAQQuestionPageViewModel()
                                    {
                                        Id = question.Id,
                                        QuestionName = language_question.QuestionName,
                                        ContentBrief = !string.IsNullOrEmpty(language_question.ContentBrief) ? language_question.ContentBrief : "",
                                        TopicName = question.Topic.Language_Topic.Where(x => x.Language.Code == languageCode).FirstOrDefault().TopicName,
                                        ImageURL = question.Topic.Image != null ? question.Topic.Image.FullPath : "/webroot/images/faq/questions/pana.png",
                                    };
                                    questionPageListVM.Add(questionPageVM);
                                }
                            }
                            model.QuestionList.Questions = questionPageListVM;
                        }
                    }
                }
            }
            else if(keyword!="")
            {
                model.KeyWord = keyword;
            }
            return View(model);
        }
        public ActionResult SearchQuestion(FAQSearchQuestionViewModel model = null, string keyword = "", int pageSize = 4, int page = 0)
        {
            var langCode = Cache.GetLang();
            if(model == null)
            {
                model = new FAQSearchQuestionViewModel();
            }
            var questions = _language_QuestionService
                .GetByKeyword(keyword, langCode).ToList();
            if (questions != null && questions.Count > 0)
            {
                foreach(var language_question in questions.Take((page + 1) * pageSize))
                {
                    var questionId = language_question.QuestionId;
                    var question = _questionService.GetById(questionId);
                    if (question == null) continue;
                    var questionVM = new FAQQuestionPageViewModel()
                    {
                        Id = questionId,
                        QuestionName = language_question.QuestionName,
                        ContentBrief = !string.IsNullOrEmpty(language_question.ContentBrief) ? language_question.ContentBrief : "",
                        TopicName = question.Topic.Language_Topic.Where(x => x.Language.Code == langCode).FirstOrDefault().TopicName,
                        ImageURL = question.Topic.Image != null ? question.Topic.Image.FullPath : "/webroot/images/faq/questions/pana.png",
                    };
                    var normalizedQuestionName = RemoveDiacritics(language_question.QuestionName);
                    var normalizedKeyword = RemoveDiacritics(keyword);
                    if(normalizedQuestionName.IndexOf(normalizedKeyword) > -1)
                    {
                        var index = normalizedQuestionName.IndexOf(normalizedKeyword);
                        var questionNameLength = language_question.QuestionName.Length;
                        var newName = "";
                        if (index > 0 && index + keyword.Length < questionNameLength)
                        {
                            newName = $"{language_question.QuestionName.Substring(0, index)} <span class='textEmphasized'>{language_question.QuestionName.Substring(index, keyword.Length)}</span> {language_question.QuestionName.Substring(index + keyword.Length + 1)}";
                        }
                        else if (index == 0)
                        {
                            if (keyword.Length == questionNameLength)
                            {
                                newName = $"<span class='textEmphasized'>{language_question.QuestionName}</span>";
                            } else
                            {
                                newName = $"<span class='textEmphasized'>{language_question.QuestionName.Substring(index, keyword.Length)}</span> {language_question.QuestionName.Substring(index + keyword.Length + 1)}";
                            }
                        }
                        else if (index + keyword.Length == questionNameLength)
                        {
                            newName = $"{language_question.QuestionName.Substring(0, index)} <span class='textEmphasized'>{language_question.QuestionName.Substring(index, keyword.Length)}</span>";
                        }
                        questionVM.QuestionName = newName;
                    }
                    model.Questions.Add(questionVM);
                }
                model.TotalNumberOfRecords = questions.ToList().Count;
                model.CurrentPage = 1;
                model.TotalPage = questions.ToList().Count % 4 == 0 ? questions.ToList().Count / 4 : questions.ToList().Count / 4 + 1;
            }
            return View(model);
        }
        
        public ActionResult AutocompleteSearchQuestion(string keyword)
        {
            var langCode = Cache.GetLang();
            var questions = _language_QuestionService.GetByKeyword(keyword, langCode).ToList();
            return Json(questions, JsonRequestBehavior.AllowGet);
        }
        public ActionResult QuestionPage(int id, int langId = 2)
        {
            var languageCode = Cache.GetLang();
            ViewBag.Title = Resources.Resources.FAQ;
            var model = new FAQQuestionPageViewModel();
            var question = _questionService.GetById(id);
            if (question == null || question.DeletedAt != null)
            {
                return RedirectToAction("Index").WithError("Câu hỏi không tồn tại - EX_QUESTION_001");
            }
            var questionAttachments = _questionAttachmentService.GetByQuestion(id);
            var languageQuestion = _language_QuestionService.GetByQuestion(id).Where(x => x.Language.Code == languageCode).FirstOrDefault();
            var topics = _topicService.GetListForActive().Where(x => x.QuickAction).OrderBy(x => x.Order);
            var likedStatus = Session[$"Question_{id}"];
            if (languageQuestion == null)
            {
                return RedirectToAction("Index").WithError("Câu hỏi không tồn tại - EX_QUESTION_002");
            }
            if (question.Topic != null)
            {
                model.Topic.Id = question.Topic.Id;
                model.Topic.TopicName = question.Topic.Language_Topic.Where(x => x.Language.Id == langId).FirstOrDefault().TopicName;
                if(question.Topic.ParentTopic != null)
                {
                    var parentTopic = question.Topic.ParentTopic.Language_Topic.Where(x => x.Language.Id == langId).FirstOrDefault();
                    model.Topic.Parent = new TopicViewModel()
                    {
                        TopicName = parentTopic.TopicName,
                        Id = question.Topic.ParentTopic.Id
                    };
                }
            }
            var comments = _commentService.GetParentCommentByQuestion(id).OrderByDescending(x => x.CreatedDate);
            model.QuestionName = languageQuestion.QuestionName.Trim();
            model.Content = languageQuestion.Content;
            model.Id = question.Id;
            model.ActiveTopicId = question.TopicId;
            model.QuestionComment.TotalRecords = comments.ToList().Count;
            model.NumberOfLiked = question.NumberOfLiked;
            model.Liked = likedStatus != null && (bool)likedStatus;
            foreach(var item in questionAttachments)
            {
                var attachmentVM = Mapper.Map<QuestionAttachment, QuestionAttachmentViewModel>(item);
                if (item.Platform == 1) 
                {
                    model.MobileAttachments.Add(attachmentVM);   
                } 
                else 
                {
                    model.DesktopAttachments.Add(attachmentVM);
                }
            }
            
            foreach(var comment in comments.Take(4))
            {
                var commentVM = Mapper.Map<Comment, CommentViewModel>(comment);
                if (comment.Replies != null && comment.Replies.Count > 0)
                {
                    commentVM.Replies = new List<CommentViewModel>();
                    foreach(var reply in comment.Replies)
                    {
                        var replyVM = Mapper.Map<Comment, CommentViewModel>(reply);
                        commentVM.Replies.Add(replyVM);
                    }
                }
                model.QuestionComment.Comments.Add(commentVM);
            }
            return View(model);
        }

        [ChildActionOnly]
        public ActionResult FAQHeader()
        {
            var languageCode = Cache.GetLang();
            var model = new FAQHomeViewModel();
            var topics = _topicService
                .GetListForActive()
                .OrderBy(x => x.Order);
            var parents = topics.Where(x => x.ParentTopic == null);
            var quickSearches = _tagService.GetListOfQuickSearch(languageCode).OrderBy(x => x.Priority);
            model.QuickSearches = quickSearches.Select(x => x.TagName).Take(4).ToList();
            return PartialView(model);
        }

        [ChildActionOnly]
        public ActionResult QuickActionPartialView(int activeTopicId = 0, string questionName = "", int totalNumberOfRecords = 0)
        {
            var languageCode = Cache.GetLang();
            var model = new QuickActionViewModel();
            model.TotalNumberOfRecords = totalNumberOfRecords;
            var topics = _topicService
                .GetListForActive()
                .Where(x => x.QuickAction)
                .OrderBy(x => x.Order);
            var activeTopic = new Topic();
            if (activeTopicId != 0 ) activeTopic = _topicService.GetById(activeTopicId);
            var tags = _tagService.GetListOfPopular(languageCode).OrderBy(x => x.Priority);
            model.Tags = tags.Select(x => x.TagName).ToList();
            if (topics != null && topics.ToList().Count > 0)
            {
                foreach(var topic in topics)
                {
                    var topicVM = new FAQTopicViewModel()
                    {
                        TopicName = topic.Language_Topic.Where(x => x.Language.Code == languageCode).FirstOrDefault().TopicName,
                        Id = topic.Id,
                    };
                    model.Topics.Add(topicVM);
                    if (activeTopicId == topic.Id)
                    {
                        model.ActiveTopicId = topic.Id;
                    }
                }
                if (activeTopic != null)
                {
                    var activeLanguageTopic = activeTopic.Language_Topic.Where(x => x.Language.Code == languageCode).FirstOrDefault();
                    if(activeLanguageTopic != null)
                    {
                        var activeTopicVM = new TopicViewModel()
                        {
                            Id = activeTopic.Id,
                            TopicName = activeLanguageTopic.TopicName,
                            TopicDescription = !string.IsNullOrEmpty(activeLanguageTopic.TopicDescription) ? activeLanguageTopic.TopicDescription : "",
                        };
                        if (activeTopic.ParentTopic != null)
                        {
                            var parentVM = new TopicViewModel()
                            {
                                Id = activeTopic.ParentTopic.Id,
                                TopicName = activeTopic.ParentTopic.Language_Topic.Where(x => x.Language.Code == languageCode).FirstOrDefault().TopicName,
                            };
                            activeTopicVM.Parent = parentVM;
                        }
                        model.ActiveTopic = activeTopicVM;
                    }
                }
                if (!string.IsNullOrEmpty(questionName))
                {
                    model.QuestionName = questionName;
                }
            }
            return PartialView(model);
        }
        [ChildActionOnly]
        public ActionResult SearchQuestionResultPartialView(FAQSearchQuestionViewModel viewModel)
        {
            return PartialView(viewModel);
        }
        [HttpPost]
        public ActionResult GetMoreData(string keyword = "", int topicId = 0, int page = 1)
        {
            var langCode = Cache.GetLang();
            var questions = new List<Language_Question>();
            if (!string.IsNullOrEmpty(keyword)) questions = _language_QuestionService
                .GetByKeyword(keyword, langCode).ToList();
            if (topicId != 0) questions = _language_QuestionService
                    .GetList()
                    .Where(x => x.Question.Topic.Id == topicId)
                    .OrderBy(x => x.Question.Priority)
                    .ToList();

            var viewModel = new FAQSearchQuestionViewModel()
            {
                Keyword = keyword,
                CurrentPage = page,
                TotalPage = questions.Count % 4 == 0 ? questions.Count / 4 : questions.Count / 4 + 1,
                TotalNumberOfRecords = questions.Count,
                TopicId = topicId,
            };
            if (questions != null && questions.ToList().Count > 0)
            {
                viewModel.TotalNumberOfRecords = questions.ToList().Count;
                foreach (var language_question in questions.Skip((page - 1)* 4).Take(4))
                {
                    var questionId = language_question.QuestionId;
                    var question = _questionService.GetById(questionId);
                    if (question == null) continue;
                    var questionVM = new FAQQuestionPageViewModel()
                    {
                        Id = questionId,
                        QuestionName = language_question.QuestionName,
                        ContentBrief = !string.IsNullOrEmpty(language_question.ContentBrief) ? language_question.ContentBrief : "",
                        TopicName = question.Topic.Language_Topic.Where(x => x.Language.Code == langCode).FirstOrDefault().TopicName,
                        ImageURL = question.Topic.Image != null ? question.Topic.Image.FullPath : "/webroot/images/faq/questions/pana.png",
                    };
                    if(!string.IsNullOrEmpty(keyword))
                    {
                        var normalizedQuestionName = RemoveDiacritics(language_question.QuestionName);
                        var normalizedKeyword = RemoveDiacritics(keyword);
                        var index = normalizedQuestionName.IndexOf(normalizedKeyword);
                        var questionNameLength = language_question.QuestionName.Length;
                        var newName = language_question.QuestionName;
                        if (index > 0 && index + keyword.Length < questionNameLength)
                        {
                            newName = $"{language_question.QuestionName.Substring(0, index)} <span class='textEmphasized'>{language_question.QuestionName.Substring(index, keyword.Length)}</span> {language_question.QuestionName.Substring(index + keyword.Length + 1)}";
                        }
                        else if (index == 0)
                        {
                            if (keyword.Length == questionNameLength)
                            {
                                newName = $"<span class='textEmphasized'>{language_question.QuestionName}</span>";
                            }
                            else
                            {
                                newName = $"<span class='textEmphasized'>{language_question.QuestionName.Substring(index, keyword.Length)}</span> {language_question.QuestionName.Substring(index + keyword.Length + 1)}";
                            }
                        }
                        else if (index + keyword.Length == questionNameLength)
                        {
                            newName = $"{language_question.QuestionName.Substring(0, index)} <span class='textEmphasized'>{language_question.QuestionName.Substring(index, keyword.Length)}</span>";
                        }
                        questionVM.QuestionName = newName;
                    }
                    viewModel.Questions.Add(questionVM);
                }
            }
            return PartialView("SearchQuestionResultPartialView", viewModel);
        }
        [HttpPost]
        public JsonResult publicFormQuestion(FAQQuestionFormViewModel faqQuestionData)
        {
            var objMap = new PendingQuestion();
            if (string.IsNullOrEmpty(faqQuestionData.PhoneNumber) 
                || string.IsNullOrEmpty(faqQuestionData.FullName)
                || string.IsNullOrEmpty(faqQuestionData.Content))
            {
                var reponse = new
                {
                    Code = AjaxAlertConsts.WithError,
                    Msg = "Vui lòng nhập thông tin các trường có (*) (PQ-004)",
                };
                return Json(reponse, JsonRequestBehavior.AllowGet);
            }
            objMap.PhoneNumber = faqQuestionData.PhoneNumber.Trim();
            if(!string.IsNullOrEmpty(faqQuestionData.AccountNumber)) 
            objMap.AccountNumber = faqQuestionData.AccountNumber.Trim();
            objMap.FullName = faqQuestionData.FullName.Trim();
            if(!string.IsNullOrEmpty(faqQuestionData.PendingQuestionTitle)) objMap.PendingQuestionTitle = faqQuestionData.PendingQuestionTitle.Trim();
            objMap.Content = faqQuestionData.Content.Trim();
            objMap.CreatedDate = DateTime.Now;
            var FileName = "";

            var temp = new List<HttpPostedFileBase>();
            if(Request.Files.Count > 0)
            {
                foreach (string upload in Request.Files)
                {
                    temp.Add(Request.Files[upload]);
                }
                FileName = Path.GetFileNameWithoutExtension(temp[0].FileName);
                var FileExtension = Path.GetExtension(temp[0].FileName);
                if (!allowedFileExtensions.Contains(FileExtension))
                {
                    var reponse = new
                    {
                        Code = AjaxAlertConsts.WithError,
                        Msg = "Định dạng file đính kèm không chính xác. Vui lòng thử lại (PQ-003)",
                    };
                    return Json(reponse, JsonRequestBehavior.AllowGet);
                }
                if (temp[0].ContentLength > 2097152)
                {
                    var reponse = new
                    {
                        Code = AjaxAlertConsts.WithError,
                        Msg = "Vui lòng gửi file có dung lượng nhỏ hơn 2MB (PQ-002)",
                    };
                    return Json(reponse, JsonRequestBehavior.AllowGet);
                }
                var timestamp = Math.Floor(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds).ToString();

                FileName = $"{timestamp}-{objMap.FullName}-{objMap.PendingQuestionTitle}-{FileName.Trim()}{FileExtension}";
                var UploadPath = ConfigurationManager.AppSettings["CustomerUploadsPath"].ToString();
                var filePath = UploadPath + FileName;
                try
                {
                    temp[0].SaveAs(filePath);
                    objMap.AttachmentURL = FileName;
                } catch (Exception ex)
                {
                    HandleException("Message ", ex);
                    var reponse = new
                    {
                        Code = AjaxAlertConsts.WithError,
                        Msg = "Lỗi khi gửi câu hỏi. Vui lòng thử lại (PQ-001)",
                    };
                    return Json(reponse, JsonRequestBehavior.AllowGet);
                }
            }

            try
            {
                var latest = _pendingquestionService.Create(objMap);
                _pendingquestionService.SaveChange();
                SendMailToReviewPendingQuestion(
                    latest, 
                    objMap.FullName, 
                    objMap.PhoneNumber, 
                    objMap.AccountNumber,
                    objMap.PendingQuestionTitle,
                    objMap.Content,
                    FileName
                );
                var reponse = new
                {
                    Code = AjaxAlertConsts.WithSuccess,
                    Msg = "Cảm ơn Bạn đã tạo câu hỏi. TVSI sẽ liên hệ Bạn trong vòng 01 ngày làm việc. Chúc Bạn 1 ngày vui vẻ!",
                };
                return Json(reponse, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                HandleException("Message ", ex);
                var reponse = new
                {
                    Code = AjaxAlertConsts.WithError,
                    Msg = "Lỗi khi gửi câu hỏi. Vui lòng thử lại (PQ-006)",
                };
                return Json(reponse, JsonRequestBehavior.AllowGet);
            }
        }

        [ChildActionOnly]
        public ActionResult QuestionCommentPartialView(QuestionCommentViewModel viewModel)
        {
            return PartialView(viewModel);
        }

        [HttpGet]
        public ActionResult SubCategoriesExpanded(int topicId, int languageId = 2)
        {
            var parentTopic = _topicService.GetById(topicId);
            if (parentTopic == null)
            {

            }
            var languageParentTopic = parentTopic.Language_Topic.Where(x => x.Language.Id == languageId).FirstOrDefault();
            if (languageParentTopic == null)
            {

            }
            var model = new FAQTopicViewModel()
            {
                Id = parentTopic.Id,
                TopicName = languageParentTopic.TopicName
            };
            if (parentTopic.SubTopics != null && parentTopic.SubTopics.Count > 0)
            {
                model.SubTopics = new List<FAQTopicViewModel>();
                foreach(var subTopic in parentTopic.SubTopics.Where(x => x.DeletedAt == null && x.Active).OrderBy(x => x.Order))
                {
                    var languageSubTopic = subTopic.Language_Topic.Where(x => x.Language.Id == languageId).FirstOrDefault();
                    if (languageSubTopic == null) continue;
                    var subTopicVM = new FAQTopicViewModel()
                    {
                        Id = subTopic.Id,
                        TopicName = languageSubTopic.TopicName,
                    };
                    if (subTopicVM != null) model.SubTopics.Add(subTopicVM);
                }
            }
            return PartialView("_SubCategoriesExpanded", model);
        }
        [HttpPost]
        public ActionResult LoadMoreComments(int questionId, int limit = 4)
        {
            var model = new QuestionCommentViewModel()
            {
                QuestionId = questionId,
            };
            var comments = 
                _commentService
                    .GetParentCommentByQuestion(questionId)
                    .OrderByDescending(x => x.CreatedDate);
            model.TotalRecords = comments.ToList().Count;
            foreach (var comment in comments.Take(limit).ToList())
            {
                var commentVM = Mapper.Map<Comment, CommentViewModel>(comment);
                if (comment.Replies != null && comment.Replies.Count > 0)
                {
                    commentVM.Replies = new List<CommentViewModel>();
                    foreach (var reply in comment.Replies)
                    {
                        var replyVM = Mapper.Map<Comment, CommentViewModel>(reply);
                        commentVM.Replies.Add(replyVM);
                    }
                }
                model.Comments.Add(commentVM);
            }
            return PartialView("QuestionCommentPartialView", model);
        }

        [HttpPost]
        public ActionResult SendComment(
            FAQCommentFormViewModel viewModel
        )
        {
            try
            {
                var question = _questionService.GetById(viewModel.questionId);
                if (question == null)
                {
                    var reponse = new
                    {
                        Code = AjaxAlertConsts.WithError,
                        Msg = "Lỗi xảy ra khi gửi bình luận. Vui lòng thử lại (COMM-001)",
                    };
                    return Json(reponse, JsonRequestBehavior.AllowGet);
                }

                var newComment = new Comment()
                {
                    Content = viewModel.content,
                    CreatedBy = viewModel.name,
                    Question = question,
                    AdminCreated = false,
                    Approved = true,
                    CreatedDate = DateTime.Now
                };

                if(!string.IsNullOrEmpty(viewModel.PhoneNumber))
                {
                    newComment.PhoneNumber = viewModel.PhoneNumber;
                }
                
                var parentComment = _commentService.GetById(viewModel.parentId);
                
                if (parentComment != null)
                {
                    newComment.ParentCommentId = parentComment.Id;
                }

                var temp = new List<HttpPostedFileBase>();
                if (Request.Files.Count > 0)
                {
                    foreach (string upload in Request.Files)
                    {
                        temp.Add(Request.Files[upload]);
                    }
                    var FileName = Path.GetFileNameWithoutExtension(temp[0].FileName);
                    var FileExtension = Path.GetExtension(temp[0].FileName);
                    if (!allowedFileExtensions.Contains(FileExtension))
                    {
                        var reponse = new
                        {
                            Code = AjaxAlertConsts.WithError,
                            Msg = "Định dạng file đính kèm không chính xác. Vui lòng thử lại (COMM-003)",
                        };
                        return Json(reponse, JsonRequestBehavior.AllowGet);
                    }
                    if (temp[0].ContentLength > 2097152)
                    {
                        var reponse = new
                        {
                            Code = AjaxAlertConsts.WithError,
                            Msg = "Vui lòng gửi file có dung lượng nhỏ hơn 2MB (COMM-002)",
                        };
                        return Json(reponse, JsonRequestBehavior.AllowGet);
                    }
                    var timestamp = Math.Floor(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds).ToString();

                    FileName = $"{timestamp}-{viewModel.questionId}-Comment-{viewModel.name}-{FileName.Trim()}{FileExtension}";
                    var UploadPath = ConfigurationManager.AppSettings["CustomerUploadsPath"].ToString();
                    //string UploadPath = "D:/workspace/tvsi_websites_webui/TTCMS.Web/Content/CustomerUploads/";
                    var filePath = UploadPath + FileName;
                    try
                    {
                        temp[0].SaveAs(filePath);
                        newComment.AttachmentURL = FileName;
                        
                    }
                    catch (Exception ex)
                    {
                        HandleException("Message ", ex);
                        var reponse = new
                        {
                            Code = AjaxAlertConsts.WithError,
                            Msg = "Lỗi khi gửi bình luận. Vui lòng thử lại (COMM-004)",
                        };
                        return Json(reponse, JsonRequestBehavior.AllowGet);
                    }
                }

                var createdComment = _commentService.Create(newComment);
                if (createdComment == null)
                {
                    var reponse = new
                    {
                        Code = AjaxAlertConsts.WithError,
                        Msg = "Lỗi khi gửi bình luận. Vui lòng thử lại (COMM-005)",
                    };
                    return Json(reponse, JsonRequestBehavior.AllowGet);
                }
                _commentService.SaveChange();
                var questionName = question.Language_Questions.Where(x => x.LanguageId == 2).FirstOrDefault().QuestionName;
                SendMailForCommentNotification(viewModel.questionId, questionName, viewModel.name, viewModel.PhoneNumber);
                return Json(new { success = true, responseText = "Succesfully created a new comment", CommentedBy = viewModel.name, Content = viewModel.content }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                HandleException("Message", ex);
                return Json(new { success = false, responseText = ex }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult AjaxAlert(string code = "", string msg = "")
        {
            var model = new AjaxAlertViewModel();
            if (code != "" && msg != "")
            {
                model.Code = code;
                model.Msg = msg;
            }
            return PartialView(model);
        }

        public ActionResult BotMenu()
        {
            var languageCode = Cache.GetLang();
            string _Cache_BotMenu = "BotMenu";
            IList<Menu> m_menu = new List<Menu>();

            if (!Cache.IsSet(_Cache_BotMenu))
            {
                m_menu = menuService.GetList(MenuGroupType.BotMenu, languageCode).Where(x => x.IsActived).OrderBy(x => x.Order).ToArray();
                foreach (var item in m_menu)
                {
                    item.Link = getLinkMenu(item.TextType, item.Link, item.WithId ?? 0, item.Action, item.Controller);
                }
                Cache.Set(_Cache_BotMenu, m_menu);
            }
            else
            {
                m_menu = Cache.Get(_Cache_BotMenu) as Menu[];
            }
            ViewBag.Contact = Config.contactus_setting;
            return PartialView(m_menu);
        }

        [HttpPost]
        public JsonResult LikePost(int questionId)
        {
            var question = _questionService.GetById(questionId);
            if(question == null)
            {
                return Json(new { }, JsonRequestBehavior.AllowGet);
            }
            var likedStatus = Session[$"Question_{questionId}"];
            if (likedStatus != null && likedStatus is bool && (bool)likedStatus)
            {
                var reponse = new
                {
                    Code = AjaxAlertConsts.WithWarning,
                    Msg = "Bạn đã thích bài viết trước đây",
                };
                return Json(reponse, JsonRequestBehavior.AllowGet);
            }
            try{
                Session[$"Question_{questionId}"] = true;
                question.NumberOfLiked = question.NumberOfLiked + 1;
                _questionService.Update(question);
                _questionService.SaveChange();
                var reponse = new
                {
                    Code = AjaxAlertConsts.WithSuccess,
                    Msg = "Cảm ơn Bạn đã thích bài viết này",
                    Id = questionId,
                    NumberOfLiked = question.NumberOfLiked
                };
                return Json(reponse, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex) 
            {
                var reponse = new
                {
                    Code = AjaxAlertConsts.WithError,
                    Msg = "Lỗi hệ thống. Vui lòng thử lại (Q-002)",
                };
                return Json(reponse, JsonRequestBehavior.AllowGet);
            }
        }
        public static void SendMailToReviewPendingQuestion(
            int id,
            string fullName,
            string phone,
            string accountNo,
            string title,
            string questionContent,
            string attachment = "")
        {
            var templateFile = templateMailFolderPath + @"Mail_FAQ_NewQuestion.html";

            var content = "";
            using (var readFile = new FileStream(templateFile, FileMode.OpenOrCreate, FileAccess.Read, FileShare.Read))
            {
                using (var readFileStr = new StreamReader(readFile))
                {
                    content = readFileStr.ReadToEnd();
                }
                if (string.IsNullOrEmpty(content))
                {

                }

                content = content.Replace("{FullName}", fullName ?? "");
                content = content.Replace("{Phone}", phone ?? "");
                content = content.Replace("{AccountNo}", accountNo ?? "");
                content = content.Replace("{Title}", title ?? "");
                content = content.Replace("{Content}", questionContent ?? "");
                content = content.Replace("{Attachment}", attachment ?? "");
                content = content.Replace("{ID}", id.ToString());
                TVSI.DAL.SystemNotificationDB.ActionQuery.TVSI_sHANG_DOI_EMAIL_INSERT("Câu hỏi mới trên FAQ", content, "dvkh1@tvsi.com.vn", 0);
            }
        }

        public static void SendMailForCommentNotification(
            int questionId,
            string questionName,
            string fullName,
            string phone)
        {
            var templateFile = templateMailFolderPath + @"Mail_FAQ_NewComment.html";
            var content = "";
            using (var readFile = new FileStream(templateFile, FileMode.OpenOrCreate, FileAccess.Read, FileShare.Read))
            {
                using (var readFileStr = new StreamReader(readFile))
                {
                    content = readFileStr.ReadToEnd();
                }
                if (string.IsNullOrEmpty(content))
                {

                }
                content = content.Replace("{QuestionId}", questionId.ToString() ?? "");
                content = content.Replace("{QuestionName}", questionName ?? "");
                content = content.Replace("{FullName}", fullName ?? "");
                content = content.Replace("{Phone}", phone ?? "");
                TVSI.DAL.SystemNotificationDB.ActionQuery.TVSI_sHANG_DOI_EMAIL_INSERT("Bình luận mới trên FAQ", content, "dvkh1@tvsi.com.vn", 0);
            }
        }
    }
}