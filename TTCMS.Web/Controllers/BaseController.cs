﻿using AutoMapper;
using System;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using TTCMS.Core;
using TTCMS.Core.Interfaces;
using TTCMS.Data;
using TTCMS.Data.Entities;
using TTCMS.Web.Models;
using TTCMS.Resources;
using log4net;
using System.Reflection;
using System.Text;
using System.Globalization;

namespace TTCMS.Web.Controllers
{
    [AllowAnonymous]
    public class BaseController : Controller
    {
        protected readonly ICacheHelper Cache;
        protected string cultureName = "vi-VN";
        protected ConfigViewModel Config = new ConfigViewModel();
        protected const string CacheLanguageCode = "LanguageCode";
        private static readonly ILog logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private const string CacheSystemConfig = "SystemConfig";
        private readonly TTCMSDbContext db = new TTCMSDbContext();
        protected BaseController() : this(new CacheHelper()) {
            if (System.Web.HttpContext.Current.Session["Language"] != null)
            {
                cultureName = System.Web.HttpContext.Current.Session["Language"].ToString();
                Resources.Resources.Culture = new System.Globalization.CultureInfo(cultureName);
            }
            else
            {
                System.Web.HttpContext.Current.Session["Language"] = "vi-VN";
                Resources.Resources.Culture = new System.Globalization.CultureInfo("vi-VN");
            }
        }
        protected BaseController(ICacheHelper cacheHelper)
        {
           
            // TODO: Complete member initialization
            this.Cache = cacheHelper;
        }
        protected string RemoveDiacritics(string s)
        {
            string normalizedString = null;
            StringBuilder stringBuilder = new StringBuilder();
            normalizedString = s.Normalize(NormalizationForm.FormD);
            int i = 0;
            char c = '\0';

            for (i = 0; i <= normalizedString.Length - 1; i++)
            {
                c = normalizedString[i];
                if (CharUnicodeInfo.GetUnicodeCategory(c) != UnicodeCategory.NonSpacingMark)
                {
                    stringBuilder.Append(c);
                }
            }

            return stringBuilder.ToString().ToLower();
        }
        protected override IAsyncResult BeginExecuteCore(AsyncCallback callback, object state)
        {
            //binding language dropdownlist
            ////get system name from cache
            try
            {
                var configuration = new ConfigurationViewModel();

                if (!Cache.IsSet(CacheSystemConfig))
                {
                    var temp = db.Settings.FirstOrDefault();
                    ConfigurationViewModel model = Mapper.Map<Setting, ConfigurationViewModel>(db.Settings.FirstOrDefault());
                    var lang_setting = db.Language_Settings.FirstOrDefault(); //db.Language_Settings.SingleOrDefault(x => x.LanguageId == cultureName && x.SettingId == model.Id);
                    model.ApplicationName = lang_setting.ApplicationName;
                    model.Description = lang_setting.Description;
                    model.Keywords = lang_setting.Keywords;
                    model.Page_Footer = lang_setting.Page_Footer;
                    Cache.Set(CacheSystemConfig, model);
                    configuration = model;
                }
                else
                {
                    configuration = Cache.Get(CacheSystemConfig) as ConfigurationViewModel;
                }

                if (Cache.IsSet(CacheLanguageCode))
                {
                    cultureName = Cache.Get(CacheSystemConfig) as string;
                }

                Config = Mapper.Map<ConfigurationViewModel, ConfigViewModel>(configuration);
                return base.BeginExecuteCore(callback, state);
            }
            catch (Exception ex)
            {
                var a = ex.Message;
                throw;
            }
        }
        public string getLinkMenu(string type, string Link, int WithId, string Action, string Controller)
        {
            string Strlink = "";
            switch (type)
            {
                case "Custom Link":
                    Strlink = Link;
                    break;
                case "Page":
                    Strlink = Url.Action(Action, Controller, new { Id = WithId, alias = Link });
                    break;
                case "Download":
                    Strlink = Url.Action(Action, Controller, new { Id = WithId, alias = Link });
                    break;
                case "Custom Action":
                    Strlink = Url.Action(Action, Controller);
                    break;
                case "Product Category":
                    Strlink = Url.Action(Action, Controller, new { Id = WithId, alias = Link });
                    break;
                case "News Category":
                    Strlink = Url.Action(Action, Controller, new { Id = WithId, alias = Link });
                    break;
            }
            return Strlink;
        }
        protected string RenderPartialViewToString(string viewName, object model = null)
        {
            if (string.IsNullOrEmpty(viewName))
            {
                viewName = ControllerContext.RouteData.GetRequiredString("action");
            }

            ViewData.Model = model;

            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }
        protected void HandleException(string message, Exception ex)
        {
            logger.Error(message + "::" + ex.Message);
            RedirectToAction("Index", "Home");
        }
    }
}
