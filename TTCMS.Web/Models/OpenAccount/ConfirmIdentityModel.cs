﻿namespace TTCMS.Web.Models.OpenAccount
{
    public class ConfirmIdentityModel
    {
        public byte[] CamFront { get; set; }
        public byte[] CamBack { get; set; }
    }

    public class RegisterOpenAccountModel
    {
        public int? type { get; set; } = null;

    }
}