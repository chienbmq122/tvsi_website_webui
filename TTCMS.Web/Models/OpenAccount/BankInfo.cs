﻿using System.ComponentModel;

namespace TTCMS.Web.Models.OpenAccount
{
    public class BankInfo
    {
        
    }
    public class BankModel
    {
        [Description("Mã ngân hàng")]
        public string BankNo { get; set; }
        [Description("Tên ngân hàng")]
        public string ShortName { get; set; }
    }
    public class SubBranchModel
    {
        public string BranchNo { get; set; }
        public string ShortBranchName { get; set; }
    }
}