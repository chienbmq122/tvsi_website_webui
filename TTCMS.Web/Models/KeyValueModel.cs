﻿namespace TTCMS.Web.Models
{
    public class KeyValueModel
    {
        public KeyValueModel()
        {
            Id = 0;
            Value = "";
        }

        public KeyValueModel(int id, string value)
        {
            Id = id;
            Value = value;
        }

        public int Id { get; set; }
        public string Value { get; set; }
    }
}