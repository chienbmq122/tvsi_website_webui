﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TTCMS.Web.Models
{
    public class ImageModel
    {
        public String PathImage { get; set; }
        public String FolderImage { get; set; }
        public String Title { get; set; }
    }
}