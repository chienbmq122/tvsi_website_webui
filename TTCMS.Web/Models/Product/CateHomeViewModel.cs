﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TTCMS.Data.Entities;

namespace TTCMS.Web.Models.Product
{
    public class CateHomeViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Keywords { get; set; }
        public string Route { get; set; }
        public int? ParentID { get; set; }
        public string Target { get; set; }
        public string Img_Icon { get; set; }
        public string Img_Thumbnail { get; set; }
        public CategoryType CategoryType { get; set; }
        public int Order { get; set; }
        public string CssClass { set; get; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
        public bool IsHome { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public string LanguageId { get; set; }
        public string CodeColor { get; set; }
        public int ShowProduct { get; set; }
        public int CountProduct { get; set; }
        public bool ActiveClass { get; set; }
        public List<ProductHomeViewModel> ListProductNews { get; set; }
        public List<ProductHomeViewModel> ListProductHot { get; set; }
        public List<ProductHomeViewModel> ListProductPrice { get; set; }
        public List<CateHomeViewModel> ListParent { get; set; }
    }
    
    public class ProductHomeViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string MaSP { get; set; }
        public string Summary { get; set; }
        public string Body { get; set; }
        public double GiaBan { get; set; }
        public double GiaKM { get; set; }
        public string Route { get; set; }
        public string Description { get; set; }
        public string Keywords { get; set; }
        public string Tag { get; set; }
        public int Views { get; set; }
        public int Order { get; set; }
        public string Img_Icon { get; set; }
        public string Img_Thumbnail { get; set; }
        public string CssClass { set; get; }
        public bool IsActive { get; set; }
        public bool IsHot { get; set; }
        public bool IsBanChay { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime Published { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public string LanguageId { get; set; }
        public int CategoryId { get; set; }
        public int DaMua { get; set; }
        public double Percent
        {
            get { return Math.Round(Convert.ToDouble(100 - ((GiaKM * 100) / GiaBan))); }
        }
        public double Price
        {
            get { return GiaKM > 0 ? GiaKM : GiaBan; }
        }
    }
}