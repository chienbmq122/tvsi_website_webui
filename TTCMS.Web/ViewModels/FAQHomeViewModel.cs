﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TTCMS.Web.Areas.TT_Admin.Models;

namespace TTCMS.Web.ViewModels
{
    public class FAQHomeViewModel
    {
        public FAQHomeViewModel()
        {
            FAQs = new List<FAQObject>();
            HomeTopics = new List<FAQTopicViewModel>();
            Latest = new List<FAQQuestionPageViewModel>();
        }
        public int  TotalFAQ { get; set; }
        public List<FAQObject> FAQs { get; set; }
        public List<FAQQuestionPageViewModel> Latest { get; set; }
        public List<FAQTopicViewModel> HomeTopics { get; set; }
        public FAQQuestionFormViewModel formVM { get; set; }
        public List<string> QuickSearches { get; set; }
        public string FullName { get; set; }
        public string PhoneNumber { get; set; }
        public string AccountNumber { get; set; }
        public string PendingQuestionTitle { get; set; }
        public string Content { get; set; }
        public string FilePath { get; set; }
        public HttpPostedFile File { get; set; }
    }
    public class FAQTopicViewModel
    {
        public FAQTopicViewModel()
        {
            CurrentPage = 1;
            Questions = new List<QuestionViewModel>();
            HomeViewModel = new FAQHomeViewModel();
        }
        //public QuickActionViewModel QuickAction { get; set; }
        public int Id { get; set; }
        public string KeyWord { get; set; }
        public string TopicName { get; set; }
        public string TopicDescription { get; set; }
        public int Order { get; set; }
        public int CurrentPage { get; set; }
        public int TotalPage { get; set; }
        public string ImageUrl { get; set; }
        public FAQTopicViewModel ParentTopic { get; set; }
        public List<FAQTopicViewModel> SubTopics { get; set; }
        public List<QuestionViewModel> Questions { get; set; }
        public FAQHomeViewModel HomeViewModel { get; set; }
        public FAQSearchQuestionViewModel QuestionList { get; set; }
    }
    public class FAQSearchQuestionViewModel
    {
        public FAQSearchQuestionViewModel()
        {
            Questions = new List<FAQQuestionPageViewModel>();
            HomeViewModel = new FAQHomeViewModel();
        }
        public string Keyword { get; set; }
        public int TopicId { get; set; }
        public int TotalPage { get; set; }
        public List<FAQQuestionPageViewModel> Questions { get; set; }
        public int TotalNumberOfRecords { get; set; }
        [HiddenInput]
        public int CurrentPage { get; set; }
        public int PageSize { get; set; }
        public FAQHomeViewModel HomeViewModel { get; set; }
    }

    public class QuestionCommentViewModel
    {
        public QuestionCommentViewModel()
        {
            Comments = new List<CommentViewModel>();
            CurrentCommentPage = 1;
        }
        public int TotalRecords { get; set; }
        public int CurrentCommentPage { get; set; }
        public int QuestionId { get; set; }
        public List<CommentViewModel> Comments { get; set; }
    }
    public class CommentViewModel
    {
        public CommentViewModel()
        {

        }
        public int Id { get; set; }
        public string CreatedBy { get; set; }
        [AllowHtml]
        public string Content { get; set; }
        public string AttachmentURL { get; set; }
        public bool AdminCreated { get; set; }
        public string PhoneNumber { get; set; }
        public List<CommentViewModel> Replies { get; set; }
        public int ParentCommentId { get; set; }
    }
    public class FAQQuestionPageViewModel
    {
        public FAQQuestionPageViewModel()
        {
            DesktopAttachments = new List<QuestionAttachmentViewModel>();
            MobileAttachments = new List<QuestionAttachmentViewModel>();
            QuestionComment = new QuestionCommentViewModel();
            HomeViewModel = new FAQHomeViewModel();
            Topic = new TopicViewModel();
        }
        public int Id { get; set; }
        public string TopicName { get; set; }
        public TopicViewModel Topic { get; set; }
        public string ImageURL { get; set; }
        public string QuestionName { get; set; }
        public string ContentBrief { get; set; }
        public string Content { get; set; }
        public int ActiveTopicId { get; set; }
        public bool Liked { get; set;}
        public int NumberOfLiked { get; set; }
        public DateTime CreatedDate { get; set; }
        public List<QuestionAttachmentViewModel> DesktopAttachments { get; set; }
        public List<QuestionAttachmentViewModel> MobileAttachments { get; set; }
        public LanguageViewModel Language { get; set; }
        public QuestionCommentViewModel QuestionComment { get; set; }
        public FAQHomeViewModel HomeViewModel { get; set; }
    }
    public class QuickActionViewModel
    {
        public QuickActionViewModel()
        {
            Topics = new List<FAQTopicViewModel>();
        }
        public List<FAQTopicViewModel> Topics { get; set; }
        public int ActiveTopicId { get; set; }
        public List<string> Tags { get; set; }
        public string ActiveTag { get; set; }
        public TopicViewModel ActiveTopic { get; set; }
        public string QuestionName { get; set; }
        public int TotalNumberOfRecords { get; set; } 
    }
}