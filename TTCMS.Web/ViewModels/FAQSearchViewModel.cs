﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TTCMS.Web.ViewModels
{
    public class FAQTopicSearchViewModel
    {
        public FAQTopicSearchViewModel()
        {
            QuickAction = new QuickActionViewModel();
            Topics = new List<FAQTopicViewModel>();
        }
        public QuickActionViewModel QuickAction { get; set; }
        public List<FAQTopicViewModel> Topics { get; set; }
    }
}