﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TTCMS.Web.ViewModels
{
    public class MarginViewModel
    {
        public string CustName { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public string Description { get; set; }
    }
}