﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TTCMS.Web.ViewModels
{
    public class FAQQuestionFormViewModel
    {
        public string FullName { get; set; }
        public string PhoneNumber { get; set; }
        public string AccountNumber { get; set; }
        public string PendingQuestionTitle { get; set; }
        public string Content { get; set; }
        public string FilePath { get; set; }
        public HttpPostedFile File { get; set; }
    }

    public class FAQCommentFormViewModel
    {
        public int questionId { get; set; }
        public string content { get; set; }
        public string name { get; set; }
        public int parentId { get; set; }
        public string PhoneNumber { get; set; }
        public HttpPostedFile File { get; set; }
        public string FilePath { get; set; }
    }
}