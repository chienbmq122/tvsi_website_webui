using System;
using System.ComponentModel.DataAnnotations;

namespace TTCMS.Web.ViewModels
{
    public class CategoryItemViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        
        [Required(ErrorMessage = "Name is required.")]
        public string Name { get; set; }        
        public string Keywords { get; set; }
        public string Slug { get; set; }
        public int CreatedBy { get; set; }
        public int UpdatedBy { get; set; }
        public bool IsRedirect { get; set; }
        public bool IsInNewsList { get; set; }
        public string RedirectUrl { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public DateTime? DeletedAt { get; set; }
        public LanguageViewModel Language { get; set; }

    }
}
