﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using TTCMS.Data.Entities;

namespace TTCMS.Web.ViewModels
{
    public class CategoryViewModel
    {
        public int CategoryId { get; set; }
        public string Name { get; set; }
        public string Icon { get; set; }
        public string Description { get; set; }
        public int? ParentID { get; set; }
        public string Target { get; set; }
        public string Thumbnail { get; set; }
        public int Order { get; set; }
        public bool IsActive { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public SelectList Lang { get; set; }
        public CategoryLayout Layout { get; set; }
        public SelectList DrpLayouts { get; set; }
        public SelectList DrpCategories { get; set; }
        public CategoryItemViewModel CategoryItem { get; set; }
        public List<CategoryViewModel> ChildCategories { get; set; }
        public List<CategoryItemViewModel> CategoryItems { get; set; }
    }
}