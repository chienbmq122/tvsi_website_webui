﻿using System.Collections.Generic;

namespace TTCMS.Web.ViewModels
{
    public class MenuViewModel
    {
        public MenuViewModel()
        {
            Children = new List<MenuViewModel>();
        }

        public int Id { get; set; }
        public int ParentID { get; set; }
        public string MenuId { get; set; }
        public string Text { get; set; }
        public string Link { get; set; }
        public string Target { set; get; }
        public string CssClass { set; get; }
        public string Active { get; set; }
        public string Icon { get; set; }
        public int Order { get; set; }
        public List<MenuViewModel> Children { get; set; }
    }

    public class HeaderMenuViewModel
    {
        public HeaderMenuViewModel()
        {
            MaxLengths = new List<int>();
            HeaderMenu = new List<MenuViewModel>();
            HeaderTopMenu = new List<MenuViewModel>();
        }
        public string iFinDataAPI { get; set; }
        public string LanguageCode { get; set; }
        public int SearchLanguageId { get; set; }
        public List<int> MaxLengths { get; set; }
        public List<MenuViewModel> HeaderTopMenu { get; set; }
        public List<MenuViewModel> HeaderMenu { get; set; }
        public List<LanguageViewModel> Languages { get; set; }
    }
}