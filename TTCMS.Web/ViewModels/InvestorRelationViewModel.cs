﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TTCMS.Data.Entities.WebsiteNews;

namespace TTCMS.Web.ViewModels
{
    public class InvestorRelationViewModel
    {
        public InvestorRelationViewModel()
        {
            Data = new List<TVSI_THONG_TIN_TVSI>();
            TotalNumberOfPage = 0;
            CurrentPage = 0;
        }

        public List<TVSI_THONG_TIN_TVSI> Data { get; set; }
        public int TotalNumberOfPage { get; set; }
        public int CurrentPage { get; set; }
    }
    public class EnterpriseManagementViewModel
    {
        public EnterpriseManagementViewModel()
        {
            Data = new TVSI_THONG_TIN_TVSI();
        }
        public TVSI_THONG_TIN_TVSI Data { get; set; }
    }
}