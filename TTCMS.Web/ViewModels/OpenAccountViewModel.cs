﻿namespace TTCMS.Web.ViewModels
{
    public class OpenAccountViewModel
    {
        public int? type { get; set; } = null;
        public string hoten { set; get; }
        public string ngaysinh { set; get; }
        public int gioitinh { set; get; }
        public string cmnd { set; get; }
        public string ngaycapcmnd { set; get; }
        public string noicap { set; get; }
        public string diachi { set; get; }
        public string Province { get; set; }
        public string ProvinceValue { get; set; }
        public string Distric { get; set; }
        public string Ward { get; set; }
        public string AddressDetail { get; set; }
        public string Note { get; set; } // yêu cầu thêm
        public string yeucau { get; set; } // yêu cầu thêm
        public string sale_id { set; get; }
        public int QA1txt { get; set; }
        public string Phone_01 { get; set; }
        public string Phone_02 { get; set; }
        public string email { set; get; }
        public bool RegisterMargin { get; set; }
        public bool TradeMethodPhone { get; set; }
        public bool TradeMethodInternet { get; set; }
        public bool TradeResultSMS { get; set; } = false;
        public bool TradeResultEmail { get; set; }

        public bool MonthlyReportMail { get; set; }
        public bool RegistChannelInfo { get; set; }
        public bool BankTransfer { get; set; } // BankTransfer or AccountTransfer

        public bool RegistChannelDirect { get; set; }
        public bool RegistItradeHome { get; set; }
        public bool MonthlyReportEMail { get; set; }
        public int CustomerRelaUsa { get; set; } = 0; // doi tuong cu tru tai hoa ky.
        public int CustomerUSA { get; set; } = 0;  // KH là công dân hoa kỳ hoặc đối tượng cư trú

        public int PlaceOfBirthUSA { get; set; } = 0; // nơi sinh tại hoa kỳ
        public int DepositoryUSA { get; set; } = 0;  // lưu ký tại hoa kỳ
        public int CustomerPhoneNumberUSA { get; set; }  = 0;  // KH có sđt tại hoa kỳ
        public int CustomerTransferBankUSA { get; set; } = 0;   // KH có lệnh định kỳ chuyển khoản
        public int CustomerAuthorizationUSA { get; set; } = 0; // KH có giấy ủy quyên từ Hoa Kỳ
        public int CustomerReceiveMailUSA { get; set; } = 0; // KH Có sử dụng địa chỉ nhận thư hộ hoặc giữ thư tại hoa kỳ
        
        public string BankAccName_01 { get; set; }
        public string BankAccNo_01 { get; set; }
        public string BankNo_01 { get; set; }
        public string SubBranchNo_01 { get; set; }
        
        public string BankAccName_02 { get; set; }
        public string BankAccNo_02 { get; set; }
        public string BankNo_02 { get; set; }
        public string SubBranchNo_02 { get; set; }

        public string Branch_Transaction { get; set; }
        public string SubBranchName_01 { get; set; }
        public string BankName_01 { get; set; }
        public string BankName_02 { get; set; }
        public string SubBranchName_02 { get; set; }
        
        public string City_01 { get; set; }
        public string City_02 { get; set; }
        public string Bank { get; set; }

        
        public string BankMethod
        {
            get
            {
                var ret = "";
                if (Bank == "remittance")
                {
                    ret = "1";
                }else
                    ret = "2";

                return ret;
            }
        }
        public string TradeMethod
        {
            get
            {
                var ret = "";
                if (TradeMethodPhone)
                    ret += "1";

                if (TradeMethodInternet)
                    ret += "2";
                return ret;
            }
        }

        public string TradeResult
        {
            get
            {
                var ret = "";
                if (TradeResultSMS)
                    ret += "1";

                if (TradeResultEmail)
                    ret += "2";

                return ret;
            }
        }
        public string MonthlyReport
        {
            get
            {
                var ret = "";
                if (MonthlyReportMail)
                    ret += "1";

                if (MonthlyReportEMail)
                    ret += "2";
                return ret;
            }
        }
        public string ChannelInfoMethod
        {
            get
            {
                var ret = "";

                if (RegistChannelDirect)
                    ret += "1";

                if (RegistItradeHome)
                    ret += "2";

                return ret;
            }
        }
        public string MarginAccount
        {
            get
            {
                var ret = string.Empty;
                if (RegisterMargin)
                {
                    ret += "1";
                }

                return ret;
            }
        }
    }
}