﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TTCMS.Data.Entities;
using TTCMS.Data.Entities.WebsiteNews;

namespace TTCMS.Web.ViewModels
{
    public class NewsViewItemModel
    {
        //public List<NewsItem> Newest { get; set; }
        //public List<NewsItem> Hotest { get; set; }
        //public NewsItem News { get; set; }
        public List<NewsInfo> Newest { get; set; }
        public List<NewsInfo> Hotest { get; set; }
        public stock_News News { get; set; }
        public string RedirectPageUrl { get; set; }
        public string RedirectPage { get; set; }
    }
}