﻿using System.Collections.Generic;
using TTCMS.Data.Entities;
using TTCMS.Web.Models;

namespace TTCMS.Web.ViewModels
{
    public class HomeViewModel
    {
        public HomeViewModel()
        {
            SlideUrls = new List<KeyValueModel>();
            InvestmentFormTitle = new KeyValueModel();
            InvestmentFormItems = new List<HomeObject>();
            QuickAccessItems = new List<HomeObject>();
            TradingTool = new HomeObject();
            FeaturedNews = new HomeObject();
            TradingToolImages = new List<KeyValueModel>();
            AnalysisCenter = new HomeObject();
            AnalysisCenterItem = new List<HomeObject>();
            FAQs = new List<FAQObject>();
            HotestNews = new List<NewsInfo>();
        }

        public List<KeyValueModel> SlideUrls { get; set; }
        public KeyValueModel InvestmentFormTitle { get; set; }
        public List<HomeObject> InvestmentFormItems { get; set; }
        public List<HomeObject> QuickAccessItems { get; set; }
        public HomeObject TradingTool { get; set; }
        public HomeObject FeaturedNews { get; set; }
        public List<KeyValueModel> TradingToolImages { get; set; }
        public HomeObject AnalysisCenter { get; set; }
        public List<HomeObject> AnalysisCenterItem { get; set; }
        public List<FAQObject> FAQs { get; set; }
        public List<NewsInfo> HotestNews { get; set; }
        public int LanguageId { get; set; }
    }

    public class FAQObject {
        public string Question { get; set; }
        public string Answer { get; set; }
    }

    public class HomeObject
    {
        public int Id { get; set; }
        public KeyValueModel Title { get; set; }
        public KeyValueModel Description { get; set; }
        public KeyValueModel ImageUrl { get; set; }
        public KeyValueModel RedirectUrl { get; set; }
        public KeyValueModel AppStoreUrl { get; set; }
        public KeyValueModel GooglePlayUrl { get; set; }
        public KeyValueModel ButtonText { get; set; }
    }
}
