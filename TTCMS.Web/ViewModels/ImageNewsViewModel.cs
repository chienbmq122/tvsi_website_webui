﻿using System.Collections.Generic;

namespace TTCMS.Web.ViewModels
{
    public class ImageNewsViewModel
    {
        public IEnumerable<string> Images { get; set; }
    }
    public class GalleryViewModel
    {
        public IEnumerable<string> Images { get; set; }
        public List<NewsInfo> Newest { get; set; }
        public int TotalPageNum { get; set; }
        public int CurrentPage { get; set; }
        public int CategoryItemId { get; set; }
        public string FolderName { get; set; }
        public string Title { get; set; }
        public string RedirectPage { get; set; }
        public string RedirectPageURL { get; set; }
    }
}