namespace TTCMS.Web.ViewModels.OpenAccount
{
    public class ResultDataViewModel<T>
    {
        public bool Status { get; set; }

        public T Data { get; set; }

        public int Type { get; set; }
    }
}