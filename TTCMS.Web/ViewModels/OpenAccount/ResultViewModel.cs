namespace TTCMS.Web.ViewModels.OpenAccount
{
    public class ResultViewModel
    {
        public bool Status { get; set; }

        public string Message { get; set; }

        public int Type { get; set; }
    }
}