﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TTCMS.Data.Entities;
using TTCMS.Data.Entities.WebsiteNews;

namespace TTCMS.Web.ViewModels
{
    public class ActivityNewsViewModel
    {
        public List<CategoryInfo> CategoriesMenu { get; set; }
        public List<NewsInfo> News { get; set; }
        public List<NewsInfo> HotestNews { get; set; }
        public int TotalPageNum { get; set; }
        public int layout { get; set; }
        public string ImagePath { get; set; }

    }
    public class PaggingNewsInfo
    {
        public int CurrentPage { get; set; }
        public List<NewsInfo> News { get; set; }
        public string CurrentSlug { get; set; }
    }

    public class SearchNewsInfo {

        public SearchNewsInfo()
        {
            News = new List<NewsInfo>();
            CategoryItems = new List<CategoryItemViewModel>();
            CurrentPage = 0;
            DisplayImageContent = false;
            CurrentKeyWord = "";
            Layout = -1;
            CategoryItemId = 0;
        }

        public List<NewsInfo> News { get; set; }
        //public IEnumerable<stock_News> stock_News { get; set; }
        public List<CategoryItemViewModel> CategoryItems { get; set; }
        public string CurrentKeyWord { get; set; }
        public int CategoryItemId { get; set; }
        public int CurrentPage { get; set; }
        public int TotalPageNum { get; set; }
        public string PageURL { get; set; }
        public int Layout { get; set; }
        public bool DisplayImageContent { get; set; }
    }

    public class NewsInfo
    {
        public string Title { get; set; }
        public string ImageUrl { get; set; }
        public DateTime CreatedDate { get; set; }
        public string category { get; set; }
        public int GroupID { get; set; }
        public string Slug { get; set; }
        public string Desc{ get; set; }
        public string RedirectUrl { get; set; }
    }
    public class CategoryInfo
    {
        public string Name { get; set; }
        public bool isActived { get; set; }
        public string Slug { get; set; }
    }
}