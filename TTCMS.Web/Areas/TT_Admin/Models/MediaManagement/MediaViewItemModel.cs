using System;

namespace TTCMS.Web.Areas.TT_Admin.Models
{
    public class MediaViewItemModel
    {
        public string Name { get; set; }
        public string Path { get; set; }
        public string FullPath { get; set; }
    }
}
