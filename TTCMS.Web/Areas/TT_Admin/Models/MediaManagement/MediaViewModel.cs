using PagedList;
using System;
using System.Collections.Generic;

namespace TTCMS.Web.Areas.TT_Admin.Models
{
    public class MediaViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
        public string FullPath { get; set; }
    }

    public class MediaPageViewModel
    {
        public MediaTableViewModel TableList { get; set; }
    }

    public class MediaTableViewModel
    {
        public string Show { get; set; }
        public string Keyword { get; set; }
        public IPagedList<MediaViewModel> ModelList { get; set; }
    }
}
