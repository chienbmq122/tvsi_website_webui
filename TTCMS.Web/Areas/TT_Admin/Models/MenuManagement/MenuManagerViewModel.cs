﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using TTCMS.Data.Entities;
using TTCMS.Web.Models;
using TTCMS.Web.ViewModels;

namespace TTCMS.Web.Areas.TT_Admin.Models
{
    public class MenuTableViewModel
    {
        public string LanguageId { get; set; }
        public IEnumerable<SelectListItem> Lang { get; set; }
        public SelectList Group { get; set; }
    }

    public class MenuTypeTableViewModel
    {
        public IEnumerable<LanguageViewModel> Languages { get; set; }
        public IEnumerable<KeyValueModel> MenuTypes { get; set; }
    }

    public class MenuEditViewModel
    {
        public string Name { get; set; }
        public string Link { get; set; }
        public MenuSourceType SourceType { get; set; }
        public int CategoryId { get; set; }
        public int SinglePageId { get; set; }
        public string LanguageCode { get; set; }
        public MenuGroupType GroupType { get; set; }
        public string Target { get; set; }
        public List<CategoryViewModel> Categories { get; set; }
        public List<SinglePageViewModel> SinglePages { get; set; }
    }

    public class MenuManagerViewModel
    {
        public int Id { get; set; }
        [StringLength(250, ErrorMessageResourceType = typeof(Resources.Resources), ErrorMessageResourceName = "LenghtString250")]
        [Required(ErrorMessageResourceType = typeof(Resources.Resources), ErrorMessageResourceName = "Required")]
        public string Name { get; set; }
        public string Link { get; set; }
        public string Action { get; set; }
        public string Controller { get; set; }
        public int WithId { get; set; }
        public string TextType { set; get; }
        public int ParentID { get; set; }
        public string ParentName { get; set; }
        public string Target { get; set; }
        public string CssClass { set; get; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public bool IsActived { get; set; }
        public int Order { get; set; }
        public MenuGroupType GroupType { get; set; }
        public string LanguageId { get; set; }
        public SelectList Lang { get; set; }
        public SelectList Parents { get; set; }
        public CategoryItemViewModel CategoryItem { get; set; }
        public Language_SinglePageViewModel Language_SinglePage { get; set; }
    }
}