﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using TTCMS.Web.ViewModels;

namespace TTCMS.Web.Areas.TT_Admin.Models
{
    public class Language_SinglePageViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        [Required, AllowHtml]
        public string TitleHTML { get; set; }
        public string Summary { get; set; }
        [Required, AllowHtml]
        public string Body { get; set; }
        [Required]
        public string Slug { get; set; }
        public string Description { get; set; }
        public string Keywords { get; set; }
        public string Tag { get; set; }
        public int SinglePageId { get; set; }
        public string LanguageId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public DateTime? DeletedAt { get; set; }
        public SinglePageViewModel SinglePage { get; set; }
        public LanguageViewModel Language { get; set; }
    }
}