﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using TTCMS.Web.ViewModels;

namespace TTCMS.Web.Areas.TT_Admin.Models
{
    public class SinglePageListViewModel
    {
        public IEnumerable<LanguageViewModel> Languages { get; set; }
        public IEnumerable<SelectListItem> Lang { get; set; }
        public PageTableViewModel TableList { get; set; }
    }

    public class PageTableViewModel
    {
        public IEnumerable<LanguageViewModel> Languages { get; set; }
        public int PageIndex { get; set; }
        public string Show { get; set; }
        public string Search { get; set; }
        public IPagedList<SinglePageViewModel> ModelList { get; set; }
    }

    public class SinglePageViewModel
    {
        public SinglePageViewModel()
        {
            Language_SinglePage = new Language_SinglePageViewModel();
            Language_SinglePages = new List<Language_SinglePageViewModel>();
        }

        public int Id { get; set; }
        public int Views { get; set; }
        public string Img_Thumbnail { get; set; }
        public string CssClass { set; get; }
        public bool IsActive { get; set; }
        public bool IsHot { get; set; }
        public bool IsHome { get; set; }
        public int Order { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedById { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string UpdatedById { get; set; }
        public long SinglePage_Id { get; set; }
        public string Title { get; set; }
        public string Summary { get; set; }
        public string Body { get; set; }
        public string Route { get; set; }
        public string Description { get; set; }
        public string Keywords { get; set; }
        public string Tag { get; set; }
        public string LanguageId { get; set; }
        public int CategoryId { get; set; }
        public string CreatedByUserName { get; set; }
        public int PageIndex { get; set; } = 1;
        public SelectList Lang { get; set; }
        public SelectList ListCategory { set; get; }
        public Language_SinglePageViewModel Language_SinglePage { get; set; }
        public List<Language_SinglePageViewModel> Language_SinglePages { get; set; }
    }
}