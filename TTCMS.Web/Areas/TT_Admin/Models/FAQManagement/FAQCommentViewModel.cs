﻿using PagedList;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TTCMS.Web.ViewModels;

namespace TTCMS.Web.Areas.TT_Admin.Models
{
    public class FAQCommentViewModel
    {
        public FAQCommentViewModel()
        {

        }
        public int Id { get; set; }
        public int QuestionId { get; set; }
        public int? ParentCommentId { get; set; }
        public string Content { get; set; }
        public string CreatedBy { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool Approved { get; set; }
        public bool AdminCreated { get; set; }
        public string AttachmentURL { get; set; }
        public List<FAQCommentViewModel> Replies { get; set; }
        public FAQCommentViewModel ParentComment { get; set; }
        public FAQQuestionManagementViewModel Question { get; set; }
    }

    public class CommentManagementViewModel
    {
        public int CommentId { get; set; }
        public string Show { get; set; }
        public string Search { get; set; }
        public List<LanguageViewModel> Languages { get; set; }
        public IPagedList<FAQCommentViewModel> ModelList { get; set; }
        public int QuestionId { get; set; }
    }

    public class CreateCommentViewModel
    {
        public int CommentId { get; set; }
        public int QuestionId { get; set; }
        [AllowHtml]
        [MaxLength]
        public string Content { get; set; }
        public bool Approved { get; set; }
    }

    public class CommentManagementPageViewModel
    {
        public CommentManagementViewModel TableList { get; set; }
    }
}