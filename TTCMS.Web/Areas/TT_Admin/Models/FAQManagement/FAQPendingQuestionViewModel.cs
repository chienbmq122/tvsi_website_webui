﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TTCMS.Web.ViewModels;

namespace TTCMS.Web.Areas.TT_Admin.Models
{
    public class FAQPendingQuestionViewModel
    {
        public FAQPendingQuestionViewModel()
        {

        }
        public int Id { get; set; }
        public string PendingQuestionTitle { get; set; } 
        public string FullName { get; set; }
        public string PhoneNumber { get; set; }
        public string AccountNumber { get; set; }
        public string Content { get; set; }
        public string AttachmentURL { get; set; }
        public string FileFullPath { get; set; }
        public bool Seen { get; set; }
        public DateTime CreatedDate { get; set; }
        public List<LanguageViewModel> Languages { get; set; }
    }

    public class PendingQuestionManagementViewModel
    {
        public string Show { get; set; }
        public string Search { get; set; }
        public List<LanguageViewModel> Languages { get; set; }
        public IPagedList<FAQPendingQuestionViewModel> ModelList { get; set; }
    }
    public class PendingQuestionManagementPageViewModel
    {
        public List<LanguageViewModel> Languages { get; set; }
        public PendingQuestionManagementViewModel TableList { get; set; }
    }

}