﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace TTCMS.Web.Areas.TT_Admin.Models
{
    public class FAQItemViewModel
    {
        public int Id { get; set; }
        public string Question { get; set; }

        [AllowHtml]
        [MaxLength]
        public string Answer { get; set; }
        public DateTime CreatedDate { get; set; }
        public int LanguageId { get; set; }
        public bool IsActived { get; set; }
        public bool IsDeleted { get; set; }
    }
}