﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TTCMS.Web.ViewModels;

namespace TTCMS.Web.Areas.TT_Admin.Models
{
    public class TagViewModel
    {
        public TagViewModel()
        {
            Language = new LanguageViewModel();
        }
        public int Id { get; set; }
        public string TagName { get; set; }
        public bool QuickSearch { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int LanguageId { get; set; }
        public int Priority { get; set; }
        public LanguageViewModel Language { get; set; }
        public SelectList Lang { get; set; }
    }

    public class EditTagViewModel
    {
        public int Id { get; set; }
        public string TagName { get; set; }
        public int Priority { get; set; }
        public bool QuickSearch { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int LanguageId { get; set; }
        public SelectList Lang { get; set; }
    }

    public class TagManagementViewModel
    {
        public string Show { get; set; }
        public string Search { get; set; }
        public List<LanguageViewModel> Languages { get; set; }
        public IPagedList<TagViewModel> ModelList { get; set; }
    }
    public class TagManagementPageViewModel
    {
        public IEnumerable<SelectListItem> Lang { get; set; }
        public List<LanguageViewModel> Languages { get; set; }
        public TagManagementViewModel TableList { get; set; }
    }
}