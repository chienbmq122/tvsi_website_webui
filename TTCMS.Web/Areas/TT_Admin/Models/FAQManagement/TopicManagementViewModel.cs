﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using TTCMS.Data.Entities;
using TTCMS.Web.ViewModels;

namespace TTCMS.Web.Areas.TT_Admin.Models
{
    public class TopicViewModel
    {
        public int Id { get; set; }
        public int LanguageTopicId { get; set; }
        public string TopicName { get; set; }
        public string TopicDescription { get; set; }
        public string ImageUrl { get; set; }
        public bool QuickSearch { get; set; }
        public bool QuickAction { get; set; }
        public bool Active { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? ParentTopicId { get; set; }
        public int LanguageId { get; set; }
        public int Order { get; set; }
        public SelectList DropTopics { get; set; }
        public SelectList Lang { get; set; }
        public virtual TopicViewModel Parent { get; set; }
        public virtual Language_TopicViewModel Language_Topic { get; set; }
        public virtual List<Language_TopicViewModel> Language_Topics { get; set; }
    }

    public class EditLanguageTopicViewModel
    {
        public int Id { get; set; }
        public string TopicName { get; set; }
        public string TopicDescription { get; set; }
        public string ImageUrl { get; set; }
        public TopicViewModel Topic { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int LanguageId { get; set; }
        public int ParentTopicId { get; set; }
        public SelectList DropTopics { get; set; }
        public SelectList Lang { get; set; }
    }

    public class CreateTopicViewModel
    {

    }

    public class Language_TopicViewModel
    {
        public int Id { get; set; }
        public string TopicName { get; set; }
        public string TopicDescription { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public LanguageViewModel Language { get; set; }

    }
    public class TopicManagementViewModel
    {
        public string Show { get; set; }
        public string Search { get; set; }
        public List<LanguageViewModel> Languages { get; set; }
        public IPagedList<TopicViewModel> ModelList { get; set; }
    }
    public class TopicManagementPageViewModel
    {
        public IEnumerable<SelectListItem> Lang { get; set; }
        public List<LanguageViewModel> Languages { get; set; }
        public TopicManagementViewModel TableList { get; set; }
    }
}