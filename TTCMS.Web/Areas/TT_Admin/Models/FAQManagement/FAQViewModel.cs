﻿using PagedList;
using System.Collections.Generic;
using System.Web.Mvc;
using TTCMS.Web.ViewModels;

namespace TTCMS.Web.Areas.TT_Admin.Models
{
    public class FAQPageViewModel
    {
        public IEnumerable<SelectListItem> Lang { get; set; }
        public FAQViewModel TableList { get; set; }
    }
    public class FAQViewModel
    {
        public string Show { get; set; }
        public string Search { get; set; }
        public List<LanguageViewModel> Languages { get; set; }
        public IPagedList<FAQItemViewModel> ModelList { get; set; }
    }
    public class FAQCreateViewModel
    {
        public SelectList Lang { get; set; }
        public FAQItemViewModel Model { get; set; }
    }
}