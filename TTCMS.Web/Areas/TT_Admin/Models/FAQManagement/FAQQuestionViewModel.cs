using PagedList;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using TTCMS.Web.ViewModels;

namespace TTCMS.Web.Areas.TT_Admin.Models
{
    public class QuestionViewModel
    {
        public int Id { get; set; }
        public bool Active { get; set; }
        public bool Latest { get; set; }
        public bool IsWebsiteShown { get; set; }
        [AllowHtml]
        public string QuestionName { get; set; }
        public string Content { get; set; }
        public int Priority { get; set; }
        public string ContentBrief { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public List<FAQLanguageQuestionViewModel> LanguageQuestions { get; set; }
        public TopicViewModel Topic { get; set; }
        public List<TopicViewModel> TopicList { get; set; }
    }
    public class QuestionEditViewModel
    {
        public int Id { get; set; }
        public int LanguageQuestionId { get; set; }
        public string QuestionName { get; set; }
        [AllowHtml]
        [MaxLength]
        public string Content { get; set; }
        public string ContentBrief { get; set; }
        public TopicViewModel Topic { get; set; }
        public LanguageViewModel Language { get; set; }
        public int Priority { get; set; }
        public SelectList Lang { get; set; }
        public SelectList DropTopics { get; set; }
        public bool Latest { get; set; }
        public bool Active { get; set; }
        public bool IsWebsiteShown { get; set; }
        public int LanguageId { get; set; }
        public int TopicId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public int Platform { get; set; }
        public FAQLanguageQuestionViewModel LanguageQuestion { get; set; }
        public List<FAQLanguageQuestionViewModel> LanguageQuestions { get; set; }
        public List<QuestionAttachmentViewModel> Attachments { get; set;}
    }
    public class FAQQuestionManagementPageViewModel
    {
        public IEnumerable<SelectListItem> Lang { get; set; }
        public List<LanguageViewModel> Languages { get; set; }
        public FAQQuestionManagementViewModel TableList { get; set; }
    }
    public class FAQQuestionManagementViewModel
    {
        public string Show { get; set; }
        public string Search { get; set; }
        public List<LanguageViewModel> Languages { get; set; }
        // public TopicViewModel Topic { get; set; }
        public IPagedList<QuestionViewModel> ModelList { get; set; }
    }
    
    public class FAQLanguageQuestionViewModel
    {
        public int Id { get; set; }
        public string QuestionName { get; set; }

        [AllowHtml]
        [MaxLength]
        public string Content { get; set; }
        public string ContentBrief { get; set; }
        public DateTime CreatedDate { get; set; }
        public int LanguageId { get; set; }
        public bool Active { get; set; }
        public LanguageViewModel Language { get; set; }
    }

    public class QuestionAttachmentViewModel
    {
        public int Id { get; set; }
        public int QuestionId { get; set; }
        [AllowHtml]
        public string Description { get; set; }
        public int Platform { get; set; }
        public int Priority { get; set; }
        public string ImageUrl { get; set; }
        public LanguageViewModel Language { get; set; }
    }
}