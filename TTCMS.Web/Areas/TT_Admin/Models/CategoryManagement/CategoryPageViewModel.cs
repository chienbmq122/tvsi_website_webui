﻿using System.Collections.Generic;
using System.Web.Mvc;
using TTCMS.Web.ViewModels;

namespace TTCMS.Web.Areas.TT_Admin.Models
{
    public class CategoryPageViewModel
    {
        public IEnumerable<LanguageViewModel> Languages { get; set; }
        public IEnumerable<SelectListItem> Lang { get; set; }
        public CategoryTableViewModel TableList { get; set; }
    }
}