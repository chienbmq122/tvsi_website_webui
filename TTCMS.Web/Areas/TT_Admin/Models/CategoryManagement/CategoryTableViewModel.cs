﻿using PagedList;
using System.Collections.Generic;
using TTCMS.Web.ViewModels;

namespace TTCMS.Web.Areas.TT_Admin.Models
{
    public class CategoryTableViewModel
    {
        public string Show { get; set; }
        public string Search { get; set; }
        public List<LanguageViewModel> Languages { get; set; }
        public IPagedList<CategoryViewModel> ModelList { get; set; }
    }
}