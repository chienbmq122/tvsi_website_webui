﻿using PagedList;
using TTCMS.Web.ViewModels;

namespace TTCMS.Web.Areas.TT_Admin.Models
{
    public class LangTableViewModel
    {
        public string Search { get; set; }
        public IPagedList<LanguageViewModel> ModelList { get; set; }
    }
}