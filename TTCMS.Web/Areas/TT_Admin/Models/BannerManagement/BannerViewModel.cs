﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using TTCMS.Web.ViewModels;

namespace TTCMS.Web.Areas.TT_Admin.Models
{
    public class BannerViewModel
    {
        public string Show { get; set; }
        public string Search { get; set; }
        public List<LanguageViewModel> Languages { get; set; }
        public IPagedList<BannerItemViewModel> ModelList { get; set; }
    }
    public class BannerItemViewModel
    {
        public int Id { get; set; }
        public string RedirectUrl { get; set; }
        public string ImageUrl { get; set; }
        public int LanguageId { get; set; }
        public int? Priority { get; set; }
        public bool IsActived { get; set; }
        public DateTime CreatedDate { get; set; }
        public int Type { get; set; }
    }
    public class BannerPageViewModel
    {
        public IEnumerable<SelectListItem> Lang { get; set; }
        public BannerViewModel TableList { get; set; }
    }
     public class BannerCreateViewModel
    {
        public SelectList Lang { get; set; }
        public SelectList ListBannerType { get; set; }
        public BannerItemViewModel Model { get; set; }
    }
}