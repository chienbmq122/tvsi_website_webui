﻿using System;
using System.ComponentModel.DataAnnotations;
using TTCMS.Web.ViewModels;

namespace TTCMS.Web.Areas.TT_Admin.Models
{
    public class NewsItemViewModel
    {
        public int Id { get; set; }
        [Required]
        public string Title { get; set; }
        public string Name { get; set; }
        public string Keywords { get; set; }
        [Required]
        public string Slug { get; set; }
        public string Description { get;set; }
        [Required]
        public string Content { get; set; }
        public int CreatedBy { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime Published { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public DateTime? DeletedAt { get; set; }
        public LanguageViewModel Language { get; set; }
        public NewsViewModel News { get; set; }
    }
}