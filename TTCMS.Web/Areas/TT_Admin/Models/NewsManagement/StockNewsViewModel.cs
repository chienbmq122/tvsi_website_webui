﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using PagedList;
using TTCMS.Web.ViewModels;

namespace TTCMS.Web.Areas.TT_Admin.Models
{
    public class StockNewsPageViewModel
    {
        public IEnumerable<LanguageViewModel> Languages { get; set; }
        public IEnumerable<SelectListItem> Lang { get; set; }
        public StockNewsTableViewModel TableList { get; set; }
    }
    public class StockNewsTableViewModel
    {
        public string Show { get; set; }
        public string Search { get; set; }
        public List<LanguageViewModel> Languages { get; set; }
        public IPagedList<StockNewsViewModel> ModelList { get; set; }
    }
    public class StockNewsViewModel
    {
        public int NewsID { get; set; }
        public int GroupID { get; set; }
        public int LanguageID { get; set; }
        public string ImageUrl { get; set; }
        public bool IsHotNews { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public SelectList Lang { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Content { get; set; }
        public SelectList ListCategory { set; get; }
    }
}