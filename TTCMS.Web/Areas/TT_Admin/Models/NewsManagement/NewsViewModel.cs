﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using TTCMS.Web.ViewModels;

namespace TTCMS.Web.Areas.TT_Admin.Models
{
    public class NewsPageViewModel
    {
        public IEnumerable<LanguageViewModel> Languages { get; set; }
        public IEnumerable<SelectListItem> Lang { get; set; }
        public NewsTableViewModel TableList { get; set; }
    }

    public class NewsTableViewModel
    {
        public string Show { get; set; }
        public string Search { get; set; }
        public List<LanguageViewModel> Languages { get; set; }
        public IPagedList<NewsViewModel> ModelList { get; set; }
    }

    public class NewsViewModel
    {
        public int Id { get; set; }
        public string Keyword { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Content { get; set; }
        public string ImageUrl { get; set; }
        public bool IsActive { get; set; }
        public bool IsHot { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public LanguageViewModel Language { get; set; }
        public SelectList Lang { get; set; }
        public int LanguageId { get; set; }
        public int CategoryId { get; set; }
        public CategoryViewModel Category { get; set; }
        public SelectList ListCategory { set; get; }
    }
}