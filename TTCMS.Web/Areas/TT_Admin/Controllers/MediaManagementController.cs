﻿using AutoMapper;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TTCMS.Areas.TT_Admin.Filters;
using TTCMS.Service;
using TTCMS.Service.Common;
using TTCMS.Web.Areas.TT_Admin.Models;
using TTCMS.Core.Infrastructure.Alerts;
using TTCMS.Areas.TT_Admin.CustomAttributes;
using TTCMS.Web.Extensions;
using TTCMS.Data.Entities;
using TTCMS.Data.Infrastructure;

namespace TTCMS.Web.Areas.TT_Admin.Controllers
{
    [Authorize]
    public class MediaManagementController : BaseController
    {
        private readonly IEnumerable<Language> languages;
        private readonly IMediaService mediaService;
        private readonly ILanguageService languageService;
        private readonly ICategoryService categoryService;
        private readonly ICategoryItemService categoryItemService;

        public MediaManagementController(
            IMediaService mediaService,
            ICategoryService categoryService,
            ILanguageService languageService,
            ICategoryItemService categoryItemService)
        {
            this.mediaService = mediaService;
            this.categoryService = categoryService;
            this.languageService = languageService;
            this.categoryItemService = categoryItemService;

            languages = languageService.GetListForActive();
        }

        [HttpGet]
        [AuthorizeUser(FunctionID = "IMAGE", RoleID = "VIEW")]
        public ActionResult Index(int page = 1, string search = "")
        {
            int pageSize = int.Parse(ConfigSettings.ReadSetting("pageSize"));
            var list = mediaService.GetPageList(new Page(page, pageSize), search);
            var actionViewModel = Mapper.Map<IPagedList<Media>, IPagedList<MediaViewModel>>(list);

            var table = new MediaTableViewModel
            {
                ModelList = actionViewModel,
                Keyword = search
            };

            if (Request.IsAjaxRequest())
            {
                return PartialView("_MediaTable", table);
            }

            var model = new MediaPageViewModel
            {
                TableList = table
            };

            return View(model);
        }

        [HttpPost]
        [AuthorizeUser(FunctionID = "IMAGE", RoleID = "CREATE")]
        public JsonResult Create(string url = "")
        {
            if (string.IsNullOrEmpty(url))
            {
                return Json(
                    new { isSuccess = false, error = "Url is empty!" },
                    JsonRequestBehavior.AllowGet
                );
            }

            try
            {
                var mediaViewModel = GetMediaByUrl(url);
                var image = mediaService.GetByFullPath(mediaViewModel.FullPath);

                if (image == null)
                {
                    mediaService.Create(Mapper.Map<MediaViewModel, Media>(mediaViewModel));
                    categoryItemService.SaveChange();
                }

                return Json(
                    new { isSuccess = true, error = "" },
                    JsonRequestBehavior.AllowGet
                );
            }
            catch (Exception ex)
            {
                return Json(
                    new { isSuccess = true, error = ex.Message },
                    JsonRequestBehavior.AllowGet
                );
            }
        }

        [HttpPost]
        [AuthorizeUser(FunctionID = "IMAGE", RoleID = "DELETE")]
        public JsonResult Delete(int id = 0)
        {
            if (id == 0)
            {
                return Json(
                    new { isSuccess = false, error = "id is not valid!" },
                    JsonRequestBehavior.AllowGet
                );
            }

            try
            {
                var media = mediaService.GetById(id);
                if (media == null)
                {
                    return Json(
                        new { isSuccess = false, error = "Data not found!" },
                        JsonRequestBehavior.AllowGet
                    );
                }

                media.DeletedAt = DateTime.Now;
                mediaService.Update(media);

                return Json(
                    new { isSuccess = true, error = "" },
                    JsonRequestBehavior.AllowGet
                );
            }
            catch (Exception ex)
            {
                return Json(
                    new { isSuccess = true, error = ex.Message },
                    JsonRequestBehavior.AllowGet
                );
            }
        }
    }
}