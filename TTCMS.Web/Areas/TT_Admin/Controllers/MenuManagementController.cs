﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TTCMS.Areas.TT_Admin.Filters;
using TTCMS.Data.Entities;
using TTCMS.Service;
using TTCMS.Web.Areas.TT_Admin.Models;
using TTCMS.Core.Infrastructure.Alerts;
using TTCMS.Areas.TT_Admin.CustomAttributes;
using TTCMS.Web.Models;
using TTCMS.Web.ViewModels;

namespace TTCMS.Web.Areas.TT_Admin.Controllers
{
    [Authorize]
    public class MenuManagementController : BaseController
    {
        private readonly ICategoryService categoryService;
        private readonly ICategoryItemService categoryItemService;
        private readonly IMenuService menuService;
        private readonly ISinglePageService singlePageService;
        private readonly ILanguage_SinglePageService language_SinglePageService;
        private readonly ILanguageService languageService;
        public MenuManagementController(
            ICategoryService categoryService,
            ICategoryItemService categoryItemService,
            ILanguage_SinglePageService language_SinglePageService,
            ISinglePageService singlePageService,
            IMenuService menuService,
            ILanguageService languageService
        )
        {
            this.categoryService = categoryService;
            this.categoryItemService = categoryItemService;
            this.singlePageService = singlePageService;
            this.language_SinglePageService = language_SinglePageService;
            this.menuService = menuService;
            this.languageService = languageService;
        }

        //
        // GET: /GroupMenuManagement/Role/
        //[OutputCache(Duration = 60)]
        [AuthorizeUser(FunctionID = "MENU", RoleID = "VIEW")]
        [HttpGet]
        public ActionResult Index(string group = "", string show = "")
        {
            var languages = languageService.GetListForActive("*");
            var viewModel = new MenuTypeTableViewModel
            {
                Languages = Mapper.Map<IEnumerable<Language>, IEnumerable<LanguageViewModel>>(languages),
                MenuTypes = new List<KeyValueModel>() {
                    new KeyValueModel((int)MenuGroupType.HeaderMenu, MenuGroupType.HeaderMenu.ToDescription()),
                    new KeyValueModel((int)MenuGroupType.TopMenu, MenuGroupType.TopMenu.ToDescription()),
                },
            };
            return View(viewModel);
        }

        [AuthorizeUser(FunctionID = "MENU", RoleID = "VIEW")]
        public ActionResult _AjaxLoadMenu(string group = "", string show = "")
        {
            if (show != "")
            {
                cultureName = show;
            }

            var menuGroup = (MenuGroupType)Enum.Parse(typeof(MenuGroupType), group);
            var menu = menuService.GetList(menuGroup, cultureName);
            var model = Mapper.Map<IEnumerable<Menu>, IEnumerable<MenuManagerViewModel>>(menu);
            //if (model.Any(x => x.CategoryItem != null))
            //{
            //    foreach (var item in model)
            //    {
            //        if (item.CategoryItem != null)
            //        {
            //            item.Name = item.CategoryItem.Name;
            //        }
            //    }
            //}

            return PartialView(model.ToArray());
        }

        private int TreeUpMenu(IEnumerable<MenuJsonList> model, int parent, int icount)
        {
            int idem = icount;
            foreach (var item in model)
            {
                idem++;
                var menu = menuService.GetById(item.id);
                menu.Order = idem;
                menu.ParentID = parent;
                menuService.Update(menu);
                if (item.children != null)
                {
                    TreeUpMenu(item.children, item.id, idem);
                }

            }
            return idem;
        }
        [HttpPost]
        [AuthorizeUser(FunctionID = "MENU", RoleID = "CREATE")]
        public ActionResult UpPage(int[] ids, string group = "", string show = "")
        {
            try
            {
                if (show != "")
                {
                    cultureName = show;
                }
                if (ids.Length > 0)
                {
                    foreach (var item in ids)
                    {
                        var page = singlePageService.GetById(item);
                        var lang_page = language_SinglePageService.GetById(cultureName, item);
                        var menu = new Menu()
                        {
                            Name = lang_page.Title,
                            Link = lang_page.Slug,
                            WithId = item,
                            Action = "PageDetail",
                            Controller = "Page",
                            ParentID = 0,
                            Target = "_self",
                            TextType = "Page",
                            CreatedBy = User.Identity.Name,
                            CreatedDate = DateTime.Now,
                            UpdatedBy = User.Identity.Name,
                            UpdatedDate = DateTime.Now,
                            IsActived = true,
                            LanguageId = cultureName,
                            Language = languageService.GetByCode(cultureName),
                            GroupType = (MenuGroupType)Enum.Parse(typeof(MenuGroupType), group),
                            Order = menuService.GetSort((MenuGroupType)Enum.Parse(typeof(MenuGroupType), group), cultureName)
                        };
                        menuService.Create(menu);
                    }
                    return Json(JsonSuccessWidthIndex(Url.Action("Index"), Resources.Resources.SuccessCreate), JsonRequestBehavior.AllowGet);
                }
                return Json(JsonError(Resources.Resources.WarningData), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                HandleException("Message ", ex);
                return Json(JsonError(Resources.Resources.ErrorCatch), JsonRequestBehavior.AllowGet);
            }

        }
        [HttpPost]
        [AuthorizeUser(FunctionID = "MENU", RoleID = "CREATE")]
        public ActionResult UpNewsCate(int[] ids, string group = "", string show = "")
        {
            try
            {
                if (show != "")
                {
                    cultureName = show;
                }

                if (ids.Length == 0)
                {
                    return Json(JsonError(Resources.Resources.WarningData), JsonRequestBehavior.AllowGet);
                }

                foreach (var item in ids)
                {
                    var categoryItem = categoryItemService.GetByCategoryId(item, cultureName);

                    if (categoryItem == null) continue;

                    var menu = new Menu()
                    {
                        Name = categoryItem.Name,
                        Link = categoryItem.Slug,
                        WithId = item,
                        Action = "Index",
                        Controller = "Page",
                        ParentID = 0,
                        Target = "_self",
                        TextType = "News Category",
                        CreatedBy = User.Identity.Name,
                        CreatedDate = DateTime.Now,
                        UpdatedBy = User.Identity.Name,
                        UpdatedDate = DateTime.Now,
                        IsActived = true,
                        LanguageId = cultureName,
                        Language = languageService.GetByCode(cultureName),
                        CategoryItem = categoryItem,
                        GroupType = (MenuGroupType)Enum.Parse(typeof(MenuGroupType), group),
                        Order = menuService.GetSort(
                            (MenuGroupType)Enum.Parse(typeof(MenuGroupType), group),
                            cultureName
                        )
                    };
                    menuService.Create(menu);
                }

                return Json(JsonSuccessWidthIndex(
                    Url.Action("Index"),
                    Resources.Resources.SuccessCreate),
                    JsonRequestBehavior.AllowGet
                );
            }
            catch (Exception ex)
            {
                HandleException("Message ", ex);
                return Json(JsonError(Resources.Resources.ErrorCatch), JsonRequestBehavior.AllowGet);
            }

        }
        [HttpPost]
        [AuthorizeUser(FunctionID = "MENU", RoleID = "CREATE")]
        public ActionResult UpProCate(int[] ids, string group = "", string show = "")
        {
            try
            {
                if (show != "")
                {
                    cultureName = show;
                }
                if (ids.Length > 0)
                {
                    foreach (var item in ids)
                    {
                        var cate = categoryService.GetById(item);
                        var menu = new Menu()
                        {
                            //CssClass = cate.CssClass,
                            //Name = cate.Name,
                            //Link = cate.Slug,
                            WithId = item,
                            Action = "Category",
                            Controller = "Product",
                            ParentID = 0,
                            Target = "_self",
                            TextType = "Product Category",
                            CreatedBy = User.Identity.Name,
                            CreatedDate = DateTime.Now,
                            UpdatedBy = User.Identity.Name,
                            UpdatedDate = DateTime.Now,
                            IsActived = true,
                            LanguageId = cultureName,
                            Language = languageService.GetByCode(cultureName),
                            CategoryItem = categoryItemService.GetByCategoryId(item, cultureName),
                            GroupType = (MenuGroupType)Enum.Parse(typeof(MenuGroupType), group),
                            Order = menuService.GetSort((MenuGroupType)Enum.Parse(typeof(MenuGroupType), group), cultureName)
                        };
                        menuService.Create(menu);
                    }
                    return Json(JsonSuccessWidthIndex(Url.Action("Index"), Resources.Resources.SuccessCreate), JsonRequestBehavior.AllowGet);
                }
                return Json(JsonError(Resources.Resources.WarningData), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                HandleException("Message ", ex);
                return Json(JsonError(Resources.Resources.ErrorCatch), JsonRequestBehavior.AllowGet);
            }

        }
        [HttpPost]
        [AuthorizeUser(FunctionID = "MENU", RoleID = "CREATE")]
        public ActionResult UpCustomLink(string url = "", string item = "", string group = "", string show = "")
        {
            try
            {
                if (show != "")
                {
                    cultureName = show;
                }
                if (url != "" && item != "")
                {
                    var menu = new Menu()
                    {
                        Name = item,
                        Link = url,
                        ParentID = 0,
                        Target = "_self",
                        TextType = "Custom Link",
                        CreatedBy = User.Identity.Name,
                        CreatedDate = DateTime.Now,
                        UpdatedBy = User.Identity.Name,
                        UpdatedDate = DateTime.Now,
                        IsActived = true,
                        LanguageId = cultureName,
                        Language = languageService.GetByCode(cultureName),
                        GroupType = (MenuGroupType)Enum.Parse(typeof(MenuGroupType), group),
                        Order = menuService.GetSort((MenuGroupType)Enum.Parse(typeof(MenuGroupType), group), cultureName)
                    };
                    menuService.Create(menu);
                    return Json(JsonSuccessWidthIndex(Url.Action("Index"), Resources.Resources.SuccessCreate), JsonRequestBehavior.AllowGet);
                }
                return Json(JsonError(Resources.Resources.WarningData), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                HandleException("Message ", ex);
                return Json(JsonError(Resources.Resources.ErrorCatch), JsonRequestBehavior.AllowGet);
            }

        }
        [AuthorizeUser(FunctionID = "MENU", RoleID = "VIEW")]
        public ActionResult _Page(string show = "")
        {
            if (show != "")
            {
                cultureName = show;
            }
            var actionViewModel = Mapper.Map<IEnumerable<SinglePage>, IEnumerable<SinglePageViewModel>>(singlePageService.GetList().Where(x => x.IsActive == true));
            foreach (var item in actionViewModel)
            {
                var lang_model = language_SinglePageService.GetById(cultureName, item.Id);
                if (lang_model != null)
                {
                    item.Title = lang_model.Title;
                    item.Route = lang_model.Slug;
                }
            }
            return PartialView(actionViewModel.ToArray());
        }
        [AuthorizeUser(FunctionID = "MENU", RoleID = "VIEW")]
        public ActionResult _NewsCate(string show = "")
        {
            if (show != "")
            {
                cultureName = show;
            }
            var actionViewModel = GetFunctionViewModel(null, show);
            return PartialView(actionViewModel.ToArray());
        }
        [AuthorizeUser(FunctionID = "MENU", RoleID = "VIEW")]
        public ActionResult _ProductCate(string show = "")
        {
            if (show != "")
            {
                cultureName = show;
            }
            var actionViewModel = GetFunctionViewModelProduct(null, show);
            return PartialView(actionViewModel.ToArray());
        }
        [AuthorizeUser(FunctionID = "MENU", RoleID = "DELETE")]
        public ActionResult Delete(int Id)
        {
            try
            {
                if (Id == 0)
                {
                    return Json(JsonError(Resources.Resources.WarningData), JsonRequestBehavior.AllowGet);
                }

                var function = menuService.GetById(Id);
                if (function == null)
                {
                    return Json(JsonError(Resources.Resources.WarningData), JsonRequestBehavior.AllowGet);
                }

                menuService.Delete(Id);
                if (Request.IsAjaxRequest())
                {
                    var reponse = new { Id = Id, Code = AjaxAlertConsts.WithSuccess, Msg = Resources.Resources.AlertDataSuccess };
                    return Json(JsonSuccessWidthIndex(Url.Action("Index"), Resources.Resources.AlertDataSuccess), JsonRequestBehavior.AllowGet);
                }

                return Json(JsonError(Resources.Resources.AlertDataSuccess), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                HandleException("Message", ex);
                return Json(JsonError(Resources.Resources.ErrorCatch), JsonRequestBehavior.AllowGet);
            }

        }

        [AuthorizeUser(FunctionID = "MENU", RoleID = "EDIT")]
        public ActionResult Create(int id = 0, string languageCode = "")
        {
            try
            {
                if (!Enum.IsDefined(typeof(MenuGroupType), id))
                {
                    return RedirectToAction("Index").WithError(Resources.Resources.ErrorNoData);
                }

                var type = (MenuGroupType)id;
                var categoryViewModels = new List<CategoryViewModel>();
                var parents = categoryService.GetParents(0, languageCode);

                if (parents != null)
                {
                    categoryViewModels = MapCategoriesToViewModels(parents, languageCode);
                }

                var singlePageViewModels = new List<SinglePageViewModel>();
                var singlePages = singlePageService.GetActiveSiglePages(languageCode);

                if (singlePages != null)
                {
                    singlePageViewModels = MapSinglePageToViewModels(singlePages, languageCode);
                }

                var viewModel = new MenuEditViewModel()
                {
                    GroupType = type,
                    LanguageCode = languageCode,
                    Categories = categoryViewModels,
                    SinglePages = singlePageViewModels,
                    Target = Resources.Resources.self
                };

                return View(viewModel);
            }
            catch (Exception ex)
            {
                HandleException("Message", ex);
                return RedirectToAction("Index").WithError(Resources.Resources.ErrorCatch);
            }
        }

        private List<SinglePageViewModel> MapSinglePageToViewModels(IEnumerable<SinglePage> SinglePages = null, string languageCode = "")
        {
            var viewModels = new List<SinglePageViewModel>();

            if (SinglePages == null)
            {
                return viewModels;
            }

            foreach (var singlePage in SinglePages)
            {
                var viewModel = Mapper.Map<SinglePage, SinglePageViewModel>(singlePage);

                if (!string.IsNullOrEmpty(languageCode))
                {
                    viewModel.Title = singlePage
                        .Language_SinglePages
                        .FirstOrDefault(x => x.Language.Code == languageCode)?.Title;

                    viewModels.Add(viewModel);
                }
            }

            return viewModels;
        }

        [AuthorizeUser(FunctionID = "MENU", RoleID = "CREATE")]
        [HttpPost, ValidateAntiForgeryToken, Log("{model}")]
        public ActionResult Create(MenuEditViewModel viewModel)
        {
            var redirectObject = new
            {
                id = (int)viewModel.GroupType,
                languageCode = viewModel.LanguageCode
            };

            try
            {
                var order = menuService.GetMaxOrder(viewModel.GroupType) + 1;
                var language = languageService.GetByCode(viewModel.LanguageCode);
                var target = string.IsNullOrEmpty(viewModel.Target) ? "_self" : viewModel.Target;
                var menu = new Menu()
                {
                    IsActived = true,
                    Target = target,
                    CreatedDate = DateTime.Now,
                    CreatedBy = User.Identity.Name,
                    GroupType = viewModel.GroupType,
                    Order = order,
                    Language = languageService.GetByCode(viewModel.LanguageCode)
                };

                if (viewModel.SourceType == MenuSourceType.Category)
                {
                    AddMenuByCategoryId(viewModel.CategoryId, viewModel.LanguageCode, viewModel.GroupType, order, language, target);
                }
                else if (viewModel.SourceType == MenuSourceType.Page)
                {
                    var language_SinglePagesinglePage = language_SinglePageService.GetBySinglePage(viewModel.SinglePageId, viewModel.LanguageCode);

                    if (language_SinglePagesinglePage != null)
                    {
                        menu.TextType = "Page";
                        menu.Name = language_SinglePagesinglePage.Title;
                        menu.Link = language_SinglePagesinglePage.Slug;
                        menu.Language_SinglePage = language_SinglePagesinglePage;
                        menuService.Create(menu);
                    }
                }
                else
                {
                    menu.TextType = "Custom Link";
                    menu.Name = viewModel.Name;
                    menu.Link = viewModel.Link;

                    menuService.Create(menu);
                }


                return RedirectToAction("Create", redirectObject);
            }
            catch (Exception ex)
            {
                HandleException("Message", ex);
                return RedirectToAction("Create", redirectObject).WithError(Resources.Resources.ErrorCatch);
            }
        }

        private void AddMenuByCategoryId(int categoryId, string languageCode, MenuGroupType groupType, int order, Language language, string target, int parentId = 0)
        {
            var categoryItem = categoryItemService.GetByCategoryId(categoryId, languageCode);

            if (categoryItem == null) return;

            var menu = new Menu()
            {
                ParentID = parentId,
                IsActived = true,
                Target = target,
                CreatedDate = DateTime.Now,
                CreatedBy = User.Identity.Name,
                GroupType = groupType,
                TextType = "Category",
                Controller = "Category",
                Name = categoryItem.Name,
                Link = categoryItem.Slug,
                CategoryItem = categoryItem,
                Order = order,
                Language = language
            };

            menu = menuService.Create(menu);
            var children = categoryService.GetByParentId(categoryItem.Category.Id);

            if (children != null && children.Count() > 0)
            {
                foreach (var child in children)
                {
                    AddMenuByCategoryId(child.Id, languageCode, groupType, order, language, target, menu.Id);
                }
            }
        }

        [AuthorizeUser(FunctionID = "MENU", RoleID = "EDIT")]
        public ActionResult Edit(int id = 0)
        {
            try
            {
                var menu = menuService.GetById(id);
                if (menu == null)
                {
                    return RedirectToAction("Index").WithError(Resources.Resources.ErrorNoData);
                }

                var viewModel = Mapper.Map<Menu, MenuManagerViewModel>(menu);
                viewModel.LanguageId = menu.Language.Code;

                // Lấy danh sách menu cha (chỉ lấy menu level 0 và level 1)
                var parents = menuService.GetParents(viewModel.Id, 1);
                parents.Insert(0, new Menu() { Id = 0, Name = Resources.Resources.MenuParentPlaceholder });
                viewModel.Parents = new SelectList(parents, "Id", "Name", viewModel.ParentID);

                return View(viewModel);
            }
            catch (Exception ex)
            {
                HandleException("Message", ex);
                return RedirectToAction("Index").WithError(Resources.Resources.ErrorCatch);
            }
        }

        [AuthorizeUser(FunctionID = "MENU", RoleID = "EDIT")]
        [HttpPost, ValidateAntiForgeryToken, Log("{model}")]
        public ActionResult Edit(MenuManagerViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var menu = menuService.GetById(model.Id);
                    if (menu == null)
                    {
                        return RedirectToAction("Index").WithError(Resources.Resources.ErrorNoData);
                    }

                    menu.UpdatedDate = DateTime.Now;
                    menu.UpdatedBy = User.Identity.Name;
                    menu.Name = model.Name;
                    menu.ParentID = model.ParentID;
                    menu.Target = string.IsNullOrEmpty(model.Target) ? "_self" : model.Target;

                    if (menu.CategoryItem == null && menu.Language_SinglePage == null)
                    {
                        menu.Link = model.Link;
                    }

                    menuService.Update(menu);

                    return RedirectToAction("Create", new
                    {
                        id = (int)menu.GroupType,
                        languageCode = menu.Language.Code
                    }).WithSuccess(Resources.Resources.UpdateSuccess);
                }
                else
                {
                    ModelState.AddModelError("", Resources.Resources.WarningData);
                }

                return RedirectToAction("Edit", new { id = model.Id }).WithError(Resources.Resources.ErrorNoData);
            }
            catch (Exception ex)
            {
                HandleException("Message", ex);
                return RedirectToAction("Edit", new { id = model.Id }).WithError(Resources.Resources.ErrorCatch);
            }
        }

        [HttpPost]
        [AuthorizeUser(FunctionID = "MENU", RoleID = "EDIT")]
        public ActionResult UpdateOrder(List<int> ids)
        {
            try
            {
                if (ids == null || ids.Count == 0)
                {
                    return Json(JsonError(Resources.Resources.WarningData), JsonRequestBehavior.AllowGet);
                }

                for (int i = 0; i < ids.Count; i++)
                {
                    var menu = menuService.GetById(ids[i]);
                    menu.Order = i;
                    menuService.Update(menu);
                }

                return Json(Resources.Resources.UpdateSuccess, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                HandleException("Message", ex);
                return Json(JsonError(Resources.Resources.ErrorCatch), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [AuthorizeUser(FunctionID = "MENU", RoleID = "EDIT")]
        public ActionResult _Activated(int Id = 0, bool key = true)
        {

            try
            {
                if (Id == 0)
                {
                    return Json(JsonError(Resources.Resources.WarningData), JsonRequestBehavior.AllowGet);
                }
                var model = menuService.GetById(Id);
                if (model == null)
                {
                    return Json(JsonError(Resources.Resources.WarningData), JsonRequestBehavior.AllowGet);
                }
                model.IsActived = key;
                menuService.Update(model);
                if (Request.IsAjaxRequest())
                {
                    return Json(JsonSuccessWidthIndex(Url.Action("Index"), Resources.Resources.UpdateSuccess), JsonRequestBehavior.AllowGet);
                }
                //báo lỗi ko requet ajax
                throw new NotSupportedException(Resources.Resources.NotAjaxRequest);
            }
            catch (Exception ex)
            {
                HandleException("Message", ex);
                return Json(JsonError(Resources.Resources.ErrorCatch), JsonRequestBehavior.AllowGet);
            }
        }

        private List<CategoryViewModel> GetFunctionViewModel(object selfId = null, string show = "")
        {
            var items = new List<CategoryViewModel>();
            var all = categoryService
                .GetActiveCategories(CategoryType.News, show)
                .OrderBy(x => x.Order)
                .ToList();

            if (selfId != null)
            {
                all = all.Where(x => x.Id != (int)selfId).ToList();
            }

            if (show != "")
            {
                cultureName = show;
            }

            //get parent categories
            var parentFunctions = all.Where(c => c.ParentID == null).OrderBy(c => c.Order);

            return MapCategoriesToViewModels(parentFunctions);
        }
        private void GetSubTree(IList<Category> allCats, CategoryViewModel parent, IList<CategoryViewModel> items)
        {
            var subCats = allCats.Where(c => c.ParentID == parent.CategoryId);
            foreach (var cat in subCats)
            {
                var cate = new CategoryViewModel()
                {
                    CategoryId = cat.Id,
                    //Name = parent.Name + " >> " + cat.Name,
                    //Content = cat.Content,
                    Order = cat.Order,
                    IsActive = cat.IsActive,
                };
                items.Add(cate);
                GetSubTree(allCats, cate, items);
                //add this category

                //recursive call in case your have a hierarchy more than 1 level deep

            }
        }
        private List<CategoryViewModel> GetFunctionViewModelProduct(object selfId = null, string show = "")
        {
            var all = categoryService.GetActiveCategories(CategoryType.Product, show).OrderBy(x => x.Order).ToList();
            List<CategoryViewModel> items = new List<CategoryViewModel>();

            if (selfId != null)
                all = all.Where(x => x.Id != (int)selfId).ToList();
            if (show != "")
            {
                cultureName = show;
            }
            //get parent categories
            IEnumerable<Category> parentFunctions = all.Where(c => c.ParentID == null).OrderBy(c => c.Order);

            foreach (var cat in parentFunctions)
            {
                var cate = new CategoryViewModel()
                {
                    CategoryId = cat.Id,
                    //Name = cat.Name,
                    //Content = cat.Content,
                    Order = cat.Order,
                    IsActive = cat.IsActive,
                };
                items.Add(cate);
                GetSubTree(all.ToList(), cate, items);
                //add the parent category to the item list

                //now get all its children (separate function in case you need recursion)
            }
            return items;
        }
    }
}