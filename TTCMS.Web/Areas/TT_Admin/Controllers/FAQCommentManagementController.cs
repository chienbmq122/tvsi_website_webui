﻿using AutoMapper;
using PagedList;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TTCMS.Areas.TT_Admin.CustomAttributes;
using TTCMS.Core.Infrastructure.Alerts;
using TTCMS.Data;
using TTCMS.Data.Entities;
using TTCMS.Service;
using TTCMS.Service.Common;
using TTCMS.Web.Areas.TT_Admin.Models;

namespace TTCMS.Web.Areas.TT_Admin.Controllers
{
    public class FAQCommentManagementController : BaseController
    {
        // GET: TT_Admin/FAQCommentManagement
        private readonly ICommentService _commentService;
        private readonly ILanguageService _languageService;
        private readonly IQuestionService _questionService;
        public FAQCommentManagementController(
            ICommentService commentService,
            ILanguageService languageService,
            IQuestionService questionService
        )
        {
            _commentService = commentService;
            _languageService = languageService;
            _questionService = questionService;
        }
        
        [HttpGet]
        [AuthorizeUser(FunctionID = "COMMENT", RoleID = "VIEW")]
        public ActionResult Index(int questionId, string show = "", string search = "", int? page = 1, int commentId = 0)
        {
            if (show != "")
            {
                cultureName = show;
            }
            int pageSize = int.Parse(ConfigSettings.ReadSetting("pageSize"));
            int pageNumber = (page ?? 1);
            var comments = commentId == 0 ? _commentService.GetAllParentCommentByQuestion(questionId) : _commentService.GetById(commentId).Replies;
            var commentItems = new List<FAQCommentViewModel>();
            if (comments != null & comments.ToList().Count() > 0)
            {
                foreach(var comment in comments)
                {
                    var commentVM = Mapper.Map<Comment, FAQCommentViewModel>(comment);
                    commentItems.Add(commentVM);
                }
            }
            var table = new CommentManagementViewModel
            {
                Show = show,
                Search = search,
                //Languages = languageVM,
                QuestionId = questionId,
                ModelList = new PagedList<FAQCommentViewModel>(commentItems, pageNumber, pageSize),
            };
            if (Request.IsAjaxRequest())
            {
                return PartialView("_CommentTablePagedList", table);
            }
            var model = new CommentManagementPageViewModel
            {
                TableList = table
            };
            return View(model);
        }

        public ActionResult Detail(int id, string show = "", int? page = 1, string search = "")
        {
            if (show != "")
            {
                cultureName = show;
            }
            int pageSize = int.Parse(ConfigSettings.ReadSetting("pageSize"));
            int pageNumber = (page ?? 1);
            var comment = _commentService.GetById(id);
            var comments = comment.Replies;
            var commentItems = new List<FAQCommentViewModel>();
            commentItems.Add(Mapper.Map<Comment, FAQCommentViewModel>(comment));
            if (comments != null & comments.ToList().Count() > 0)
            {
                foreach (var item in comments)
                {
                    var commentVM = Mapper.Map<Comment, FAQCommentViewModel>(item);
                    commentItems.Add(commentVM);
                }
            }
            var table = new CommentManagementViewModel
            {
                CommentId = id,
                Show = show,
                Search = search,
                //Languages = languageVM,
                QuestionId = comment.QuestionId,
                ModelList = new PagedList<FAQCommentViewModel>(commentItems, pageNumber, pageSize),
            };
            if (Request.IsAjaxRequest())
            {
                return PartialView("_CommentTablePagedList", table);
            }
            return View(table);
        }

        // GET: TT_Admin/FAQCommentManagement/Create
        public ActionResult Create(int questionId = 0, int commentId = 0)
        {
            var model = new CreateCommentViewModel(){
                CommentId = commentId,
                QuestionId = questionId,
            };
            return View(model);
        }

        // POST: TT_Admin/FAQCommentManagement/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateCommentViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Index");
            }
            var comment = _commentService.GetById(viewModel.CommentId);
            if(comment == null)
            {
                try
                {
                    comment = new Comment()
                    {
                        Approved = viewModel.Approved,
                        QuestionId = viewModel.QuestionId,
                        Content = viewModel.Content,
                        CreatedBy = "Admin",
                        AdminCreated = true,
                        CreatedDate = DateTime.Now
                    };
                    var createdComment = _commentService.Create(comment);
                    if(createdComment == null)
                    {

                    }
                    _commentService.SaveChange();
                    return RedirectToAction("Index", new { questionId = comment.QuestionId }).WithSuccess(Resources.Resources.UpdateSuccess);
                }
                catch (Exception ex)
                {
                    HandleException("Message", ex);
                    return RedirectToAction("Index", new { questionId = viewModel.QuestionId }).WithError(Resources.Resources.ErrorCatch);
                }
            }
            try
            {
                var reply = new Comment() { 
                    Approved = viewModel.Approved,
                    ParentCommentId = viewModel.CommentId,
                    QuestionId = comment.QuestionId,
                    Content = viewModel.Content,
                    CreatedBy = "Admin",
                    AdminCreated = true,
                    CreatedDate = DateTime.Now
                };
                if (reply == null)
                {

                }
                var createdReply = _commentService.Create(reply);
                if(createdReply == null)
                {

                }
                _commentService.SaveChange();
                return RedirectToAction("Index", new { questionId = comment.QuestionId }).WithSuccess(Resources.Resources.UpdateSuccess);
            }
            catch (Exception ex)
            {
                HandleException("Message", ex);
                return RedirectToAction("Index", new { questionId = viewModel.QuestionId }).WithError(Resources.Resources.ErrorCatch);
            }
        }

        // GET: TT_Admin/FAQCommentManagement/Edit/5
        public ActionResult Edit(int id)
        {
            var comment = _commentService.GetById(id);
            if (comment == null)
            {

            }
            var commentVM = new CreateCommentViewModel()
            {
                CommentId = comment.Id,
                QuestionId = comment.QuestionId,
                Content = comment.Content,
                Approved = comment.Approved,
            };
            return View(commentVM);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, CreateCommentViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Edit", new { id }).WithError(Resources.Resources.ErrorCatch);
            }
            var comment = _commentService.GetById(id);
            if(comment == null)
            {
                return RedirectToAction("Index", new { questionId = viewModel.QuestionId }).WithError(Resources.Resources.ErrorCatch);
            }
            try
            {
                if (viewModel.Content != null && viewModel.Content != comment.Content) comment.Content = viewModel.Content;
                if (viewModel.Approved != comment.Approved) comment.Approved = viewModel.Approved;
                comment.UpdatedDate = DateTime.Now;
                comment.UpdatedBy = "Admin";
                _commentService.Update(comment);
                return RedirectToAction("Index", new { questionId = comment.QuestionId }).WithSuccess(Resources.Resources.UpdateSuccess);
            } 
            catch (Exception ex)
            {
                HandleException("Message", ex);
                return RedirectToAction("Edit", new { id }).WithError(Resources.Resources.ErrorCatch);
            }
        }

        // POST: TT_Admin/FAQCommentManagement/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Approve(int id, int questionId, bool approved)
        {
            if (!ModelState.IsValid)
            {
                return Json(Url.Action("Index", new { questionId }));
            }
            try
            {
                var comment = _commentService.GetById(id);
                comment.Approved = approved;
                _commentService.Update(comment);
                if (Request.IsAjaxRequest())
                {
                    var reponse = new
                    {
                        Id = id,
                        Code = AjaxAlertConsts.WithSuccess,
                        Msg = Resources.Resources.AlertDataSuccess,
                        Value = approved
                    };
                    return Json(reponse, JsonRequestBehavior.AllowGet);
                }
                return Json(Url.Action("Index", new { questionId }));
            }
            catch (Exception ex)
            {
                HandleException("Message", ex);
                return Json(Url.Action("Index", new { questionId }));
            }
        }

        // GET: TT_Admin/FAQCommentManagement/Delete/5
        public ActionResult Delete(int id, int questionId)
        {
            try
            {
                var comment = _commentService.GetById(id);
                if (comment == null)
                {

                }
                if (comment.Replies != null && comment.Replies.Count > 0)
                {
                    foreach(var reply in comment.Replies.ToList())
                    {
                        _commentService.Delete(reply.Id);
                    }
                }
                _commentService.SaveChange();
                _commentService.Delete(id);
                _commentService.SaveChange();
                if (Request.IsAjaxRequest())
                {
                    var reponse = new
                    {
                        Id = id,
                        Code = AjaxAlertConsts.WithSuccess,
                        Msg = Resources.Resources.AlertDataSuccess
                    };
                    return Json(reponse, JsonRequestBehavior.AllowGet);
                }
                return RedirectToAction("Index", "FAQCommentManagement", new { questionId }).WithSuccess(Resources.Resources.SuccessDeleteFunction);
            }
            catch (Exception ex)
            {
                HandleException("Message", ex);
                return RedirectToAction("Index", new { questionId }).WithError(Resources.Resources.ErrorCatch);
            }
        }
    }
}
