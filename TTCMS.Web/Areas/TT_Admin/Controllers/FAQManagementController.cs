﻿using AutoMapper;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TTCMS.Areas.TT_Admin.Filters;
using TTCMS.Service;
using TTCMS.Service.Common;
using TTCMS.Web.Areas.TT_Admin.Models;
using TTCMS.Core.Infrastructure.Alerts;
using TTCMS.Areas.TT_Admin.CustomAttributes;
using TTCMS.Web.Extensions;
using TTCMS.Data.Entities;
using TTCMS.Web.ViewModels;

namespace TTCMS.Web.Areas.TT_Admin.Controllers
{
    public class FAQManagementController : BaseController
    {
        private readonly IEnumerable<Language> languages;
        private readonly IFAQService FAQ;
        private readonly ILangugage_FAQService langugage_FAQService;
        private readonly ILanguageService languageService;
        public FAQManagementController(IFAQService FAQService, ILangugage_FAQService langugage_FAQService, ILanguageService languageService)
        {
            this.FAQ = FAQService;
            this.langugage_FAQService = langugage_FAQService;
            this.languageService = languageService;

            languages = languageService.GetListForActive();
        }
        // GET: TT_Admin/FAQ
        [HttpGet]
        [AuthorizeUser(FunctionID = "FAQ", RoleID = "VIEW")]
        public ActionResult Index(int? page = 1, string show = "", string search = "")
        {
            if (show != "")
            {
                cultureName = show;
            }
            int pageSize = int.Parse(ConfigSettings.ReadSetting("pageSize"));
            int pageNumber = (page ?? 1);
            var faqs = FAQ.GetList();
            var faqItems = new List<FAQItemViewModel>();
            foreach (var item in faqs)
            {
                var faqDetail = langugage_FAQService.GetByFAQId(item.Id).Select(q => new FAQItemViewModel
                {
                    Id = item.Id,
                    Question = q.Question,
                    Answer = q.Answer,
                    LanguageId = q.LanguageId,
                    IsActived = item.IsActived,
                    IsDeleted = item.IsDeleted
                });
                faqItems.AddRange(faqDetail);
            }
            var table = new FAQViewModel
            {
                Show = show,
                Search = search,
                Languages = Mapper.Map<IEnumerable<Language>, List<LanguageViewModel>>(languages),
                ModelList = new PagedList<FAQItemViewModel>(faqItems, pageNumber, pageSize),
            };

            if (Request.IsAjaxRequest())
            {
                return PartialView("_FAQTable", table);
            }

            var model = new FAQPageViewModel
            {
                TableList = table
            };

            return View(model);
        }

        [HttpGet]
        [AuthorizeUser(FunctionID = "FAQ", RoleID = "CREATE")]
        public ActionResult Create(int id = 0, int languageId = 0)
        {
            var lang = languages;
            var faq = new FAQItemViewModel
            {
                Id = 0
            };
            var model = new FAQCreateViewModel()
            {
                Model = faq
            };
            model.Lang = new SelectList(
                lang.OrderByDescending(x => x.CreatedDate),
                "Id",
                "Name",
                model.Model.LanguageId
            );
            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken, Log("{model}")]
        [AuthorizeUser(FunctionID = "FAQ", RoleID = "CREATE")]
        public ActionResult Create(FAQCreateViewModel viewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    ModelState.AddModelError("", Resources.Resources.WarningData);
                    var listlang = languageService.GetListForActive().ToList();
                    return View(viewModel).WithWarning(Resources.Resources.WarningData);
                }

                var newFAQ = new FAQ() { IsActived = viewModel.Model.IsActived, CreatedDate = DateTime.Now };
                var newLanguageFAQ = new Language_FAQ()
                {
                    Question = viewModel.Model.Question,
                    Answer = viewModel.Model.Answer,
                    LanguageId = viewModel.Model.LanguageId,
                    CreatedDate = DateTime.Now,
                    FAQId = FAQ.Create(newFAQ)
                };
                langugage_FAQService.Create(newLanguageFAQ);
                langugage_FAQService.SaveChange();
                return RedirectToAction("Index").WithSuccess(Resources.Resources.SuccessCreate);
            }
            catch (Exception ex)
            {
                HandleException("Message ", ex);
                return RedirectToAction("Index").WithError(Resources.Resources.ErrorCatch);
            }
        }
        [AuthorizeUser(FunctionID = "FAQ", RoleID = "EDIT")]
        public ActionResult Edit(int Id = 0, int LanguageId = 0)
        {
            try
            {
                if (Id == 0) return RedirectToAction("Index").WithError(Resources.Resources.ErrorNoData);

                var curFAQ = FAQ.GetById(Id);
                if (curFAQ == null)
                {
                    return RedirectToAction("Index").WithError(Resources.Resources.ErrorNoData);
                }

                var lang = languages;
                var faq = FAQ.GetById(curFAQ.Id);
                var faqItems = langugage_FAQService.GetById(curFAQ.Id, LanguageId).Select(q => new FAQItemViewModel
                {
                    Id = curFAQ.Id,
                    Question = q.Question,
                    Answer = q.Answer,
                    LanguageId = q.LanguageId,
                    IsActived = curFAQ.IsActived,
                    IsDeleted = curFAQ.IsDeleted
                });
                var model = new FAQCreateViewModel();
                model.Model = faqItems.FirstOrDefault();
                model.Lang = new SelectList(
                    lang.Where(l =>l.Id == model.Model.LanguageId),
                    "Id",
                    "Name",
                    model.Model.LanguageId
                );
                return View("Create", model);
            }
            catch (Exception ex)
            {
                HandleException("Message", ex);
                return RedirectToAction("Index").WithError(Resources.Resources.ErrorCatch);
            }

        }
        [AuthorizeUser(FunctionID = "FAQ", RoleID = "EDIT"), ValidateInput(false)]
        [HttpPost, ValidateAntiForgeryToken, Log("{model}")]
        public ActionResult Edit(FAQCreateViewModel viewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    ModelState.AddModelError("", Resources.Resources.WarningData);
                    var lang = languageService.GetListForActive().ToList();
                    viewModel.Lang = new SelectList(languages.Where(l => l.Id == viewModel.Model.LanguageId), "Id", "Name", viewModel.Model.LanguageId);
                    return View("Create", viewModel).WithWarning(Resources.Resources.WarningData);
                }
                // Lay tin chi tiet
                var faq = FAQ.GetById(viewModel.Model.Id);
                var LanguageFAQ = langugage_FAQService.GetById(viewModel.Model.Id, viewModel.Model.LanguageId).FirstOrDefault();

                if (faq == null)
                {
                    var newFAQ = new FAQ();
                    newFAQ.IsActived = viewModel.Model.IsActived;
                    newFAQ.CreatedDate = DateTime.Now;
                    var newLanguageFAQ = new Language_FAQ()
                    {
                        Question = viewModel.Model.Question,
                        Answer = viewModel.Model.Answer,
                        LanguageId = viewModel.Model.LanguageId,
                        CreatedDate = DateTime.Now,
                        FAQId = FAQ.Create(newFAQ)
                    };
                    langugage_FAQService.Create(newLanguageFAQ);
                }
                else
                {
                    faq.IsActived = viewModel.Model.IsActived;
                    LanguageFAQ.Question = viewModel.Model.Question;
                    LanguageFAQ.Answer = viewModel.Model.Answer;
                    LanguageFAQ.LanguageId = viewModel.Model.LanguageId;
                    faq.UpdatedDate = DateTime.Now;
                    langugage_FAQService.Update(LanguageFAQ);
                    FAQ.Update(faq);
                }
                FAQ.SaveChange();
                return RedirectToAction("Index").WithSuccess(Resources.Resources.UpdateSuccess);
            }
            catch (Exception ex)
            {
                HandleException("Message", ex);
                return RedirectToAction("Index").WithError(Resources.Resources.ErrorCatch);
            }

        }
        [AuthorizeUser(FunctionID = "FAQ", RoleID = "DELETE"), Log("{Id}")]
        public ActionResult Delete(int id = 0)
        {
            try
            {
                var faq = FAQ.GetById(id);
                faq.UpdatedDate = DateTime.Now;
                faq.DeletedAt = DateTime.Now;
                FAQ.Update(faq);
                FAQ.SaveChange();
                if (Request.IsAjaxRequest())
                {
                    var reponse = new
                    {
                        Id = id,
                        Code = AjaxAlertConsts.WithSuccess,
                        Msg = Resources.Resources.AlertDataSuccess
                    };

                    return Json(reponse, JsonRequestBehavior.AllowGet);
                }

                return RedirectToAction("Index").WithSuccess(Resources.Resources.SuccessDeleteFunction);
            }
            catch (Exception ex)
            {
                HandleException("Message", ex);
                var reponse = new { Id = id, Code = AjaxAlertConsts.WithError, Msg = Resources.Resources.AlertDataCatch };
                return Json(reponse, JsonRequestBehavior.AllowGet);
            }

        }

    }
}