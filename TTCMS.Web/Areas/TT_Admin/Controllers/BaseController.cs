﻿using AutoMapper;
using log4net;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using TTCMS.Core;
using TTCMS.Core.Infrastructure.Alerts;
using TTCMS.Core.Json;
using TTCMS.Data.Entities;
using TTCMS.Service;
using TTCMS.Web.Areas.TT_Admin.Models;
using TTCMS.Web.ViewModels;

namespace TTCMS.Web.Areas.TT_Admin.Controllers
{
    [Authorize]
    public class BaseController : Controller
    {
        protected string cultureName = null;
        private static readonly ILog logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private readonly ILanguageService languageService;

        public BaseController(ILanguageService languageService)
        {
            this.languageService = languageService;
        }

        public BaseController()
        {
            // TODO: Complete member initialization
        }

        protected void HandleApplicationException(ApplicationException ex)
        {
            logger.Error(ex);
            RedirectToAction("Index", "Home");
        }

        protected void HandleException(string message, Exception ex)
        {
            logger.Error(message + "::" + ex.Message);
            if (ex.InnerException != null)
            {
                HandleException(ex.Message, ex.InnerException);
            }
            RedirectToAction("Index", "Home");
        }

        [ChildActionOnly]
        public ActionResult _TabLanguage()
        {
            IEnumerable<LanguageItemViewModel> model = Mapper.Map<IEnumerable<Language>, IEnumerable<LanguageItemViewModel>>(languageService.GetListForActive());
            return PartialView(model.ToArray());
        }

        protected override IAsyncResult BeginExecuteCore(AsyncCallback callback, object state)
        {
            // Attempt to read the culture cookie from Request
            HttpCookie cultureCookie = Request.Cookies["_culture"];
            if (cultureCookie != null)
                cultureName = cultureCookie.Value;
            else
                cultureName = Request.UserLanguages != null && Request.UserLanguages.Length == 0 ?
                           Request.UserLanguages[0] :  // obtain it from HTTP header AcceptLanguages
                           null;
            // Validate culture name
            cultureName = CultureHelper.GetImplementedCulture(cultureName); // This is safe

            // Modify current thread's cultures            
            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(cultureName);
            Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;

            return base.BeginExecuteCore(callback, state);
        }

        /// <summary>
        /// change culture method
        /// </summary>
        /// <param name="language"></param>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        /// 
        public ActionResult SetCulture(string culture)
        {
            // Validate input
            culture = CultureHelper.GetImplementedCulture(culture);
            // Save culture in a cookie
            HttpCookie cookie = Request.Cookies["_culture"];
            if (cookie != null)
                cookie.Value = culture;   // update cookie value
            else
            {
                cookie = new HttpCookie("_culture");
                cookie.Value = culture;
                cookie.Expires = DateTime.Now.AddYears(1);
            }
            Response.Cookies.Add(cookie);
            return RedirectToAction("Index", "NewsManagement");
        }
        public ActionResult ChangeCulture(string language, string returnUrl)
        {
            Session[SystemConsts.CULTURE] = new CultureInfo(language);
            return Redirect(returnUrl);
        }
        [HttpGet]
        public ActionResult AjaxAlert(string code = "", string msg = "")
        {
            var model = new AjaxAlertViewModel();
            if (code != "" && msg != "")
            {
                model.Code = code;
                model.Msg = msg;
            }
            return PartialView(model);
        }
        public JsonResponse JsonError(string message)
        {
            JsonResponse response = new JsonResponse() { Code = AjaxAlertConsts.WithError, Msg = message, Success = false };
            return response;
        }
        public JsonResponse JsonErrorWidthIndex(string message, string redirectUrl)
        {
            JsonResponse response = new JsonResponse() { Code = AjaxAlertConsts.WithError, Msg = message, Success = false, RedirectUrl = redirectUrl };
            return response;
        }
        public JsonResponse JsonSuccess(string redirectUrl = null, string message = null, string partialViewHtml = null)
        {
            JsonResponse response = new JsonResponse();
            response.Code = AjaxAlertConsts.WithSuccess;
            response.RedirectUrl = redirectUrl;
            response.Msg = message;
            response.PartialViewHtml = partialViewHtml;
            response.Success = true;
            return response;
        }
        public JsonResponse JsonSuccessWidthIndex(string redirectUrl = null, string message = null)
        {
            JsonResponse response = new JsonResponse();
            response.Code = AjaxAlertConsts.WithSuccess;
            response.RedirectUrl = redirectUrl;
            response.Msg = message;
            response.PartialViewHtml = null;
            response.Success = true;
            return response;
        }
        protected string RenderPartialViewToString(string viewName, object model = null)
        {
            if (string.IsNullOrEmpty(viewName))
            {
                viewName = ControllerContext.RouteData.GetRequiredString("action");
            }

            ViewData.Model = model;

            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }
        protected string RenderViewToString(string viewName, object model = null)
        {
            if (string.IsNullOrEmpty(viewName))
            {
                viewName = ControllerContext.RouteData.GetRequiredString("action");
            }

            var viewData = new ViewDataDictionary(model);

            using (StringWriter sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindView(ControllerContext, viewName, null);
                var viewContext = new ViewContext(ControllerContext, viewResult.View, viewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }
        protected string RenderViewToString<T>(string viewName, T model)
        {
            if (string.IsNullOrEmpty(viewName))
            {
                viewName = ControllerContext.RouteData.GetRequiredString("action");
            }

            var viewData = new ViewDataDictionary<T>(model);

            using (StringWriter sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindView(ControllerContext, viewName, null);
                var viewContext = new ViewContext(ControllerContext, viewResult.View, viewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }

        // Lay danh sach ngon ngu
        protected SelectList GetLanguageDropdownList(IEnumerable<Language> languages, int languageId, int selectedValue)
        {
            return new SelectList(
                languages.Where(item => item.Id == languageId),
                "Id",
                "Name",
                selectedValue
            );
        }

        // Lay danh sach danh muc
        protected SelectList GetCategoryDropdownList(ICategoryService categoryService, string languageCode, int selectedValue = 0)
        {
            var categories = categoryService.GetActiveCategories(languageCode);
            var categoryViewModels = MapCategoriesToViewModels(categories, languageCode);

            // Them chuoi rong trong truong hop danh muc da tao khong co categoryItem cho ngon ngu nay
            if (selectedValue > 0 && !categoryViewModels.Any(x => x.CategoryId == selectedValue))
            {
                categoryViewModels.Insert(0, new CategoryViewModel() { CategoryId = selectedValue, Name = "" });
            }

            // Them lua chon khong co danh muc
            categoryViewModels.Insert(0, new CategoryViewModel() { CategoryId = 0, Name = Resources.Resources.NoCategory });

            return new SelectList(
                categoryViewModels,
                "CategoryId",
                "Name",
                selectedValue
            );
        }

        protected List<CategoryViewModel> MapCategoriesToViewModels(IEnumerable<Category> categories = null, string languageCode = "")
        {
            var viewModels = new List<CategoryViewModel>();

            if (categories == null)
            {
                return viewModels;
            }

            foreach (var category in categories)
            {
                var viewModel = Mapper.Map<Category, CategoryViewModel>(category);

                if (string.IsNullOrEmpty(languageCode))
                {
                    //viewModel.Name = viewModel
                    //  .CategoryItems
                    //  .OrderBy(item => item.CreatedDate)
                    //  .FirstOrDefault()?.Name;
                }
                else
                {
                    viewModel.Name = viewModel
                        .CategoryItems
                        .FirstOrDefault(x => x.Language.Code == languageCode)?.Name;
                }

                viewModels.Add(viewModel);
            }

            return viewModels;
        }

        protected MediaViewModel GetMediaByUrl(string fileUrl)
        {
            var result = new MediaViewModel();
            if (string.IsNullOrEmpty(fileUrl))
            {
                return result;
            }

            var splitIndex = fileUrl.LastIndexOf('/');
            result = new MediaViewModel
            {
                Name = fileUrl.Substring(splitIndex + 1, fileUrl.Length - (splitIndex + 1)),
                Path = fileUrl.Substring(0, (splitIndex + 1)),
                FullPath = fileUrl
            };
            return result;
        }

    }
}