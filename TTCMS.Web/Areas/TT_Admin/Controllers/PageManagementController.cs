﻿using AutoMapper;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TTCMS.Areas.TT_Admin.Filters;
using TTCMS.Data.Entities;
using TTCMS.Web.Areas.TT_Admin.Models;
using TTCMS.Core.Infrastructure.Alerts;
using TTCMS.Areas.TT_Admin.CustomAttributes;
using TTCMS.Web.Extensions;
using Microsoft.AspNet.Identity;
using TTCMS.Service;
using TTCMS.Data.Infrastructure;
using TTCMS.Service.Common;
using TTCMS.Web.ViewModels;

namespace TTCMS.Web.Areas.TT_Admin.Controllers
{
    [Authorize]
    public class PageManagementController : BaseController
    {
        private readonly IEnumerable<Language> languages;
        private readonly ILanguageService languageService;
        private readonly ISinglePageService singlePageService;
        private readonly ICategoryService categoryService;
        private readonly ILanguage_SinglePageService language_SinglePageService;
        public PageManagementController(
            ILanguageService languageService,
            ISinglePageService singlePageService,
            ICategoryService categoryService,
            ILanguage_SinglePageService language_SinglePageService
        )
        {
            this.languageService = languageService;
            this.singlePageService = singlePageService;
            this.categoryService = categoryService;
            this.language_SinglePageService = language_SinglePageService;

            languages = languageService.GetListForActive("*");
        }

        // GET: /PageManagement/Role/
        //[OutputCache(Duration = 60)]
        [AuthorizeUser(FunctionID = "PAGE", RoleID = "VIEW")]
        [HttpGet]
        public ActionResult Index(int page = 1, string show = "", string search = "")
        {
            int pageSize = int.Parse(ConfigSettings.ReadSetting("pageSize"));
            var list = singlePageService.GetPageList(new Page(page, pageSize), search, show);

            // map it to a paged list of models.
            var actionViewModel = Mapper.Map<IPagedList<SinglePage>, IPagedList<SinglePageViewModel>>(list);
            var languageViewModels = Mapper.Map<IEnumerable<Language>, List<LanguageViewModel>>(languages);

            var table = new PageTableViewModel
            {
                Languages = languageViewModels,
                ModelList = actionViewModel,
                Show = show,
                Search = search,
                PageIndex = page
            };

            if (Request.IsAjaxRequest())
            {
                return PartialView("_PageTable", table);
            }

            var model = new SinglePageListViewModel
            {
                Languages = languageViewModels,
                TableList = table,
                Lang = languages.ToSelectListItems(show)
            };

            return View(model);
        }

        // GET: /PageManagement/Role/Create
        [AuthorizeUser(FunctionID = "PAGE", RoleID = "CREATE")]
        [HttpGet]
        public ActionResult Create(int id = 0, int languageId = 0)
        {
            var model = new SinglePageViewModel
            {
                Id = id
            };

            // Truong hop them ngon ngu (Trang da ton tai)
            if (id != 0)
            {
                var singlePage = singlePageService.GetById(id);

                model.IsActive = singlePage.IsActive;
                model.Img_Thumbnail = singlePage.Img_Thumbnail;
                model.IsHot = singlePage.IsHot;
                model.IsHome = singlePage.IsHome;
                model.Order = singlePage.Order;
                model.CategoryId = singlePage.Category?.Id ?? 0;
            }

            // Lay ngon ngu mac dinh duoc chon
            var language = languages.FirstOrDefault(
                 item =>
                     (languageId == 0 && item.Code == "*") ||
                     (languageId != 0 && item.Id == languageId)
            );

            model.Language_SinglePage = new Language_SinglePageViewModel()
            {
                Language = Mapper.Map<Language, LanguageViewModel>(language),
            };

            // Danh sach ngon ngu
            model.Lang = GetLanguageDropdownList(languages, languageId, model.Language_SinglePage.Id);

            // Danh sach danh muc
            model.ListCategory = GetCategoryDropdownList(categoryService, language.Code, model.CategoryId);

            return View(model);
        }

        // POST: /GroupMenuManagement/Role/Create
        [HttpPost, ValidateInput(false)]
        [ValidateAntiForgeryToken, Log("{model}")]
        [AuthorizeUser(FunctionID = "PAGE", RoleID = "CREATE")]
        public ActionResult Create(SinglePageViewModel viewModel)
        {
            try
            {
                var language = languageService.GetById(viewModel.Language_SinglePage.Language.Id);
                if (!ModelState.IsValid)
                {
                    ModelState.AddModelError("", Resources.Resources.WarningData);
                    viewModel.Lang = GetLanguageDropdownList(languages, language.Id, language.Id);
                    viewModel.ListCategory = GetCategoryDropdownList(categoryService, language.Code, viewModel.CategoryId);
                    return View(viewModel).WithWarning(Resources.Resources.WarningData);
                }

                viewModel.Language_SinglePage.Slug = viewModel.Language_SinglePage.Slug.Trim();
                viewModel.UpdatedDate = DateTime.Now;

                var singlePage = Mapper.Map<SinglePageViewModel, SinglePage>(viewModel);
                singlePage.CreatedDate = DateTime.Now;
                singlePage.CreatedById = User.Identity.GetUserId();
                singlePage.UpdatedById = User.Identity.GetUserId();
                singlePage.Category = categoryService.GetById(viewModel.CategoryId);

                var result = singlePageService.Create(singlePage);
                var language_SinglePage = Mapper.Map<Language_SinglePageViewModel, Language_SinglePage>(viewModel.Language_SinglePage);
                language_SinglePage.SinglePage = result;
                language_SinglePage.CreatedDate = DateTime.Now;
                language_SinglePage.Language = language;
                language_SinglePage.LanguageId = language_SinglePage.Language.Code;

                language_SinglePageService.Create(language_SinglePage);
                language_SinglePageService.SaveChange();

                return RedirectToAction("Index").WithSuccess(Resources.Resources.SuccessCreate);
            }
            catch (Exception ex)
            {
                HandleException("Message ", ex);
                return RedirectToAction("Index").WithError(Resources.Resources.ErrorCatch);
            }
        }

        [AuthorizeUser(FunctionID = "PAGE", RoleID = "EDIT")]
        public ActionResult Edit(int id = 0, int pageIndex = 1)
        {
            try
            {
                if (id == 0) return RedirectToAction("Index").WithError(Resources.Resources.ErrorNoData);

                var language_SinglePage = language_SinglePageService.GetById(id);
                if (language_SinglePage == null)
                {
                    return RedirectToAction("Index").WithError(Resources.Resources.ErrorNoData);
                }

                var lang = languageService.GetListForActive().ToList();
                var singlePage = singlePageService.GetById(language_SinglePage.SinglePage.Id);
                var model = Mapper.Map<SinglePage, SinglePageViewModel>(singlePage);
                model.Language_SinglePage = Mapper.Map<Language_SinglePage, Language_SinglePageViewModel>(language_SinglePage);
                model.PageIndex = pageIndex;
                model.Lang = GetLanguageDropdownList(languages, language_SinglePage.Language.Id, model.Language_SinglePage.Id);
                model.ListCategory = GetCategoryDropdownList(categoryService, language_SinglePage.Language.Code, model.CategoryId);

                return View("Create", model);
            }
            catch (Exception ex)
            {
                HandleException("Message", ex);
                return RedirectToAction("Index").WithError(Resources.Resources.ErrorCatch);
            }

        }
        [AuthorizeUser(FunctionID = "PAGE", RoleID = "EDIT"), ValidateInput(false)]
        [HttpPost, ValidateAntiForgeryToken, Log("{model}")]

        public ActionResult Edit(SinglePageViewModel viewModel)
        {
            try
            {
                var language = languageService.GetById(viewModel.Language_SinglePage.Language.Id);
                if (!ModelState.IsValid)
                {
                    ModelState.AddModelError("", Resources.Resources.WarningData);
                    viewModel.Lang = GetLanguageDropdownList(languages, language.Id, language.Id);
                    viewModel.ListCategory = GetCategoryDropdownList(categoryService, language.Code, viewModel.CategoryId);
                    return View("Create", viewModel).WithWarning(Resources.Resources.WarningData);
                }

                viewModel.Language_SinglePage.Slug = viewModel.Language_SinglePage.Slug.Trim();

                // Lay tin chi tiet
                var newsItem = language_SinglePageService.GetById(viewModel.Language_SinglePage.Id);
                if (newsItem == null)
                {
                    // Neu khong co du lieu thi tao moi
                    var item = Mapper.Map<Language_SinglePageViewModel, Language_SinglePage>(viewModel.Language_SinglePage);
                    item.UpdatedDate = DateTime.Now;
                    item.CreatedDate = DateTime.Now;
                    item.SinglePage = singlePageService.GetById(viewModel.Id);
                    item.Language = language;
                    item.LanguageId = item.Language.Code;
                    item.SinglePageId = item.SinglePage.Id;
                    newsItem = language_SinglePageService.Create(item);
                }
                else
                {
                    // Neu co du lieu thi cap nhat lai du lieu
                    var item = Mapper.Map(viewModel.Language_SinglePage, newsItem);
                    item.UpdatedDate = DateTime.Now;
                    item.Language = language;
                    item.LanguageId = item.Language.Code;
                    language_SinglePageService.Update(item);
                }

                // Cap nhat trang
                var singlePage = Mapper.Map(viewModel, newsItem.SinglePage);
                singlePage.UpdatedDate = DateTime.Now;
                singlePage.UpdatedById = User.Identity.GetUserId();

                // Cap nhat danh muc
                if (viewModel.CategoryId != singlePage.Category?.Id)
                {
                    singlePage.Category = categoryService.GetById(viewModel.CategoryId);
                }

                singlePageService.Update(singlePage);
                language_SinglePageService.SaveChange();

                return RedirectToAction("Index").WithSuccess(Resources.Resources.UpdateSuccess);
            }
            catch (Exception ex)
            {
                HandleException("Message", ex);
                return RedirectToAction("Index").WithError(Resources.Resources.ErrorCatch);
            }

        }


        [AuthorizeUser(FunctionID = "PAGE", RoleID = "DELETE"), Log("{Id}")]
        public ActionResult Delete(int id = 0)
        {
            try
            {
                var news = singlePageService.GetById(id);
                news.UpdatedDate = DateTime.Now;
                news.DeletedAt = DateTime.Now;

                var newsItems = language_SinglePageService.GetBySinglePage(id);

                foreach (var item in newsItems)
                {
                    item.UpdatedDate = DateTime.Now;
                    item.DeletedAt = DateTime.Now;
                    language_SinglePageService.Update(item);
                }

                singlePageService.Update(news);
                singlePageService.SaveChange();

                if (Request.IsAjaxRequest())
                {
                    var reponse = new
                    {
                        Id = id,
                        Code = AjaxAlertConsts.WithSuccess,
                        Msg = Resources.Resources.AlertDataSuccess
                    };

                    return Json(reponse, JsonRequestBehavior.AllowGet);
                }

                return RedirectToAction("Index").WithSuccess(Resources.Resources.SuccessDeleteFunction);
            }
            catch (Exception ex)
            {
                HandleException("Message", ex);
                var reponse = new { Id = id, Code = AjaxAlertConsts.WithError, Msg = Resources.Resources.AlertDataCatch };
                return Json(reponse, JsonRequestBehavior.AllowGet);
            }

        }
    }
}