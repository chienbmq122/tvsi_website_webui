﻿using AutoMapper;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TTCMS.Areas.TT_Admin.CustomAttributes;
using TTCMS.Areas.TT_Admin.Filters;
using TTCMS.Core.Infrastructure.Alerts;
using TTCMS.Data.Entities;
using TTCMS.Service;
using TTCMS.Service.Common;
using TTCMS.Web.Areas.TT_Admin.Models;
using TTCMS.Web.ViewModels;

namespace TTCMS.Web.Areas.TT_Admin.Controllers
{
    public class FAQQuestionManagementController : BaseController
    {
        private readonly IQuestionService _questionService;
        private readonly ILanguageService _languageService;
        private readonly ILanguage_QuestionService _language_QuestionService;
        private readonly IEnumerable<Language> languages;
        private readonly IFAQService faqService;
        private readonly ITopicService _topicService;
        private readonly IQuestionAttachmentService _questionAttachmentService;
        private readonly IMediaService mediaService;
        private readonly IFAQ_MediaService faq_mediaService;
        public FAQQuestionManagementController(
            IQuestionService questionService,
            ILanguage_QuestionService language_QuestionService,
            ILanguageService languageService,
            IFAQService FAQService,
            ITopicService topicService,
            IQuestionAttachmentService questionAttachmentService,
            IMediaService mediaService,
            IFAQ_MediaService FAQ_MediaService
        )
        {
            _language_QuestionService = language_QuestionService;
            _questionService = questionService;
            _languageService = languageService;
            faqService = FAQService;
            _topicService = topicService;
            languages = _languageService.GetListForActive();
            _questionAttachmentService = questionAttachmentService;
            this.mediaService = mediaService;
            faq_mediaService = FAQ_MediaService;
        }
        // GET: TT_Admin/FAQQuestionManagement
        [AuthorizeUser(FunctionID = "QUESTION", RoleID = "VIEW")]
        public ActionResult Index(int? page = 1, string show = "", string search = "")
        {
            if (show != "")
            {
                cultureName = show;
            }
            int pageSize = int.Parse(ConfigSettings.ReadSetting("pageSize"));
            int pageNumber = (page ?? 1);
            var questions = !string.IsNullOrEmpty(search) ? 
                _language_QuestionService
                .GetList().Where(x => x.QuestionName.ToLower().Contains(search.ToLower()))
                .OrderByDescending(x => x.CreatedDate).Select(x => x.Question) 
                : _questionService.GetList().OrderByDescending(x => x.CreatedDate);
            var questionItems = new List<QuestionViewModel>();
            if (questions != null && questions.ToList().Count() > 0)
            {
                foreach (var item in questions)
                {
                    var questionDetail = new QuestionViewModel
                    {
                        Id = item.Id,
                        Active = item.Active,
                        IsWebsiteShown = item.IsWebsiteShown,
                        Latest = item.Latest,
                        CreatedBy = item.CreatedBy,
                        CreatedDate = item.CreatedDate,
                        TopicList = new List<TopicViewModel>()
                    };
                    if (questionDetail != null)
                    {
                        var languageQuestions = _language_QuestionService.GetByQuestion(item.Id).Select(x => new FAQLanguageQuestionViewModel
                        {
                            Id = x.Id,
                            QuestionName = x.QuestionName,
                            Content = x.Content,
                            ContentBrief = !string.IsNullOrEmpty(x.ContentBrief) ? x.ContentBrief : "",
                            Language = Mapper.Map<Language, LanguageViewModel>(x.Language)
                        });
                        questionDetail.LanguageQuestions = languageQuestions.ToList();
                        if (item.Topic != null && item.Topic.Language_Topic.ToList().Count > 0)
                        {
                            foreach(var topic in item.Topic.Language_Topic)
                            {
                                var topicVM = new TopicViewModel()
                                {
                                    Id = topic.Id,
                                    TopicName = topic.TopicName,
                                };
                                questionDetail.TopicList.Add(topicVM);
                            }
                        }
                    }
                    questionItems.Add(questionDetail);
                }
            }
            var languageVM = Mapper.Map<IEnumerable<Language>, List<LanguageViewModel>>(languages.Where(x => x.Id != 3));
            var table = new FAQQuestionManagementViewModel
            {
                Show = show,
                Search = search,
                Languages = languageVM,
                ModelList = new PagedList<QuestionViewModel>(questionItems, pageNumber, pageSize),
            };

            if (Request.IsAjaxRequest())
            {
                return PartialView("_QuestionTablePagedList", table);
            }

            var model = new FAQQuestionManagementPageViewModel
            {
                Languages = languageVM,
                TableList = table
            };

            return View(model);
        }

        [HttpGet]
        [AuthorizeUser(FunctionID = "QUESTION", RoleID = "CREATE")]
        public ActionResult Create(int id = 0, int languageId = 0)
        {
            var model = new QuestionEditViewModel();

            if (id != 0)
            {
                var question = _questionService.GetById(id);
                model = Mapper.Map<Question, QuestionEditViewModel>(question);
            }

            // Lay ngon ngu mac dinh duoc chon
            var language = languages.FirstOrDefault(
                item =>
                    (languageId == 0 && item.Code == "*") ||
                    (languageId != 0 && item.Id == languageId)
            );

            model.LanguageQuestion = new FAQLanguageQuestionViewModel()
            {
                Language = Mapper.Map<Language, LanguageViewModel>(language),
            };

            // Danh sach ngon ngu
            model.Lang = languageId == 0 ? new SelectList(
                languages,
                "Id",
                "Name",
                model.LanguageQuestion.Language.Id
            ) : new SelectList(
                languages.Where(item => item.Id == languageId),
                "Id",
                "Name",
                model.LanguageQuestion.Language.Id
            );


            // Truong hop them ngon ngu (danh muc da ton tai)
            if (id != 0)
            {
                var question = _questionService.GetById(id);
                if (question != null)
                {
                    model.Topic = Mapper.Map<Topic, TopicViewModel>(question.Topic);
                }
            }
            else
            {
                var topics = _topicService.GetListForActive();
                var topicVMs = MapTopicsToViewModels(topics, language.Code);
                model.DropTopics = new SelectList(
                    topicVMs,
                    "Id",
                    "TopicName",
                    null
                );
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken, Log("{model}")]
        [AuthorizeUser(FunctionID = "TOPIC", RoleID = "CREATE")]
        public ActionResult Create(QuestionEditViewModel viewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    ModelState.AddModelError("", Resources.Resources.WarningData);
                    var listlang = _languageService.GetListForActive().ToList();
                    viewModel.Lang = new SelectList(listlang.OrderByDescending(x => x.CreatedDate), "Id", "Name", viewModel.LanguageId);
                    return View(viewModel).WithWarning(Resources.Resources.WarningData);
                }
                var language = _languageService.GetById(viewModel.LanguageId);

                if (viewModel.Id == 0)
                {
                    var topic = _topicService.GetById(viewModel.TopicId);
                    var question = Mapper.Map<QuestionEditViewModel, Question>(viewModel);
                    if (question == null)
                    {
                        return RedirectToAction("Index").WithError("Lỗi: Không khởi tạo được Câu hỏi (EX_QUESTION_001)");
                    }
                    question.CreatedBy = "admin";
                    question.CreatedDate = DateTime.Now;
                    question.UpdatedBy = "admin";
                    question.UpdatedDate = DateTime.Now;
                    question.Topic = topic;
                    var createdQuestion = _questionService.Create(question);
                    var languageQuestion = Mapper.Map<QuestionEditViewModel, Language_Question>(viewModel);
                    if (languageQuestion == null)
                    {
                        return RedirectToAction("Index").WithError("Lỗi: Không khởi tạo được Câu hỏi (EX_QUESTION_002)");
                    }
                    languageQuestion.CreatedBy = "admin";
                    languageQuestion.CreatedDate = DateTime.Now;
                    languageQuestion.UpdatedBy = "admin";
                    languageQuestion.UpdatedDate = DateTime.Now;
                    languageQuestion.QuestionId = createdQuestion.Id;
                    languageQuestion.Question = createdQuestion;
                    languageQuestion.Language = language;
                    var createdLanguageTopic = _language_QuestionService.Create(languageQuestion);
                    if (createdLanguageTopic != null && createdQuestion != null)
                    {
                        _questionService.SaveChange();
                        _language_QuestionService.SaveChange();
                    }
                }
                else
                {
                    var question = _questionService.GetById(viewModel.Id);
                    if (question == null)
                    {
                        return RedirectToAction("Index").WithError(Resources.Resources.ErrorCatch);
                    }
                    
                    var languageQuestion = _language_QuestionService.GetByQuestion(viewModel.Id, language.Code);
                    if (languageQuestion != null)
                    {
                        return RedirectToAction("Index").WithError(Resources.Resources.ErrorCatch);
                    }
                    languageQuestion = Mapper.Map<QuestionEditViewModel, Language_Question>(viewModel);
                    if (languageQuestion != null)
                    {
                        languageQuestion.QuestionId = question.Id;
                        languageQuestion.Question = question;
                        languageQuestion.Language = language;
                        languageQuestion.CreatedBy = "admin";
                        languageQuestion.CreatedDate = DateTime.Now;
                    }
                    var createdLanguageQuestion = _language_QuestionService.Create(languageQuestion);
                    if(createdLanguageQuestion == null)
                    {
                        return RedirectToAction("Index").WithError("Lỗi: Không khởi tạo được Câu hỏi (EX_QUESTION_003)");
                    }
                    try
                    {
                        _language_QuestionService.SaveChange();
                    } catch(Exception ex)
                    {
                        return RedirectToAction("Index").WithError("Lỗi: Không khởi tạo được Câu hỏi (EX_QUESTION_004)");
                    }
                }
                return RedirectToAction("Index").WithSuccess(Resources.Resources.SuccessCreate);
            }
            catch (Exception ex)
            {
                HandleException("Message ", ex);
                return RedirectToAction("Index").WithError("Lỗi: Không khởi tạo được Câu hỏi (EX_QUESTION_000)");
            }
        }

        [HttpGet]
        [AuthorizeUser(FunctionID = "QUESTION", RoleID = "CREATE")]
        public ActionResult Edit(int id, int languageId = 0)
        {
            var languageQuestion = _language_QuestionService
                .GetById(id);
            if (languageQuestion == null)
            {
                return RedirectToAction("Index").WithError("Lỗi: Câu hỏi không tồn tại (EX_EDIT_QUESTION_001)");
            }
            var question = languageQuestion.Question;
            if (question == null)
            {
                return RedirectToAction("Index").WithError("Lỗi: Câu hỏi không tồn tại (EX_EDIT_QUESTION_002)");
            }
            var model = Mapper.Map<Language_Question, QuestionEditViewModel>(languageQuestion);
            model = Mapper.Map(question, model);
            // Lay ngon ngu mac dinh duoc chon
            var language = languages.FirstOrDefault(
                item =>
                    (languageId == 0 && item.Code == "*") ||
                    (languageId != 0 && item.Id == languageId)
            );
            model.Topic = Mapper.Map<Topic, TopicViewModel>(question.Topic);

            model.LanguageQuestion = new FAQLanguageQuestionViewModel()
            {
                Language = Mapper.Map<Language, LanguageViewModel>(language),
            };

            // Danh sach ngon ngu
            model.Lang = languageId == 0 ? new SelectList(
                languages,
                "Id",
                "Name",
                model.LanguageQuestion.Language.Id
            ) : new SelectList(
                languages.Where(item => item.Id == languageId),
                "Id",
                "Name",
                model.LanguageQuestion.Language.Id
            );
            // Danh sach danh muc (khong phai la danh muc con)
            var topics = _topicService.GetListForActive();
            var topicVMs = MapTopicsToViewModels(topics, language.Code);
            model.DropTopics = new SelectList(
                topicVMs,
                "Id",
                "TopicName",
                null
            );
            return View(model);
        }

        [HttpPost, ValidateAntiForgeryToken, Log("{model}")]
        [AuthorizeUser(FunctionID = "QUESTION", RoleID = "EDIT")]
        public ActionResult Edit(QuestionEditViewModel viewModel, int questionId = 0, int languageQuestionId = 0)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    ModelState.AddModelError("", Resources.Resources.WarningData);
                    var lang = _languageService.GetListForActive().ToList();
                    viewModel.Lang = new SelectList(lang, "Id", "Name", viewModel.LanguageId);
                    return View(viewModel).WithWarning(Resources.Resources.WarningData);
                }
                viewModel.UpdatedDate = DateTime.Now;
                viewModel.UpdatedBy = "admin";
                var language = _languageService.GetById(viewModel.LanguageId);
                // Lay danh muc chi tiet
                var languageQuestion = languageQuestionId == 0 ? _language_QuestionService.GetByQuestion(questionId).FirstOrDefault() : _language_QuestionService.GetById(languageQuestionId);
                if (languageQuestion == null)
                {
                    // Neu khong co du lieu thi tao moi
                    languageQuestion = Mapper.Map<QuestionEditViewModel, Language_Question>(viewModel);
                    languageQuestion.Language = language;
                    languageQuestion = _language_QuestionService.Create(languageQuestion);
                }
                else
                {
                    // Neu co du lieu thi cap nhat lai du lieu
                    languageQuestion = Mapper.Map(viewModel, languageQuestion);
                    if (languageQuestion != null)
                    {
                        languageQuestion.Language = language;
                        _language_QuestionService.Update(languageQuestion);
                    }
                }

                // Cap nhat danh muc
                var question = languageQuestion.Question;
                var topic = languageQuestion.Question.Topic;
                question = Mapper.Map(viewModel, question);
                if (question != null)
                {
                    if (viewModel.TopicId != question.Topic.Id)
                    {
                        var newQuestionTopic = _topicService.GetById(viewModel.TopicId);
                        if (newQuestionTopic != null)
                        {
                            question.Topic = newQuestionTopic;
                        }
                    }
                    else
                    {
                        question.Topic = topic;
                    }
                }
                _questionService.Update(question);
                _questionService.SaveChange();
                languageQuestion.Question = question;
                _language_QuestionService.SaveChange();

                return RedirectToAction("Index").WithSuccess(Resources.Resources.UpdateSuccess);
            }
            catch (Exception ex)
            {
                HandleException("Message", ex);
                return RedirectToAction("Index").WithError(Resources.Resources.ErrorCatch);
            }
        }

        [AuthorizeUser(FunctionID = "QUESTION", RoleID = "DELETE")]
        public ActionResult Delete(int questionId, int languageId = 0)
        {
            try
            {
                var question = _questionService.GetById(questionId);
                var languageQuestions = languageId == 0 ? question.Language_Questions : question.Language_Questions.Where(x => x.Language.Id == languageId);
                foreach (var languageQuestion in languageQuestions.ToList())
                {
                    _language_QuestionService.Delete(languageQuestion);
                    _language_QuestionService.SaveChange();
                }
                //_questionService.Update(question);
                //_questionService.SaveChange();
                if (question.Language_Questions.Count == 0)
                {
                    question.DeletedBy = "admin";
                    question.DeletedAt = DateTime.Now;
                }
                _questionService.Update(question);
                _questionService.SaveChange();

                if (Request.IsAjaxRequest())
                {
                    var reponse = new
                    {
                        Id = questionId,
                        Code = AjaxAlertConsts.WithSuccess,
                        Msg = Resources.Resources.AlertDataSuccess,
                    };

                    return Json(reponse, JsonRequestBehavior.AllowGet);
                }
                return RedirectToAction("Index", "FAQQuestionManagement").WithSuccess(Resources.Resources.SuccessDeleteFunction);
            } catch (Exception ex)
            {
                HandleException("Message", ex);
                var reponse = new { Id = questionId, Code = AjaxAlertConsts.WithError, Msg = Resources.Resources.AlertDataCatch };
                return Json(reponse, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [AuthorizeUser(FunctionID = "QUESTION", RoleID = "EDIT")]
        public ActionResult EditAttachment(int questionId, int platform = 0, int languageId = 0)
        {
            var question = _questionService.GetById(questionId);
            var language = languages.FirstOrDefault(
                item =>
                    (languageId == 0 && item.Code == "*") ||
                    (languageId != 0 && item.Id == languageId)
            );
            var model = new QuestionEditViewModel
            {
                Platform = platform,
                LanguageId = language.Id,
                LanguageQuestionId = question.Language_Questions.SingleOrDefault(x => x.Language.Id == languageId).Id
            };
            var questionAttachments = new List<QuestionAttachment>();
            if (question != null)
            {
                model.Id = question.Id;
                questionAttachments = _questionAttachmentService
                    .GetByQuestion(question.Id)
                    .Where(x => x.Language.Code == language.Code && x.Platform == platform)
                    .OrderBy(x => x.Priority)
                    .ToList();
                if (questionAttachments != null && questionAttachments.Count > 0)
                {
                    model.Attachments = new List<QuestionAttachmentViewModel>();
                    foreach(var item in questionAttachments)
                    {
                        var questionAttachmentVM = Mapper.Map<QuestionAttachment, QuestionAttachmentViewModel>(item);
                        questionAttachmentVM.ImageUrl = item.Image != null ? item.Image.FullPath : "";
                        model.Attachments.Add(questionAttachmentVM);
                    }
                }
            }
            return View(model);
        }

        [HttpGet]
        [AuthorizeUser(FunctionID = "QUESTION", RoleID = "EDIT")]
        public ActionResult AddAttachment(int questionId, int platform, int languageId = 2)
        {
            var language = _languageService.GetById(languageId);
            try 
            {
                InsertNewQuestionAttachment(questionId, platform, language);
                return RedirectToAction("EditAttachment", new { questionId , platform, languageId = languageId }).WithSuccess(Resources.Resources.UpdateSuccess);
            } 
            catch (Exception ex) {
                HandleException("Message", ex);
                return RedirectToAction("EditAttachment", new { questionId, platform, languageId = languageId }).WithError(Resources.Resources.ErrorCatch);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken, Log("{model}")]
        [AuthorizeUser(FunctionID = "QUESTION", RoleID = "EDIT")]
        public ActionResult EditAttachment(
            QuestionAttachmentViewModel viewModel, 
            int questionId = 0, 
            int platform = 0,
            int questionAttachmentId = 0,
            int languageId = 2
        )
        {
            try {
                var language = _languageService.GetById(languageId);
                var questionAttachment = _questionAttachmentService.GetById(questionAttachmentId);
                if(questionAttachment == null)
                {
                    return RedirectToAction("EditAttachment", new { questionId, languageId = languageId }).WithError(Resources.Resources.ErrorCatch);
                }
                questionAttachment.Description = viewModel.Description;
                questionAttachment.Priority = viewModel.Priority;
                questionAttachment.UpdatedBy = "admin";
                questionAttachment.UpdatedDate = DateTime.Now;
                if (questionAttachment.Image?.FullPath != viewModel.ImageUrl)
                {
                    var mediaViewModel = GetMediaByUrl(viewModel.ImageUrl);
                    var image = faq_mediaService.GetByFullPath(mediaViewModel.FullPath);
                    if (image == null)
                    {
                        image = faq_mediaService.Create(Mapper.Map<MediaViewModel, FAQ_Media>(mediaViewModel));
                    }
                    questionAttachment.Image = image;
                }
                _questionAttachmentService.SaveChange();
                return RedirectToAction("EditAttachment", new { questionId , platform, languageId = languageId }).WithSuccess(Resources.Resources.UpdateSuccess);
            }
            catch (Exception ex)
            {
                HandleException("Message", ex);
                return RedirectToAction("EditAttachment", new { questionId , platform, languageId = languageId }).WithError(Resources.Resources.ErrorCatch);
            }
        }

        [HttpGet]
        [AuthorizeUser(FunctionID = "QUESTION", RoleID = "EDIT")]
        public ActionResult DeleteAttachment(int questionAttachmentId, int questionId, int languageId, int platform = 0)
        {
            try
            {
                _questionAttachmentService.Delete(questionAttachmentId);
                return RedirectToAction("EditAttachment", new { questionId, platform, languageId = languageId }).WithSuccess(Resources.Resources.UpdateSuccess);
            }
            catch (Exception ex)
            {
                HandleException("Message", ex);
                return RedirectToAction("EditAttachment", new { questionId, platform, languageId = languageId }).WithError(Resources.Resources.ErrorCatch);
            }
        }

        private SelectList GetParentTopics(string languageCode, int skipId = 0, int parentId = 0, object selectedValue = null)
        {
            // Chỉ lấy các danh mục cấp 0
            var parents = _topicService.GetParents(skipId, parentId).ToList();
            var parentLists = MapTopicsToViewModels(parents, languageCode, parentId);

            var selectList = new SelectList(
                parentLists,
                "Id",
                "TopicName",
                selectedValue
            );
            return selectList;
        }

        private List<TopicViewModel> MapTopicsToViewModels(IEnumerable<Topic> topics = null, string languageCode = "", int selectedTopicId = 0)
        {
            var viewModels = new List<TopicViewModel>();

            if (topics == null || topics.ToList().Count == 0)
            {
                return viewModels;
            }
            if (selectedTopicId != 0)
            {
                var topic = topics.FirstOrDefault(x => x.Id == selectedTopicId);
                var viewModel = Mapper.Map<Topic, TopicViewModel>(topic);
                if (string.IsNullOrEmpty(languageCode))
                {
                    viewModel.TopicName = topic
                        .Language_Topic
                        .OrderBy(item => item.CreatedDate)
                        .FirstOrDefault()?.TopicName;
                }
                else
                {
                    var languageQuestion = topic.Language_Topic.FirstOrDefault(x => x.Language.Code == languageCode) ?? topic
                        .Language_Topic
                        .OrderBy(item => item.CreatedDate)
                        .FirstOrDefault();
                    if (languageQuestion != null)
                    {
                        viewModel.TopicName = languageQuestion.TopicName;
                    }
                }
                viewModels.Add(viewModel);
            }
            else
            {
                foreach (var topic in topics)
                {
                    var viewModel = Mapper.Map<Topic, TopicViewModel>(topic);
                    if (string.IsNullOrEmpty(languageCode))
                    {
                        viewModel.TopicName = topic
                            .Language_Topic
                            .OrderBy(item => item.CreatedDate)
                            .FirstOrDefault()?.TopicName;
                    }
                    else
                    {
                        viewModel.TopicName = topic.Language_Topic.FirstOrDefault(x => x.Language.Code == languageCode)?.TopicName;
                    }
                    viewModels.Add(viewModel);
                }
            }
            return viewModels;
        }
        private void InsertNewQuestionAttachment(int questionId, int platform, Language language)
        {
            var question = _questionService.GetById(questionId);
            var questionAttachment = _questionAttachmentService.Create(new QuestionAttachment()
            {
                Description = "",
                Language = language,
                Platform = platform,
                Question = question,
                CreatedBy = "admin",
                CreatedDate = DateTime.Now
            });
            _questionAttachmentService.SaveChange();
        }

        private void UpdateQuestionAttachment(int questionAttachmentId, QuestionAttachmentViewModel viewModel)
        {

        }
    }
}