﻿using AutoMapper;
using PagedList;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TTCMS.Areas.TT_Admin.CustomAttributes;
using TTCMS.Areas.TT_Admin.Filters;
using TTCMS.Core.Infrastructure.Alerts;
using TTCMS.Data.Entities;
using TTCMS.Service;
using TTCMS.Service.Common;
using TTCMS.Web.Areas.TT_Admin.Models;
using TTCMS.Web.ViewModels;

namespace TTCMS.Web.Areas.TT_Admin.Controllers
{
    public class FAQPendingQuestionManagementController : BaseController
    {
        // GET: TT_Admin/FAQPendingQuestion
        private readonly IPendingQuestionService _pendingQuestionServivce;
        private readonly ILanguageService _languageService;
        public FAQPendingQuestionManagementController(
            IPendingQuestionService pendingQuestionServivce, 
            ILanguageService languageService)
        {
            _pendingQuestionServivce = pendingQuestionServivce;
            _languageService = languageService;
        }

        public ActionResult Index(int? page = 1, string show = "", string search = "")
        {
            var pendingQuestions = _pendingQuestionServivce.GetList().OrderBy(x => x.CreatedDate);
            var pendingQuestionItems = new List<FAQPendingQuestionViewModel>();
            int pageSize = int.Parse(ConfigSettings.ReadSetting("pageSize"));
            int pageNumber = (page ?? 1);
            if (pendingQuestions != null && pendingQuestions.ToList().Count() > 0)
            {
                foreach (var item in pendingQuestions)
                {
                    var pendingQuestionDetail = Mapper.Map<PendingQuestion, FAQPendingQuestionViewModel>(item);
                    pendingQuestionItems.Add(pendingQuestionDetail);
                }
            }
            var table = new PendingQuestionManagementViewModel
            {
                Show = show,
                Search = search,
                ModelList = new PagedList<FAQPendingQuestionViewModel>(pendingQuestionItems, pageNumber, pageSize),
            };
            var languages = _languageService.GetListForActive().Where(x => x.Code != "*");
            if (languages != null)
            {
                table.Languages = new List<LanguageViewModel>();
                foreach (var item in languages)
                {
                    var languageVM = Mapper.Map<Language, LanguageViewModel>(item);
                    if (languageVM != null)
                    {
                        table.Languages.Add(languageVM);
                    }
                }
            }
            if (Request.IsAjaxRequest())
            {
                return PartialView("_PendingQuestionTablePagedList", table);
            }

            var model = new PendingQuestionManagementPageViewModel
            {
                TableList = table
            };
            
            return View(model);
        }

        public ActionResult Details(int id)
        {
            var pendingQuestion = _pendingQuestionServivce.GetById(id);
            var model = Mapper.Map<PendingQuestion, FAQPendingQuestionViewModel>(pendingQuestion);
            
            var languages = _languageService.GetListForActive().Where(x => x.Code != "*");
            if(languages != null)
            {
                model.Languages = new List<LanguageViewModel>();
                foreach(var item in languages)
                {
                    var languageVM = Mapper.Map<Language, LanguageViewModel>(item);
                    if (languageVM != null)
                    {
                        model.Languages.Add(languageVM);
                    }
                }
            }
            return View(model);
        }

        [AuthorizeUser(FunctionID = "QUESTION", RoleID = "EDIT"), Log("{Id}")]
        public ActionResult Approve(int id = 0)
        {
            try
            {
                var pendingQuestion = _pendingQuestionServivce.GetById(id);
                if (pendingQuestion == null)
                {
                    var reponse = new
                    {
                        Id = id,
                        Code = AjaxAlertConsts.WithError,
                        Msg = "PQ-001 - Câu hỏi không tồn tại"
                    };

                    return Json(reponse, JsonRequestBehavior.AllowGet);
                }
                pendingQuestion.Seen = true;
                _pendingQuestionServivce.Update(pendingQuestion);

                if (Request.IsAjaxRequest())
                {
                    var reponse = new
                    {
                        Id = id,
                        Code = AjaxAlertConsts.WithSuccess,
                        Msg = Resources.Resources.AlertDataSuccess
                    };

                    return Json(reponse, JsonRequestBehavior.AllowGet);
                }

                return RedirectToAction("Index", "FAQPendingQuestionManagement").WithSuccess(Resources.Resources.SuccessDeleteFunction);
            }
            catch (Exception ex)
            {
                HandleException("Message", ex);
                var reponse = new { Id = id, Code = AjaxAlertConsts.WithError, Msg = Resources.Resources.AlertDataCatch };
                return Json(reponse, JsonRequestBehavior.AllowGet);
            }
        }

        [AuthorizeUser(FunctionID = "QUESTION", RoleID = "DELETE"), Log("{Id}")]
        public ActionResult Delete(int id = 0)
        {
            try
            {
                var pendingQuestion = _pendingQuestionServivce.GetById(id);
                if (pendingQuestion == null)
                {
                    var reponse = new
                    {
                        Id = id,
                        Code = AjaxAlertConsts.WithError,
                        Msg = "PQ-001 - Câu hỏi không tồn tại"
                    };

                    return Json(reponse, JsonRequestBehavior.AllowGet);
                }
                _pendingQuestionServivce.Delete(id);

                if (Request.IsAjaxRequest())
                {
                    var reponse = new
                    {
                        Id = id,
                        Code = AjaxAlertConsts.WithSuccess,
                        Msg = Resources.Resources.AlertDataSuccess
                    };

                    return Json(reponse, JsonRequestBehavior.AllowGet);
                }

                return RedirectToAction("Index", "FAQPendingQuestionManagement").WithSuccess(Resources.Resources.SuccessDeleteFunction);
            }
            catch (Exception ex)
            {
                HandleException("Message", ex);
                var reponse = new { Id = id, Code = AjaxAlertConsts.WithError, Msg = Resources.Resources.AlertDataCatch };
                return Json(reponse, JsonRequestBehavior.AllowGet);
            }
        }
    }
}