﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TTCMS.Service;
using TTCMS.Web.Areas.TT_Admin.Models;
using TTCMS.Core.Infrastructure.Alerts;
using TTCMS.Areas.TT_Admin.CustomAttributes;
using TTCMS.Data.Entities;
using TTCMS.Web.ViewModels;
using TTCMS.Areas.TT_Admin.Filters;
using TTCMS.Web.Helper;
using TTCMS.Web.Models;

namespace TTCMS.Web.Areas.TT_Admin.Controllers
{
    [Authorize]
    public class HomeManagementController : BaseController
    {
        private readonly ILanguageService languageService;
        private readonly ISiteSettingService siteSettingService;

        public HomeManagementController(
            ILanguageService languageService,
            ISiteSettingService siteSettingService)
        {
            this.languageService = languageService;
            this.siteSettingService = siteSettingService;
        }

        [HttpGet]
        [AuthorizeUser(FunctionID = "HOMECONFIG", RoleID = "VIEW")]
        public ActionResult Index()
        {
            var languages = languageService.GetListForActive("*");
            var viewModel = Mapper.Map<IEnumerable<Language>, IEnumerable<LanguageViewModel>>(languages);
            return View(viewModel);
        }

        [AuthorizeUser(FunctionID = "HOMECONFIG", RoleID = "EDIT")]
        public ActionResult Edit(int id)
        {
            try
            {
                if (id == 0)
                {
                    return RedirectToAction("Index").WithError(Resources.Resources.ErrorNoData);
                }

                var model = HomeHelper.GetHomeViewModel(id, siteSettingService,languageService);

                return View(model);
            }
            catch (Exception ex)
            {
                HandleException("Message", ex);
                return RedirectToAction("Index").WithError(Resources.Resources.ErrorCatch);
            }
        }

        [HttpPost, ValidateAntiForgeryToken, Log("{model}")]
        [AuthorizeUser(FunctionID = "HOMECONFIG", RoleID = "EDIT")]
        public ActionResult Edit(HomeViewModel viewModel, bool isAddClick = false, int itemId = 0)
        {
            try
            {
                if (isAddClick)
                {
                    InsertSiteSetting(viewModel.LanguageId);
                    return RedirectToAction("EDIT", new { id = viewModel.LanguageId });
                }

                if (itemId != 0)
                {
                    DeleteSiteSetting(itemId, viewModel.LanguageId);
                    return RedirectToAction("EDIT", new { id = viewModel.LanguageId });
                }

                if (!ModelState.IsValid)
                {
                    ModelState.AddModelError("", Resources.Resources.WarningData);
                    return View(viewModel).WithWarning(Resources.Resources.WarningData);
                }

                foreach (var item in viewModel.SlideUrls)
                {
                    UpdateSiteSettingValue(item);
                }

                foreach (var item in viewModel.TradingToolImages)
                {
                    UpdateSiteSettingValue(item);
                }

                foreach (var item in viewModel.QuickAccessItems)
                {
                    UpdateSiteSettingValue(item.Title);
                    UpdateSiteSettingValue(item.ImageUrl);
                    UpdateSiteSettingValue(item.RedirectUrl);
                }

                foreach (var item in viewModel.InvestmentFormItems)
                {
                    UpdateSiteSettingValue(item.Title);
                    UpdateSiteSettingValue(item.Description);
                    UpdateSiteSettingValue(item.ImageUrl);
                    UpdateSiteSettingValue(item.ButtonText);
                    UpdateSiteSettingValue(item.RedirectUrl);
                }

                foreach (var item in viewModel.AnalysisCenterItem)
                {
                    UpdateSiteSettingValue(item.Title);
                    UpdateSiteSettingValue(item.Description);
                    UpdateSiteSettingValue(item.RedirectUrl);
                }

                UpdateSiteSettingValue(viewModel.InvestmentFormTitle);
                UpdateSiteSettingValue(viewModel.TradingTool.Title);
                UpdateSiteSettingValue(viewModel.TradingTool.Description);
                UpdateSiteSettingValue(viewModel.FeaturedNews.Title);
                UpdateSiteSettingValue(viewModel.AnalysisCenter.Title);
                UpdateSiteSettingValue(viewModel.AnalysisCenter.Description);

                return RedirectToAction("Index").WithSuccess(Resources.Resources.UpdateSuccess);
            }
            catch (Exception ex)
            {
                HandleException("Message", ex);
                return RedirectToAction("Index").WithError(Resources.Resources.ErrorCatch);
            }
        }

        private void UpdateSiteSettingValue(KeyValueModel configValue)
        {
            var item = siteSettingService.GetById(configValue.Id);
            item.Value = configValue.Value;
            siteSettingService.Update(item);
        }

        private void InsertSiteSetting(int languageId)
        {
            var setting = new SiteSetting();
            //var ListParent = siteSettingService.GetAllParent();
            var lang = languageService.GetById(languageId);

            //foreach (var item in ListParent)
            //{
            //    if (siteSettingService.GetManyByParentId(languageId, item.Id).Any())
            //        continue;
            //    setting = item;
            //    break;
            //}

            if (setting.Id == 0)
            {
                setting.Value = SiteSettingKey.HomeConfigInvestmentFormItem.ToString();
                setting.Description = "Cấu hình các phần tử trong khối Lựa chọn đầu tư trên trang chủ";
                setting.Key = SiteSettingKey.HomeConfigInvestmentFormItem;
                setting.Language = lang;
                siteSettingService.Create(setting);
            }

            for (int i = 0; i < 5; i++)
            {
                var settingChild = new SiteSetting
                {
                    Key = (SiteSettingKey)i,
                    Value = "",
                    Parent = setting,
                    Language = lang
                };
                siteSettingService.Create(settingChild);
            }

            siteSettingService.SaveChange();
        }

        private void DeleteSiteSetting(int itemId, int languageId)
        {
            var listConfig = siteSettingService.GetManyByParentId(languageId, itemId);

            foreach (var item in listConfig)
            {
                siteSettingService.Delete(item);
            }

            var investmentFormItems = siteSettingService.GetById(itemId);
            siteSettingService.Delete(investmentFormItems);
            siteSettingService.SaveChange();
        }
    }
}