﻿using AutoMapper;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TTCMS.Areas.TT_Admin.CustomAttributes;
using TTCMS.Areas.TT_Admin.Filters;
using TTCMS.Core.Infrastructure.Alerts;
using TTCMS.Data.Entities;
using TTCMS.Service;
using TTCMS.Service.Common;
using TTCMS.Web.Areas.TT_Admin.Models;
using TTCMS.Web.ViewModels;

namespace TTCMS.Web.Areas.TT_Admin.Controllers
{
    public class BannerManagementController : BaseController
    {
        private readonly IEnumerable<Language> languages;
        private readonly IBannerService BannerService;
        private readonly ILanguage_BannerService language_BannerService;
        private readonly IMediaService mediaService;
        private readonly ILanguageService languageService;
        public BannerManagementController(IBannerService BannerService, ILanguage_BannerService language_BannerService,
            IMediaService mediaService,
            ILanguageService languageService)
        {
            this.BannerService = BannerService;
            this.mediaService = mediaService;
            this.language_BannerService = language_BannerService;
            this.languageService = languageService;
            languages = languageService.GetListForActive();
        }
        // GET: TT_Admin/Banner
        [HttpGet]
        [AuthorizeUser(FunctionID = "BANNER", RoleID = "VIEW")]
        public ActionResult Index(int? page = 1, string show = "", string search = "")
        {
            if (show != "")
            {
                cultureName = show;
            }
            int pageSize = int.Parse(ConfigSettings.ReadSetting("pageSize"));
            int pageNumber = (page ?? 1);
            var banners = BannerService.GetList();
            var bannerItems = new List<BannerItemViewModel>();
            foreach (var item in banners)
            {
                var bannerDetail = language_BannerService.GetByBannerId(item.Id).Select(q => new BannerItemViewModel
                {
                    Id = item.Id,
                    ImageUrl = q.Image?.FullPath,
                    RedirectUrl = q.RedirectURL,
                    Type = (int)item.Type,
                    IsActived = item.IsActived,
                    LanguageId = q.Language.Id,
                    CreatedDate = item.CreatedDate
                });
                bannerItems.AddRange(bannerDetail);
            }
            var table = new BannerViewModel
            {
                Show = show,
                Search = search,
                Languages = Mapper.Map<IEnumerable<Language>, List<LanguageViewModel>>(languages),
                ModelList = new PagedList<BannerItemViewModel>(bannerItems, pageNumber, pageSize),
            };
            table.ModelList.ToPagedList(pageNumber, pageSize);
            if (Request.IsAjaxRequest())
            {
                return PartialView("_BannerTable", table);
            }

            var model = new BannerPageViewModel
            {
                TableList = table
            };
            return View(model);
        }

        [HttpGet]
        [AuthorizeUser(FunctionID = "BANNER", RoleID = "CREATE")]
        public ActionResult Create()
        {
            var lang = languages;
            var Banner = new BannerItemViewModel
            {
                Id = 0
            };
            var model = new BannerCreateViewModel()
            {
                Model = Banner
            };
            model.Lang = new SelectList(
                lang.OrderByDescending(x => x.CreatedDate),
                "Id",
                "Name",
                model.Model.LanguageId
            );
            var listType = Enum.GetValues(typeof(BannerType))
               .Cast<BannerType>()
               .Select(t => new
               {
                   Id = ((int)t),
                   Name = t.ToString()
               });
            model.ListBannerType = new SelectList(
                listType.OrderByDescending(x => x.Id),
                "Id",
                "Name",
                model.Model.Type
            );
            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken, Log("{model}")]
        [AuthorizeUser(FunctionID = "BANNER", RoleID = "CREATE")]
        public ActionResult Create(BannerCreateViewModel viewModel)
        {
            try
            {
                if (!ModelState.IsValid || viewModel.Model.ImageUrl == null || viewModel.Model.ImageUrl == string.Empty)
                {
                    viewModel.Lang = new SelectList(
                        languages.OrderByDescending(x => x.CreatedDate),
                        "Id",
                        "Name",
                        viewModel.Model.LanguageId
                    );
                    var listType = Enum.GetValues(typeof(BannerType))
                        .Cast<BannerType>()
                        .Select(t => new
                        {
                            Id = ((int)t),
                            Name = t.ToString()
                        });
                    viewModel.ListBannerType = new SelectList(
                            listType.OrderByDescending(x => x.Id),
                            "Id",
                            "Name",
                            viewModel.Model.Type
                        );
                    ModelState.AddModelError("", Resources.Resources.WarningData);
                    var listlang = languageService.GetListForActive().ToList();
                    return View(viewModel).WithWarning(Resources.Resources.WarningData);
                }
                var newBanner = new Banner() { IsActived = viewModel.Model.IsActived, Type = (BannerType)viewModel.Model.Type, CreatedDate = DateTime.Now };
                var newLanguageBanner = new Language_Banner()
                {
                    RedirectURL = viewModel.Model.RedirectUrl,
                    LanguageId = viewModel.Model.LanguageId,
                    Priority = viewModel.Model.Priority,
                    CreatedDate = DateTime.Now,
                    BannerId = BannerService.Create(newBanner),
                    Banner = newBanner
                };
                if (!string.IsNullOrEmpty(viewModel.Model.ImageUrl))
                {
                    var mediaViewModel = GetMediaByUrl(viewModel.Model.ImageUrl);
                    var image = mediaService.GetByFullPath(mediaViewModel.FullPath);
                    if (image == null)
                    {
                        image = mediaService.Create(Mapper.Map<MediaViewModel, Media>(mediaViewModel));
                        mediaService.SaveChange();
                    }
                    newLanguageBanner.Image = image;
                }
                language_BannerService.Create(newLanguageBanner);
                language_BannerService.SaveChange();
                return RedirectToAction("Index").WithSuccess(Resources.Resources.SuccessCreate);
            }
            catch (Exception ex)
            {
                HandleException("Message ", ex);
                return RedirectToAction("Index").WithError(Resources.Resources.ErrorCatch);
            }
        }
        [AuthorizeUser(FunctionID = "FAQ", RoleID = "EDIT")]
        public ActionResult Edit(int Id = 0, int LanguageId = 0)
        {
            try
            {
                if (Id == 0 || LanguageId == 0) return RedirectToAction("Index").WithError(Resources.Resources.ErrorNoData);

                var curBanner = BannerService.GetById(Id);
                if (curBanner == null)
                {
                    return RedirectToAction("Index").WithError(Resources.Resources.ErrorNoData);
                }

                var lang = languages;
                var bannerItems = language_BannerService.GetByBannerId(curBanner.Id).Where(b => b.LanguageId == LanguageId).Select(q => new BannerItemViewModel
                {
                    Id = curBanner.Id,
                    ImageUrl = q.Image?.FullPath,
                    RedirectUrl = q.RedirectURL,
                    Priority = q.Priority,
                    CreatedDate = curBanner.CreatedDate,
                    Type = (int)curBanner.Type,
                    LanguageId = q.LanguageId,
                    IsActived = curBanner.IsActived
                });
                var model = new BannerCreateViewModel();
                model.Model = bannerItems.FirstOrDefault();
                model.Lang = new SelectList(
                    lang.Where(l => l.Id == model.Model.LanguageId),
                    "Id",
                    "Name",
                    model.Model.LanguageId
                );
                var listType = Enum.GetValues(typeof(BannerType))
              .Cast<BannerType>()
              .Select(t => new
              {
                  Id = ((int)t),
                  Name = t.ToString()
              });
                model.ListBannerType = new SelectList(
                    listType.OrderByDescending(x => x.Id),
                    "Id",
                    "Name",
                    model.Model.LanguageId
                );
                return View("Create", model);
            }
            catch (Exception ex)
            {
                HandleException("Message", ex);
                return RedirectToAction("Index").WithError(Resources.Resources.ErrorCatch);
            }

        }
        [AuthorizeUser(FunctionID = "FAQ", RoleID = "EDIT"), ValidateInput(false)]
        [HttpPost, ValidateAntiForgeryToken, Log("{model}")]
        public ActionResult Edit(BannerCreateViewModel viewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    ModelState.AddModelError("", Resources.Resources.WarningData);
                    var lang = languageService.GetListForActive().ToList();
                    return View(viewModel).WithWarning(Resources.Resources.WarningData);
                }
                // Lay tin chi tiet
                var banner = BannerService.GetById(viewModel.Model.Id);
                var LanguageBanner = language_BannerService.GetByBannerId(viewModel.Model.Id).Where(b => b.LanguageId == viewModel.Model.LanguageId).FirstOrDefault();

                if (banner == null)
                {
                    var newBanner = new Banner();
                    newBanner.IsActived = viewModel.Model.IsActived;
                    newBanner.Type = (BannerType)viewModel.Model.Type;
                    newBanner.CreatedDate = DateTime.Now;
                    var newLanguageBanner = new Language_Banner()
                    {
                        RedirectURL = viewModel.Model.RedirectUrl,
                        LanguageId = viewModel.Model.LanguageId,
                        CreatedDate = DateTime.Now,
                        BannerId = BannerService.Create(newBanner)
                    };
                    if (!string.IsNullOrEmpty(viewModel.Model.ImageUrl))
                    {
                        var mediaViewModel = GetMediaByUrl(viewModel.Model.ImageUrl);
                        var image = mediaService.GetByFullPath(mediaViewModel.FullPath);
                        if (image == null)
                        {
                            image = mediaService.Create(Mapper.Map<MediaViewModel, Media>(mediaViewModel));
                            mediaService.SaveChange();
                        }
                        newLanguageBanner.Image = image;
                    }
                    language_BannerService.Create(newLanguageBanner);
                }
                else
                {
                    banner.IsActived = viewModel.Model.IsActived;
                    LanguageBanner.RedirectURL = viewModel.Model.RedirectUrl;
                    LanguageBanner.LanguageId = viewModel.Model.LanguageId;
                    banner.UpdatedDate = DateTime.Now;
                    if (!string.IsNullOrEmpty(viewModel.Model.ImageUrl) && LanguageBanner.Image.Path != viewModel.Model.ImageUrl)
                    {
                        var mediaViewModel = GetMediaByUrl(viewModel.Model.ImageUrl);
                        var image = mediaService.GetByFullPath(mediaViewModel.FullPath);
                        if (image == null)
                        {
                            image = mediaService.Create(Mapper.Map<MediaViewModel, Media>(mediaViewModel));
                            mediaService.SaveChange();
                        }
                        LanguageBanner.Image = image;
                    }
                    language_BannerService.Update(LanguageBanner);
                    BannerService.Update(banner);
                }
                BannerService.SaveChange();
                return RedirectToAction("Index").WithSuccess(Resources.Resources.UpdateSuccess);
            }
            catch (Exception ex)
            {
                HandleException("Message", ex);
                return RedirectToAction("Index").WithError(Resources.Resources.ErrorCatch);
            }

        }
        [AuthorizeUser(FunctionID = "FAQ", RoleID = "DELETE"), Log("{Id}")]
        public ActionResult Delete(int id = 0)
        {
            try
            {
                var banner = BannerService.GetById(id);
                banner.UpdatedDate = DateTime.Now;
                banner.DeletedAt = DateTime.Now;
                BannerService.Update(banner);
                BannerService.SaveChange();
                if (Request.IsAjaxRequest())
                {
                    var reponse = new
                    {
                        Id = id,
                        Code = AjaxAlertConsts.WithSuccess,
                        Msg = Resources.Resources.AlertDataSuccess
                    };

                    return Json(reponse, JsonRequestBehavior.AllowGet);
                }

                return RedirectToAction("Index").WithSuccess(Resources.Resources.SuccessDeleteFunction);
            }
            catch (Exception ex)
            {
                HandleException("Message", ex);
                var reponse = new { Id = id, Code = AjaxAlertConsts.WithError, Msg = Resources.Resources.AlertDataCatch };
                return Json(reponse, JsonRequestBehavior.AllowGet);
            }

        }

    }
}