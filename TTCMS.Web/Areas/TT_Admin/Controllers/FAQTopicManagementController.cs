﻿using AutoMapper;
using PagedList;
using System.Collections.Generic;
using System.Web.Mvc;
using TTCMS.Service;
using TTCMS.Service.Common;
using TTCMS.Web.Areas.TT_Admin.Models;
using TTCMS.Areas.TT_Admin.CustomAttributes;
using TTCMS.Data.Entities;
using TTCMS.Web.ViewModels;
using System.Linq;
using TTCMS.Areas.TT_Admin.Filters;
using TTCMS.Core.Infrastructure.Alerts;
using System;

namespace TTCMS.Web.Areas.TT_Admin.Controllers
{
    public class FAQTopicManagementController : BaseController
    {
        private readonly IEnumerable<Language> languages;
        private readonly ITopicService _topicService;
        private readonly IQuestionService _questionService;
        private readonly ILanguageService languageService;
        private readonly IMediaService mediaService;
        private readonly IFAQ_MediaService faq_mediaService;
        private readonly ILanguage_TopicService _language_TopicService;

        public FAQTopicManagementController(
            ITopicService TopicService, 
            ILanguageService languageService,
            IMediaService mediaService,
            ILanguage_TopicService language_TopicService,
            IQuestionService questionService,
            IFAQ_MediaService faq_mediaService)
        {
            _topicService = TopicService;
            this.languageService = languageService;
            languages = languageService.GetListForActive();
            this.mediaService = mediaService;
            _language_TopicService = language_TopicService;
            _questionService = questionService;
            this.faq_mediaService = faq_mediaService;
        }
        // GET: TT_Admin/Topic
        [HttpGet]
        [AuthorizeUser(FunctionID = "TOPIC", RoleID = "VIEW")]
        public ActionResult Index(int? page = 1, string show = "", string search = "")
        {
            if (show != "")
            {
                cultureName = show;
            }
            int pageSize = int.Parse(ConfigSettings.ReadSetting("pageSize"));
            int pageNumber = (page ?? 1);
            var topics = _topicService.GetList();
            var topicItems = new List<TopicViewModel>();
            if (topics != null && topics.ToList().Count() > 0)
            {
                foreach (var item in topics)
                {
                    var topicDetail = new TopicViewModel
                    {
                        Id = item.Id,
                        ParentTopicId = item.ParentTopicId,
                        Active = item.Active,
                        CreatedDate = item.CreatedDate,
                        CreatedBy = item.CreatedBy,
                    };
                    if (topicDetail != null)
                    {
                        var language_topics = _language_TopicService.GetByTopic(item.Id).Select(x => new Language_TopicViewModel
                        {
                            Id = x.Id,
                            TopicDescription = !string.IsNullOrEmpty(x.TopicDescription) ? x.TopicDescription : "",
                            TopicName = x.TopicName,
                            CreatedBy = x.CreatedBy,
                            CreatedDate = x.CreatedDate,
                            UpdatedBy = x.UpdatedBy,
                            UpdatedDate = x.UpdatedDate,
                            Language = Mapper.Map<Language, LanguageViewModel>(x.Language)
                        });
                        topicDetail.Language_Topics = language_topics.ToList();
                    }
                    topicItems.Add(topicDetail);
                }
            }
            var languageVM = Mapper.Map<IEnumerable<Language>, List<LanguageViewModel>>(languages.Where(x => x.Id != 3)); 
            var table = new TopicManagementViewModel
            {
                Show = show,
                Search = search,
                Languages = languageVM,
                ModelList = new PagedList<TopicViewModel>(topicItems, pageNumber, pageSize),
            };

            if (Request.IsAjaxRequest())
            {
                return PartialView("_TopicTablePagedList", table);
            }

            var model = new TopicManagementPageViewModel
            {
                Languages = languageVM,
                TableList = table
            };

            return View(model);
        }

        [HttpGet]
        [AuthorizeUser(FunctionID = "TOPIC", RoleID = "EDIT")]
        public ActionResult Edit(int Id)
        {
            var viewModel = new EditLanguageTopicViewModel();
            var languageTopic = _language_TopicService.GetById(Id);
            
            if (languageTopic != null)
            {
                viewModel = Mapper.Map<Language_Topic, EditLanguageTopicViewModel>(languageTopic);
                var topicVM = new TopicViewModel();
                if (languageTopic.Topic != null) 
                {
                    topicVM = Mapper.Map<Topic, TopicViewModel>(languageTopic.Topic);
                    viewModel.Topic = topicVM;
                    viewModel.ParentTopicId = topicVM.ParentTopicId ?? 0;
                    viewModel.ImageUrl = languageTopic.Topic.Image != null ? languageTopic.Topic.Image.FullPath : "";
                }
                var language = languages.FirstOrDefault(
                    item => item.Code == languageTopic.Language.Code
                );
                if (language != null)
                {
                    // Danh sach ngon ngu
                    viewModel.Lang = new SelectList(
                        languages,
                        "Id",
                        "Name",
                        viewModel.LanguageId
                    );
                    viewModel.DropTopics = GetParentTopics(language.Code, languageTopic.Topic.Id);
                }
            }

            return View(viewModel);
        }

        [HttpGet]
        [AuthorizeUser(FunctionID = "TOPIC", RoleID = "CREATE")]
        public ActionResult Create(int id = 0, int languageId = 0)
        {
            var model = new TopicViewModel()
            {
                Order = 1,
            };

            if (id != 0)
            {
                var topic = _topicService.GetById(id);
                model = Mapper.Map<Topic, TopicViewModel>(topic);
            }

            // Lay ngon ngu mac dinh duoc chon
            var language = languages.FirstOrDefault(
                item =>
                    (languageId == 0 && item.Code == "*") ||
                    (languageId != 0 && item.Id == languageId)
            );

            model.Language_Topic = new Language_TopicViewModel()
            {
                Language = Mapper.Map<Language, LanguageViewModel>(language),
            };

            // Danh sach ngon ngu
            model.Lang = languageId == 0 ? new SelectList(
                languages,
                "Id",
                "Name",
                model.Language_Topic.Language.Id
            ) : new SelectList(
                languages.Where(item => item.Id == languageId),
                "Id",
                "Name",
                model.Language_Topic.Language.Id
            );

            
            // Truong hop them ngon ngu (danh muc da ton tai)
            if (id != 0)
            {
                var topic = _topicService.GetById(id);
                if (topic != null)
                {
                    model.ImageUrl = topic.Image?.FullPath;
                    model.Order = topic.Order;
                    model.ParentTopicId = topic.ParentTopicId;
                }
            } 
            else
            {
                // Danh sach danh muc (khong phai la danh muc con)
                model.DropTopics = GetParentTopics(language.Code);
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken, Log("{model}")]
        [AuthorizeUser(FunctionID = "TOPIC", RoleID = "CREATE")]
        public ActionResult Create(TopicViewModel viewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    ModelState.AddModelError("", Resources.Resources.WarningData);
                    var listlang = languageService.GetListForActive().ToList();
                    viewModel.Lang = new SelectList(listlang.OrderByDescending(x => x.CreatedDate), "Id", "Name", viewModel.LanguageId);
                    return View(viewModel).WithWarning(Resources.Resources.WarningData);
                }
                var language = languageService.GetById(viewModel.LanguageId);

                if (viewModel.Id == 0)
                {
                    var topic = Mapper.Map<TopicViewModel, Topic>(viewModel);
                    if (topic == null)
                    {
                        return RedirectToAction("Index").WithError("Lỗi: Không khởi tạo được chủ đề (EX_TOPIC_001)");
                    }
                    topic.CreatedBy = "admin";
                    topic.CreatedDate = DateTime.Now;

                    if (!string.IsNullOrEmpty(viewModel.ImageUrl))
                    {
                        var mediaViewModel = GetMediaByUrl(viewModel.ImageUrl);
                        var image = faq_mediaService.GetByFullPath(mediaViewModel.FullPath);
                        if (image == null)
                        {
                            image = faq_mediaService.Create(Mapper.Map<MediaViewModel, FAQ_Media>(mediaViewModel));
                            _language_TopicService.SaveChange();
                        }
                        topic.Image = image;
                    }
                    if (viewModel.ParentTopicId == 0) topic.ParentTopicId = null;
                    var createdTopic = _topicService.Create(topic);
                    if(createdTopic != null)
                    {
                        _topicService.SaveChange();
                    }
                    var language_topic = Mapper.Map<TopicViewModel, Language_Topic>(viewModel);
                    if (language_topic == null)
                    {
                        return RedirectToAction("Index").WithError("Lỗi: Không khởi tạo được chủ đề (EX_TOPIC_002)");
                    }
                    language_topic.CreatedBy = "admin";
                    language_topic.CreatedDate = DateTime.Now;
                    language_topic.TopicId = createdTopic.Id;
                    language_topic.Topic = createdTopic;
                    language_topic.Language = language;
                    var createdLanguageTopic = _language_TopicService.Create(language_topic);
                    if (createdLanguageTopic != null && createdTopic != null)
                    {
                        _language_TopicService.SaveChange();
                    }
                }
                else
                {
                    var topic = _topicService.GetById(viewModel.Id);
                    if (topic == null)
                    {
                        return RedirectToAction("Index").WithError(Resources.Resources.ErrorCatch);
                    }
                    viewModel.CreatedBy = "admin";
                    viewModel.CreatedDate = DateTime.Now;
                    var language_topic = _language_TopicService.GetByTopic(viewModel.Id, language.Code);
                    if (language_topic != null)
                    {
                        return RedirectToAction("Index").WithError(Resources.Resources.ErrorCatch);
                    }
                    language_topic = Mapper.Map<TopicViewModel, Language_Topic>(viewModel);
                    if (language_topic != null)
                    {
                        language_topic.TopicId = topic.Id;
                        language_topic.Topic = topic;
                        language_topic.Language = language;
                    }
                    var createdLanguageTopic = _language_TopicService.Create(language_topic);
                    if (createdLanguageTopic != null)
                    {
                        _language_TopicService.SaveChange();
                    }
                }


                return RedirectToAction("Index").WithSuccess(Resources.Resources.SuccessCreate);
            }
            catch (Exception ex)
            {
                HandleException("Message ", ex);
                return RedirectToAction("Index").WithError(Resources.Resources.ErrorCatch);
            }
        }

        [HttpPost, ValidateAntiForgeryToken, Log("{model}")]
        [AuthorizeUser(FunctionID = "TOPIC", RoleID = "EDIT")]
        public ActionResult Edit(EditLanguageTopicViewModel viewModel, int topicId = 0, int languageTopicId = 0)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    ModelState.AddModelError("", Resources.Resources.WarningData);
                    var lang = languageService.GetListForActive().ToList();
                    viewModel.Lang = new SelectList(lang, "Id", "Name", viewModel.LanguageId);
                    return View(viewModel).WithWarning(Resources.Resources.WarningData);
                }
                viewModel.UpdatedDate = DateTime.Now;
                viewModel.UpdatedBy = "admin";
                var language = languageService.GetById(viewModel.LanguageId);
                var arr = new int[] { topicId, languageTopicId };
                // Lay danh muc chi tiet
                var language_topic = languageTopicId == 0 ? _language_TopicService.GetByTopic(topicId).FirstOrDefault() : _language_TopicService.GetById(languageTopicId);
                var topic = language_topic.Topic;
                if (language_topic == null)
                {
                    // Neu khong co du lieu thi tao moi
                    language_topic = Mapper.Map<EditLanguageTopicViewModel, Language_Topic>(viewModel);
                    language_topic.Language = language;
                    language_topic = _language_TopicService.Create(language_topic);
                }
                else
                {
                    // Neu co du lieu thi cap nhat lai du lieu
                    language_topic = Mapper.Map(viewModel, language_topic);
                    if (language_topic != null)
                    {
                        language_topic.Language = language;
                        _language_TopicService.Update(language_topic);
                    }
                }

                // Cap nhat danh muc
                if(topic != null)
                {
                    topic.Active = viewModel.Topic.Active;
                    topic.QuickSearch = viewModel.Topic.QuickSearch;
                    topic.QuickAction = viewModel.Topic.QuickAction;
                    topic.Order = viewModel.Topic.Order;
                    // Cap nhat anh neu anh tu viewModel khac voi anh hien tai
                    if (topic.Image?.FullPath != viewModel.ImageUrl)
                    {
                        var mediaViewModel = GetMediaByUrl(viewModel.ImageUrl);
                        var image = faq_mediaService.GetByFullPath(mediaViewModel.FullPath);
                        if (image == null)
                        {
                            image = faq_mediaService.Create(Mapper.Map<MediaViewModel, FAQ_Media>(mediaViewModel));
                        }
                        topic.Image = image;
                    }

                    if (viewModel.ParentTopicId == 0)
                    {
                        topic.ParentTopic = null;
                    }
                    else if (viewModel.ParentTopicId != topic.ParentTopicId)
                    {
                        var newParentTopic = _topicService.GetById(viewModel.ParentTopicId);
                        if (newParentTopic != null)
                        {
                            topic.ParentTopicId = newParentTopic.Id;
                        }
                    }
                }
                _topicService.Update(topic);
                _language_TopicService.SaveChange();
                _topicService.SaveChange();

                return RedirectToAction("Index").WithSuccess(Resources.Resources.UpdateSuccess);
            }
            catch (Exception ex)
            {
                HandleException("Message", ex);
                return RedirectToAction("Index").WithError(Resources.Resources.ErrorCatch);
            }
        }

        [AuthorizeUser(FunctionID = "TOPIC", RoleID = "DELETE"), Log("{Id}")]
        public ActionResult Delete(int id = 0)
        {
            try
            {
                var topic = _topicService.GetById(id);
                var children = _topicService.GetChildren(id);
                foreach (var child in children)
                {
                    child.DeletedAt = DateTime.Now;
                    child.DeletedBy = "admin";
                    child.ParentTopicId = null;
                    child.ParentTopic = null;
                    foreach (var question in child.Questions)
                    {
                        question.DeletedAt = DateTime.Now;
                        question.DeletedBy = "admin";
                    }
                    _questionService.SaveChange();
                    _topicService.SaveChange();
                }
                foreach (var language_topic in topic.Language_Topic.ToList())
                {
                    _language_TopicService.Delete(language_topic.Id);
                    _language_TopicService.SaveChange();
                }
                _topicService.SaveChange();
                topic.DeletedBy = "admin";
                topic.DeletedAt = DateTime.Now;
                _topicService.SaveChange();

                if (Request.IsAjaxRequest())
                {
                    var reponse = new
                    {
                        Id = id,
                        Code = AjaxAlertConsts.WithSuccess,
                        Msg = Resources.Resources.AlertDataSuccess
                    };

                    return Json(reponse, JsonRequestBehavior.AllowGet);
                }

                return RedirectToAction("Index", "FAQTopicManagement").WithSuccess(Resources.Resources.SuccessDeleteFunction);
            }
            catch (Exception ex)
            {
                HandleException("Message", ex);
                var reponse = new { Id = id, Code = AjaxAlertConsts.WithError, Msg = Resources.Resources.AlertDataCatch };
                return Json(reponse, JsonRequestBehavior.AllowGet);
            }

        }

        private SelectList GetParentTopics(string languageCode, int skipId = 0, int parentId = 0, object selectedValue = null)
        {
            // Chỉ lấy các danh mục cấp 0
            var parents = _topicService.GetParents(skipId, parentId).ToList();
            var parentLists = MapTopicsToViewModels(parents, languageCode, parentId);
            var rootParent = new TopicViewModel() { TopicName = Resources.Resources.ParentTopic, Id = 0 };
            parentLists.AddRange(new List<TopicViewModel>() { rootParent }) ;

            var selectList = new SelectList(
                parentLists,
                "Id",
                "TopicName",
                selectedValue
            );
            return selectList;
        }

        private List<TopicViewModel> MapTopicsToViewModels(IEnumerable<Topic> topics = null, string languageCode = "", int selectedTopicId = 0)
        {
            var viewModels = new List<TopicViewModel>();

            if (topics == null || topics.ToList().Count == 0)
            {
                return viewModels;
            }
            if(selectedTopicId != 0)
            {
                var topic = topics.FirstOrDefault(x => x.Id == selectedTopicId);
                var viewModel = Mapper.Map<Topic, TopicViewModel>(topic);
                if (string.IsNullOrEmpty(languageCode))
                {
                    viewModel.TopicName = topic
                        .Language_Topic
                        .OrderBy(item => item.CreatedDate)
                        .FirstOrDefault()?.TopicName;
                }
                else
                {
                    var language_topic = topic.Language_Topic.FirstOrDefault(x => x.Language.Code == languageCode) ?? topic
                        .Language_Topic
                        .OrderBy(item => item.CreatedDate)
                        .FirstOrDefault(); 
                    if (language_topic != null)
                    {
                        viewModel.TopicName = language_topic.TopicName;
                    }
                }
                viewModels.Add(viewModel);
            }
            else
            {
                foreach (var topic in topics)
                {
                    var viewModel = Mapper.Map<Topic, TopicViewModel>(topic);
                    if (string.IsNullOrEmpty(languageCode))
                    {
                        viewModel.TopicName = viewModel
                            .Language_Topics
                            .OrderBy(item => item.CreatedDate)
                            .FirstOrDefault()?.TopicName;
                    }
                    else
                    {
                        viewModel.TopicName = topic.Language_Topic.FirstOrDefault(x => x.Language.Code == languageCode)?.TopicName;
                    }
                    viewModels.Add(viewModel);
                }
            }
            return viewModels;
        }

    }
}