﻿using AutoMapper;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TTCMS.Areas.TT_Admin.Filters;
using TTCMS.Service;
using TTCMS.Service.Common;
using TTCMS.Web.Areas.TT_Admin.Models;
using TTCMS.Core.Infrastructure.Alerts;
using TTCMS.Areas.TT_Admin.CustomAttributes;
using TTCMS.Web.Extensions;
using TTCMS.Data.Entities;
using TTCMS.Web.Models;
using TTCMS.Data.Infrastructure;
using TTCMS.Web.ViewModels;

namespace TTCMS.Web.Areas.TT_Admin.Controllers
{
    [Authorize]
    public class NewsCateManagementController : BaseController
    {
        private readonly IEnumerable<Language> languages;
        private readonly IMediaService mediaService;
        private readonly IMenuService menuService;
        private readonly ILanguageService languageService;
        private readonly ICategoryService categoryService;
        private readonly ICategoryItemService categoryItemService;

        public NewsCateManagementController(
            IMediaService mediaService,
            IMenuService menuService,
            ICategoryService categoryService,
            ILanguageService languageService,
            ICategoryItemService categoryItemService)
        {
            this.mediaService = mediaService;
            this.menuService = menuService;
            this.categoryService = categoryService;
            this.languageService = languageService;
            this.categoryItemService = categoryItemService;

            languages = languageService.GetListForActive("*");
        }

        [HttpGet]
        [AuthorizeUser(FunctionID = "NEWSCATE", RoleID = "VIEW")]
        public ActionResult Index(int page = 1, string show = "", string search = "")
        {
            if (show != "")
            {
                cultureName = show;
            }

            int pageSize = int.Parse(ConfigSettings.ReadSetting("pageSize"));
            var list = categoryService.GetPageList(new Page(page, pageSize), search, show);

            // map it to a paged list of models.
            var actionViewModel = Mapper.Map<IPagedList<Category>, IPagedList<CategoryViewModel>>(list);
            var languageViewModels = Mapper.Map<IEnumerable<Language>, List<LanguageViewModel>>(languages);
            var table = new CategoryTableViewModel
            {
                Show = show,
                Search = search,
                Languages = languageViewModels,
                ModelList = actionViewModel,
            };

            if (Request.IsAjaxRequest())
            {
                return PartialView("_NewsCateTable", table);
            }

            var model = new CategoryPageViewModel
            {
                Languages = languageViewModels,
                TableList = table,
                Lang = languages.ToSelectListItems()
            };

            return View(model);
        }

        [HttpGet]
        [AuthorizeUser(FunctionID = "NEWSCATE", RoleID = "CREATE")]
        public JsonResult GetSlug(string val)
        {
            if (val != null)
            {
                return Json(XString.ToAscii(val).ToLower(), JsonRequestBehavior.AllowGet);
            }

            return Json("", JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [AuthorizeUser(FunctionID = "NEWSCATE", RoleID = "CREATE")]
        public ActionResult Create(int id = 0, int languageId = 0)
        {
            var model = new CategoryViewModel()
            {
                Order = 1,
                CategoryId = id
            };

            // Truong hop them ngon ngu (danh muc da ton tai)
            if (id != 0)
            {
                var category = categoryService.GetById(id);

                model.Thumbnail = category?.Image?.FullPath;
                model.Order = category.Order;
            }

            // Lay ngon ngu mac dinh duoc chon
            var language = languages.FirstOrDefault(
                item =>
                    (languageId == 0 && item.Code == "*") ||
                    (languageId != 0 && item.Id == languageId)
            );

            model.CategoryItem = new CategoryItemViewModel()
            {
                Language = Mapper.Map<Language, LanguageViewModel>(language),
            };

            // Danh sach ngon ngu
            model.Lang = new SelectList(
                languages.Where(item => item.Id == languageId),
                "Id",
                "Name",
                model.CategoryItem.Language.Id
            );

            // Danh sach danh muc (khong phai la danh muc con)
            model.DrpCategories = GetParentCategories(id, language.Code, model.ParentID);

            model.DrpLayouts = new SelectList(
                new List<KeyValueModel>() {
                    new KeyValueModel((int)CategoryLayout.Grid, Resources.Resources.CategoryLayoutGrid),
                    new KeyValueModel((int)CategoryLayout.FeaturedPostGrid, Resources.Resources.CategoryLayoutFeaturedPostGrid)
                },
                "Id",
                "Value",
                model.Layout
            );
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken, Log("{model}")]
        [AuthorizeUser(FunctionID = "NEWSCATE", RoleID = "CREATE")]
        public ActionResult Create(CategoryViewModel viewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    ModelState.AddModelError("", Resources.Resources.WarningData);
                    var listlang = languageService.GetListForActive().ToList();
                    viewModel.Lang = new SelectList(listlang.OrderByDescending(x => x.CreatedDate), "Id", "Name", viewModel.CategoryItem.Language.Id);
                    return View(viewModel).WithWarning(Resources.Resources.WarningData);
                }

                viewModel.UpdatedBy = 1;
                viewModel.UpdatedDate = DateTime.Now;

                var category = Mapper.Map<CategoryViewModel, Category>(viewModel);
                category.CreatedDate = DateTime.Now;

                if (!string.IsNullOrEmpty(viewModel.Thumbnail))
                {
                    var mediaViewModel = GetMediaByUrl(viewModel.Thumbnail);
                    var image = mediaService.GetByFullPath(mediaViewModel.FullPath);
                    if (image == null)
                    {
                        image = mediaService.Create(Mapper.Map<MediaViewModel, Media>(mediaViewModel));
                        categoryItemService.SaveChange();
                    }
                    category.Image = image;
                }

                var language = languageService.GetById(viewModel.CategoryItem.Language.Id);
                var createdCategory = categoryService.Create(category);
                var categoryItem = Mapper.Map<CategoryItemViewModel, CategoryItem>(viewModel.CategoryItem);
                categoryItem.Slug = XString.ToAscii(viewModel.CategoryItem.Name).ToLower();
                categoryItem.Category = createdCategory;
                categoryItem.CreatedDate = DateTime.Now;
                categoryItem.Language = language;

                var createdCategoryItem = categoryItemService.Create(categoryItem);
                categoryItemService.SaveChange();

                // Thêm danh mục này vào menu trong trường hợp cha của nó đã có bên menu
                if (createdCategory.ParentID.HasValue && createdCategory.ParentID > 0)
                {
                    var parentId = (int)createdCategory.ParentID;

                    // Tính số thứ tự
                    var children = categoryService.GetByParentId(parentId);
                    var order = children != null ? (children.Count() + 1) : 1;

                    foreach (int i in Enum.GetValues(typeof(MenuGroupType)))
                    {
                        var groupType = (MenuGroupType)i;
                        var parentMenu = menuService.GetByCategoryId(groupType, parentId, language.Code);
                        if (parentMenu == null) continue;

                        menuService.Create(new Menu()
                        {
                            ParentID = parentMenu.Id,
                            IsActived = viewModel.IsActive,
                            Target = "_self",
                            CreatedDate = DateTime.Now,
                            CreatedBy = User.Identity.Name,
                            GroupType = groupType,
                            TextType = "Category",
                            Controller = "Category",
                            Name = categoryItem.Name,
                            Link = categoryItem.Slug,
                            CategoryItem = categoryItem,
                            Order = order,
                            Language = language
                        });
                    }
                }

                return RedirectToAction("Index").WithSuccess(Resources.Resources.SuccessCreate);
            }
            catch (Exception ex)
            {
                HandleException("Message ", ex);
                return RedirectToAction("Index").WithError(Resources.Resources.ErrorCatch);
            }
        }

        [AuthorizeUser(FunctionID = "NEWSCATE", RoleID = "EDIT")]
        public ActionResult Edit(int id)
        {
            try
            {
                if (id == 0) return RedirectToAction("Index").WithError(Resources.Resources.ErrorNoData);

                var categoryItem = categoryItemService.GetById(id);
                if (categoryItem == null)
                {
                    return RedirectToAction("Index").WithError(Resources.Resources.ErrorNoData);
                }

                var lang = languageService.GetListForActive().ToList();
                var model = Mapper.Map<Category, CategoryViewModel>(categoryItem.Category);
                model.CategoryItem = Mapper.Map<CategoryItem, CategoryItemViewModel>(categoryItem);

                model.Thumbnail = categoryItem.Category?.Image?.FullPath;

                model.Lang = new SelectList(
                    lang.Where(item => item.Id == categoryItem.Language.Id),
                    "Id",
                    "Name",
                    model.CategoryItem.Language.Id
                );

                model.DrpCategories = GetParentCategories(model.CategoryId, categoryItem.Language.Code, model.ParentID, categoryItem.Category.Id);

                model.DrpLayouts = new SelectList(
                    new List<KeyValueModel>() {
                        new KeyValueModel((int)CategoryLayout.Grid, Resources.Resources.CategoryLayoutGrid),
                        new KeyValueModel((int)CategoryLayout.FeaturedPostGrid, Resources.Resources.CategoryLayoutFeaturedPostGrid)
                    },
                    "Id",
                    "Value",
                    model.Layout
                );
                return View("Create", model);
            }
            catch (Exception ex)
            {
                HandleException("Message", ex);
                return RedirectToAction("Index").WithError(Resources.Resources.ErrorCatch);
            }
        }

        // Lấy dropdown list danh sách các danh mục cha
        private SelectList GetParentCategories(int categoryId, string languageCode, object selectedValue, int ignoredId = 0)
        {
            // Chỉ lấy các danh mục cấp 0 & 1
            var parents = categoryService.GetParents(categoryId, languageCode).ToList();
            var children = categoryService.GetByParentIds(parents.Select(x => x.Id), ignoredId, languageCode);

            if (children != null)
            {
                parents.AddRange(children);
            }

            return new SelectList(
                MapCategoriesToViewModels(parents, languageCode),
                "CategoryId",
                "Name",
                selectedValue
            );
        }

        [HttpPost, ValidateAntiForgeryToken, Log("{model}")]
        [AuthorizeUser(FunctionID = "NEWSCATE", RoleID = "EDIT")]
        public ActionResult Edit(CategoryViewModel viewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    ModelState.AddModelError("", Resources.Resources.WarningData);
                    var lang = languageService.GetListForActive().ToList();
                    viewModel.Lang = new SelectList(lang, "Id", "Name", viewModel.CategoryItem.Language.Id);
                    return View(viewModel).WithWarning(Resources.Resources.WarningData);
                }

                var language = languageService.GetById(viewModel.CategoryItem.Language.Id);

                // Lay danh muc chi tiet
                var categoryItem = categoryItemService.GetById(viewModel.CategoryItem.Id);
                if (categoryItem == null)
                {
                    // Neu khong co du lieu thi tao moi
                    var item = Mapper.Map<CategoryItemViewModel, CategoryItem>(viewModel.CategoryItem);
                    item.Slug = XString.ToAscii(viewModel.CategoryItem.Name).ToLower();
                    item.UpdatedDate = DateTime.Now;
                    item.CreatedDate = DateTime.Now;
                    item.Category = categoryService.GetById(viewModel.CategoryId);
                    item.Language = language;
                    categoryItem = categoryItemService.Create(item);
                }
                else
                {
                    // Neu co du lieu thi cap nhat lai du lieu
                    var item = Mapper.Map(viewModel.CategoryItem, categoryItem);
                    item.Slug = XString.ToAscii(viewModel.CategoryItem.Name).ToLower();
                    item.UpdatedDate = DateTime.Now;
                    item.Language = language;
                    categoryItemService.Update(categoryItem);
                }

                // Cập nhật lại quan hệ cha-con của danh mục này bên menu
                if (viewModel.IsActive != categoryItem.Category.IsActive || viewModel.ParentID != categoryItem.Category.ParentID)
                {
                    var parentId = viewModel.ParentID ?? 0;
                    foreach (int i in Enum.GetValues(typeof(MenuGroupType)))
                    {
                        var groupType = (MenuGroupType)i;
                        var menu = menuService.GetByCategoryId(groupType, categoryItem.Category.Id, language.Code);
                        if (menu == null) continue;

                        var parentMenu = menuService.GetByCategoryId(groupType, parentId, language.Code);

                        menu.IsActived = viewModel.IsActive;
                        menu.ParentID = parentMenu != null ? parentMenu.Id : 0;

                        menuService.Update(menu);
                    }
                }

                // Cap nhat danh muc
                var category = Mapper.Map(viewModel, categoryItem.Category);
                category.UpdatedDate = DateTime.Now;

                // Cap nhat anh neu anh tu viewModel khac voi anh hien tai
                if (category.Image?.FullPath != viewModel.Thumbnail)
                {
                    var mediaViewModel = GetMediaByUrl(viewModel.Thumbnail);
                    var image = mediaService.GetByFullPath(mediaViewModel.FullPath);
                    if (image == null)
                    {
                        image = mediaService.Create(Mapper.Map<MediaViewModel, Media>(mediaViewModel));
                    }
                    category.Image = image;
                }

                categoryService.Update(category);
                categoryItemService.SaveChange();

                return RedirectToAction("Index").WithSuccess(Resources.Resources.UpdateSuccess);
            }
            catch (Exception ex)
            {
                HandleException("Message", ex);
                return RedirectToAction("Index").WithError(Resources.Resources.ErrorCatch);
            }
        }

        [Log("{Id}")]
        [AuthorizeUser(FunctionID = "NEWSCATE", RoleID = "DELETE")]
        public ActionResult Delete(int id)
        {
            try
            {
                var category = categoryService.GetById(id);
                category.UpdatedDate = DateTime.Now;
                category.DeletedAt = DateTime.Now;

                categoryService.Update(category);
                categoryService.SaveChange();

                var categories = categoryService.GetByParentId(category.Id);

                foreach (var item in categories)
                {
                    item.ParentID = null;
                    category.UpdatedDate = DateTime.Now;
                }

                categoryService.SaveChange();

                if (Request.IsAjaxRequest())
                {
                    var reponse = new
                    {
                        Id = id,
                        Code = AjaxAlertConsts.WithSuccess,
                        Msg = Resources.Resources.AlertDataSuccess
                    };

                    return Json(reponse, JsonRequestBehavior.AllowGet);
                }

                return RedirectToAction("Index").WithSuccess(Resources.Resources.SuccessDeleteFunction);
            }
            catch (Exception ex)
            {
                HandleException("Message", ex);
                var reponse = new
                {
                    Id = id,
                    Code = AjaxAlertConsts.WithError,
                    Msg = Resources.Resources.AlertDataCatch
                };
                return Json(reponse, JsonRequestBehavior.AllowGet);
            }

        }
    }
}