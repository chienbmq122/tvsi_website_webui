using AutoMapper;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TTCMS.Areas.TT_Admin.CustomAttributes;
using TTCMS.Areas.TT_Admin.Filters;
using TTCMS.Core.Infrastructure.Alerts;
using TTCMS.Data.Entities;
using TTCMS.Service;
using TTCMS.Service.Common;
using TTCMS.Web.Areas.TT_Admin.Models;
using TTCMS.Web.ViewModels;

namespace TTCMS.Web.Areas.TT_Admin.Controllers
{
    public class FAQTagManagementController : BaseController
    {
        private readonly ILanguageService _languageService;
        private readonly IEnumerable<Language> languages;
        private readonly ITagService _tagService;
        public FAQTagManagementController(
            ILanguageService languageService,
            ITagService tagService
        )
        {
            _tagService = tagService;
            _languageService = languageService;
            languages = _languageService.GetListForActive();
        }
        // GET: TT_Admin/FAQQuestionManagement
        [AuthorizeUser(FunctionID = "QUESTION", RoleID = "VIEW")]
        public ActionResult Index(int? page = 1, string show = "", string search = "")
        {
            if (show != "")
            {
                cultureName = show;
            }
            int pageSize = int.Parse(ConfigSettings.ReadSetting("pageSize"));
            int pageNumber = (page ?? 1);
            var tags = _tagService.GetAll();
            var tagItems = new List<TagViewModel>();
            if (tags != null && tags.ToList().Count > 0)
            {
                foreach (var item in tags)
                {
                    var tagVM = Mapper.Map<Tag, TagViewModel>(item);
                    if (tagVM != null) tagItems.Add(tagVM);
                }
            }
            var languageVM = Mapper.Map<IEnumerable<Language>, List<LanguageViewModel>>(languages.Where(x => x.Id != 3));
            var table = new TagManagementViewModel
            {
                Show = show,
                Search = search,
                Languages = languageVM,
                ModelList = new PagedList<TagViewModel>(tagItems, pageNumber, pageSize),
            };
            if (Request.IsAjaxRequest())
            {
                return PartialView("_TagTablePagedList", table);
            }

            var model = new TagManagementPageViewModel
            {
                Languages = languageVM,
                TableList = table
            };

            return View(model);
        }

        [HttpGet]
        [AuthorizeUser(FunctionID = "QUESTION", RoleID = "CREATE")]
        public ActionResult Create(int id = 0, int languageId = 0)
        {
            var model = new TagViewModel()
            {
                Priority = 1
            };
            var language = languages.FirstOrDefault(
                item =>
                    (languageId == 0 && item.Code == "*") ||
                    (languageId != 0 && item.Id == languageId)
            );
            model.Lang = languageId == 0 ? new SelectList(
                languages,
                "Id",
                "Name",
                model.Language.Id
            ) : new SelectList(
                languages.Where(item => item.Id == languageId),
                "Id",
                "Name",
                model.Language.Id
            );
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken, Log("{model}")]
        [AuthorizeUser(FunctionID = "QUESTION", RoleID = "CREATE")]
        public ActionResult Create(TagViewModel viewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    ModelState.AddModelError("", Resources.Resources.WarningData);
                    var listlang = _languageService.GetListForActive().ToList();
                    viewModel.Lang = new SelectList(listlang.OrderByDescending(x => x.CreatedDate), "Id", "Name", viewModel.LanguageId);
                    return View(viewModel).WithWarning(Resources.Resources.WarningData);
                }
                var language = _languageService.GetById(viewModel.LanguageId);
                var tag = Mapper.Map<TagViewModel, Tag>(viewModel);
                if (language == null)
                {
                    return RedirectToAction("Index").WithError("L?i: Kh�ng kh?i t?o ???c t? kho� (EX_TAG_001)");
                }
                if (tag == null)
                {
                    return RedirectToAction("Index").WithError("L?i: Kh�ng kh?i t?o ???c t? kho� (EX_TAG_002)");
                }
                tag.Language = language;
                tag.CreatedBy = "admin";
                tag.CreatedDate = DateTime.Now;
                var createdTag = _tagService.Create(tag);
                _tagService.SaveChange();
                return RedirectToAction("Index").WithSuccess(Resources.Resources.SuccessCreate);
            } catch (Exception ex)
            {
                HandleException("Message ", ex);
                return RedirectToAction("Index").WithError(Resources.Resources.ErrorCatch);
            }
        }

        [AuthorizeUser(FunctionID = "QUESTION", RoleID = "VIEW")]
        public ActionResult Edit()
        {
            return View();
        }
        [AuthorizeUser(FunctionID = "QUESTION", RoleID = "VIEW")]
        public ActionResult Delete()
        {
            return View();
        }
    }
}