﻿using AutoMapper;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TTCMS.Areas.TT_Admin.Filters;
using TTCMS.Data.Entities;
using TTCMS.Web.Areas.TT_Admin.Models;
using TTCMS.Core.Infrastructure.Alerts;
using TTCMS.Areas.TT_Admin.CustomAttributes;
using TTCMS.Web.Extensions;
using TTCMS.Service;
using TTCMS.Data.Infrastructure;
using TTCMS.Service.Common;
using TTCMS.Web.ViewModels;
using TTCMS.Data.Entities.WebsiteNews;
using System.Threading.Tasks;

namespace TTCMS.Web.Areas.TT_Admin.Controllers
{
    [Authorize]
    public class NewsManagementController : BaseController
    {
        private readonly IEnumerable<Language> languages;
        private readonly ICategoryService categoryService;
        private readonly IMediaService mediaService;
        private readonly INewsService newsService;
        private readonly ILanguageService languageService;
        private readonly INewsItemService newsItemService;
        private readonly ITVSIWebsiteNewsService _websiteNewsService;
        public NewsManagementController(
            ICategoryService categoryService,
            IMediaService mediaService,
            INewsService newsService,
            ILanguageService languageService,
            INewsItemService newsItemService,
            ITVSIWebsiteNewsService websiteNewsService
        )
        {
            this.categoryService = categoryService;
            this.mediaService = mediaService;
            this.newsService = newsService;
            this.languageService = languageService;
            this.newsItemService = newsItemService;
            _websiteNewsService = websiteNewsService;
            languages = languageService.GetListForActive("*");
        }
        //
        // GET: /PageManagement/Role/
        //[OutputCache(Duration = 60)]
        [AuthorizeUser(FunctionID = "NEWS", RoleID = "VIEW")]
        [HttpGet]
        public async Task<ActionResult> Index(int page = 1, string show = "", string search = "")
        {
            var pageSize = int.Parse(ConfigSettings.ReadSetting("pageSize"));
            var list = newsService.GetPageList(new Page(page, pageSize), search, show);
            int languageId;
            switch (show)
            {
                case "vi-VN":
                    languageId = 2;
                    break;
                case "en-US":
                    languageId = 1;
                    break;
                default:
                    languageId = 0;
                    break;
            }
            //var newsList = await _websiteNewsService.GetNews(10, languageId, page);
            //var total = await _websiteNewsService.GetNumberOfData(null, null, languageId);
            var newsList = newsService.GetAll();
            var total = 100000;
            var pagedList = new StaticPagedList<News>(newsList.ToList(), page, 10, total) as IPagedList<News>;
            // map it to a paged list of models.
            var actionVM = Mapper.Map<IPagedList<News>, IPagedList<NewsViewModel>>(pagedList);
            //var actionViewModel = Mapper.Map<IPagedList<stock_News>, IPagedList<StockNewsViewModel>>(pagedList);
            var languageViewModels = Mapper.Map<IEnumerable<Language>, List<LanguageViewModel>>(languages);
            var table = new NewsTableViewModel
            {
                Languages = languageViewModels,
                ModelList = actionVM,
                Show = show,
                Search = search,
            };

            if (Request.IsAjaxRequest())
            {
                return PartialView("_NewsTable", table);
            }

            var model = new NewsPageViewModel
            {
                TableList = table,
                Languages = languageViewModels,
                Lang = languages.ToSelectListItems(show),
            };

            return View(model);
        }

        // GET: /PageManagement/Role/
        //[OutputCache(Duration = 60)]
        [AuthorizeUser(FunctionID = "NEWS", RoleID = "VIEW")]
        [HttpGet]
        public ActionResult Approves(int page = 1, string show = "", string search = "")
        {
            //string langid = "";
            //if (show != "")
            //{
            //    langid = languageService.GetByCode(show).Code;
            //}
            //int pageSize = int.Parse(ConfigSettings.ReadSetting("pageSize"));

            //var list = newsService.GetPageListApproves(new Page(page, pageSize), search, langid);


            //// map it to a paged list of models.
            //var actionViewModel = Mapper.Map<IPagedList<News>, IPagedList<NewsViewModel>>(list);

            //var table = new NewsTableViewModel { ModelList = actionViewModel, Show = show, Search = search };
            //if (Request.IsAjaxRequest())
            //{
            //    return PartialView("_NewsTableApproves", table);
            //}
            //var lang = languageService.GetListForActive();
            //var model = new NewsPageViewModel();
            //model.TableList = table;
            //model.Lang = lang.ToSelectListItems();
            //return View(model);
            return View();
        }

        //
        // GET: /PageManagement/Role/
        //[OutputCache(Duration = 60)]
        [AuthorizeUser(FunctionID = "NEWS", RoleID = "VIEW")]
        [HttpGet]
        public ActionResult ListPost(int page = 1, string show = "", string search = "")
        {
            string langid = "";
            if (show != "")
            {
                langid = languageService.GetByCode(show).Code;
            }
            //int pageSize = int.Parse(ConfigSettings.ReadSetting("pageSize"));
            int pageSize = 1;

            var list = newsService.GetPageListApproves(new Page(page, pageSize), search, langid);


            // map it to a paged list of models.
            var actionViewModel = Mapper.Map<IPagedList<News>, IPagedList<NewsViewModel>>(list);

            var table = new NewsTableViewModel { ModelList = actionViewModel, Show = show, Search = search };
            if (Request.IsAjaxRequest())
            {
                return PartialView("_NewsTableListPost", table);
            }
            var lang = languageService.GetListForActive();
            var model = new NewsPageViewModel();
            model.TableList = table;
            model.Lang = lang.ToSelectListItems();
            return View(model);
        }

        [HttpGet]
        [AuthorizeUser(FunctionID = "NEWS", RoleID = "CREATE")]
        public JsonResult GetSlug(string val)
        {
            if (val != null)
            {
                return Json(XString.ToAscii(val).ToLower(), JsonRequestBehavior.AllowGet);
            }

            return Json("", JsonRequestBehavior.AllowGet);
        }

        // GET: /PageManagement/Role/Create
        [AuthorizeUser(FunctionID = "NEWS", RoleID = "CREATE")]
        [HttpGet]
        public ActionResult Create(int id = 0, int languageId = 0)
        {
            var model = new NewsViewModel
            {
                Id = id,
                Language = new LanguageViewModel()
            };

            // Truong hop them ngon ngu (Tin da ton tai)
            if (id != 0)
            {
                var news = newsService.GetById(id);

                model.Keyword = news.Keyword;
                model.IsActive = news.IsActive;
                model.ImageUrl = news.ImageUrl;
                model.CategoryId = news.Category?.Id ?? 0;
            }

            // Lay ngon ngu mac dinh duoc chon
            var language = languages.FirstOrDefault(
                 item =>
                     (languageId == 0 && item.Code == "*") ||
                     (languageId != 0 && item.Id == languageId)
            );

            // Danh sach ngon ngu
            model.Lang = new SelectList(
                languages.Where(item => item.Id == languageId),
                "Id",
                "Name",
                model.Language.Id
            );

            // Danh sach danh muc
            model.ListCategory = GetCategoryDropdownList(categoryService, language.Code, model.CategoryId);

            return View(model);
        }

        // POST: /GroupMenuManagement/Role/Create
        [HttpPost, ValidateInput(false)]
        [ValidateAntiForgeryToken, Log("{model}")]
        [AuthorizeUser(FunctionID = "NEWS", RoleID = "CREATE")]
        public ActionResult Create(NewsViewModel viewModel)
        {
            try
            {
                var language = languageService.GetById(viewModel.Language.Id);
                if (!ModelState.IsValid)
                {
                    ModelState.AddModelError("", Resources.Resources.WarningData);
                    viewModel.Lang = GetLanguageDropdownList(languages, language.Id, language.Id);
                    viewModel.ListCategory = GetCategoryDropdownList(categoryService, language.Code, viewModel.CategoryId);
                    return View("Create", viewModel).WithWarning(Resources.Resources.WarningData);
                }

                viewModel.UpdatedDate = DateTime.Now;

                var news = Mapper.Map<NewsViewModel, News>(viewModel);
                news.CreatedDate = DateTime.Now;
                news.Category = categoryService.GetById(viewModel.CategoryId);

                if (!string.IsNullOrEmpty(viewModel.ImageUrl))
                {
                    news.ImageUrl = viewModel.ImageUrl;
                }

                var result = newsService.Create(news);
                newsItemService.SaveChange();

                return RedirectToAction("Index").WithSuccess(Resources.Resources.SuccessCreate);
            }
            catch (Exception ex)
            {
                HandleException("Message ", ex);
                return RedirectToAction("Index").WithError(Resources.Resources.ErrorCatch);
            }
        }

        [AuthorizeUser(FunctionID = "NEWS", RoleID = "EDIT")]
        public ActionResult Edit(int id = 0)
        {
            try
            {
                if (id == 0) return RedirectToAction("Index").WithError(Resources.Resources.ErrorNoData);

                var newsItem = newsItemService.GetById(id);
                if (newsItem == null)
                {
                    return RedirectToAction("Index").WithError(Resources.Resources.ErrorNoData);
                }

                var lang = languageService.GetListForActive().ToList();
                var news = newsService.GetById(newsItem.News.Id);
                var model = Mapper.Map<News, NewsViewModel>(news);
                model.ImageUrl = news.ImageUrl;

                model.Lang = new SelectList(
                    lang.Where(item => item.Id == newsItem.Language.Id),
                    "Id",
                    "Name",
                    model.Id
                );

                // Danh sach danh muc
                model.ListCategory = GetCategoryDropdownList(categoryService, newsItem.Language.Code, model.CategoryId);

                return View("Create", model);
            }
            catch (Exception ex)
            {
                HandleException("Message", ex);
                return RedirectToAction("Index").WithError(Resources.Resources.ErrorCatch);
            }
        }

        [AuthorizeUser(FunctionID = "NEWS", RoleID = "EDIT"), ValidateInput(false)]
        [HttpPost, ValidateAntiForgeryToken, Log("{model}")]
        public ActionResult Edit(NewsViewModel viewModel)
        {
            try
            {
                var language = languageService.GetById(viewModel.Language.Id);
                if (!ModelState.IsValid)
                {
                    ModelState.AddModelError("", Resources.Resources.WarningData);
                    viewModel.Lang = GetLanguageDropdownList(languages, language.Id, language.Id);
                    viewModel.ListCategory = GetCategoryDropdownList(categoryService, language.Code, viewModel.CategoryId);
                    return View("Create", viewModel).WithWarning(Resources.Resources.WarningData);
                }

                // Cap nhat tin
                var news = Mapper.Map<NewsViewModel, News>(viewModel);
                news.UpdatedDate = DateTime.Now;

                // Cap nhat danh muc
                if (viewModel.CategoryId != news.Category?.Id)
                {
                    news.Category = categoryService.GetById(viewModel.CategoryId);
                }

                // Cap nhat anh neu anh tu viewModel khac voi anh hien tai
                if (news.ImageUrl != viewModel.ImageUrl)
                {
                    news.ImageUrl = viewModel.ImageUrl;
                }

                newsService.Update(news);
                newsItemService.SaveChange();

                return RedirectToAction("Index").WithSuccess(Resources.Resources.UpdateSuccess);
            }
            catch (Exception ex)
            {
                HandleException("Message", ex);
                return RedirectToAction("Index").WithError(Resources.Resources.ErrorCatch);
            }
        }

        [AuthorizeUser(FunctionID = "NEWS", RoleID = "DELETE"), Log("{Id}")]
        public ActionResult Delete(int id = 0)
        {
            try
            {
                var news = newsService.GetById(id);
                news.UpdatedDate = DateTime.Now;
                news.DeletedAt = DateTime.Now;

                var newsItems = newsItemService.GetByNewsId(id);

                foreach (var item in newsItems)
                {
                    item.UpdatedDate = DateTime.Now;
                    item.DeletedAt = DateTime.Now;
                    newsItemService.Update(item);
                }

                newsService.Update(news);
                newsService.SaveChange();


                if (Request.IsAjaxRequest())
                {
                    var reponse = new
                    {
                        Id = id,
                        Code = AjaxAlertConsts.WithSuccess,
                        Msg = Resources.Resources.AlertDataSuccess
                    };

                    return Json(reponse, JsonRequestBehavior.AllowGet);
                }

                return RedirectToAction("Index").WithSuccess(Resources.Resources.SuccessDeleteFunction);
            }
            catch (Exception ex)
            {
                HandleException("Message", ex);
                var reponse = new { Id = id, Code = AjaxAlertConsts.WithError, Msg = Resources.Resources.AlertDataCatch };
                return Json(reponse, JsonRequestBehavior.AllowGet);
            }

        }

        [AuthorizeUser(FunctionID = "NEWS", RoleID = "DELETE"), Log("{Id}")]
        public ActionResult IsApprove(int Id = 0)
        {
            try
            {
                if (Id == 0)
                {
                    var reponse = new { Id = Id, Code = AjaxAlertConsts.WithError, Msg = Resources.Resources.AlertDataNull };
                    return Json(reponse, JsonRequestBehavior.AllowGet);
                }
                var function = newsService.GetById(Id);
                if (function == null)
                {
                    var reponse = new { Id = Id, Code = AjaxAlertConsts.WithError, Msg = Resources.Resources.AlertDataNull };
                    return Json(reponse, JsonRequestBehavior.AllowGet);
                }
                //function.IsApprove = 1;
                newsService.Update(function);
                if (Request.IsAjaxRequest())
                {
                    var reponse = new { Id = Id, Code = AjaxAlertConsts.WithSuccess, Msg = "Duyệt bản tin thành công" };
                    return Json(reponse, JsonRequestBehavior.AllowGet);
                }
                return RedirectToAction("Index").WithSuccess("Duyệt bản tin thành công");
            }
            catch (Exception ex)
            {
                HandleException("Message", ex);
                var reponse = new { Id = Id, Code = AjaxAlertConsts.WithError, Msg = Resources.Resources.AlertDataCatch };
                return Json(reponse, JsonRequestBehavior.AllowGet);
            }

        }

        [AuthorizeUser(FunctionID = "NEWS", RoleID = "DELETE"), Log("{Id}")]
        public ActionResult UnApprove(int Id = 0)
        {
            try
            {
                if (Id == 0)
                {
                    var reponse = new { Id = Id, Code = AjaxAlertConsts.WithError, Msg = Resources.Resources.AlertDataNull };
                    return Json(reponse, JsonRequestBehavior.AllowGet);
                }
                var function = newsService.GetById(Id);
                if (function == null)
                {
                    var reponse = new { Id = Id, Code = AjaxAlertConsts.WithError, Msg = Resources.Resources.AlertDataNull };
                    return Json(reponse, JsonRequestBehavior.AllowGet);
                }
                //function.IsApprove = 0;
                newsService.Update(function);
                if (Request.IsAjaxRequest())
                {
                    var reponse = new { Id = Id, Code = AjaxAlertConsts.WithSuccess, Msg = "Bỏ duyệt bản tin thành công" };
                    return Json(reponse, JsonRequestBehavior.AllowGet);
                }
                return RedirectToAction("Index").WithSuccess("Bỏ duyệt bản tin thành công");
            }
            catch (Exception ex)
            {
                HandleException("Message", ex);
                var reponse = new { Id = Id, Code = AjaxAlertConsts.WithError, Msg = Resources.Resources.AlertDataCatch };
                return Json(reponse, JsonRequestBehavior.AllowGet);
            }

        }

        [AuthorizeUser(FunctionID = "NEWS", RoleID = "DELETE"), Log("{Id}")]
        public ActionResult IsPost(int Id = 0)
        {
            try
            {
                if (Id == 0)
                {
                    var reponse = new { Id = Id, Code = AjaxAlertConsts.WithError, Msg = Resources.Resources.AlertDataNull };
                    return Json(reponse, JsonRequestBehavior.AllowGet);
                }
                var function = newsService.GetById(Id);
                if (function == null)
                {
                    var reponse = new { Id = Id, Code = AjaxAlertConsts.WithError, Msg = Resources.Resources.AlertDataNull };
                    return Json(reponse, JsonRequestBehavior.AllowGet);
                }
                //function.IsPost = 1;
                newsService.Update(function);
                if (Request.IsAjaxRequest())
                {
                    var reponse = new { Id = Id, Code = AjaxAlertConsts.WithSuccess, Msg = "Đăng bài thành công" };
                    return Json(reponse, JsonRequestBehavior.AllowGet);
                }
                return RedirectToAction("Index").WithSuccess("Đăng bài thành công");
            }
            catch (Exception ex)
            {
                HandleException("Message", ex);
                var reponse = new { Id = Id, Code = AjaxAlertConsts.WithError, Msg = Resources.Resources.AlertDataCatch };
                return Json(reponse, JsonRequestBehavior.AllowGet);
            }

        }
        private void GetSubTree(IList<Category> allCats, CategoryViewModel parent, IList<CategoryViewModel> items)
        {
            var subCats = allCats.Where(c => c.ParentID == parent.CategoryId);
            foreach (var cat in subCats)
            {
                //add this category
                var cate = new CategoryViewModel()
                {
                    CategoryId = cat.Id,
                    //Name = parent.Name + " >> " + cat.Name,
                    //Content = cat.Name,
                    Order = cat.Order,
                    IsActive = cat.IsActive,
                };
                items.Add(cate);
                //recursive call in case your have a hierarchy more than 1 level deep
                GetSubTree(allCats, cate, items);
            }
        }
    }
}