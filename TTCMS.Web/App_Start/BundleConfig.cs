﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace TTCMS.Web.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/content/scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/content/scripts/jquery.validate.js",
                        "~/content/scripts/jquery.validate.unobtrusive.js",
                        "~/content/scripts/jquery.validate.bootstrap.js"));

            bundles.Add(new ScriptBundle("~/bundles/main").Include(
                      "~/webroot/js/main.js"
                ));
            bundles.Add(new ScriptBundle("~/bundles/faq").Include(
                  "~/asset/js/FAQ/main.js",
                  "~/asset/js/FAQ/faq-header.js",
                  "~/asset/js/FAQ/question-form.js"
            ));
            bundles.Add(new ScriptBundle("~/bundles/faqQuestionPage").Include(
                  "~/asset/js/FAQ/question-page.js"
            ));
            bundles.Add(new ScriptBundle("~/bundles/faqSearchQuestion").Include(
                  "~/asset/js/FAQ/search-question.js"
            ));
            bundles.Add(new ScriptBundle("~/bundles/faqSearchCategory").Include(
                  "~/asset/js/FAQ/search-category.js"
            ));
            //bundles.Add(new ScriptBundle("~/bundles/scripts")
            //        .IncludeDirectory("~/asset/js/FAQ/", "*.js", true));
            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/content/scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/content/scripts/bootstrap.js",
                      "~/content/scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Styles/css").Include(
                      "~/content/styles/bootstrap.css",
                      "~/content/styles/site.css"));
        }
    }
}