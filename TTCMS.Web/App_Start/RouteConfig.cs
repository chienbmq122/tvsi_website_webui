﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace TTCMS.Web
{
    public class RouteConfig
    {
        public static void RegisterFAQRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapRoute(
                name: "Default",
                url: "{action}/{id}",
                defaults: new { controller = "FAQ", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "TTCMS.Web.Controllers" }
            );
        }
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapRoute(
                name: "GetSymbol",
                url: "MenuController/GetSymbol",
                defaults: new { controller = "Menu", action = "GetSymbol", code = UrlParameter.Optional, langId = UrlParameter.Optional },
                namespaces: new[] { "TTCMS.Web.Controllers" }
            );
            routes.MapRoute(
                name: "margin-tv10-9",
                url: "margin-tv10-9",
                defaults: new { controller = "Margin", action = "Index" },
                namespaces: new[] { "TTCMS.Web.Controllers" }
            );
            routes.MapRoute(
                name: "FullNews",
                url: "News/Danh-sach-tin-tuc",
                defaults: new {controller = "News", action = "Search", keyWord = UrlParameter.Optional},
                namespaces: new[] {"TTCMS.Web.Controllers"}
            );
            routes.MapRoute(
                name: "SearchNews",
                url: "Search/{keyWord}",
                defaults: new {controller = "News", action = "Search", keyWord = UrlParameter.Optional},
                namespaces: new[] {"TTCMS.Web.Controllers"}
            );
            routes.MapRoute(
               name: "CateOnly",
               url: "Category/{slug}",
               defaults: new { controller = "ActivityNews", action = "Index", slug = UrlParameter.Optional },
                namespaces: new[] { "TTCMS.Web.Controllers" }
            );
            routes.MapRoute(
                name: "SinglePage",
                url: "page/{slug}",
                defaults: new {controller = "SinglePage", action = "Index", slug = UrlParameter.Optional},
                namespaces: new[] {"TTCMS.Web.Controllers"}
            );
            routes.MapRoute(
                name: "News",
                url: "News/{slug}.html",
                defaults: new {controller = "News", action = "Index", slug = UrlParameter.Optional},
                namespaces: new[] {"TTCMS.Web.Controllers"}
            );
            routes.MapRoute(
                name: "Gallery",
                url: "News/Gallery/{folder}.html",
                defaults: new { controller = "News", action = "Gallery", folder = UrlParameter.Optional },
                namespaces: new[] { "TTCMS.Web.Controllers" }
            );
            routes.MapRoute(
                "PageDetail", // Route name
                "page/{alias}/{id}.html", // URL with parameters
                new {controller = "Page", action = "Detail", id = UrlParameter.Optional, alias = UrlParameter.Optional}
            );
            routes.MapRoute(
                "Product-detail", // Route name
                "san-pham/{alias}-{id}.html", // URL with parameters
                new
                {
                    controller = "Product", action = "Detail", id = UrlParameter.Optional, alias = UrlParameter.Optional
                }
            );
            routes.MapRoute(
                "NewCata", // Route name
                "cate/{alias}-{id}.html", // URL with parameters
                new {controller = "Page", action = "Index", id = UrlParameter.Optional, alias = UrlParameter.Optional},
                namespaces: new[] {"TTCMS.Web.Controllers"}
            );
            routes.MapRoute(
                "page-detail", // Route name
                "page/{alias}-{id}.html", // URL with parameters
                new
                {
                    controller = "Page", action = "PageDetail", id = UrlParameter.Optional,
                    alias = UrlParameter.Optional
                }
            );
            routes.MapRoute(
                name: "AboutUS",
                url: "lien-he.html",
                defaults: new {controller = "Home", action = "LienHe", id = UrlParameter.Optional},
                namespaces: new[] {"TTCMS.Web.Controllers"}
            );
            routes.MapRoute(
                name: "thay-doi-mat-khau",
                url: "thay-doi-mat-khau.html",
                defaults: new {controller = "Account", action = "ChangePass", id = UrlParameter.Optional},
                namespaces: new[] {"TTCMS.Web.Controllers"}
            );
            routes.MapRoute(
                name: "thong-tin-tai-khoan",
                url: "thong-tin-tai-khoan.html",
                defaults: new {controller = "Account", action = "Profile", id = UrlParameter.Optional},
                namespaces: new[] {"TTCMS.Web.Controllers"}
            );
            routes.MapRoute(
                name: "dang-ky-tai-khoan",
                url: "dang-ky-tai-khoan.html",
                defaults: new {controller = "Account", action = "Register", id = UrlParameter.Optional},
                namespaces: new[] {"TTCMS.Web.Controllers"}
            );
            routes.MapRoute(
                name: "khoi-phuc-mat-khau",
                url: "khoi-phuc-mat-khau.html",
                defaults: new {controller = "Account", action = "ForgotPassword", id = UrlParameter.Optional},
                namespaces: new[] {"TTCMS.Web.Controllers"}
            );
            routes.MapRoute(
                name: "dang-nhap",
                url: "dang-nhap.html",
                defaults: new {controller = "Account", action = "LoginS", id = UrlParameter.Optional},
                namespaces: new[] {"TTCMS.Web.Controllers"}
            );
            routes.MapRoute(
                name: "timkiem",
                url: "tim-kiem",
                defaults: new {controller = "Home", action = "TimKiem", id = UrlParameter.Optional},
                namespaces: new[] {"TTCMS.Web.Controllers"}
            );
            routes.MapRoute(
                name: "dangxuat",
                url: "dang-xuat",
                defaults: new {controller = "Account", action = "LogOff", id = UrlParameter.Optional},
                namespaces: new[] {"TTCMS.Web.Controllers"}
            );
            routes.MapRoute(
                name: "spkm",
                url: "san-pham-khuyen-mai.html",
                defaults: new {controller = "Product", action = "Sales", id = UrlParameter.Optional},
                namespaces: new[] {"TTCMS.Web.Controllers"}
            );
            routes.MapRoute(
                "DetailNew", // Route name
                "tin-tuc/{alias}-{id}.html", // URL with parameters
                new
                {
                    controller = "Page", action = "DetailNews", id = UrlParameter.Optional,
                    alias = UrlParameter.Optional
                } // Parameter defaults
            );
            routes.MapRoute(
                "dm-product", // Route name
                "danh-muc/{alias}-{id}.html", // URL with parameters
                new
                {
                    controller = "Product", action = "Category", newsRoute = UrlParameter.Optional
                } // Parameter defaults
            );
            

            // Open account ekyc
            routes.MapRoute(
                name: "register customer personal ekyc",
                url: "home/register-customer-personal-ekyc/{id}",
                defaults: new
                {
                    controller = "OpenAccount",
                    action = "RegisterOpenAccountEKYC",
                    id = UrlParameter.Optional
                },
                namespaces: new[] {"TTCMS.Web.Controllers"}
            );
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new {controller = "Home", action = "Index", id = UrlParameter.Optional},
                namespaces: new[] {"TTCMS.Web.Controllers"}
            );
        }
    }
}