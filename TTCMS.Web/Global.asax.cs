﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Http;
using TTCMS.Web.App_Start;
using System.Web.Optimization;
using TTCMS.Web.Themes;
using System.Data.Entity;
using TTCMS.Data;

namespace TTCMS.Web
{
    public class Global : HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {
            //remove all view engines
            ViewEngines.Engines.Clear();
            //except the themeable razor view engine we use
            ViewEngines.Engines.Add(new ThemeableRazorViewEngine());
            // Code that runs on application startup
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            //RouteConfig.RegisterRoutes(RouteTable.Routes);
            RouteConfig.RegisterFAQRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
           
            log4net.Config.XmlConfigurator.Configure();
            Bootstrapper.Run();
            //Database.SetInitializer(new DropCreateDatabaseIfModelChanges<TTCMSDbContext>());
        }
    }
}