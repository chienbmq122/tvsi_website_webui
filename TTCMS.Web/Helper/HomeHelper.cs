﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TTCMS.Data.Entities;
using TTCMS.Service;
using TTCMS.Web.Models;
using TTCMS.Web.ViewModels;

namespace TTCMS.Web.Helper
{
    public static class HomeHelper
    {
        public static HomeViewModel GetHomeViewModel(int languageId, ISiteSettingService siteSettingService, ILanguageService languageService)
        {
            var anySetting = siteSettingService.GetByLang(languageId).Any();
            if (!anySetting)
            {
                InsertSettingForNewLang(languageId, siteSettingService, languageService);
            }
            var slider = siteSettingService.GetByKey(SiteSettingKey.HomeConfigSlider, languageId);
            var investmentForm = siteSettingService.GetByKey(SiteSettingKey.HomeConfigInvestmentForm, languageId);
            var investmentFormTitle = siteSettingService.GetByKey(SiteSettingKey.Title, languageId, investmentForm.Id);

            var viewModel = new HomeViewModel()
            {
                LanguageId = languageId,
                SlideUrls = siteSettingService.GetManyByKey(SiteSettingKey.Url, languageId, slider.Id).Select(x => new KeyValueModel(x.Id, x.Value)).ToList(),
                InvestmentFormTitle = new KeyValueModel(investmentFormTitle.Id, investmentFormTitle.Value),
            };

            // Lấy dữ liệu khối QuickAccess
            var quickAccessItems = siteSettingService.GetManyByKey(SiteSettingKey.HomeConfigQuickAccessItem, languageId);
            foreach (var item in quickAccessItems)
            {
                var homeObject = new HomeObject()
                {
                    Title = new KeyValueModel(),
                    ImageUrl = new KeyValueModel(),
                    RedirectUrl = new KeyValueModel(),
                };
                var itemConfigs = siteSettingService.GetManyByParentId(languageId, item.Id);

                foreach (var config in itemConfigs)
                {
                    switch (config.Key)
                    {
                        case SiteSettingKey.Title:
                            homeObject.Title = new KeyValueModel(config.Id, config.Value);
                            continue;
                        case SiteSettingKey.Url:
                            homeObject.ImageUrl = new KeyValueModel(config.Id, config.Value);
                            continue;
                        case SiteSettingKey.RedirectUrl:
                            homeObject.RedirectUrl = new KeyValueModel(config.Id, config.Value);
                            continue;
                        default:
                            continue;
                    }
                }

                viewModel.QuickAccessItems.Add(homeObject);
            }

            // lấy dữ liệu khối Lựa chọn đầu tư
            var investmentFormItems = siteSettingService.GetManyByKey(SiteSettingKey.HomeConfigInvestmentFormItem, languageId).OrderBy(i => i.Id);
            foreach (var item in investmentFormItems)
            {
                var homeObject = new HomeObject() { Id = item.Id };
                var investmentFormConfig = siteSettingService.GetManyByParentId(languageId, item.Id);
                if (investmentFormConfig != null && investmentFormConfig.Count() > 0)
                {
                    foreach (var config in investmentFormConfig)
                    {
                        switch (config.Key)
                        {
                            case SiteSettingKey.Title:
                                homeObject.Title = new KeyValueModel(config.Id, config.Value);
                                continue;
                            case SiteSettingKey.ButtonText:
                                homeObject.ButtonText = new KeyValueModel(config.Id, config.Value);
                                continue;
                            case SiteSettingKey.Description:
                                homeObject.Description = new KeyValueModel(config.Id, config.Value);
                                continue;
                            case SiteSettingKey.Url:
                                homeObject.ImageUrl = new KeyValueModel(config.Id, config.Value);
                                continue;
                            case SiteSettingKey.RedirectUrl:
                                homeObject.RedirectUrl = new KeyValueModel(config.Id, config.Value);
                                continue;
                            default:
                                continue;
                        }
                    };
                    viewModel.InvestmentFormItems.Add(homeObject);
                }
            }

            // Lấy dữ liệu khối Công cụ giao dịch
            var tradingTools = siteSettingService.GetByKey(SiteSettingKey.HomeConfigTradingTool, languageId);
            var tradingToolConfig = siteSettingService.GetManyByParentId(languageId, tradingTools.Id);

            foreach (var config in tradingToolConfig)
            {
                switch (config.Key)
                {
                    case SiteSettingKey.Title:
                        viewModel.TradingTool.Title = new KeyValueModel(config.Id, config.Value);
                        continue;
                    case SiteSettingKey.Description:
                        viewModel.TradingTool.Description = new KeyValueModel(config.Id, config.Value);
                        continue;
                    case SiteSettingKey.Url:
                        viewModel.TradingToolImages.Add(new KeyValueModel(config.Id, config.Value));
                        continue;
                    default:
                        continue;
                }
            }

            // Lấy dữ liệu khối Trung tâm phân tích
            var analysisCenters = siteSettingService.GetByKey(SiteSettingKey.HomeConfigAnalysisCenter, languageId);
            var analysisCenterConfig = siteSettingService.GetManyByParentId(languageId, analysisCenters.Id);

            foreach (var config in analysisCenterConfig)
            {
                switch (config.Key)
                {
                    case SiteSettingKey.Title:
                        viewModel.AnalysisCenter.Title = new KeyValueModel(config.Id, config.Value);
                        continue;
                    case SiteSettingKey.Description:
                        viewModel.AnalysisCenter.Description = new KeyValueModel(config.Id, config.Value);
                        continue;
                    default:
                        continue;
                }
            }

            // Lấy dữ liệu khối Trung tâm phân tích
            var analysisCenterItems = siteSettingService.GetManyByKey(SiteSettingKey.HomeConfigAnalysisCenterItem, languageId);
            foreach (var item in analysisCenterItems)
            {
                var homeObject = new HomeObject();
                var analysisCenterItemConfig = siteSettingService.GetManyByParentId(languageId, item.Id);

                foreach (var config in analysisCenterItemConfig)
                {
                    switch (config.Key)
                    {
                        case SiteSettingKey.Title:
                            homeObject.Title = new KeyValueModel(config.Id, config.Value);
                            continue;
                        case SiteSettingKey.ButtonText:
                            homeObject.ButtonText = new KeyValueModel(config.Id, config.Value);
                            continue;
                        case SiteSettingKey.Description:
                            homeObject.Description = new KeyValueModel(config.Id, config.Value);
                            continue;
                        case SiteSettingKey.RedirectUrl:
                            homeObject.RedirectUrl = new KeyValueModel(config.Id, config.Value);
                            continue;
                        default:
                            continue;
                    }
                }

                viewModel.AnalysisCenterItem.Add(homeObject);
            }

            // Lấy dữ liệu khối Tin túc nổi bật
            var featuredNews = siteSettingService.GetByKey(SiteSettingKey.HomeConfigFeaturedNews, languageId);
            var featuredNewsConfig = siteSettingService.GetManyByParentId(languageId, featuredNews.Id);

            foreach (var config in featuredNewsConfig)
            {
                switch (config.Key)
                {
                    case SiteSettingKey.Title:
                        viewModel.FeaturedNews.Title = new KeyValueModel(config.Id, config.Value);
                        continue;
                    default:
                        continue;
                }
            }

            return viewModel;
        }
        public static void InsertSettingForNewLang(int languageId, ISiteSettingService siteSettingService, ILanguageService languageService)
        {
            var lang = languageService.GetById(languageId);
            #region HomeConfigSlider
            var settingUrl = new SiteSetting();
            settingUrl.Value = SiteSettingKey.HomeConfigSlider.ToString();
            settingUrl.Description = "Cấu hình Slide Url";
            settingUrl.Key = SiteSettingKey.HomeConfigSlider;
            settingUrl.Language = lang;
            siteSettingService.Create(settingUrl);
            for (int i = 0; i < 4; i++)
            {
                var settingChild = new SiteSetting
                {
                    Key = SiteSettingKey.Url,
                    Value = "",
                    Parent = settingUrl,
                    Language = lang
                };
                siteSettingService.Create(settingChild);
            }
            #region HomeConfigQuickAccessItem
            for (int i = 0; i < 5; i++)
            {
                var settingQuickAccess = new SiteSetting
                {
                    Key = SiteSettingKey.HomeConfigQuickAccessItem,
                    Value = SiteSettingKey.HomeConfigQuickAccessItem.ToString(),
                    Parent = null,
                    Language = lang,
                    Description = "Cấu hình Truy cập nhanh"
                };
                siteSettingService.Create(settingQuickAccess);
                for (int j = 0; j < 5; j++)
                {
                    var settingChild = new SiteSetting
                    {
                        Key = (SiteSettingKey)j,
                        Value = "",
                        Parent = settingQuickAccess,
                        Language = lang
                    };
                    siteSettingService.Create(settingChild);
                }
            }
            #endregion
            #endregion
            #region HomeConfigTradingTool
            var settingTradingTool = new SiteSetting();
            settingTradingTool.Value = SiteSettingKey.HomeConfigTradingTool.ToString();
            settingTradingTool.Description = "Cấu hình Công cụ giao dịch";
            settingTradingTool.Key = SiteSettingKey.HomeConfigTradingTool;
            settingTradingTool.Language = lang;
            siteSettingService.Create(settingTradingTool);
            siteSettingService.Create(new SiteSetting
            {
                Key = SiteSettingKey.Title,
                Value = "",
                Parent = settingTradingTool,
                Language = lang
            });
            siteSettingService.Create(new SiteSetting
            {
                Key = SiteSettingKey.Description,
                Value = "",
                Parent = settingTradingTool,
                Language = lang
            });
            for (int i = 0; i < 4; i++)
            {
                var settingChild = new SiteSetting
                {
                    Key = SiteSettingKey.Url,
                    Value = "",
                    Parent = settingTradingTool,
                    Language = lang
                };
                siteSettingService.Create(settingChild);
            }
            #endregion
            #region HomeConfigAnalysisCenter
            var settingAnalysisCenter = new SiteSetting();
            settingAnalysisCenter.Value = SiteSettingKey.HomeConfigAnalysisCenter.ToString();
            settingAnalysisCenter.Description = "Cấu hình Trung tâm phân tích";
            settingAnalysisCenter.Key = SiteSettingKey.HomeConfigAnalysisCenter;
            settingAnalysisCenter.Language = lang;
            siteSettingService.Create(settingAnalysisCenter);
            siteSettingService.Create(new SiteSetting
            {
                Key = SiteSettingKey.Title,
                Value = "",
                Parent = settingAnalysisCenter,
                Language = lang
            });
            siteSettingService.Create(new SiteSetting
            {
                Key = SiteSettingKey.Description,
                Value = "",
                Parent = settingAnalysisCenter,
                Language = lang
            });
            for (int i = 0; i < 3; i++)
            {
                var settingAnalysisCenterItem = new SiteSetting();
                settingAnalysisCenterItem.Value = SiteSettingKey.HomeConfigAnalysisCenterItem.ToString();
                settingAnalysisCenterItem.Description = "Cấu hình Công cụ giao dịch mục " + (i + 1);
                settingAnalysisCenterItem.Key = SiteSettingKey.HomeConfigAnalysisCenterItem;
                settingAnalysisCenterItem.Language = lang;
                siteSettingService.Create(settingAnalysisCenterItem);
                for (int j = 0; j < 5; j++)
                {
                    var settingChild = new SiteSetting
                    {
                        Key = (SiteSettingKey)j,
                        Value = "",
                        Parent = settingAnalysisCenterItem,
                        Language = lang
                    };
                    siteSettingService.Create(settingChild);
                }
            }
            #endregion
            #region HomeConfigInvestmentForm
            var settingInvestmentForm = new SiteSetting();
            settingInvestmentForm.Value = SiteSettingKey.HomeConfigInvestmentForm.ToString();
            settingInvestmentForm.Description = "Cấu hình khối Lựa chọn đầu tư";
            settingInvestmentForm.Key = SiteSettingKey.HomeConfigInvestmentForm;
            settingInvestmentForm.Language = lang;
            siteSettingService.Create(settingInvestmentForm);

            var settingInvestmentFormTitle = new SiteSetting();
            settingInvestmentFormTitle.Value = "";
            settingInvestmentFormTitle.Description = "Cấu hình khối Lựa chọn đầu tư tiêu đề";
            settingInvestmentFormTitle.Key = SiteSettingKey.Title;
            settingInvestmentFormTitle.Language = lang;
            settingInvestmentFormTitle.Parent = settingInvestmentForm;
            siteSettingService.Create(settingInvestmentFormTitle);
            #endregion
            #region HomeConfigFeaturedNews
            var settingFeaturedNews = new SiteSetting();
            settingFeaturedNews.Value = SiteSettingKey.HomeConfigFeaturedNews.ToString();
            settingFeaturedNews.Description = "Cấu hình tin tức nổi bật";
            settingFeaturedNews.Key = SiteSettingKey.HomeConfigFeaturedNews;
            settingFeaturedNews.Language = lang;
            siteSettingService.Create(settingFeaturedNews);
            var settingFeaturedNewsTitle = new SiteSetting();
            settingFeaturedNewsTitle.Value = "";
            settingFeaturedNewsTitle.Description = "Cấu hình tin tức nổi bật tiêu đề";
            settingFeaturedNewsTitle.Key = SiteSettingKey.Title;
            settingFeaturedNewsTitle.Language = lang;
            settingFeaturedNewsTitle.Parent = settingFeaturedNews;
            siteSettingService.Create(settingFeaturedNewsTitle);
            #endregion
            siteSettingService.SaveChange();
        }
    }
}