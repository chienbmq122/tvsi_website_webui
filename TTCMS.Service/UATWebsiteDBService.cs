﻿using log4net;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TTCMS.Data.Entities.WebsiteNews;
using Dapper;

namespace TTCMS.Service
{
    public interface IUATWebsiteDBService
    {
        Task<IEnumerable<TVSI_THONG_TIN_TVSI>> GetByCategory(int categoryId, int languageId, int pageSize, int page);
        Task<int> GetTotalNumberOfRecords(int categoryId, int languageId);
        Task<TVSI_THONG_TIN_TVSI> GetById(int id);
        
    }
    public class UATWebsiteDBService : BaseService, IUATWebsiteDBService
    {
        private readonly ILog log = LogManager.GetLogger(typeof(TVSIWebsiteNewsService));

        public async Task<IEnumerable<TVSI_THONG_TIN_TVSI>> GetByCategory(int categoryId, int languageId, int pageSize, int page)
        {
            try
            {
                return await WithUATWebisteDbConnection(async conn => {
                    var query = $@"
                    SELECT 
                        thong_tin_tvsiid
                      ,chuyen_muc_tin_tvsiid
                      ,nganhid
                      ,doanh_nghiepid
                      ,ngay_dang_tai
                      ,tieu_de
                      ,tom_tat
                      ,noi_dung
                      ,tin_quan_trong
                      ,ngon_nguid
                      ,trang_thai
                      ,ngay_cap_nhat_cuoi
                      ,nguoi_cap_nhat_cuoi
                      ,file_dang_tai
                      ,duong_dan_file_anh
                      ,thu_tu_hien_thi
                      ,hien_thi_on_top
                      ,rowguid
                    FROM TVSI_THONG_TIN_TVSI
                    WHERE chuyen_muc_tin_tvsiid = @categoryId
                        AND ngon_nguid = @languageId
                    ORDER BY ngay_dang_tai DESC 
                    OFFSET @Offset ROWS FETCH NEXT @pageSize ROWS ONLY;
                    ";
                    return await conn.QueryAsync<TVSI_THONG_TIN_TVSI>(query, new { categoryId, languageId, Offset = page * pageSize, pageSize});
                });
            }
            catch (Exception ex)
            {
                log.Error(string.Format("LOI METHOD UATWebsiteDBService.GetByCategory --- {0}", ex.Message));
                throw;
            }
        }

        public async Task<TVSI_THONG_TIN_TVSI> GetById(int id)
        {
            try
            {
                return await WithUATWebisteDbConnection(async conn => {
                    var query = $@"
                    SELECT 
                        [thong_tin_tvsiid]
                      ,[chuyen_muc_tin_tvsiid]
                      ,[nganhid]
                      ,[doanh_nghiepid]
                      ,[ngay_dang_tai]
                      ,[tieu_de]
                      ,[tom_tat]
                      ,[noi_dung]
                      ,[tin_quan_trong]
                      ,[ngon_nguid]
                      ,[trang_thai]
                      ,[ngay_cap_nhat_cuoi]
                      ,[nguoi_cap_nhat_cuoi]
                      ,[file_dang_tai]
                      ,[duong_dan_file_anh]
                      ,[thu_tu_hien_thi]
                      ,[hien_thi_on_top]
                      ,[rowguid]
                    FROM TVSI_THONG_TIN_TVSI
                    WHERE [thong_tin_tvsiid] = @id";
                    return await conn.QueryFirstOrDefaultAsync<TVSI_THONG_TIN_TVSI>(query, new { id });
                });
            }
            catch (Exception ex)
            {
                log.Error(string.Format("LOI METHOD UATWebsiteDBService.GetById --- {0}", ex.Message));
                throw;
            }
        }

        public async Task<int> GetTotalNumberOfRecords(int categoryId, int languageId)
        {
            try
            {
                return await WithUATWebisteDbConnection(async conn => {
                    var query = $@"
                    SELECT 
                        count(*)
                    FROM TVSI_THONG_TIN_TVSI
                    WHERE [chuyen_muc_tin_tvsiid] = @categoryId
                        AND ngon_nguid = @languageId";
                    return await conn.QueryFirstOrDefaultAsync<int>(query, new { categoryId, languageId});
                });
            }
            catch (Exception ex)
            {
                log.Error(string.Format("LOI METHOD UATWebsiteDBService.GetTotalNumberOfRecords --- {0}", ex.Message));
                throw;
            }
        }
    }
}
