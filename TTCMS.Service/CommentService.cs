﻿using System.Collections.Generic;
using System.Linq;
using TTCMS.Data.Repositories;
using TTCMS.Data.Infrastructure;
using PagedList;
using TTCMS.Data.Entities;

namespace TTCMS.Service
{
    public interface ICommentService
    {
        IEnumerable<Comment> GetList();
        IEnumerable<Comment> GetListTop(int limit);
        IEnumerable<Comment> GetParentCommentByQuestion(int questionId);
        IEnumerable<Comment> GetAllParentCommentByQuestion(int questionId);
        Comment GetById(int id);
        Comment Create(Comment model);
        void Update(Comment model);
        void Delete(int Id);
        void SaveChange();
    }
    public class CommentService : ICommentService
    {
        private readonly ICommentRepository _commentRepository;
        private readonly IUnitOfWork unitOfWork;

        public CommentService(ICommentRepository CommentRepository, IUnitOfWork unitOfWork)
        {
            _commentRepository = CommentRepository;
            this.unitOfWork = unitOfWork;
        }
        public Comment Create(Comment model)
        {
            _commentRepository.Add(model);
            return model;
        }

        public void Delete(int Id)
        {
            var Comment = _commentRepository.GetById(Id);
            _commentRepository.Delete(Comment);
        }
        public void Update(Comment model)
        {
            _commentRepository.Update(model);
            SaveChange();
        }

        public Comment GetById(int id)
        {
            var result = _commentRepository.GetById(id);
            return result;
        }

        public IEnumerable<Comment> GetList()
        {
            var result = _commentRepository.GetAll();
            return result;
        }

        public void SaveChange()
        {
            unitOfWork.Commit();
        }

        public IEnumerable<Comment> GetListTop(int limit)
        {
            var result = _commentRepository.GetAll().Take(limit);
            return result;
        }

        public IEnumerable<Comment> GetParentCommentByQuestion(int questionId)
        {
            return _commentRepository.GetMany(x => 
                x.Question.Id == questionId 
                && x.Question.DeletedAt == null 
                && x.ParentComment == null
                && x.Approved);
        }

        public IPagedList<Comment> GetPagedList(Page page, string search)
        {
            if (search != "")
            {
                return _commentRepository.GetPageDESC(page, x => x.Content.Contains(search), order => order.CreatedDate);
            }
            return _commentRepository.GetPageDESC(page, x => true, order => order.CreatedDate);
        }

        public IEnumerable<Comment> GetAllParentCommentByQuestion(int questionId)
        {
            return _commentRepository.GetMany(x =>
                x.Question.Id == questionId
                && x.Question.DeletedAt == null
                && x.ParentComment == null);
        }
    }
}
