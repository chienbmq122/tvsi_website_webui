﻿using System.Collections.Generic;
using System.Linq;
using TTCMS.Data.Entities;
using TTCMS.Data.Repositories;
using TTCMS.Data.Infrastructure;

namespace TTCMS.Service
{

    public interface ILanguage_TopicService
    {
        IEnumerable<Language_Topic> GetList();
        Language_Topic GetByTopic(int topicId, string languageCode);
        IEnumerable<Language_Topic> GetByTopic(int topicId);
        //IEnumerable<Language_Topic> GetByRoute(string route);
        Language_Topic GetById(int id);
        Language_Topic GetById(string languageId, int id);
        bool Check(string languageId, int Id);
        Language_Topic Create(Language_Topic model);
        void Update(Language_Topic model);
        void Delete(int id);
        void DeleteByTopicId(int id);

        void SaveChange();
    }

    public class Language_TopicService : ILanguage_TopicService
    {
        private readonly ILanguage_TopicRepository _language_TopicRepository;
        private readonly IUnitOfWork unitOfWork;

        public Language_TopicService(ILanguage_TopicRepository language_TopicRepository, IUnitOfWork unitOfWork)
        {
            this._language_TopicRepository = language_TopicRepository;
            this.unitOfWork = unitOfWork;
        }

        #region Language_TopicService Members
        public IEnumerable<Language_Topic> GetList()
        {
            var model = _language_TopicRepository.GetAll();
            return model;
        }
        public Language_Topic GetById(int id)
        {
            return _language_TopicRepository.Get(x => x.Id == id);
        }

        public Language_Topic GetById(string languageId, int id)
        {
            var model = _language_TopicRepository.Get(x => x.Language.Code == languageId && x.Topic.Id == id);
            return model;
        }
        public bool Check(string languageId, int Id)
        {
            var language = GetList().SingleOrDefault(x => x.Language.Code == languageId && x.Topic.Id == Id);
            if (language != null)
            {
                return true;
            }
            return false;

        }
        public Language_Topic Create(Language_Topic model)
        {
            var result = _language_TopicRepository.Add(model);
            return result;
        }
        public void Update(Language_Topic model)
        {
            _language_TopicRepository.Update(model);
        }
        public void Delete(int id)
        {
            var model = _language_TopicRepository.GetById(id);
            _language_TopicRepository.Delete(model);
        }
        public void DeleteByTopicId(int id)
        {
            var model = _language_TopicRepository.GetMany(x => x.Topic.Id == id);
            foreach (var lang in model)
            {
                _language_TopicRepository.Delete(lang);
            }
        }

        public void SaveChange()
        {
            unitOfWork.Commit();
        }

        public IEnumerable<Language_Topic> GetByTopic(int topicId)
        {
            return _language_TopicRepository.GetMany(item => item.Topic.Id == topicId && item.Topic.DeletedAt == null);
        }

        //public IEnumerable<Language_Topic> GetByRoute(string route)
        //{
        //    return _language_TopicRepository.GetAll().Where(l => l.Slug == route 
        //    && l.SinglePage.IsActive && l.Language.DeletedAt == null);
        //}

        public Language_Topic GetByTopic(int topicId, string languageCode)
        {
            if (languageCode != "")
            {
                return _language_TopicRepository.Get(x =>
                    x.Topic.Id == topicId
                    && x.Language.Code == languageCode
                );
            }
            return _language_TopicRepository.Get(x => x.Topic.Id == topicId);
        }


        #endregion
    }
}
