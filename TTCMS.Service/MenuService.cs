﻿using System.Collections.Generic;
using System.Linq;
using System;
using TTCMS.Data.Entities;
using PagedList;
using TTCMS.Data.Repositories;
using TTCMS.Data.Infrastructure;

namespace TTCMS.Service
{

    public interface IMenuService
    {
        IEnumerable<Menu> GetList(MenuGroupType group, string show);
        IEnumerable<Menu> GetActivatedMenu(MenuGroupType group, string languageCode);
        IPagedList<Menu> GetPageList(Page page, string sreach);
        List<Menu> GetParents(int ignoredId = 0, int level = 0);
        int GetMaxOrder(MenuGroupType group, int parentId = 0);
        Menu GetById(int id);
        Menu GetByCategoryId(MenuGroupType group, int categoryId, string languageCode);
        Menu GetByOrder(int order, int ignoredId = 0);
        int GetSort(MenuGroupType group, string show);
        Menu Create(Menu model);
        void Update(Menu model);
        void Delete(int id);
        void SaveChange();
    }

    public class MenuService : IMenuService
    {
        private readonly IMenuRepository menuRepository;
        private readonly IUnitOfWork unitOfWork;

        public MenuService(IMenuRepository menuRepository, IUnitOfWork unitOfWork)
        {
            this.menuRepository = menuRepository;
            this.unitOfWork = unitOfWork;
        }

        #region MenuService
        public IEnumerable<Menu> GetList(MenuGroupType group, string show)
        {
            return menuRepository.GetMany(x => x.GroupType == group && x.Language.Code == show);
        }

        public IEnumerable<Menu> GetActivatedMenu(MenuGroupType group, string languageCode)
        {
            return menuRepository.GetMany(x => x.GroupType == group && x.Language.Code == languageCode && x.IsActived);
        }

        public IPagedList<Menu> GetPageList(Page page, string search)
        {
            if (search != "")
            {
                return menuRepository.GetPageASC(page, x => x.Name.Contains(search), order => order.Order);
            }
            return menuRepository.GetPageASC(page, x => true, order => order.Order);
        }
        public Menu GetById(int id)
        {
            var model = menuRepository.GetById(id);
            return model;
        }
        public int GetSort(MenuGroupType group, string show)
        {
            int Sort = 0;
            var model = GetList(group, show).ToList().Count;
            if (model > 0)
            {
                int key = GetList(group, show).Max(m => m.Order);
                Sort = key + 1;
            }
            else
            {
                Sort = 1;
            }
            return Sort;
        }
        public Menu Create(Menu model)
        {
            var result = menuRepository.Add(model);
            SaveChange();
            return result;
        }
        public void Update(Menu model)
        {
            menuRepository.Update(model);
            SaveChange();
        }
        public void Delete(int id)
        {
            DeleteByParentId(id);
            var model = menuRepository.GetById(id);
            menuRepository.Delete(model);
            SaveChange();
        }

        private void DeleteByParentId(int parentId)
        {
            var list = menuRepository.GetMany(x => x.ParentID == parentId);

            if (list == null || list.Count() == 0) return;

            foreach (var item in list)
            {
                DeleteByParentId(item.Id);
                menuRepository.Delete(item);
            }
        }

        public void SaveChange()
        {
            unitOfWork.Commit();
        }

        public Menu GetByOrder(int order, int ignoredId = 0)
        {
            return menuRepository.Get(x => x.Order == order && (ignoredId == 0 || (ignoredId != 0 && x.Id != ignoredId)));
        }

        public List<Menu> GetParents(int ignoredId = 0, int level = 0)
        {
            var roots = menuRepository.GetMany(x => x.ParentID == 0 && x.Id != ignoredId);

            if (roots == null || !roots.Any()) return null;

            var result = roots.ToList();
            var parents = result;

            for (int i = 0; i < level; i++)
            {
                var menus = GetByParents(parents);
                result.AddRange(menus);
                parents = menus;
            }

            return result;
        }
        private List<Menu> GetByParents(List<Menu> parents)
        {
            var result = new List<Menu>();

            foreach (var item in parents)
            {
                var children = menuRepository.GetMany(x => x.ParentID == item.Id);
                if (children != null && children.Any())
                {
                    result.AddRange(children);
                }
            }

            return result;
        }


        public int GetMaxOrder(MenuGroupType groupType, int parentId = 0)
        {
            var menus = menuRepository.GetMany(
                x => x.GroupType == groupType && x.ParentID == parentId
            );

            if (menus == null || menus.Count() == 0) return 0;

            return menus.OrderByDescending(x => x.Order).FirstOrDefault().Order;
        }

        public Menu GetByCategoryId(MenuGroupType groupType, int categoryId, string languageCode)
        {
            return menuRepository.Get(
                x => x.GroupType == groupType &&
                x.Language.Code == languageCode &&
                x.CategoryItem != null &&
                x.CategoryItem.Category != null &&
                x.CategoryItem.Category.Id == categoryId);
        }

        #endregion
    }
}
