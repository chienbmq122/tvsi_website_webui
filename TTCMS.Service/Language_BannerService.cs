﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TTCMS.Data.Entities;
using TTCMS.Data.Infrastructure;
using TTCMS.Data.Repositories;


namespace TTCMS.Service
{
    public interface ILanguage_BannerService
    {
        IEnumerable<Language_Banner> GetByBannerId(int bannerId);
        int Create(Language_Banner model);
        void Update(Language_Banner model);
        void Delete(string Id);
        void SaveChange();
    }
    class Language_BannerService : ILanguage_BannerService
    {
        private readonly ILanguage_BannerRepository Language_BannerRepository;
        private readonly IUnitOfWork unitOfWork;
        public Language_BannerService(ILanguage_BannerRepository Language_BannerRepository, IUnitOfWork unitOfWork)
        {
            this.Language_BannerRepository = Language_BannerRepository;
            this.unitOfWork = unitOfWork;
        }
        public int Create(Language_Banner model)
        {
            Language_BannerRepository.Add(model);
            SaveChange();
            return model.Id;
        }

        public void Delete(string Id)
        {
            var Banner = Language_BannerRepository.GetById(Id);
            Language_BannerRepository.Delete(Banner);
            SaveChange();
        }
        public void Update(Language_Banner model)
        {
            Language_BannerRepository.Update(model);
            SaveChange();
        }

        public Language_Banner GetById(int id)
        {
            var result = Language_BannerRepository.GetById(id);
            return result;
        }

        public IEnumerable<Language_Banner> GetByBannerId(int bannerId)
        {
            var result = Language_BannerRepository.GetAll().Where(b=>b.BannerId == bannerId);
            return result;
        }

        public void SaveChange()
        {
            unitOfWork.Commit();
        }
    }
}
