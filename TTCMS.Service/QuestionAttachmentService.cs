﻿using System.Collections.Generic;
using System.Linq;
using TTCMS.Data.Repositories;
using TTCMS.Data.Infrastructure;
using PagedList;
using TTCMS.Data.Entities;

namespace TTCMS.Service
{
    public interface IQuestionAttachmentService
    {
        IEnumerable<QuestionAttachment> GetList();
        IEnumerable<QuestionAttachment> GetListTop(int limit);
        IEnumerable<QuestionAttachment> GetListForActive();
        IEnumerable<QuestionAttachment> GetByQuestion(int questionId);
        QuestionAttachment GetById(int id);
        QuestionAttachment Create(QuestionAttachment model);
        void Update(QuestionAttachment model);
        void Delete(int Id);
        void SaveChange();
    }
    public class QuestionAttachmentService : IQuestionAttachmentService
    {
        private readonly IQuestionAttachmentRepository _questionAttachmentRepository;
        private readonly IUnitOfWork unitOfWork;

        public QuestionAttachmentService(IQuestionAttachmentRepository questionAttachmentRepository, IUnitOfWork unitOfWork)
        {
            _questionAttachmentRepository = questionAttachmentRepository;
            this.unitOfWork = unitOfWork;
        }
        public QuestionAttachment Create(QuestionAttachment model)
        {
            return _questionAttachmentRepository.Add(model);
        }

        public void Delete(int Id)
        {
            var questionAttachment = _questionAttachmentRepository.GetById(Id);
            _questionAttachmentRepository.Delete(questionAttachment);
            SaveChange();
        }
        public void Update(QuestionAttachment model)
        {
            _questionAttachmentRepository.Update(model);
        }

        public QuestionAttachment GetById(int id)
        {
            var result = _questionAttachmentRepository.GetById(id);
            return result;
        }

        public IEnumerable<QuestionAttachment> GetList()
        {
            return _questionAttachmentRepository.GetMany(x => x.DeletedAt == null);
        }

        public IEnumerable<QuestionAttachment> GetListForActive()
        {
            return _questionAttachmentRepository.GetMany(g => g.DeletedAt == null).OrderBy(x => x.CreatedDate);
        }

        public void SaveChange()
        {
            unitOfWork.Commit();
        }

        public IEnumerable<QuestionAttachment> GetListTop(int limit)
        {
            var result = _questionAttachmentRepository.GetMany(x => x.DeletedAt == null).OrderBy(x => x.CreatedDate).Take(limit);
            return result;
        }

        public IEnumerable<QuestionAttachment> GetByQuestion(int questionId)
        {
            return _questionAttachmentRepository.GetMany(x => (x.Question.Id == questionId) && x.DeletedAt == null).OrderBy(x => x.Priority);
        }
    }
}
