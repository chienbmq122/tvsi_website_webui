﻿using System.Collections.Generic;
using System.Linq;
using TTCMS.Data.Entities;
using TTCMS.Data.Repositories;
using TTCMS.Data.Infrastructure;
using PagedList;

namespace TTCMS.Service
{

    public interface ICategoryService
    {
        IEnumerable<Category> GetActiveCategories(CategoryType filter, string lang);
        IEnumerable<Category> GetActiveCategories(string lang = "");
        IEnumerable<Category> GetCategoriesInNews(string lang);
        Category GetById(int id);
        Category Create(Category model);
        void Update(Category model);
        void SaveChange();
        IEnumerable<Category> GetAll();
        IEnumerable<Category> GetParents(int currentId = 0, string languageCode = "");
        IEnumerable<Category> GetByParentIds(IEnumerable<int> parentIds, int ignoredId = 0, string languageCode = "");
        IEnumerable<Category> GetByParentId(int parentId);
        IPagedList<Category> GetPageList(Page page, string search, string show);
    }

    public class CategoryService : ICategoryService
    {
        private readonly ICategoryRepository categoryRepository;
        private readonly IUnitOfWork unitOfWork;

        public CategoryService(ICategoryRepository categoryRepository, IUnitOfWork unitOfWork)
        {
            this.categoryRepository = categoryRepository;
            this.unitOfWork = unitOfWork;
        }

        #region CategoryService

        public IEnumerable<Category> GetActiveCategories(CategoryType filter, string lang)
        {
            return categoryRepository.GetMany(
                item => item.DeletedAt == null
                && item.CategoryItems.Any(c => c.Language.Code == lang)
                && item.IsActive
            );
        }

        public IEnumerable<Category> GetActiveCategories(string lang = "")
        {
            return categoryRepository.GetMany(
                item => item.DeletedAt == null
                && (lang == "" || (lang != "" && item.CategoryItems.Any(c => c.Language.Code == lang)))
                && item.IsActive
            );
        }

        public IEnumerable<Category> GetParents(int ignoredId = 0, string languageCode = "")
        {
            return categoryRepository.GetMany(
                item => item.DeletedAt == null
                && item.ParentID == null
                && item.Id != ignoredId
                && (languageCode == "" || (item.CategoryItems.Any(x => x.Language.Code == languageCode)))
            );
        }

        public IEnumerable<Category> GetAll()
        {
            return categoryRepository.GetMany(item => item.DeletedAt == null);
        }

        public IEnumerable<Category> GetByParentId(int parentId)
        {
            return categoryRepository.GetMany(item => item.DeletedAt == null && item.ParentID == parentId);
        }

        public Category GetById(int id)
        {
            return categoryRepository.Get(item => item.DeletedAt == null && item.Id == id);
        }

        public Category Create(Category model)
        {
            var result = categoryRepository.Add(model);
            return result;
        }

        public void Update(Category model)
        {
            categoryRepository.Update(model);
        }

        public void SaveChange()
        {
            unitOfWork.Commit();
        }

        public IEnumerable<Category> GetByParentIds(IEnumerable<int> parentIds, int ignoredId = 0, string languageCode = "")
        {
            return categoryRepository.GetMany(
                x => x.Id != ignoredId
                && (languageCode == "" || (x.CategoryItems.Any(c => c.Language.Code == languageCode)))
                && parentIds.Contains(x.ParentID ?? 0));
        }

        public IPagedList<Category> GetPageList(Page page, string search, string lang)
        {
            if (lang != "")
            {
                if (search != "")
                {
                    return categoryRepository.GetPageDESC(
                        page,
                        x => x.DeletedAt == null
                        && x.CategoryItems.Any(
                            item => item.Language.Code == lang
                            || item.Name.Contains(search)
                        ),
                        order => order.CreatedDate);
                }

                return categoryRepository.GetPageDESC(
                    page,
                    x => x.DeletedAt == null
                    && x.CategoryItems.Any(
                        item => item.Language.Code == lang
                    ),
                    order => order.CreatedDate);
            }

            if (search != "")
            {
                return categoryRepository.GetPageDESC(
                    page,
                    x => x.DeletedAt == null
                    && x.CategoryItems.Any(
                        item => item.Name.Contains(search)
                    ),
                    order => order.CreatedDate);
            }

            return categoryRepository.GetPageDESC(page, x => x.DeletedAt == null, order => order.CreatedDate);
        }

        public IEnumerable<Category> GetCategoriesInNews(string lang)
        {
            return categoryRepository.GetMany(
               item => item.DeletedAt == null
               && item.IsActive
               && item.CategoryItems.Any(
                   x => x.Language.Code == lang
                   && x.IsInNewsList &&
                   x.DeletedAt == null
            ));
        }

        #endregion
    }
}
