﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Dapper;
using log4net;
using TTCMS.Data.Entities;
using TTCMS.Data.Infrastructure;
using TTCMS.Data.Repositories;

namespace TTCMS.Service
{
    public interface IOpenAccountService
    {
        Task<IEnumerable<Other>> GetPlaceOfIssueList();
        Task<IEnumerable<ProvinceModel>> GetProvinceList();
        Task<IEnumerable<Distric>> GetDistrictList(string provinceCode);
        Task<IEnumerable<Ward>> GetWardList(string district);
        Task<string> GetSaleName(string saleID);

    }
    public class OpenAccountService : BaseService,IOpenAccountService
    {
        private readonly ILog log = LogManager.GetLogger(typeof(OpenAccountService));
        private readonly IUnitOfWork _unitOfWork;
        public OpenAccountService( IUnitOfWork unitOfWork )
        {
            this._unitOfWork = unitOfWork;

        }

        public async Task<IEnumerable<Other>> GetPlaceOfIssueList()
        {
            try
            {
                var sql = "SELECT A.Category AS Category , A.CategoryName as CategoryName FROM ExtendCust A WHERE A.CategoryType = 13";
                var result = 
                    await WithCrmDbConnection(async conn => await conn.QueryAsync<Other>(sql));
                return result;
            }
            catch (Exception e)
            {
                log.Error(string.Format("GetPlaceOfIssueList - {0}",e.Message));
                log.Error(string.Format("GetPlaceOfIssueList - {0}",e.InnerException));
                throw;
            }
        }

        public async Task<IEnumerable<ProvinceModel>> GetProvinceList()
        {
            try
            {
                var sql = "SELECT * FROM Province WHERE status = 1";
                var result = await WithCrmDbConnection(async conn => await conn.QueryAsync<ProvinceModel>(sql));
                return result;
            }
            catch (Exception e)
            {
                log.Error(string.Format("GetProvinceList - {0}",e.Message));
                log.Error(string.Format("GetProvinceList - {0}",e.InnerException));
                throw;
            }
            
        }

        public async Task<IEnumerable<Distric>> GetDistrictList(string provinceCode)
        {
            try
            {
                var sql = "SELECT * FROM District  WHERE status = 1 And ProvinceCode = @ProvinceCode";
                var result = await WithCrmDbConnection(async conn => 
                    await conn.QueryAsync<Distric>(sql, new
                    {
                        ProvinceCode = provinceCode
                    }));
                return result;
            }
            catch (Exception e)
            {
                log.Error(string.Format("GetDistrictList - {0}",e.Message));
                log.Error(string.Format("GetDistrictList - {0}",e.InnerException));
                throw;
            }
        }

        public async Task<IEnumerable<Ward>> GetWardList(string district)
        {
            try
            {
                var sql = "SELECT * FROM Ward  WHERE status = 1 AND DistrictCode = @DistrictCode";
                var result = await WithCrmDbConnection(async conn => await conn.QueryAsync<Ward>(sql,new
                {
                    @DistrictCode = district
                }));
                return result;
            }
            catch (Exception e)
            {
                log.Error(string.Format("GetWardList - {0}",e.Message));
                log.Error(string.Format("GetWardList - {0}",e.InnerException));
                throw;
            }
        }

        public async Task<string> GetSaleName(string saleID)
        {
            try
            {
                var sql = "select ho_va_ten from TVSI_NHAN_SU where ma_quan_ly = @saleID and trang_thai = 1";
                return await WithEmsDbConnection(async conn =>
                {
                    return await conn.QueryFirstOrDefaultAsync<string>(sql,new
                    {
                        saleID = saleID.Substring(0,4)
                    });
                });
            }
            catch (Exception e)
            {
                log.Error(string.Format("GetSaleName - {0}",e.Message));
                log.Error(string.Format("GetSaleName - {0}",e.InnerException));
                throw;
            }
        }
    }
}