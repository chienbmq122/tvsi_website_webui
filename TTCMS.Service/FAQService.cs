﻿using System.Collections.Generic;
using System.Linq;
using System;
using TTCMS.Data.Entities;
using TTCMS.Data.Repositories;
using TTCMS.Data.Infrastructure;
using PagedList;

namespace TTCMS.Service
{
    public interface IFAQService
    {
        IEnumerable<FAQ> GetList();
        IEnumerable<FAQ> GetListTop(int limit);
        IEnumerable<FAQ> GetListForActive();
        FAQ GetById(int id);
        int Create(FAQ model);
        void Update(FAQ model);
        void Delete(string Id);
        void SaveChange();
    }
    public class FAQService : IFAQService
    {
        private readonly IFAQRepository FAQRepository;
        private readonly IUnitOfWork unitOfWork;

        public FAQService(IFAQRepository FAQRepository, IUnitOfWork unitOfWork)
        {
            this.FAQRepository = FAQRepository;
            this.unitOfWork = unitOfWork;
        }
        public int Create(FAQ model)
        {
            FAQRepository.Add(model);
            SaveChange();
            return model.Id;
        }

        public void Delete(string Id)
        {
            var faq = FAQRepository.GetById(Id);
            FAQRepository.Delete(faq);
            SaveChange();
        }
        public void Update(FAQ model)
        {
            FAQRepository.Update(model);
            SaveChange();
        }

        public FAQ GetById(int id)
        {
            var result = FAQRepository.GetById(id);
            return result;
        }

        public IEnumerable<FAQ> GetList()
        {
            var result = FAQRepository.GetAll().Where(i=>i.DeletedAt == null);
            return result;
        }

        public IEnumerable<FAQ> GetListForActive()
        {
            return FAQRepository.GetMany(g => g.IsActived == true && g.DeletedAt == null).OrderBy(x => x.CreatedDate);
        }

        public void SaveChange()
        {
            unitOfWork.Commit();
        }

        public IEnumerable<FAQ> GetListTop(int limit)
        {
            var result = FAQRepository.GetAll().Where(i => i.DeletedAt == null).Take(limit);
            return result;
        }
    }
}
