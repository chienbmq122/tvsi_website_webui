﻿using System.Collections.Generic;
using System.Linq;
using System;
using PagedList;
using TTCMS.Data.Repositories;
using TTCMS.Data.Infrastructure;
using TTCMS.Data.Entities;

namespace TTCMS.Service
{

    public interface INewsItemService
    {
        NewsItem GetById(int id);
        NewsItem GetBySlug(string slug);
        IEnumerable<NewsItem> GetByNewsId(int newsId);
        IEnumerable<NewsItem> GetByTitle(string title);
        IEnumerable<NewsItem> GetAllNewsTitle();
        NewsItem Create(NewsItem model);
        void Update(NewsItem model);
        void Delete(int id);
        void SaveChange();
    }

    public class NewsItemService : INewsItemService
    {
        private readonly INewsItemRepository newsItemRepository;
        private readonly IUnitOfWork unitOfWork;


        public NewsItemService(INewsItemRepository newsItemRepository, IUnitOfWork unitOfWork)
        {
            this.newsItemRepository = newsItemRepository;
            this.unitOfWork = unitOfWork;
        }

        #region NewsService


        public NewsItem GetById(int id)
        {
            return newsItemRepository.Get(item => item.DeletedAt == null && item.Id == id);
        }
        public NewsItem GetBySlug(string slug)
        {
            return newsItemRepository.Get(item => item.DeletedAt == null && item.Slug == slug);
        }
        public NewsItem Create(NewsItem model)
        {
            var result = newsItemRepository.Add(model);
            return result;
        }
        public void Update(NewsItem model)
        {
            newsItemRepository.Update(model);
        }
        public void Delete(int id)
        {
            var model = newsItemRepository.GetById(id);
            newsItemRepository.Delete(model);
        }

        public void SaveChange()
        {
            unitOfWork.Commit();
        }

        public IEnumerable<NewsItem> GetByNewsId(int newsId)
        {
            return newsItemRepository.GetMany(item => item.DeletedAt == null && item.News.Id == newsId);
        }

        public IEnumerable<NewsItem> GetByTitle(string title)
        {
            return newsItemRepository.GetAll().Where(n => n.Title.ToLower().Contains(title.ToLower()) && n.News.IsActive && n.Published <= DateTime.Now && n.DeletedAt == null);
        }

        public IEnumerable<NewsItem> GetAllNewsTitle()
        {
            return newsItemRepository.GetAll().Where(n=> n.Slug != null && n.News.IsActive && n.Published <= DateTime.Now && n.DeletedAt == null);
        }

        #endregion
    }
}
