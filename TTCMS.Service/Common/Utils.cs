namespace TTCMS.Service.Common
{
    public class Utils
    {
        public static string name_lang_tvsi_wb = "lang_tvsi_wb";
        public static string name_lang_tvsi_fp = "fp_tvsi_lang";
        public static string FormatDate = "dd/MM/yyyy";
        public static string EmailSubject = "TVSI:";
        public static string Path = "/user/confirm-ekyc-open-account";
        public static string FolderKYC = "~/EkycCustomer/";
        public static string Front = "_Front";
        public static string Back = "_Back";
        public static string Face = "_Face";
    }
}