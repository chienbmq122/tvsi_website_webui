﻿using System.Linq;
using System.Collections.Generic;
using TTCMS.Data.Repositories;
using TTCMS.Data.Infrastructure;
using TTCMS.Data.Entities;
using System;

namespace TTCMS.Service
{
    public interface ICategoryItemService
    {
        CategoryItem GetById(int id);
        CategoryItem GetByCategoryId(int categoryId, string languageCode);
        CategoryItem Create(CategoryItem model);
        CategoryItem GetBySlug(string slug);
        void Update(CategoryItem model);
        void SaveChange();
        IEnumerable<CategoryItem> GetAll();
        IEnumerable<CategoryItem> GetByTitle(string title, int languageId);
    }

    public class CategoryItemService : ICategoryItemService
    {
        private readonly ICategoryItemRepository categoryItemRepository;
        private readonly IUnitOfWork unitOfWork;

        public CategoryItemService(ICategoryItemRepository categoryItemRepository, IUnitOfWork unitOfWork)
        {
            this.categoryItemRepository = categoryItemRepository;
            this.unitOfWork = unitOfWork;
        }

        #region CategoryService

        public IEnumerable<CategoryItem> GetAll()
        {
            return categoryItemRepository.GetMany(item => item.DeletedAt == null);
        }

        public CategoryItem GetById(int id)
        {
            return categoryItemRepository.GetById(id);
        }

        public CategoryItem Create(CategoryItem model)
        {
            return categoryItemRepository.Add(model);
        }

        public void Update(CategoryItem model)
        {
            categoryItemRepository.Update(model);
        }

        public void SaveChange()
        {
            unitOfWork.Commit();
        }

        public CategoryItem GetByCategoryId(int categoryId, string languageCode)
        {
            return categoryItemRepository.Get(
                x => x.DeletedAt == null && x.Category.Id == categoryId && x.Language.Code == languageCode
            );
        }
        public CategoryItem GetBySlug(string slug)
        {
            return categoryItemRepository.GetAll().Where(c => c.Slug == slug && c.Category.IsActive == true
           && c.Category.DeletedAt == null).FirstOrDefault();
        }
        public IEnumerable<CategoryItem> GetByTitle(string title, int languageId)
        {
            return categoryItemRepository
                .GetAll()
                .Where(c => c.Name.ToLower().Contains(title.ToLower()) 
                    && c.Category.IsActive == true
                    && c.Category.DeletedAt == null
                    && c.Language.Id == languageId
                );
        }
        #endregion
    }
}
