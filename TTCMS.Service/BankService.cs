﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using TTCMS.Data.Entities;
using TTCMS.Service.Common;

namespace TTCMS.Service
{

    public interface IBankService
    {
        Task<IEnumerable<Bank>> GetBankList();
        Task<IEnumerable<SubBranchBank>> GetSubBranchList(string bankNo);
        Task<IEnumerable<Province>> GetProvinceList();
        


    }
    public class BankService : IBankService
    {
        private static readonly HttpClient _Client = new HttpClient();
        private static JavaScriptSerializer _Serializer = new JavaScriptSerializer();
        
        /// <summary>
        /// Call API lấy ra danh sách ngân hàng
        /// </summary>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public async Task<IEnumerable<Bank>> GetBankList()
        {
            try
            {
                var url = ConfigSettings.ReadSetting("URL_API_BOND");
                var urlAPI = url + "api/Ekyc/EK_BA_GetBankList?src=M&lang=VN";
                var json = _Serializer.Serialize(null);
                var response = await APIConfig.Request(HttpMethod.Post, urlAPI, json, new Dictionary<string, string>());
                var responseText= await response.Content.ReadAsStringAsync();
                var serializedResult = _Serializer.Deserialize<SuccessListModel<Bank>>(responseText);
                if (serializedResult.RetCode.Equals("000"))
                {
                    return serializedResult.RetData;
                }

                return new List<Bank>();

            }
            catch (Exception e)
            {
                throw new Exception(string.Format("GetBankList - {0}, {1}",e.Message,e.InnerException));

            }
        }
        /// <summary>
        /// call api lấy ra nhánh của ngân hàng
        /// </summary>
        /// <param name="bankNo">mã ngân hàng</param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public async Task<IEnumerable<SubBranchBank>> GetSubBranchList(string bankNo)
        {
            try
            {
                var url = ConfigSettings.ReadSetting("URL_API_BOND");
                var urlAPI = url + "api/Ekyc/EK_BA_GetBankBranchList?src=M&lang=VN";
                var json = _Serializer.Serialize(new
                {
                   BankNo = bankNo

                });
                var response = await APIConfig.Request(HttpMethod.Post, urlAPI, json, new Dictionary<string, string>());
                var responseText= await response.Content.ReadAsStringAsync();
                var serializedResult = _Serializer.Deserialize<SuccessListModel<SubBranchBank>>(responseText);
                if (serializedResult.RetCode.Equals("000"))
                {
                    return serializedResult.RetData;
                }
                return new List<SubBranchBank>();
            }
            catch (Exception e)
            {
                throw new Exception(string.Format("GetSubBranchList - {0}, {1}",e.Message,e.InnerException));

            }
        }
        
        /// <summary>
        /// Call API lấy ra province
        /// </summary>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public async Task<IEnumerable<Province>> GetProvinceList()
        {
            try
            {
                var url = ConfigSettings.ReadSetting("URL_API_BOND");
                var urlAPI = url + "api/Ekyc/EK_BP_GetProvinceList?src=M&lang=VN";
                var json = _Serializer.Serialize(null);
                var response = await APIConfig.Request(HttpMethod.Post, urlAPI, json, new Dictionary<string, string>());
                var responseText= await response.Content.ReadAsStringAsync();
                var serializedResult = _Serializer.Deserialize<SuccessListModel<Province>>(responseText);
                if (serializedResult.RetCode.Equals("000"))
                {
                    return serializedResult.RetData;
                }
                return new List<Province>();
            }
            catch (Exception e)
            {
                throw new Exception(string.Format("GetProvinceList - {0}, {1}",e.Message,e.InnerException));

            }
        }
    }
}