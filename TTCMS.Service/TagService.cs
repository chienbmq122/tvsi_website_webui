﻿using System.Collections.Generic;
using System.Linq;
using TTCMS.Data.Repositories;
using TTCMS.Data.Infrastructure;
using PagedList;
using TTCMS.Data.Entities;

namespace TTCMS.Service
{
    public interface ITagService
    {
        IEnumerable<Tag> GetAll();
        IEnumerable<Tag> GetListOfPopular(string languageCode);
        IEnumerable<Tag> GetListOfQuickSearch(string languageCode);
        Tag GetById(int id);
        int Create(Tag model);
        void Update(Tag model);
        void Delete(int Id);
        void SaveChange();
    }
    public class TagService : ITagService
    {
        private readonly ITagRepository _tagRepository;
        private readonly IUnitOfWork unitOfWork;

        public TagService(ITagRepository TagRepository, IUnitOfWork unitOfWork)
        {
            _tagRepository = TagRepository;
            this.unitOfWork = unitOfWork;
        }
        public int Create(Tag model)
        {
            _tagRepository.Add(model);
            return model.Id;
        }

        public void Delete(int Id)
        {
            var Tag = _tagRepository.GetById(Id);
            _tagRepository.Delete(Tag);
            SaveChange();
        }
        public void Update(Tag model)
        {
            _tagRepository.Update(model);
        }

        public Tag GetById(int id)
        {
            var result = _tagRepository.GetById(id);
            return result;
        }

        public IEnumerable<Tag> GetListOfPopular(string languageCode = "")
        {
            var result = string.IsNullOrEmpty(languageCode) ? _tagRepository.GetMany(x => !x.QuickSearch) : _tagRepository.GetMany(x => !x.QuickSearch && x.Language.Code == languageCode);
            return result;
        }

        public IEnumerable<Tag> GetListOfQuickSearch(string languageCode = "")
        {
            var result = string.IsNullOrEmpty(languageCode) ? _tagRepository.GetMany(x => x.QuickSearch) : _tagRepository.GetMany(x => x.QuickSearch && x.Language.Code == languageCode);
            return result;
        }

        public void SaveChange()
        {
            unitOfWork.Commit();
        }

        public IEnumerable<Tag> GetAll()
        {
            return _tagRepository.GetAll();
        }
    }
}
