﻿using System.Collections.Generic;
using System.Linq;
using PagedList;
using TTCMS.Data.Repositories;
using TTCMS.Data.Infrastructure;
using TTCMS.Data.Entities;

namespace TTCMS.Service
{

    public interface INewsService
    {
        IEnumerable<News> GetAll();
        IPagedList<News> GetPageList(Page page, string search, string lang);
        IPagedList<News> GetPageListApproves(Page page, string search, string lang);
        News GetById(int id);
        IEnumerable<News> GetNewest(List<int> SkipIds, int limit);
        IEnumerable<News> GetHotest(List<int> SkipIds, int limit);
        IEnumerable<News> GetByCategory(int cateId);
        News Create(News model);
        void Update(News model);
        void Delete(int id);
        void SaveChange();
    }

    public class NewsService : INewsService
    {
        private readonly INewsRepository newsRepository;
        private readonly IUnitOfWork unitOfWork;

        public NewsService(INewsRepository newsRepository, IUnitOfWork unitOfWork)
        {
            this.newsRepository = newsRepository;
            this.unitOfWork = unitOfWork;
        }

        #region NewsService

        public IEnumerable<News> GetAll()
        {
            return newsRepository.GetAll();
        }

        public News GetById(int id)
        {
            var model = newsRepository.GetById(id);
            return model;
        }
        public News Create(News model)
        {
            var result = newsRepository.Add(model);
            SaveChange();
            return result;
        }
        public void Update(News model)
        {
            newsRepository.Update(model);
            SaveChange();
        }
        public void Delete(int id)
        {
            var model = newsRepository.GetById(id);
            newsRepository.Delete(model);
            SaveChange();
        }

        public void SaveChange()
        {
            unitOfWork.Commit();
        }

        public IPagedList<News> GetPageListApproves(Page page, string search, string lang)
        {
            return null;
        }

        public IPagedList<News> GetPageList(Page page, string search, string lang)
        {
            if (lang != "")
            {
                if (search != "")
                {
                    return newsRepository.GetPageDESC(
                        page,
                        x => x.DeletedAt == null
                        && x.Title.Contains(search) && x.Language.Code == lang,
                        order => order.CreatedDate);
                }

                return newsRepository.GetPageDESC(
                    page,
                    x => x.DeletedAt == null
                    && x.Language.Code == lang,
                    order => order.CreatedDate);
            }

            if (search != "")
            {
                return newsRepository.GetPageDESC(
                    page,
                    x => x.DeletedAt == null
                    && x.Title.Contains(search),
                    order => order.CreatedDate);
            }

            return newsRepository.GetPageDESC(page, x => x.DeletedAt == null, order => order.CreatedDate);
        }

        public IEnumerable<News> GetByCategory(int cateId)
        {
            return newsRepository.GetAll().Where(n => n.Category != null && n.Category.Id == cateId);
        }
        public IEnumerable<News> GetNewest(List<int> skipIds, int limit)
        {
            return newsRepository.GetAll().Where(n => !skipIds.Contains(n.Id) && n.DeletedAt == null && n.IsActive).OrderBy(o => o.CreatedDate).Take(limit);
        }
        public IEnumerable<News> GetHotest(List<int> skipIds, int limit)
        {
            return newsRepository.GetAll().Where(n => !skipIds.Contains(n.Id)).Where(o => o.IsHot == true && o.DeletedAt == null && o.IsActive).Take(limit);
        }

        #endregion
    }
}
