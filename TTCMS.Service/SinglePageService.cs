﻿using System.Collections.Generic;
using System.Linq;
using TTCMS.Data.Entities;
using PagedList;
using TTCMS.Data.Repositories;
using TTCMS.Data.Infrastructure;

namespace TTCMS.Service
{

    public interface ISinglePageService
    {
        IEnumerable<SinglePage> GetList();
        IEnumerable<SinglePage> GetActiveSiglePages(string languageCode);
        IPagedList<SinglePage> GetPageList(Page page, string sreach, string show = "");
        SinglePage GetById(int id);
        int GetSort();
        SinglePage Create(SinglePage model);
        void Update(SinglePage model);
        void Delete(int id);
        void SaveChange();
    }

    public class SinglePageService : ISinglePageService
    {
        private readonly ISinglePageRepository singlePageRepository;
        private readonly IUnitOfWork unitOfWork;

        public SinglePageService(ISinglePageRepository singlePageRepository, IUnitOfWork unitOfWork)
        {
            this.singlePageRepository = singlePageRepository;
            this.unitOfWork = unitOfWork;
        }

        #region SinglePageService
        public IEnumerable<SinglePage> GetList()
        {
            var model = singlePageRepository.GetMany(x => x.DeletedAt == null);
            return model;
        }
        public IPagedList<SinglePage> GetPageList(Page page, string search, string show = "")
        {
            if (show != "")
            {
                if (search != "")
                {
                    return singlePageRepository.GetPageDESC(
                        page,
                        x => x.DeletedAt == null
                        && x.Language_SinglePages.Any(
                            item => item.Language.Code == show
                            || item.Title.Contains(search)
                        ),
                        order => order.CreatedDate);
                }

                return singlePageRepository.GetPageDESC(
                    page,
                    x => x.DeletedAt == null
                    && x.Language_SinglePages.Any(
                        item => item.Language.Code == show
                    ),
                    order => order.CreatedDate);
            }

            if (search != "")
            {
                return singlePageRepository.GetPageDESC(
                    page,
                    x => x.DeletedAt == null
                    && x.Language_SinglePages.Any(
                        item => item.Title.Contains(search)
                    ),
                    order => order.CreatedDate);
            }

            return singlePageRepository.GetPageDESC(page, x => x.DeletedAt == null, order => order.CreatedDate);
        }
        public SinglePage GetById(int id)
        {
            var model = singlePageRepository.Get(x => x.DeletedAt == null && x.Id == id);
            return model;
        }

        public int GetSort()
        {
            int Sort = 0;
            var model = GetList().ToList().Count;
            if (model > 0)
            {
                int key = GetList().Max(m => m.Order);
                Sort = key + 1;
            }
            else
            {
                Sort = 1;
            }
            return Sort;
        }

        public SinglePage Create(SinglePage model)
        {
            var result = singlePageRepository.Add(model);
            SaveChange();
            return result;
        }

        public void Update(SinglePage model)
        {
            singlePageRepository.Update(model);
            SaveChange();
        }

        public void Delete(int id)
        {
            var model = singlePageRepository.GetById(id);
            singlePageRepository.Delete(model);
            SaveChange();
        }

        public void SaveChange()
        {
            unitOfWork.Commit();
        }

        public IEnumerable<SinglePage> GetActiveSiglePages(string languageCode)
        {
            return singlePageRepository.GetMany(
                x => x.DeletedAt == null
                && x.IsActive
                && x.Language_SinglePages.Any(z => z.Language.Code == languageCode));
        }

        #endregion
    }
}
