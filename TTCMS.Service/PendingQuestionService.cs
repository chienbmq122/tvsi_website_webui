﻿using System.Collections.Generic;
using System.Linq;
using TTCMS.Data.Repositories;
using TTCMS.Data.Infrastructure;
using PagedList;
using TTCMS.Data.Entities;

namespace TTCMS.Service
{
    public interface IPendingQuestionService
    {
        IEnumerable<PendingQuestion> GetList();
        IEnumerable<PendingQuestion> GetListTop(int limit);
        IEnumerable<PendingQuestion> GetListForActive();
        IPagedList<PendingQuestion> GetPagedList(Page page, string search);
        PendingQuestion GetById(int id);
        int Create(PendingQuestion model);
        void Update(PendingQuestion model);
        void Delete(int Id);
        void SaveChange();
    }
    public class PendingQuestionService : IPendingQuestionService
    {
        private readonly IPendingQuestionRepository _pendingQuestionRepository;
        private readonly IUnitOfWork unitOfWork;

        public PendingQuestionService(IPendingQuestionRepository PendingQuestionRepository, IUnitOfWork unitOfWork)
        {
            _pendingQuestionRepository = PendingQuestionRepository;
            this.unitOfWork = unitOfWork;
        }
        public int Create(PendingQuestion model)
        {
            _pendingQuestionRepository.Add(model);
            SaveChange();
            return model.Id;
        }

        public void Delete(int Id)
        {
            var Pendingquestion = _pendingQuestionRepository.GetById(Id);
            _pendingQuestionRepository.Delete(Pendingquestion);
            SaveChange();
        }
        public void Update(PendingQuestion model)
        {
            _pendingQuestionRepository.Update(model);
            SaveChange();
        }

        public PendingQuestion GetById(int id)
        {
            var result = _pendingQuestionRepository.GetById(id);
            return result;
        }

        public IEnumerable<PendingQuestion> GetList()
        {
            var result = _pendingQuestionRepository.GetAll();
            return result;
        }

        public IEnumerable<PendingQuestion> GetListForActive()
        {
            return _pendingQuestionRepository.GetAll().OrderBy(x => x.CreatedDate);
        }

        public void SaveChange()
        {
            unitOfWork.Commit();
        }

        public IEnumerable<PendingQuestion> GetListTop(int limit)
        {
            var result = _pendingQuestionRepository.GetAll().Take(limit);
            return result;
        }

        public IPagedList<PendingQuestion> GetPagedList(Page page, string search)
        {
            if (search != "")
            {
                return _pendingQuestionRepository.GetPageDESC(page,
                    x => x.Content.Contains(search) || x.PendingQuestionTitle.Contains(search) || x.PhoneNumber.Contains(search) || x.AccountNumber.Contains(search),
                    order => order.CreatedDate
                );
            }
            return _pendingQuestionRepository.GetPageDESC(page, x => true, order => order.CreatedDate);
        }
    }
}
