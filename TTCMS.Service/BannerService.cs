﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TTCMS.Data.Entities;
using TTCMS.Data.Infrastructure;
using TTCMS.Data.Repositories;

namespace TTCMS.Service
{
    public interface IBannerService
    {
        IEnumerable<Banner> GetList();
        IEnumerable<Banner> GetByType(int bannerType);
        IEnumerable<Banner> GetListForActive();
        Banner GetById(int id);
        int Create(Banner model);
        void Update(Banner model);
        void Delete(string Id);
        void SaveChange();
    }
    public class BannerService : IBannerService
    {
        private readonly IBannerRepository BannerRepository;
        private readonly IUnitOfWork unitOfWork;
        public BannerService(IBannerRepository BannerRepository, IUnitOfWork unitOfWork)
        {
            this.BannerRepository = BannerRepository;
            this.unitOfWork = unitOfWork;
        }
        public int Create(Banner model)
        {
            BannerRepository.Add(model);
            SaveChange();
            return model.Id;
        }

        public void Delete(string Id)
        {
            var Banner = BannerRepository.GetById(Id);
            BannerRepository.Delete(Banner);
            SaveChange();
        }
        public void Update(Banner model)
        {
            BannerRepository.Update(model);
            SaveChange();
        }

        public Banner GetById(int id)
        {
            var result = BannerRepository.GetById(id);
            return result;
        }

        public IEnumerable<Banner> GetByType(int bannerType)
        {
            return BannerRepository.GetMany(g => g.IsActived == true && g.DeletedAt == null && (int)g.Type == bannerType).OrderBy(x => x.CreatedDate);
        }

        public IEnumerable<Banner> GetList()
        {
            var result = BannerRepository.GetAll().Where(i => i.DeletedAt == null);
            return result;
        }

        public IEnumerable<Banner> GetListForActive()
        {
            return BannerRepository.GetMany(g => g.IsActived == true && g.DeletedAt == null).OrderBy(x => x.CreatedDate);
        }

        public void SaveChange()
        {
            unitOfWork.Commit();
        }
    }
}
