﻿using TTCMS.Data.Entities;
using PagedList;
using TTCMS.Data.Repositories;
using TTCMS.Data.Infrastructure;

namespace TTCMS.Service
{

    public interface IMediaService
    {
        IPagedList<Media> GetPageList(Page page, string keyword);
        Media GetById(int id);
        Media GetByFullPath(string fullPath);
        Media Create(Media model);
        void Update(Media model);
        void Delete(Media model);
        void SaveChange();
    }

    public class MediaService : IMediaService
    {
        private readonly IMediaRepository mediaRepository;
        private readonly IUnitOfWork unitOfWork;

        public MediaService(IMediaRepository MediaRepository, IUnitOfWork unitOfWork)
        {
            this.mediaRepository = MediaRepository;
            this.unitOfWork = unitOfWork;
        }

        public Media GetById(int id)
        {
            return mediaRepository.Get(item => item.Id == id && item.DeletedAt == null);
        }

        public Media GetByFullPath(string fullPath)
        {
            if (string.IsNullOrEmpty(fullPath))
            {
                return null;
            }
            return mediaRepository.Get(item => item.FullPath == fullPath && item.DeletedAt == null);
        }

        public Media Create(Media model)
        {
            var result = mediaRepository.Add(model);
            SaveChange();
            return result;
        }

        public void Update(Media model)
        {
            mediaRepository.Update(model);
            SaveChange();
        }

        public void SaveChange()
        {
            unitOfWork.Commit();
        }

        public IPagedList<Media> GetPageList(Page page, string keyword)
        {

            if (keyword != "")
            {
                return mediaRepository.GetPageDESC(page, x => x.DeletedAt == null  && x.FullPath.Contains(keyword), order => order.CreatedDate);
            }
            return mediaRepository.GetPageDESC(page, x => x.DeletedAt == null, order => order.CreatedDate);

        }

        public void Delete(Media model)
        {
            mediaRepository.Delete(model);
            SaveChange();
        }
    }
}
