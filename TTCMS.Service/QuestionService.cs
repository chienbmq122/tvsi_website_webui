﻿using System.Collections.Generic;
using System.Linq;
using TTCMS.Data.Repositories;
using TTCMS.Data.Infrastructure;
using PagedList;
using TTCMS.Data.Entities;

namespace TTCMS.Service
{
    public interface IQuestionService
    {
        IEnumerable<Question> GetList();
        IEnumerable<Question> GetByKeyword(string keyword);
        IEnumerable<Question> GetListTop(int limit);
        IEnumerable<Question> GetListForActive();
        IEnumerable<Question> GetByTopicId(int topicId);
        IPagedList<Question> GetPagedList(Page page, string search);
        Question GetById(int id);
        Question Create(Question model);
        void Update(Question model);
        void Delete(string Id);
        void SaveChange();
    }
    public class QuestionService : IQuestionService
    {
        private readonly IQuestionRepository _questionRepository;
        private readonly IUnitOfWork unitOfWork;

        

        public QuestionService(IQuestionRepository questionRepository, IUnitOfWork unitOfWork)
        {
            _questionRepository = questionRepository;
            this.unitOfWork = unitOfWork;
        }
        public Question Create(Question model)
        {
            return _questionRepository.Add(model);
        }

        public void Delete(string Id)
        {
            var question = _questionRepository.GetById(Id);
            _questionRepository.Delete(question);
            SaveChange();
        }
        public void Update(Question model)
        {
            _questionRepository.Update(model);
        }

        public Question GetById(int id)
        {
            var result = _questionRepository.GetById(id);
            return result;
        }

        public IEnumerable<Question> GetList()
        {
            return _questionRepository.GetMany(x => x.DeletedAt == null);
        }

        public IEnumerable<Question> GetListForActive()
        {
            return _questionRepository.GetMany(g => g.Active == true && g.DeletedAt == null).OrderBy(x => x.CreatedDate);
        }

        public void SaveChange()
        {
            unitOfWork.Commit();
        }

        public IEnumerable<Question> GetListTop(int limit)
        {
            var result = _questionRepository.GetMany(x => x.DeletedAt == null).OrderBy(x => x.CreatedDate).Take(limit);
            return result;
        }

        public IEnumerable<Question> GetLatest()
        {
            return _questionRepository.GetMany(x => x.DeletedAt == null && x.Active && x.Latest);
        }

        public IEnumerable<Question> GetByTopic(int topicId)
        {
            return _questionRepository.GetMany(x => (x.Topic.Id == topicId || x.Topic.ParentTopicId == topicId) && x.DeletedAt == null);
        }

        public IPagedList<Question> GetPagedList(Page page, string search)
        {
            if (search != "")
            {
                return _questionRepository.GetPageDESC(page,
                    x => x.Language_Questions.Any(
                        y => y.Content.Contains(search) || y.ContentBrief.Contains(search) || y.QuestionName.Contains(search)
                    ) && x.DeletedAt == null,
                    order => order.CreatedDate
                );
            }
            return _questionRepository.GetPageDESC(page, x => x.DeletedAt == null, order => order.CreatedDate);
        }

        public IEnumerable<Question> GetByKeyword(string keyword)
        {
            var questions = _questionRepository.
                GetMany(x => x.DeletedAt == null);
            var result = questions.Where(x => x.Language_Questions.Where(x => x.QuestionName.Contains(keyword)).ToList().Count > 0);
            return questions;
        }

        public IEnumerable<Question> GetByTopicId(int topicId)
        {
            return _questionRepository.GetMany(x => x.DeletedAt == null && x.Topic.Id == topicId);
        }
    }
}
