﻿using log4net;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TTCMS.Data.Entities.WebsiteNews;
using Dapper;

namespace TTCMS.Service
{
    public interface ITVSIWebsiteNewsService
    {
        Task<IEnumerable<stock_News>> GetNews(int limit, int LanguageId, int page);
        Task<stock_News> GetNewsById(string slug, int LanguageId);
        Task<IEnumerable<stock_News>> GetNewsByGroupID(int groupId, int LanguageId, int limit, int page);
        Task<IEnumerable<stock_News>> GetHotest(List<int> skipIds, int LanguageId, int groupId);
        Task<IEnumerable<stock_News>> GetNewest(List<int> skipIds, int LanguageId, int groupId);
        Task<IEnumerable<stock_News>> GetNewsByTitle(string title, int LanguageId, int limit, int page);
        Task<int> GetNumberOfData(int? groupid, string keyword, int LanguageId);
    }
    public class TVSIWebsiteNewsService : BaseService, ITVSIWebsiteNewsService
    {
        private readonly ILog log = LogManager.GetLogger(typeof(TVSIWebsiteNewsService));

        public async Task<IEnumerable<stock_News>> GetHotest(List<int> skipIds, int LanguageId, int groupId)
        {
            var skipIdsString = string.Join(",", skipIds.ToArray());
            try
            {
                return await WithTVSIWebsiteNewsDbConnection(async conn => {
                    var query = $@"SELECT TOP(5)
                            NewsID,
                            GroupID,
                            LanguageID,
                            Title,
                            Description,
                            Content,
                            Date,
                            ImageUrl,
                            IsHotNews,
                            CreatedBy,
                            CreatedDate,
                            LastModifiedBy,
                            LastModifiedDate
                        FROM stock_News
                        WHERE CreatedDate >= '2021-01-01 00:00:00'
                            AND IsHotNews = 1
                            AND LanguageId = @LanguageId
                            AND GroupID = @GroupId
                            " + (skipIdsString.Length > 0 ? $" AND NewsID NOT IN @skipIds" : "")
                            + " ORDER BY CreatedDate DESC";
                    return await conn.QueryAsync<stock_News>(query, new { LanguageId = LanguageId, GroupId = groupId, skipIds });
                });
            }
            catch (Exception ex)
            {
                log.Error(string.Format("LOI METHOD TVSIWebsiteNewsService.GetHotest --- {0}", ex.Message));
                throw;
            }
        }

        public async Task<IEnumerable<stock_News>> GetNewest(List<int> skipIds, int LanguageId, int groupId)
        {
            var skipIdsString = string.Join(",", skipIds.ToArray());
            try
            {
                return await WithTVSIWebsiteNewsDbConnection(async conn => {
                    var query = @"SELECT TOP(5)
                            NewsID,
                            GroupID,
                            LanguageID,
                            Title,
                            Description,
                            Content,
                            Date,
                            ImageUrl,
                            IsHotNews,
                            CreatedBy,
                            CreatedDate,
                            LastModifiedBy,
                            LastModifiedDate
                        FROM stock_News
                        WHERE CreatedDate >= '2021-01-01 00:00:00'
                            AND LanguageId = @LanguageId
                            AND GroupID = @GroupId
                            " + (skipIdsString.Length > 0 ? $" AND NewsID NOT IN @skipIds " : "")
                            + " ORDER BY CreatedDate DESC";
                    return await conn.QueryAsync<stock_News>(query, new { LanguageId = LanguageId, GroupId = groupId, skipIds });
                });
            }
            catch (Exception ex)
            {
                log.Error(string.Format("LOI METHOD TVSIWebsiteNewsService.GetNewest --- {0}", ex.Message));
                throw;
            }
        }

        public async Task<IEnumerable<stock_News>> GetNews(int limit, int LanguageId, int page = 0)
        {
            try
            {
                return await WithTVSIWebsiteNewsDbConnection(async conn => {
                    var query = $@"SELECT
                            NewsID,
                            GroupID,
                            LanguageID,
                            Title,
                            Description,
                            Content,
                            Date,
                            ImageUrl,
                            IsHotNews,
                            CreatedBy,
                            CreatedDate,
                            LastModifiedBy,
                            LastModifiedDate
                        FROM stock_News
                        WHERE CreatedDate >= '2021-01-01 00:00:00'
                        " + (LanguageId > 0 ? $"AND LanguageId = @LanguageId" : "")
                          + $@" AND GroupID in (29, 35, 36)
                        ORDER BY CreatedDate DESC "
                    + (page >= 0 ? $" OFFSET @Offset ROWS FETCH NEXT @Limit ROWS ONLY" : "");
                    return await conn.QueryAsync<stock_News>(query, new { LanguageId = LanguageId, Offset = page * limit, Limit = limit});
                });
            }
            catch (Exception ex)
            {
                log.Error(string.Format("LOI METHOD TVSIWebsiteNewsService.GetNews --- {0}", ex.Message));
                throw;
            }
        }

        public async Task<IEnumerable<stock_News>> GetNewsByGroupID(int groupid, int LanguageId, int limit, int page = 0)
        {
            try
            {
                return await WithTVSIWebsiteNewsDbConnection(async conn => {
                    var query = $@"SELECT
                            NewsID,
                            GroupID,
                            LanguageID,
                            Title,
                            Description,
                            Content,
                            Date,
                            ImageUrl,
                            IsHotNews,
                            CreatedBy,
                            CreatedDate,
                            LastModifiedBy,
                            LastModifiedDate
                        FROM stock_News
                        WHERE GroupId = @GroupId 
                            AND LanguageId = @LanguageId
                            AND CreatedDate >= '2021-01-01 00:00:00'
                        ORDER BY CreatedDate DESC " + (page >= 0 ? $" OFFSET @Offset ROWS FETCH NEXT @Limit ROWS ONLY " : "");
                    return await conn.QueryAsync<stock_News>(query, new { GroupId = groupid , LanguageId = LanguageId, Offset = page * limit , Limit = limit});
                });
            }
            catch (Exception ex)
            {
                log.Error(string.Format("LOI METHOD TVSIWebsiteNewsService.GetNewsByGroupID --- {0}", ex.Message));
                throw;
            }
        }

        public async Task<stock_News> GetNewsById(string slug, int LanguageId)
        {
            try
            {
                return await WithTVSIWebsiteNewsDbConnection(async conn => {
                    var query = $@"SELECT 
                            NewsID,
                            GroupID,
                            LanguageID,
                            Title,
                            Description,
                            Content,
                            Date,
                            ImageUrl,
                            IsHotNews,
                            CreatedBy,
                            CreatedDate,
                            LastModifiedBy,
                            LastModifiedDate
                        FROM stock_News
                        WHERE NewsID = @Slug
                            AND LanguageId = @LanguageId";
                    return await conn.QueryFirstOrDefaultAsync<stock_News>(query, new { Slug = slug, LanguageId });
                });
            }
            catch (Exception ex)
            {
                log.Error(string.Format("LOI METHOD TVSIWebsiteNewsService.GetNewsById --- {0}", ex.Message));
                throw;
            }
        }

        public async Task<int> GetNumberOfData(int? groupid, string keyword, int LanguageId)
        {
            var query = $@"SELECT count(*)
                        FROM stock_News
                        WHERE CreatedDate >= '2021-01-01 00:00:00' "
                        + (LanguageId != 0 ? $" AND LanguageID = @LanguageId " : String.Empty)
                        + (keyword != null ? $" AND FREETEXT(Title, @Keyword) " : String.Empty)
                        + (groupid != null ? $" AND GroupID = @GroupId" : " AND GroupId in (29, 35, 36)");

            try
            {
                return await WithTVSIWebsiteNewsDbConnection(async conn => {
                    
                    return await conn.QueryFirstOrDefaultAsync<int>(query, new { LanguageId, GroupId = groupid, Keyword = keyword});
                });
            }
            catch (Exception ex)
            {
                log.Error(string.Format("LOI METHOD TVSIWebsiteNewsService.GetNumberOfData --- {0}", ex.Message));
                throw;
            }
        }

        public async Task<IEnumerable<stock_News>> GetNewsByTitle(string title, int LanguageId, int limit, int page = 0)
        {
            try
            {
                return await WithTVSIWebsiteNewsDbConnection(async conn => {
                    var query = $@"SELECT
                            NewsID,
                            GroupID,
                            LanguageID,
                            Title,
                            Description,
                            Content,
                            Date,
                            ImageUrl,
                            IsHotNews,
                            CreatedBy,
                            CreatedDate,
                            LastModifiedBy,
                            LastModifiedDate
                        FROM stock_News
                        WHERE LanguageID = @LanguageId
                            AND GROUPID in (29, 35, 36)
                            AND CreatedDate >= '2021-01-01 00:00:00' "
                        + (title.Length > 0 ? $" AND FREETEXT(Title, @Title)" : "")
                        + " ORDER BY CreatedDate DESC "
                    +(page >= 0 ? $" OFFSET @Offset ROWS FETCH NEXT @Limit ROWS ONLY " : "");
                    return await conn.QueryAsync<stock_News>(query, 
                        new { LanguageId, Title = title, Offset = page * limit, Limit = limit});
                });
            }
            catch (Exception ex)
            {
                log.Error(string.Format("LOI METHOD TVSIWebsiteNewsService.GetNewsByTitle --- {0}", ex.Message));
                throw;
            }
        }
        //private string HandleFilter(string createdDate, string keyword, string slug, int languageId, int groupId, List<int> skipIds, bool hotest = false)
        //{
        //    var filter_literals = new List<string>();
        //    if (!string.IsNullOrWhiteSpace(createdDate)) filter_literals.Add($"CreatedDate > {createdDate}");
        //    if (keyword != null && keyword.Length > 0) filter_literals.Add($"LOWER(Title) LIKE LOWER(N'%{keyword}%')");
        //    if (slug != null && slug.Length > 0) filter_literals.Add($"NewsID = {slug}");
        //    if (languageId > 0 && languageId != 3) filter_literals.Add($"LanguageId = {languageId}");
        //    if (groupId > 0) filter_literals.Add($"GroupId = {groupId}");
        //    if (groupId == 0) filter_literals.Add("GroupId in (29, 35, 36)");
        //    if (hotest) filter_literals.Add("AND IsHotNews = 1");
        //    if (skipIds != null && skipIds.Count() > 0)
        //    {
        //        var skipIdsString = string.Join(",", skipIds.ToArray());
        //        filter_literals.Add($"NewsID NOT IN ({skipIdsString})");
        //    }
        //    return filter_literals.Count() > 0 ? "WHERE " + String.Join(" AND ", filter_literals) : string.Empty;
        //}
        //private string HandleOffset(int page, int pageSize)
        //{
        //    return page >= 0 && pageSize > 0 ? $" OFFSET {page * pageSize} ROWS FETCH NEXT {pageSize} ROWS ONLY " : "";
        //}
    }
}
