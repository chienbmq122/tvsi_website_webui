﻿using TTCMS.Data.Entities;
using PagedList;
using TTCMS.Data.Repositories;
using TTCMS.Data.Infrastructure;

namespace TTCMS.Service
{

    public interface IFAQ_MediaService
    {
        IPagedList<FAQ_Media> GetPageList(Page page, string keyword);
        FAQ_Media GetById(int id);
        FAQ_Media GetByFullPath(string fullPath);
        FAQ_Media Create(FAQ_Media model);
        void Update(FAQ_Media model);
        void Delete(FAQ_Media model);
        void SaveChange();
    }

    public class FAQ_MediaService : IFAQ_MediaService
    {
        private readonly IFAQ_MediaRepository mediaRepository;
        private readonly IUnitOfWork unitOfWork;

        public FAQ_MediaService(IFAQ_MediaRepository MediaRepository, IUnitOfWork unitOfWork)
        {
            this.mediaRepository = MediaRepository;
            this.unitOfWork = unitOfWork;
        }

        public FAQ_Media GetById(int id)
        {
            return mediaRepository.Get(item => item.Id == id && item.DeletedAt == null);
        }

        public FAQ_Media GetByFullPath(string fullPath)
        {
            if (string.IsNullOrEmpty(fullPath))
            {
                return null;
            }
            return mediaRepository.Get(item => item.FullPath == fullPath && item.DeletedAt == null);
        }

        public FAQ_Media Create(FAQ_Media model)
        {
            var result = mediaRepository.Add(model);
            SaveChange();
            return result;
        }

        public void Update(FAQ_Media model)
        {
            mediaRepository.Update(model);
            SaveChange();
        }

        public void SaveChange()
        {
            unitOfWork.Commit();
        }

        public IPagedList<FAQ_Media> GetPageList(Page page, string keyword)
        {

            if (keyword != "")
            {
                return mediaRepository.GetPageDESC(page, x => x.DeletedAt == null  && x.FullPath.Contains(keyword), order => order.CreatedDate);
            }
            return mediaRepository.GetPageDESC(page, x => x.DeletedAt == null, order => order.CreatedDate);

        }

        public void Delete(FAQ_Media model)
        {
            mediaRepository.Delete(model);
            SaveChange();
        }
    }
}
