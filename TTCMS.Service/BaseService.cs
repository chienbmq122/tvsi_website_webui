using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace TTCMS.Service
{
    public class BaseService
    {
        private readonly string _crmdbConnectionString;
        private readonly string _emsdbConnectionString;
        private readonly string _commondbConnectionString;
        private readonly string _systemdbConnectionString;
        private readonly string _innoConnectionString;
        private readonly string _tvsiWebsiteNewsDbConnectionString;
        private readonly string _uatWebsiteDbConnectionString;
        protected BaseService()
        {
            _crmdbConnectionString = ConfigurationManager.ConnectionStrings["TTCRMEntities"].ConnectionString;
            _emsdbConnectionString = ConfigurationManager.ConnectionStrings["TTEMSEntities"].ConnectionString;
            _commondbConnectionString = ConfigurationManager.ConnectionStrings["TTCommonEntities"].ConnectionString;
            _systemdbConnectionString = ConfigurationManager.ConnectionStrings["TTSystemNotifiEntities"].ConnectionString;
            _innoConnectionString = ConfigurationManager.ConnectionStrings["TTInnoEntities"].ConnectionString;
            _tvsiWebsiteNewsDbConnectionString = ConfigurationManager.ConnectionStrings["TVSIWebsiteNews"].ConnectionString;
            _uatWebsiteDbConnectionString = ConfigurationManager.ConnectionStrings["UATWebsiteDB"].ConnectionString;
        }
        
        #region "CRM connection"
        // use for buffered queries that return a type
        protected async Task<T> WithCrmDbConnection<T>(Func<IDbConnection, Task<T>> getData)
        {
            try
            {
                using (var connection = new SqlConnection(_crmdbConnectionString))
                {
                    connection.Open();
                    return await getData(connection);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithCRMDBConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithCRMDBConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }

        // use for buffered queries that do not return a type
        protected async Task WithCrmDbConnection(Func<IDbConnection, Task> getData)
        {
            try
            {
                using (var connection = new SqlConnection(_crmdbConnectionString))
                {
                    connection.Open();
                    await getData(connection);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithCRMDBConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithCRMDBConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }

        // use for non-buffered queries that return a type
        protected async Task<TResult> WithCrmDbConnection<TRead, TResult>(Func<IDbConnection, Task<TRead>> getData, Func<TRead, Task<TResult>> process)
        {
            try
            {
                using (var connection = new SqlConnection(_crmdbConnectionString))
                {
                    connection.Open();
                    var data = await getData(connection);
                    return await process(data);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithCRMDBConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithCRMDBConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }
        #endregion   
        #region "Ems connection"
        // use for buffered queries that return a type
        protected async Task<T> WithEmsDbConnection<T>(Func<IDbConnection, Task<T>> getData)
        {
            try
            {
                using (var connection = new SqlConnection(_emsdbConnectionString))
                {
                    connection.Open();
                    return await getData(connection);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithEmsDBConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithEmsDBConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }

        // use for buffered queries that do not return a type
        protected async Task WithEmsDbConnection(Func<IDbConnection, Task> getData)
        {
            try
            {
                using (var connection = new SqlConnection(_emsdbConnectionString))
                {
                    connection.Open();
                    await getData(connection);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithEmsDBConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithEmsDBConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }

        // use for non-buffered queries that return a type
        protected async Task<TResult> WithEmsDbConnection<TRead, TResult>(Func<IDbConnection, Task<TRead>> getData, Func<TRead, Task<TResult>> process)
        {
            try
            {
                using (var connection = new SqlConnection(_emsdbConnectionString))
                {
                    connection.Open();
                    var data = await getData(connection);
                    return await process(data);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithEmsDBConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithEmsDBConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }
        #endregion   
        #region "common connection"
        // use for buffered queries that return a type
        protected async Task<T> WithCommonDbConnection<T>(Func<IDbConnection, Task<T>> getData)
        {
            try
            {
                using (var connection = new SqlConnection(_commondbConnectionString))
                {
                    connection.Open();
                    return await getData(connection);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithCommonDBConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithCommonDBConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }

        // use for buffered queries that do not return a type
        protected async Task WithCommonDbConnection(Func<IDbConnection, Task> getData)
        {
            try
            {
                using (var connection = new SqlConnection(_commondbConnectionString))
                {
                    connection.Open();
                    await getData(connection);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithCommonDBConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithCommonDBConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }

        // use for non-buffered queries that return a type
        protected async Task<TResult> WithCommonDbConnection<TRead, TResult>(Func<IDbConnection, Task<TRead>> getData, Func<TRead, Task<TResult>> process)
        {
            try
            {
                using (var connection = new SqlConnection(_commondbConnectionString))
                {
                    connection.Open();
                    var data = await getData(connection);
                    return await process(data);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithCommonDBConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithCommonDBConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }
        #endregion   
        #region "WithSystemNotifi connection"
        // use for buffered queries that return a type
        protected async Task<T> WithSystemNotificationDbConnection<T>(Func<IDbConnection, Task<T>> getData)
        {
            try
            {
                using (var connection = new SqlConnection(_systemdbConnectionString))
                {
                    connection.Open();
                    return await getData(connection);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithSystemNotifiDBConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithSystemNotifiDBConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }

        // use for buffered queries that do not return a type
        protected async Task WithSystemNotificationDbConnection(Func<IDbConnection, Task> getData)
        {
            try
            {
                using (var connection = new SqlConnection(_systemdbConnectionString))
                {
                    connection.Open();
                    await getData(connection);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithSystemNotifiDBConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithSystemNotifiDBConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }

        // use for non-buffered queries that return a type
        protected async Task<TResult> WithSystemNotificationDbConnection<TRead, TResult>(Func<IDbConnection, Task<TRead>> getData, Func<TRead, Task<TResult>> process)
        {
            try
            {
                using (var connection = new SqlConnection(_systemdbConnectionString))
                {
                    connection.Open();
                    var data = await getData(connection);
                    return await process(data);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithSystemNotifiDBConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithSystemNotifiDBConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }
        #endregion   
        #region "WithInno connection"
        // use for buffered queries that return a type
        protected async Task<T> WithInnoDbConnection<T>(Func<IDbConnection, Task<T>> getData)
        {
            try
            {
                using (var connection = new SqlConnection(_innoConnectionString))
                {
                    connection.Open();
                    return await getData(connection);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithInnoDbConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithInnoDbConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }

        // use for buffered queries that do not return a type
        protected async Task WithInnoDbConnection(Func<IDbConnection, Task> getData)
        {
            try
            {
                using (var connection = new SqlConnection(_innoConnectionString))
                {
                    connection.Open();
                    await getData(connection);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithInnoDbConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithInnoDbConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }

        // use for non-buffered queries that return a type
        protected async Task<TResult> WithInnoDbConnection<TRead, TResult>(Func<IDbConnection, Task<TRead>> getData, Func<TRead, Task<TResult>> process)
        {
            try
            {
                using (var connection = new SqlConnection(_innoConnectionString))
                {
                    connection.Open();
                    var data = await getData(connection);
                    return await process(data);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithInnoDbConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithInnoDbConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }
        #endregion   
        #region "TVSIWebsiteNews connection"
        // use for buffered queries that return a type
        protected async Task<T> WithTVSIWebsiteNewsDbConnection<T>(Func<IDbConnection, Task<T>> getData)
        {
            try
            {
                using (var connection = new SqlConnection(_tvsiWebsiteNewsDbConnectionString))
                {
                    connection.Open();
                    return await getData(connection);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithTVSIWebsiteNewsDbConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithTVSIWebsiteNewsDbConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }

        // use for buffered queries that do not return a type
        protected async Task WithTVSIWebsiteNewsDbConnection(Func<IDbConnection, Task> getData)
        {
            try
            {
                using (var connection = new SqlConnection(_tvsiWebsiteNewsDbConnectionString))
                {
                    connection.Open();
                    await getData(connection);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithTVSIWebsiteNewsDbConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithTVSIWebsiteNewsDbConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }

        // use for non-buffered queries that return a type
        protected async Task<TResult> WithTVSIWebsiteNewsDbConnection<TRead, TResult>(Func<IDbConnection, Task<TRead>> getData, Func<TRead, Task<TResult>> process)
        {
            try
            {
                using (var connection = new SqlConnection(_tvsiWebsiteNewsDbConnectionString))
                {
                    connection.Open();
                    var data = await getData(connection);
                    return await process(data);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithTVSIWebsiteNewsDbConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithTVSIWebsiteNewsDbConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }
        #endregion  
        #region "UATWebisteDb connection"
        // use for buffered queries that return a type
        protected async Task<T> WithUATWebisteDbConnection<T>(Func<IDbConnection, Task<T>> getData)
        {
            try
            {
                using (var connection = new SqlConnection(_uatWebsiteDbConnectionString))
                {
                    connection.Open();
                    return await getData(connection);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithUATWebisteDbDbConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithUATWebisteDbConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }

        // use for buffered queries that do not return a type
        protected async Task WithUATWebisteDbConnection(Func<IDbConnection, Task> getData)
        {
            try
            {
                using (var connection = new SqlConnection(_uatWebsiteDbConnectionString))
                {
                    connection.Open();
                    await getData(connection);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithUATWebisteDbConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithUATWebisteDbConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }

        // use for non-buffered queries that return a type
        protected async Task<TResult> WithUATWebisteDbConnection<TRead, TResult>(Func<IDbConnection, Task<TRead>> getData, Func<TRead, Task<TResult>> process)
        {
            try
            {
                using (var connection = new SqlConnection(_uatWebsiteDbConnectionString))
                {
                    connection.Open();
                    var data = await getData(connection);
                    return await process(data);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithUATWebisteDbConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithUATWebisteDbConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }
        #endregion  
    }
}