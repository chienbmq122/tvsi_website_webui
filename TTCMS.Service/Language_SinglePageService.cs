﻿using System.Collections.Generic;
using System.Linq;
using TTCMS.Data.Entities;
using TTCMS.Data.Repositories;
using TTCMS.Data.Infrastructure;

namespace TTCMS.Service
{

    public interface ILanguage_SinglePageService
    {
        IEnumerable<Language_SinglePage> GetList();
        Language_SinglePage GetBySinglePage(int singlePageId, string languageCode);
        IEnumerable<Language_SinglePage> GetBySinglePage(int singlePageId);
        IEnumerable<Language_SinglePage> GetByRoute(string route);
        Language_SinglePage GetById(int id);
        Language_SinglePage GetById(string languageId, int id);
        bool Check(string languageId, int Id);
        Language_SinglePage Create(Language_SinglePage model);
        void Update(Language_SinglePage model);
        void Delete(int id);
        void SaveChange();
    }

    public class Language_SinglePageService : ILanguage_SinglePageService
    {
        private readonly ILanguage_SinglePageRepository language_SinglePageRepository;
        private readonly IUnitOfWork unitOfWork;

        public Language_SinglePageService(ILanguage_SinglePageRepository language_SinglePageRepository, IUnitOfWork unitOfWork)
        {
            this.language_SinglePageRepository = language_SinglePageRepository;
            this.unitOfWork = unitOfWork;
        }

        #region Language_CategoryService Members
        public IEnumerable<Language_SinglePage> GetList()
        {
            var model = language_SinglePageRepository.GetAll();
            return model;
        }
        public Language_SinglePage GetById(int id)
        {
            return language_SinglePageRepository.Get(x => x.DeletedAt == null && x.Id == id);
        }

        public Language_SinglePage GetById(string languageId, int id)
        {
            var model = language_SinglePageRepository.Get(x => x.Language.Code == languageId && x.SinglePageId == id);
            return model;
        }
        public bool Check(string languageId, int Id)
        {
            var language = GetList().SingleOrDefault(x => x.Language.Code == languageId && x.SinglePageId == Id);
            if (language != null)
            {
                return true;
            }
            return false;

        }
        public Language_SinglePage Create(Language_SinglePage model)
        {
            var result = language_SinglePageRepository.Add(model);
            SaveChange();
            return result;
        }
        public void Update(Language_SinglePage model)
        {
            language_SinglePageRepository.Update(model);
            SaveChange();
        }
        public void Delete(int id)
        {
            var model = language_SinglePageRepository.GetMany(x => x.SinglePageId == id);
            foreach (var lang in model)
            {
                language_SinglePageRepository.Delete(lang);
            }
            SaveChange();
        }

        public void SaveChange()
        {
            unitOfWork.Commit();
        }

        public IEnumerable<Language_SinglePage> GetBySinglePage(int singlePageId)
        {
            return language_SinglePageRepository.GetMany(item => item.DeletedAt == null && item.SinglePage.Id == singlePageId);
        }

        public IEnumerable<Language_SinglePage> GetByRoute(string route)
        {
            return language_SinglePageRepository.GetAll().Where(l => l.Slug == route 
            && l.SinglePage.IsActive && l.SinglePage.DeletedAt == null);
        }

        public Language_SinglePage GetBySinglePage(int singlePageId, string languageCode)
        {
            return language_SinglePageRepository.Get(
                x => x.DeletedAt == null
                && x.SinglePage.Id == singlePageId
                && x.Language.Code == languageCode
            );
        }

        #endregion
    }
}
