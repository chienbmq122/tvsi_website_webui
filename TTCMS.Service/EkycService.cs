using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Dapper;
using log4net;
using Newtonsoft.Json;
using OtpNet;
using TTCMS.Data.Entities;
using TTCMS.Service.Common;


namespace TTCMS.Service
{
    public interface IEkycService
    {
        Task<HashDetail> GetHashImage(string path);

        Task<LivenessDetail> CheckCardLiveness(string hashImage, string clientSession);

        Task<int> CheckClassifyId(string hashImage, string clientSession, string tokenId);

        Task<OcrId> GetOcrCardId(
            string hashImageFront,
            string hashImageBack,
            int type,
            string clientSession,
            string tokenId);

        Task<bool> CheckUnder18(string dateofBirth);

        Task<bool> CheckValidDate(string validDate);

        Task<bool> ValidCardId(string dateCardId);

        Task<bool> CheckCardIdExist(string cardId);

        Task<long?> GetValueIssuePlace(string cardId, string issuePlace);

        Task<string> ShortPlaceOfIssueCardId(string issuePlace);

        Task<string> CheckFaceLiveness(string hashFace, string clientSession, string tokenId);

        Task<FaceEKYC> CheckFaceCompare(
            string hashImageFront,
            string hashFace,
            string clientSession,
            string tokenId);

        Task<GetFaceEKYC> GetHashFace(string path);

        Task<IEnumerable<District>> GetDistrictList(string provinceCode);

        Task<IEnumerable<WardList>> GetWardList(string districtCode);

        Task<string> ValidDateOfBirth(DateTime dateTime);

        Task<bool> UnderAgePolicy(string dateOfBirthh);

        Task<string> EncriptMd5(string content);

        Task<string> RandomNumber(int length = 6);

        Task<string> GenerateTOTP(string secret, DateTime? timeStamp, int step = 60, int digits = 6);

        Task<bool> QueueSMSOTP(string phone, string message, int status);

        Task<bool> VerifyTOTP(
            string clientOTP,
            DateTime? timeStamp,
            string secretKey,
            int step = 60,
            int digits = 6);

        Task<string> ProcessGenSaleID(string province, int value);

        Task<string> GenerateSaleID(string branchID, int value);

        Task<string> CutPlaceIssueCardID(string issuePlace);

        Task<IEnumerable<string>> ValidBank(
            string bankAccNo01,
            string bankAccNo02,
            string bankNo01,
            string bankNo02,
            string bankName01,
            string bankName02,
            string SubBranchNo01,
            string SubBranchNo02);

        Task<bool> IsValidDatetime(string datetime, string formatTime = "dd/MM/yyyy");

        Task<bool> IsValidEmail(string email);

        Task<bool> CheckSaleInfo(string saleID);

        Task<bool> CheckCardID(string cardID);
        Task<string> GetSaleID(string saleID);
        Task<string> GetBankName(string bankNo);
        Task<string> GetSubBranchName(string bankNo, string branchNo);
        Task<IEnumerable<ManInfo>> GetManInfo();
        Task<string> GetBranchID(string saleID);
        Task<string> GetBranchName(string saleID);
        Task<bool> SendEmailOpenAccount(string user, string email, string confirmCode, string confirmSms, string url);
        Task<string> ReplaceEmailContent(string content, EmailData data);
        Task<SaleInfoModel> GetEmailSale(string saleID);

        Task<bool> SendEmailSaleEKYC(string saleName, string fullName, string birhtDay, string cardID, string issueDate,
            string phone, string email, string address, int package, string note, string channelOpen, string department,
            string subject, string emailSend);

        Task<bool> SendMailSystem(string email, string content, string subject, int status);
        Task<bool> UpdateImageEKYC(int regisID, string pathFront, string pathBack, string pathFace);
        Task<bool> DeleteFileEKYC(string path);
        Task<bool> RegisterConsultant(string name, string phonenumber, string email, string request);
    }

    public class EkycService : BaseService, IEkycService
    {
        private readonly ILog log = LogManager.GetLogger(typeof(OpenAccountService));
        private readonly string tokenID = ConfigSettings.ReadSetting("TokenID");
        private readonly string tokenKey = ConfigSettings.ReadSetting("TokenKey");
        private readonly string accessToken = ConfigSettings.ReadSetting("AccessToken");

        public async Task<HashDetail> GetHashImage(string path)
        {
            HashDetail hashImage;
            try
            {
                string urlEKYC = ConfigSettings.ReadSetting("EKYC_AddFile");
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Token-id", tokenID);
                    client.DefaultRequestHeaders.Add("Token-key", tokenKey);
                    client.DefaultRequestHeaders.Authorization =
                        new AuthenticationHeaderValue("Bearer", accessToken);
                    MultipartFormDataContent formData = new MultipartFormDataContent();
                    FileStream fs = System.IO.File.OpenRead(path);
                    StreamContent streamContent = new StreamContent((Stream) fs);
                    byte[] content = await streamContent.ReadAsByteArrayAsync();
                    ByteArrayContent fileContent = new ByteArrayContent(content);
                    content = (byte[]) null;
                    fileContent.Headers.ContentType = MediaTypeHeaderValue.Parse("multipart/form-data");
                    formData.Add((HttpContent) fileContent, "file", Path.GetFileName(path));
                    formData.Add((HttpContent) new StringContent("TVSI-REQUEST"), "title");
                    formData.Add((HttpContent) new StringContent("TVSI-DESCRIPTION"), "description");
                    HttpResponseMessage response = await client.PostAsync(urlEKYC, (HttpContent) formData);
                    string responseString = await response.Content.ReadAsStringAsync();
                    HashFile responseJson = JsonConvert.DeserializeObject<HashFile>(responseString);
                    hashImage = response.StatusCode != HttpStatusCode.OK ? new HashDetail() : responseJson.@object;
                }
            }
            catch (Exception ex)
            {
                log.Error( string.Format("LOI METHOD GetHashImage -- {0}",  ex.Message));
                throw;
            }

            return hashImage;
        }

        public async Task<LivenessDetail> CheckCardLiveness(
            string hashImage,
            string clientSession = "client_session")
        {
            LivenessDetail livenessDetail;
            try
            {
                string urlEKYC = ConfigSettings.ReadSetting("EKYC_CheckCardLiveness");
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Token-id", tokenID);
                    client.DefaultRequestHeaders.Add("Token-key", tokenKey);
                    client.DefaultRequestHeaders.Authorization =
                        new AuthenticationHeaderValue("Bearer", accessToken);
                    client.DefaultRequestHeaders.Add("mac-address", "TVSI-MAC-ADDRESS");
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    string data = JsonConvert.SerializeObject( new
                    {
                        img = hashImage,
                        client_session = "client_session"
                    });
                    HttpResponseMessage response = await client.PostAsync(urlEKYC,
                        (HttpContent) new StringContent(data, Encoding.UTF8, "application/json"));
                    string responseString = await response.Content.ReadAsStringAsync();
                    CardLiveness responseData = JsonConvert.DeserializeObject<CardLiveness>(responseString);
                    livenessDetail = response.StatusCode != HttpStatusCode.OK
                        ? new LivenessDetail()
                        : responseData.@object;
                }
            }
            catch (Exception ex)
            {
                log.Error( string.Format("LOI METHOD CheckCardLiveness --- {0}",  ex.Message));
                throw;
            }

            return livenessDetail;
        }

        public async Task<int> CheckClassifyId(
            string hashImage,
            string clientSession,
            string tokenId)
        {
            int num;
            try
            {
                string urlEKYC = ConfigSettings.ReadSetting("EKYC_CheckClassId");
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Token-id", tokenID);
                    client.DefaultRequestHeaders.Add("Token-key", tokenKey);
                    client.DefaultRequestHeaders.Authorization =
                        new AuthenticationHeaderValue("Bearer", accessToken);
                    client.DefaultRequestHeaders.Add("mac-address", "TVSI-MAC-ADDRESS");
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    string data = JsonConvert.SerializeObject( new
                    {
                        img_card = hashImage,
                        client_session = "client_session",
                        token = "TVSI_Token_id_"
                    });
                    HttpResponseMessage response = await client.PostAsync(urlEKYC,
                        (HttpContent) new StringContent(data, Encoding.UTF8, "application/json"));
                    string responseString = await response.Content.ReadAsStringAsync();
                    ClassifyId responseData = JsonConvert.DeserializeObject<ClassifyId>(responseString);
                    num = response.StatusCode != HttpStatusCode.OK ? -1 : responseData.@object.type;
                }
            }
            catch (Exception ex)
            {
                log.Error( string.Format("LOI METHOD CheckClassifyId --- {0}",  ex.Message));
                throw;
            }

            return num;
        }

        public async Task<OcrId> GetOcrCardId(
            string hashImageFront,
            string hashImageBack,
            int type,
            string clientSession,
            string tokenId)
        {
            OcrId ocrCardId;
            try
            {
                string urlEKYC = ConfigSettings.ReadSetting("EKYC_OCRId");
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Token-id", tokenID);
                    client.DefaultRequestHeaders.Add("Token-key", tokenKey);
                    client.DefaultRequestHeaders.Authorization =
                        new AuthenticationHeaderValue("Bearer", accessToken);
                    client.DefaultRequestHeaders.Add("mac-address", "TVSI-MAC-ADDRESS");
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    string data = JsonConvert.SerializeObject( new
                    {
                        img_front = hashImageFront,
                        img_back = hashImageBack,
                        client_session = "client_session",
                        type = type,
                        crop_param = "",
                        validate_postcode = true,
                        token = "Token_TVSI_DSAHDA"
                    });
                    HttpResponseMessage response = await client.PostAsync(urlEKYC,
                        (HttpContent) new StringContent(data, Encoding.UTF8, "application/json"));
                    string responseString = await response.Content.ReadAsStringAsync();
                    OcrId responseData = JsonConvert.DeserializeObject<OcrId>(responseString);
                    ocrCardId = response.StatusCode != HttpStatusCode.OK ? (OcrId) null : responseData;
                }
            }
            catch (Exception ex)
            {
                log.Error( string.Format("LOI METHOD GetOcrCardId --- {0}",  ex.Message));
                throw;
            }

            return ocrCardId;
        }

        public async Task<bool> CheckUnder18(string dateofBirthh)
        {
           
            try
            {
                DateTime dateOfBirth = DateTime.ParseExact(dateofBirthh, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                DateTime today = DateTime.Today;
                int a = (today.Year * 100 + today.Month) * 100 + today.Day;
                int b = (dateOfBirth.Year * 100 + dateOfBirth.Month) * 100 + dateOfBirth.Day;
                return (a - b) / 10000 >= 18;
            }
            catch (Exception ex)
            {
                log.Error( string.Format("LOI METHOD CheckUnder18 --- {0}",  ex.Message));
                throw;
            }
        }

        public async Task<bool> CheckValidDate(string validDate)
        {
            try
            {
                DateTime validateid = DateTime.ParseExact(validDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                string now = DateTime.Now.ToString("dd/MM/yyyy");
                DateTime datenow =
                    DateTime.ParseExact(now, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                return validateid > datenow;
            }
            catch (Exception ex)
            {
                log.Error( string.Format("LOI METHOD CheckValidDate --- {0}",  ex.Message));
                throw;
            }
        }

        public async Task<bool> ValidCardId(string dateCardId)
        {
            bool flag;
            try
            {
                DateTime dateclient = DateTime.ParseExact(dateCardId, "dd/MM/yyyy",
                    (IFormatProvider) CultureInfo.InvariantCulture);
                string ddatenow = DateTime.Now.ToString("dd/MM/yyyy");
                DateTime datenow = DateTime.ParseExact(ddatenow, "dd/MM/yyyy",
                    (IFormatProvider) CultureInfo.InvariantCulture);
                DateTime clientfifteenyear = dateclient.AddYears(15);
                TimeSpan nDatenow = datenow - dateclient;
                TimeSpan totaldate = clientfifteenyear - dateclient;
                flag = totaldate > nDatenow;
            }
            catch (Exception ex)
            {
                log.Error( string.Format("LOI METHOD ValidCardId --- {0}",  ex.Message));
                throw;
            }

            return flag;
        }

        public async Task<bool> CheckCardIdExist(string cardId)
        {
            try
            {
                var cardIdCrm = await WithCrmDbConnection(async conn =>
                    {
                        string sqlCrm =
                            "select so_cmnd from TVSI_DANG_KY_MO_TAI_KHOAN where so_cmnd = @cardId and trang_thai_tk <> 99";
                        var data = await conn.QueryFirstOrDefaultAsync<string>(sqlCrm,  new
                        {
                            cardId = cardId
                        });
                        return data;
                    });
                var cardIdCommon = await WithCommonDbConnection(async conn =>
                    {
                        string sqlCommon =
                            "select so_cmnd_passport from TVSI_TAI_KHOAN_KHACH_HANG where trang_thai = 1 and so_cmnd_passport =@cardId";
                        var data = await conn.QueryFirstOrDefaultAsync<string>(sqlCommon,  new
                        {
                            cardId = cardId
                        });
                        return data;
                    });
               return string.IsNullOrEmpty(cardIdCrm) && string.IsNullOrEmpty(cardIdCommon);
            }
            catch (Exception ex)
            {
                log.Error( string.Format("LOI METHOD ValidCardId --- {0}",  ex.Message));
                throw;
            }
        }

        public async Task<long?> GetValueIssuePlace(string cardId, string issuePlace)
        {
            try
            {
                string sql =
                    "SELECT A.Category AS Category , A.CategoryName as CategoryName FROM ExtendCust A WHERE A.CategoryType = 13";
                var dataCategories = await WithCrmDbConnection(async conn =>
                    {
                        IEnumerable<Other> valueIssuePlace = await conn.QueryAsync<Other>(sql);
                        return valueIssuePlace;
                    });
                if (cardId.Length == 9)
                    return dataCategories.Where(x => x.CategoryName.Contains(issuePlace))
                        .Select(x => x.Category).First();
                if (cardId.Length != 12)
                {
                    string cutIssuePlace;
                    cutIssuePlace = await ShortPlaceOfIssueCardId(issuePlace);
                    return dataCategories.Where(x => x.CategoryName.Contains(cutIssuePlace))
                        .Select(x => x.Category).First();
                }

                return 0;

            }
            catch (Exception ex)
            {
                log.Error( string.Format("LOI METHOD GetValueIssuePlace --- {0}",  ex.Message));
                throw;
            }
        }

        public async Task<string> ShortPlaceOfIssueCardId(string issuePlace)
        {
            try
            {
                bool checkissuePalce = false;
                string result = string.Empty;
                var listnoicap = new List<string>();
                listnoicap.Add("trật tự");
                listnoicap.Add("xã hội");
                listnoicap.Add("trật tự xã hội");
                listnoicap.Add("cục trưởng cục cảnh sát quản lý hành chính về trật tự xã hội");
                var listnoicap1 = new List<string>();
                listnoicap1.Add("ĐKQL");
                listnoicap1.Add("DLQG");
                listnoicap1.Add("về dân cư");
                listnoicap1.Add("cục trưởng cục cảnh sát đkql cư trú và dlqg về dân cư");
                if (issuePlace != null)
                {
                    for (int i = 0; i < listnoicap.Count; ++i)
                    {
                        if (issuePlace.ToLower().Contains(listnoicap[i]))
                        {
                            result = "CCS QLHC về TTXH";
                            checkissuePalce = true;
                            break;
                        }
                    }

                    if (!checkissuePalce)
                    {
                        for (int i = 0; i < listnoicap1.Count; ++i)
                        {
                            if (issuePlace.ToLower().Contains(listnoicap1[i]))
                            {
                                result = "CCS ĐKQL Cư trú và DLQG về DC";
                                break;
                            }
                        }
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                log.Error(
                     string.Format("LOI METHOD ShortPlaceOfIssueCardId --- {0}",  ex.Message));
                throw;
            }

        }

        public async Task<string> CheckFaceLiveness(
            string hashFace,
            string clientSession,
            string tokenId)
        {
            string str;
            try
            {
                string urlEKYC = ConfigSettings.ReadSetting("EKYC_CheckFaceLiveness");
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Token-id", tokenID);
                    client.DefaultRequestHeaders.Add("Token-key", tokenKey);
                    client.DefaultRequestHeaders.Authorization =
                        new AuthenticationHeaderValue("Bearer", accessToken);
                    client.DefaultRequestHeaders.Add("mac-address", "TVSI-MAC-ADDRESS");
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    string data = JsonConvert.SerializeObject( new
                    {
                        img = hashFace,
                        client_session = "client_session",
                        token = "TVSI_Token"
                    });
                    HttpResponseMessage response = await client.PostAsync(urlEKYC,
                        (HttpContent) new StringContent(data, Encoding.UTF8, "application/json"));
                    string responseString = await response.Content.ReadAsStringAsync();
                    Face responseData = JsonConvert.DeserializeObject<Face>(responseString);
                    str = response.StatusCode != HttpStatusCode.OK ? "" : responseData.@object.liveness;
                }
            }
            catch (Exception ex)
            {
                log.Error( string.Format("LOI METHOD CheckFaceLiveness --- {0}",  ex.Message));
                throw;
            }

            return str;
        }

        public async Task<FaceEKYC> CheckFaceCompare(
            string hashImageFront,
            string hashFace,
            string clientSession,
            string tokenId)
        {
            FaceEKYC faceEkyc;
            try
            {
                string urlEKYC = ConfigSettings.ReadSetting("EKYC_CheckFaceCompare");
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Token-id", tokenID);
                    client.DefaultRequestHeaders.Add("Token-key", tokenKey);
                    client.DefaultRequestHeaders.Authorization =
                        new AuthenticationHeaderValue("Bearer", accessToken);
                    client.DefaultRequestHeaders.Add("mac-address", "TVSI-MAC-ADDRESS");
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    string data = JsonConvert.SerializeObject( new
                    {
                        img_front = hashImageFront,
                        img_face = hashFace,
                        client_session = "client_session",
                        token = "TVSI-SESSION"
                    });
                    HttpResponseMessage response = await client.PostAsync(urlEKYC,
                        (HttpContent) new StringContent(data, Encoding.UTF8, "application/json"));
                    string responseString = await response.Content.ReadAsStringAsync();
                    FaceEKYC responseData = JsonConvert.DeserializeObject<FaceEKYC>(responseString);
                    faceEkyc = response.StatusCode != HttpStatusCode.OK ? new FaceEKYC() : responseData;
                }
            }
            catch (Exception ex)
            {
                log.Error( string.Format("LOI METHOD CheckFaceCompare --- {0}",  ex.Message));
                throw;
            }

            return faceEkyc;
        }

        public async Task<GetFaceEKYC> GetHashFace(string path)
        {
            GetFaceEKYC hashFace;
            try
            {
                string urlEKYC = ConfigSettings.ReadSetting("EKYC_AddFile");
                string title = "TVSI";
                string description = "TVSI";
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Token-id", tokenID);
                    client.DefaultRequestHeaders.Add("Token-key", tokenKey);
                    client.DefaultRequestHeaders.Authorization =
                        new AuthenticationHeaderValue("Bearer", accessToken);
                    client.DefaultRequestHeaders.Add("mac-address", "TVSI-MAC-ADDRESS");
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    using (MultipartFormDataContent formData = new MultipartFormDataContent())
                    {
                        FileStream fs = System.IO.File.OpenRead(path);
                        StreamContent streamContent = new StreamContent((Stream) fs);
                        byte[] content = await streamContent.ReadAsByteArrayAsync();
                        ByteArrayContent fileContent = new ByteArrayContent(content);
                        content = (byte[]) null;
                        fileContent.Headers.ContentType = MediaTypeHeaderValue.Parse("multipart/form-data");
                        formData.Add((HttpContent) fileContent, "file", Path.GetFileName(path));
                        formData.Add((HttpContent) new StringContent(title), "title");
                        formData.Add((HttpContent) new StringContent(description), "description");
                        HttpResponseMessage response = await client.PostAsync(urlEKYC, (HttpContent) formData);
                        string responseString = await response.Content.ReadAsStringAsync();
                        hashFace = JsonConvert.DeserializeObject<GetFaceEKYC>(responseString);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error( string.Format("LOI METHOD GetHashFace --- {0}",  ex.Message));
                throw;
            }

            return hashFace;
        }

        public async Task<IEnumerable<District>> GetDistrictList(
            string provinceCode)
        {
            IEnumerable<District> districtList1;
            try
            {
                districtList1 = await WithCrmDbConnection<IEnumerable<District>>(
                    (Func<IDbConnection, Task<IEnumerable<District>>>) (async conn =>
                    {
                        string sql = "SELECT * FROM District  WHERE status = 1 And ProvinceCode = @ProvinceCode";
                        IEnumerable<District> districtList2 = await conn.QueryAsync<District>(sql,  new
                        {
                            ProvinceCode = provinceCode
                        });
                        sql = (string) null;
                        return districtList2;
                    }));
            }
            catch (Exception ex)
            {
                log.Error( string.Format("LOI METHOD GetDistrictList --- {0}",  ex.Message));
                throw;
            }

            return districtList1;
        }

        public async Task<IEnumerable<WardList>> GetWardList(string districtCode)
        {
            IEnumerable<WardList> wardList1;
            try
            {
                wardList1 = await WithCrmDbConnection<IEnumerable<WardList>>(
                    (Func<IDbConnection, Task<IEnumerable<WardList>>>) (async conn =>
                    {
                        string sql = "SELECT * FROM Ward  WHERE status = 1 AND DistrictCode = @DistrictCode";
                        IEnumerable<WardList> wardList2 = conn.Query<WardList>(sql,  new
                        {
                            DistrictCode = districtCode
                        });
                        sql = (string) null;
                        return wardList2;
                    }));
            }
            catch (Exception ex)
            {
                log.Error( string.Format("LOI METHOD GetWardList --- {0}",  ex.Message));
                throw;
            }

            return wardList1;
        }

        public async Task<string> ValidDateOfBirth(DateTime dateTime)
        {
            string str;
            try
            {
                DateTime today = DateTime.Today;
                int a = (today.Year * 100 + today.Month) * 100 + today.Day;
                int b = (dateTime.Year * 100 + dateTime.Month) * 100 + dateTime.Day;
                int value = (a - b) / 10000;
                str = value <= 150
                    ? (value >= 18 ? (string) null : "Nhà Đầu Tư phải trên 18 tuổi")
                    : "Ngày sinh không hợp lệ";
            }
            catch (Exception ex)
            {
                log.Error( string.Format("LOI METHOD ValidDateOfBirth --- {0}",  ex.Message));
                throw;
            }

            return str;
        }

        public async Task<bool> UnderAgePolicy(string dateOfBirthh)
        {
            bool flag;
            try
            {
                DateTime dateOfBirth = DateTime.ParseExact(dateOfBirthh, "dd/MM/yyyy",
                    (IFormatProvider) CultureInfo.InvariantCulture);
                DateTime today = DateTime.Today;
                int a = (today.Year * 100 + today.Month) * 100 + today.Day;
                int b = (dateOfBirth.Year * 100 + dateOfBirth.Month) * 100 + dateOfBirth.Day;
                flag = (a - b) / 10000 >= 18;
            }
            catch (Exception ex)
            {
                log.Error( string.Format("LOI METHOD UnderAgePolicy --- {0}",  ex.Message));
                throw;
            }

            return flag;
        }

        public async Task<string> EncriptMd5(string content)
        {
            string str;
            try
            {
                byte[] textBytes = Encoding.Default.GetBytes(content);
                MD5CryptoServiceProvider cryptHandler = new MD5CryptoServiceProvider();
                byte[] hash = cryptHandler.ComputeHash(textBytes);
                string ret = "";
                byte[] numArray = hash;
                for (int index = 0; index < numArray.Length; ++index)
                {
                    byte a = numArray[index];
                    ret += a.ToString("x2");
                }

                numArray = (byte[]) null;
                str = ret;
            }
            catch (Exception ex)
            {
                log.Error( string.Format("LOI METHOD EncriptMd5 --- {0}",  ex.Message));
                throw;
            }

            return str;
        }

        public async Task<string> RandomNumber(int length = 6)
        {
            string str;
            try
            {
                byte[] seed = Guid.NewGuid().ToByteArray();
                Random random = new Random(BitConverter.ToInt32(seed, 0));
                int randNumber = 0;
                string randomNumber = "";
                for (int i = 0; i < length; ++i)
                {
                    randNumber = random.Next(48, 58);
                    randomNumber += ((char) randNumber).ToString();
                }

                str = randomNumber;
            }
            catch (Exception ex)
            {
                log.Error( string.Format("LOI METHOD RandomNumber --- {0}",  ex.Message));
                throw;
            }

            return str;
        }

        public async Task<string> GenerateTOTP(
            string secret,
            DateTime? timeStamp,
            int step = 60,
            int digits = 6)
        {
            if (string.IsNullOrEmpty(secret))
                return string.Empty;
            try
            {
                byte[] bytes = Encoding.ASCII.GetBytes(secret);
                var totp = new Totp(bytes, step, totpSize: digits);
                if (!timeStamp.HasValue)
                    timeStamp = DateTime.UtcNow;
                return totp.ComputeTotp(timeStamp.Value);
            }
            catch (Exception ex)
            {
                log.Error( string.Format("LOI METHOD GenerateTOTP --- {0}",  ex.Message));
                return string.Empty;
            }
        }

        public async Task<bool> QueueSMSOTP(string phone, string message, int status)
        {
            bool flag1;
            try
            {
                flag1 = await WithSystemNotificationDbConnection<bool>(
                    (Func<IDbConnection, Task<bool>>) (async conn =>
                    {
                        DynamicParameters parameters = new DynamicParameters();
                        parameters.Add("@noi_dung",  message);
                        parameters.Add("@so_dien_thoai",  phone);
                        parameters.Add("@ma_dich_vu",  "SMS_OTP");
                        parameters.Add("@trang_thai",  status);
                        parameters.Add("@smsid", dbType: new DbType?(DbType.Int32),
                            direction: new ParameterDirection?(ParameterDirection.Output));
                        int num = await conn.ExecuteAsync("TVSI_sHANG_DOI_SMS_INSERT_SMS_OTP",  parameters,
                            commandType: new CommandType?(CommandType.StoredProcedure));
                        bool flag2 = parameters.Get<int>("@smsid") > 0;
                        parameters = (DynamicParameters) null;
                        return flag2;
                    }));
            }
            catch (Exception ex)
            {
                log.Error( string.Format("LOI METHOD QueueSMSOTP --- {0}",  ex.Message));
                throw;
            }

            return flag1;
        }

        public async Task<bool> VerifyTOTP(
            string clientOTP,
            DateTime? timeStamp,
            string secretKey,
            int step = 60,
            int digits = 6)
        {
            try
            {
                if (string.IsNullOrEmpty(clientOTP))
                    return false;
                string serverOTP = await GenerateTOTP(secretKey, timeStamp, step, digits);
                return !string.IsNullOrEmpty(serverOTP) && clientOTP.Equals(serverOTP);
            }
            catch (Exception ex)
            {
                log.Error( string.Format("LOI METHOD VerifyTOTP --- {0}",  ex.Message));
                throw;
            }
        }

        public async Task<string> ProcessGenSaleID(string province, int value)
        {
            string str;
            try
            {
                str = await WithCrmDbConnection(async conn =>
                {
                    string sql = "SELECT Type FROM Province WHERE ProvinceCode = @ProvinceCode And Status = 1";
                    int typeProvince = await conn.QueryFirstOrDefaultAsync<int>(sql,  new
                    {
                        ProvinceCode = province
                    });
                    if (typeProvince > 0)
                    {
                        if (typeProvince == 1)
                        {
                            switch (value)
                            {
                                case 1:
                                    string saleId1 = await GenerateSaleID("12", value);
                                    return saleId1;
                                case 2:
                                    string saleId2 = await GenerateSaleID("53-002", value);
                                    return saleId2;
                                case 3:
                                    string saleId3 = await GenerateSaleID("54", value);
                                    return saleId3;
                            }
                        }

                        if (typeProvince == 2)
                        {
                            switch (value)
                            {
                                case 1:
                                    string saleId4 = await GenerateSaleID("02", value);
                                    return saleId4;
                                case 2:
                                    string saleId5 = await GenerateSaleID("53-001", value);
                                    return saleId5;
                                case 3:
                                    string saleId6 = await GenerateSaleID("54", value);
                                    return saleId6;
                            }
                        }
                    }

                    return "";
                });
            }
            catch (Exception ex)
            {
                log.Error(string.Format("LOI METHOD ProcessGenSaleID --- {0}", ex.Message));
                throw;
            }

            return str;
        }

        public async Task<string> GenerateSaleID(string branchID, int value)
        {
            string saleId;
            try
            {
                saleId = await WithCrmDbConnection(async conn =>
                {
                    string sql = "SELECT * FROM WorkTime WHERE Status = 1 {condition}";
                    sql = value != 2
                        ? sql.Replace("{condition}", "and BranchID = @BranchID")
                        : sql.Replace("{condition}", "and BranchCode = @BranchCode");
                    IEnumerable<GenerateSaleIDModel> data = await conn.QueryAsync<GenerateSaleIDModel>(sql,  new
                    {
                        BranchID = branchID,
                        BranchCode = branchID
                    });
                    if (data.Any())
                    {
                        string min =
                            data.Min( x => x.Count);
                        GenerateSaleIDModel dataSale = data
                            .Select(t => t)
                            .Where((x => x.Count == min))
                            .FirstOrDefault();
                        if (dataSale != null)
                        {
                            int workTimeID = dataSale.WorkTimeID;
                            long countSet = Convert.ToInt64(dataSale.Count) + 1L;
                            string sqlUP = "update WorkTime set Count = @Count where WorkTimeID = @WorkTimeID";
                            if (workTimeID > 0)
                            {
                                int num = await conn.ExecuteAsync(sqlUP,  new
                                {
                                    Count = countSet,
                                    WorkTimeID = workTimeID
                                });
                            }

                            return dataSale.SaleID.Substring(0, 4);
                        }

                    }

                    return "";
                });
            }
            catch (Exception ex)
            {
                log.Error(string.Format("LOI METHOD GenerateSaleID --- {0}",  ex.Message));
                throw;
            }

            return saleId;
        }

        public async Task<string> CutPlaceIssueCardID(string issuePlace)
        {
            try
            {
                bool checknoicap = false;
                string result = string.Empty;
                var listnoicap = new List<string>();
                listnoicap.Add("trật tự");
                listnoicap.Add("xã hội");
                listnoicap.Add("trật tự xã hội");
                listnoicap.Add("cục trưởng cục cảnh sát quản lý hành chính về trật tự xã hội");
                var listnoicap1 = new List<string>();
                listnoicap1.Add("ĐKQL");
                listnoicap1.Add("DLQG");
                listnoicap1.Add("về dân cư");
                listnoicap1.Add("cục trưởng cục cảnh sát đkql cư trú và dlqg về dân cư");
                if (issuePlace != null)
                {
                    for (int i = 0; i < listnoicap.Count; ++i)
                    {
                        if (issuePlace.ToLower().Contains(listnoicap[i]))
                        {
                            result = "CCS QLHC về TTXH";
                            checknoicap = true;
                            break;
                        }
                    }

                    if (!checknoicap)
                    {
                        for (int i = 0; i < listnoicap1.Count; ++i)
                        {
                            if (issuePlace.ToLower().Contains(listnoicap1[i]))
                            {
                                result = "CCS ĐKQL Cư trú và DLQG về DC";
                                break;
                            }
                        }
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                log.Error(string.Format("LOI METHOD GenerateSaleID --- {0}",ex.Message));
                throw;
            }

        }

        public async Task<IEnumerable<string>> ValidBank(
            string bankAccNo01,
            string bankAccNo02,
            string bankNo01,
            string bankNo02,
            string bankAccName01,
            string bankAccName02,
            string subBranchNo01,
            string subBranchNo02)
        {
            try
            {
                var errors = new List<string>();
                bankAccNo01 = bankAccNo01 ?? "";
                bankAccName01 = bankAccName01 ?? "";
                bankNo01 = bankNo01 ?? "";
                subBranchNo01 = subBranchNo01 ?? "";
                bankAccNo02 = bankAccNo02 ?? "";
                bankAccName02 = bankAccName02 ?? "";
                bankNo02 = bankNo02 ?? "";
                subBranchNo01 = subBranchNo01 ?? "";
                string strBankInfo_01 = bankAccNo01 + bankNo01 + subBranchNo01;
                if (!string.IsNullOrEmpty(strBankInfo_01) && (string.IsNullOrEmpty(bankAccNo01) ||
                                                              string.IsNullOrEmpty(bankNo01) ||
                                                              string.IsNullOrEmpty(subBranchNo01)))
                    errors.Add("Bạn phải nhập đầy đủ thông tin cho ngân hàng .");
                return errors;
            }
            catch (Exception ex)
            {
                log.Error( string.Format("LOI METHOD ValidBank --- {0}",  ex.Message));
                throw;
            }

        }

        public async Task<bool> IsValidDatetime(string datetime, string formatTime = "dd/MM/yyyy")
        {
            try
            {
                return DateTime.TryParseExact(datetime, formatTime, CultureInfo.InvariantCulture,
                    DateTimeStyles.None, out DateTime _);
            }
            catch (Exception ex)
            {
                log.Error( string.Format("LOI METHOD isValidDatetime --- {0}",  ex.Message));
                throw;
            }

        }

        public async Task<bool> IsValidEmail(string email)
        {
            if (string.IsNullOrEmpty(email))
                return false;
            try
            {
                Regex _regex = new Regex(
                    "^((([a-z]|\\d|[!#\\$%&'\\*\\+\\-\\/=\\?\\^_`{\\|}~]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])+(\\.([a-z]|\\d|[!#\\$%&'\\*\\+\\-\\/=\\?\\^_`{\\|}~]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])+)*)|((\\x22)((((\\x20|\\x09)*(\\x0d\\x0a))?(\\x20|\\x09)+)?(([\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x7f]|\\x21|[\\x23-\\x5b]|[\\x5d-\\x7e]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])|(\\\\([\\x01-\\x09\\x0b\\x0c\\x0d-\\x7f]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF]))))*(((\\x20|\\x09)*(\\x0d\\x0a))?(\\x20|\\x09)+)?(\\x22)))@((([a-z]|\\d|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])|(([a-z]|\\d|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])([a-z]|\\d|-|\\.|_|~|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])*([a-z]|\\d|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])))\\.)+(([a-z]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])|(([a-z]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])([a-z]|\\d|-|\\.|_|~|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])*([a-z]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])))\\.?$",
                    RegexOptions.IgnoreCase | RegexOptions.ExplicitCapture | RegexOptions.Compiled);
                return _regex.IsMatch(email);
            }
            catch (RegexMatchTimeoutException ex)
            {
                return false;
            }
        }

        public async Task<bool> CheckSaleInfo(string saleID)
        {
            try
            {
                string sql =
                    "Select ma_nhan_vien_quan_ly from TVSI_NHAN_SU t where t.ma_quan_ly = @id_he_thong and trang_thai = 1";
               return await WithEmsDbConnection(async conn =>
                {
                   var data = await conn.QueryFirstOrDefaultAsync<string>(sql,  new
                    {
                        id_he_thong = saleID.Substring(0, 4)
                    });
                   return data != null;
                });
            }
            catch (Exception ex)
            {
                log.Error( string.Format("LOI METHOD CheckSaleInfo --- {0}",  ex.Message));
                throw;
            }
        }

        public async Task<bool> CheckCardID(string cardID)
        {
            try
            {
                string dataCRM = await WithCrmDbConnection(async conn =>
                {
                    var sql =
                        "select trang_thai_tk from TVSI_DANG_KY_MO_TAI_KHOAN where so_cmnd=@so_cmnd and trang_thai_tk <> 99";
                    var str = await conn.QueryFirstOrDefaultAsync<string>(sql, new
                    {
                        so_cmnd = cardID
                    });
                    return str;
                });
                string dataCommon = await WithCommonDbConnection(async conn =>
                {
                    var sql =
                        "select so_cmnd_passport from TVSI_TAI_KHOAN_KHACH_HANG where trang_thai = 1 and so_cmnd_passport =@so_cmnd";
                    var str = await conn.QueryFirstOrDefaultAsync<string>(sql, new
                    {
                        so_cmnd = cardID
                    });
                    return str;
                });
                return string.IsNullOrEmpty(dataCRM) && string.IsNullOrEmpty(dataCommon);
            }
            catch (Exception ex)
            {
                log.Error(string.Format("LOI METHOD CheckCardID --- {0}", ex.Message));
                throw;
            }

        }

        public async Task<string> GetSaleID(string saleID)
        {
            try
            {
                return await WithEmsDbConnection(async conn =>
                {
                    var sql = "Select ma_nhan_vien_quan_ly from TVSI_NHAN_SU t where t.ma_quan_ly = @SaleID and trang_thai = 1";
                    return await conn.QueryFirstOrDefaultAsync<string>(sql,new
                    {
                        SaleID = saleID.Substring(0,4)
                    });
                });
            }
            catch (Exception ex)
            {
                log.Error(string.Format("LOI METHOD GetSaleID --- {0}", ex.Message));
                throw;
            }
        }

        public async Task<string> GetBankName(string bankNo)
        {
            try
            {
                var sql = "SELECT A.ShortName as ShortName FROM BANKLIST A WHERE A.BankNo = @bankNo";
                return await WithInnoDbConnection(async conn =>
                {
                    return await conn.QueryFirstOrDefaultAsync<string>(sql, new
                    {
                        @bankNo = bankNo
                    });
                });
            }
            catch (Exception ex)
            {
                log.Error(string.Format("LOI METHOD GetBankName --- {0}", ex.Message));
                throw;
            }
        }

        public async Task<string> GetSubBranchName(string bankNo, string branchNo)
        {
            try
            {
                var sql =
                        "SELECT A.ShortBranchName as ShortBranchName FROM BRANCHNOLIST A WHERE BankNo = @bankNo AND A.BranchNo=@branchNo"
                    ;
                return await WithInnoDbConnection(async conn =>
                {
                    return await conn.QueryFirstOrDefaultAsync<string>(sql, new
                    {
                        bankNo = bankNo,
                        branchNo = branchNo
                    });
                });
            }
            catch (Exception ex)
            {
                log.Error(string.Format("LOI METHOD GetSubBranchName --- {0}", ex.Message));
                throw;
            }
        }

        public async Task<IEnumerable<ManInfo>> GetManInfo()
        {
            try
            {
                return await WithCommonDbConnection(async conn =>
                {
                    var data = await conn.QueryAsync<ManInfo>("TVSI_sLAY_THONG_TIN_NGUOI_DAI_DIEN");
                    return data;
                });
            }
            catch (Exception ex)
            {
                log.Error(string.Format("LOI METHOD GetManInfo --- {0}", ex.Message));
                throw;
            }
        }

        public async Task<string> GetBranchID(string saleID)
        {
            try
            {
                var sql = "select ma_chi_nhanh from TVSI_DANH_MUC_MA_QUAN_LY where ma_quan_ly = @SaleID";
                return await WithEmsDbConnection(async conn =>
                {
                    return await conn.QueryFirstOrDefaultAsync<string>(sql, new
                    {
                        SaleID = saleID.Substring(0,4)
                    });
                });
            }
            catch (Exception ex)
            {
                log.Error(string.Format("LOI METHOD GetBranchID --- {0}", ex.Message));
                throw;
            }
        }

        public async Task<string> GetBranchName(string saleID)
        {
            try
            {
                var sql = "select ten_bo_phan from TVSI_DANH_MUC_MA_QUAN_LY where ma_quan_ly = @SaleID";
                return await WithEmsDbConnection(async conn =>
                {
                    return await conn.QueryFirstOrDefaultAsync<string>(sql, new
                    {
                        SaleID = saleID.Substring(0,4)
                    });
                });
            }
            catch (Exception ex)
            {
                log.Error(string.Format("LOI METHOD GetBranchName --- {0}", ex.Message));
                throw;
            }
        }

        public async Task<bool> SendEmailOpenAccount(string user, string email, string confirmCode, string confirmSms, string url)
        {
            try
            {
                var smsMessOTP = ConfigSettings.ReadSetting("SMSMessageOTP");
                var subject = "Chứng Khoán Tân Việt(TVSI): Xác thực đăng ký mở tài khoản của Khách hàng " + user;
                
                var templateFile = "";
                templateFile = AppDomain.CurrentDomain.BaseDirectory + @"Template\Mail_EKYCTemplateConfirm.html";
                var content = "";
                using (var readFile = new FileStream(templateFile, FileMode.OpenOrCreate, FileAccess.Read, FileShare.Read))
                {
                    using (var readFileStr = new StreamReader(readFile))
                    {
                        content = readFileStr.ReadToEnd();
                    }
                }      
                if (string.IsNullOrEmpty(content))
                {
                    log.DebugFormat("khong load dc content email ");
                }
                var contentData = new EmailData
                {
                    FullName = user,
                    url = url,
                    ConfirmCode = confirmCode,
                    ConfirmSms = confirmSms
                };
                content = await ReplaceEmailContent(content, contentData);
                var sendMail = await SendMailSystem(email, content, subject, 0);
                if (!sendMail)
                {
                    log.Debug(string.Format("loi gui emai {0}",content));
                }
                return true;

            }
            catch (Exception ex)
            {
                log.Error(string.Format("LOI METHOD SendEmailOpenAccount --- {0}", ex.Message));
                return false;
            }
        }

        public async Task<string> ReplaceEmailContent(string content, EmailData data)
        {
            try
            {
                content = content.Replace("{FullName}", data.FullName);
                content = content.Replace("{url}", data.url);
                content = content.Replace("{confirmCode}", data.ConfirmCode);
                content = content.Replace("{confirmSms}", data.ConfirmSms);
                return content;
            }
            catch (Exception ex)
            {
                log.Error(string.Format("LOI METHOD ReplaceEmailContent --- {0}", ex.Message));
                throw ;
            }
        }

        public async Task<SaleInfoModel> GetEmailSale(string saleID)
        {
            try
            {
                return await WithEmsDbConnection(async conn =>
                {
                    var sql = "SELECT dia_chi_email Email, ho_va_ten SaleName   FROM TVSI_NHAN_SU WHERE ma_quan_ly = @saleID AND trang_thai = 1";
                    return await conn.QueryFirstOrDefaultAsync<SaleInfoModel>(sql, new
                    {
                        saleID = saleID.Substring(0,4)
                    });
                });
            }
            catch (Exception ex)
            {
                log.Error(string.Format("LOI METHOD GetEmailSale --- {0}", ex.Message));
                throw ;
            }
        }

        public async Task<bool> SendEmailSaleEKYC(string saleName, string fullName, string birthDay, string cardID, string issueDate, string phone,
            string email, string address, int package, string note, string channelOpen, string department, string subject,
            string emailSend)
        {
            try
            {
                var templateFile = string.Empty;
                templateFile = AppDomain.CurrentDomain.BaseDirectory + @"Template\Mail_SendSale_EKYC.html";
                var content = "";
                              using (var readFile = new FileStream(templateFile, FileMode.OpenOrCreate, FileAccess.Read, FileShare.Read))
                {
                    using (var readFileStr = new StreamReader(readFile))
                    {
                        content = readFileStr.ReadToEnd();
                    }
                    if (string.IsNullOrEmpty(content))
                    {
                        log.DebugFormat("Not content mail ");
                    }

                    var packName = "";

                    if (package == 1)
                        packName = "Dịch vụ Tư vấn Giao Dịch";
                    if (package == 2)
                        packName = "Dịch vụ Tư Vấn Đầu Tư";
                    if (package == 3)
                        packName = "Dịch Vụ Khách Hàng";

                    content = content.Replace("{Channel_Open}", channelOpen ?? "");
                    content = content.Replace("{Department}", department ?? "");
                    content = content.Replace("{SaleName}", saleName ?? "");
                    content = content.Replace("{FullName}", fullName ?? "");
                    content = content.Replace("{BirthDay}", birthDay ?? "");
                    content = content.Replace("{CardID}", cardID ?? "");
                    content = content.Replace("{IssueDate}", issueDate ?? "");
                    content = content.Replace("{Phone}", phone ?? "");
                    content = content.Replace("{Emai}", email ?? "");
                    content = content.Replace("{Address}", address ?? "");
                    content = content.Replace("{Package}", packName);
                    content = content.Replace("{Note}", note ?? "");
                    content = content.Replace("{CreateDate}", DateTime.Now.ToString("dd/MM/yyyy"));
                   var sendMail =  await SendMailSystem(email, content, subject, 0);
                   if (!sendMail)
                   {
                       log.Debug(string.Format("loi gui mail {0}",content));
                   }
                    return true;
                }

            }
            catch (Exception ex)
            {
                log.Error(string.Format("LOI METHOD GetEmailSale --- {0}", ex.Message));
                return false;
            }
        }

        public async Task<bool> SendMailSystem(string email, string content, string subject, int status)
        {
            try
            {
                await WithSystemNotificationDbConnection(async conn =>
                {
                    var parameters = new DynamicParameters();
                    parameters.Add("@tieu_de", subject);
                    parameters.Add("@noi_dung", content);
                    parameters.Add("@dia_chi_email", email);
                    parameters.Add("@trang_thai", 0);
                    await conn.ExecuteAsync("TVSI_sHANG_DOI_EMAIL_INSERT", parameters,
                        commandType: CommandType.StoredProcedure);
                });
                return true;
            }
            catch (Exception ex)
            {
                log.Error(string.Format("LOI METHOD SendMailSystem --- {0}", ex.Message));
                return false;
            }
        }

        public async Task<bool> UpdateImageEKYC(int regisID, string pathFront, string pathBack, string pathFace)
        {
            try
            {
                return await WithCrmDbConnection(async conn =>
                {
                    var parameters = new DynamicParameters();
                    parameters.Add("@dang_kyid", regisID);
                    parameters.Add("@file_front", pathFront);
                    parameters.Add("@file_back", pathBack);
                    parameters.Add("@file_face", pathFace);
                    return await conn.ExecuteAsync("TVSI_sThem_Anh_Ekyc",parameters,commandType: CommandType.StoredProcedure) > 0;
                });
            }
            catch (Exception ex)
            {
                log.Error(string.Format("LOI METHOD UpdateImageEKYC --- {0}", ex.Message));
                return false;
            }
        }

        public async Task<bool> DeleteFileEKYC(string path)
        {
            try
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                File.Delete(path);
                return true;
            }
            catch (Exception ex)
            {
                log.Error(string.Format("LOI METHOD DeleteFileEKYC --- {0}", ex.Message));
                return false;
            }
        }

        public async Task<bool> RegisterConsultant(string name, string phonenumber, string email, string description)
        {
            try
            {
                await WithCrmDbConnection(async conn =>
                {
                    string sql = $@"
                        INSERT INTO Lead (LeadName, Mobile, Email, Description, Status, ProfileType, LeadSourceID, AssignUser, BranchID, CreatedBy, CreatedDate) 
                        VALUES ('{name}', '{phonenumber}', '{email}', '{description}', 1, 1, 5, '1801-001', '54', '1801-001', getdate())";
                    return await conn.QueryAsync(sql);
                });
                //return await WithCrmDbConnection(async conn =>
                //{
                //    var parameters = new DynamicParameters();
                //    parameters.Add("@CustName", name);
                //    parameters.Add("@Email", email);
                //    parameters.Add("@Mobile", phonenumber);
                //    parameters.Add("@Description", description);
                //    return await conn.ExecuteAsync("TVSI_sDANG_KY_TU_VAN_GOI_DV", parameters, commandType: CommandType.StoredProcedure) > 0;
                //});
                return true;
            }
            catch (Exception ex)
            {
                log.Error(string.Format("LOI METHOD EkycService.RegisterConsultant --- {0}", ex.Message));
                throw ex;
            }
        }
    }
}