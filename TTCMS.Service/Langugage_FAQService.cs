﻿using TTCMS.Data.Entities;
using TTCMS.Data.Repositories;
using TTCMS.Data.Infrastructure;
using PagedList;
using System.Collections.Generic;
using System.Linq;

namespace TTCMS.Service
{
    public interface ILangugage_FAQService
    {
        IEnumerable<Language_FAQ> GetByFAQId(int id);
        IEnumerable<Language_FAQ> GetByLanguageId(int id);
        IEnumerable<Language_FAQ> GetById(int faqId, int languageId);
        void Create(Language_FAQ model);
        void Update(Language_FAQ model);
        void Delete(string Id);
        void SaveChange();
    }
    class Langugage_FAQService: ILangugage_FAQService
    {
        private readonly ILanguage_FAQRepository language_FAQRepository;
        private readonly IUnitOfWork unitOfWork;

        public Langugage_FAQService(ILanguage_FAQRepository language_FAQRepository, IUnitOfWork unitOfWork)
        {
            this.language_FAQRepository = language_FAQRepository;
            this.unitOfWork = unitOfWork;
        }
        public void Create(Language_FAQ model)
        {
            language_FAQRepository.Add(model);
            SaveChange();
        }

        public void Delete(string Id)
        {
            var lFaq = language_FAQRepository.GetById(Id);
            language_FAQRepository.Delete(lFaq);
            SaveChange();
        }
        public void Update(Language_FAQ model)
        {
            language_FAQRepository.Update(model);
            SaveChange();
        }
        public IEnumerable<Language_FAQ> GetByFAQId(int id)
        {
            return language_FAQRepository.GetAll().Where(q => q.FAQId == id);
        }

        public IEnumerable<Language_FAQ> GetById(int faqId, int languageId)
        {
            return language_FAQRepository.GetAll().Where(q => (q.LanguageId == languageId || q.LanguageId == 3) && q.FAQId == faqId);
        }

        public IEnumerable<Language_FAQ> GetByLanguageId(int id)
        {
            return language_FAQRepository.GetAll().Where(q => q.LanguageId == id);
        }

        public void SaveChange()
        {
            unitOfWork.Commit();
        }

       
    }
}
