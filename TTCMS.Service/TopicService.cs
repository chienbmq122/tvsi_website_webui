﻿using System.Collections.Generic;
using System.Linq;
using TTCMS.Data.Repositories;
using TTCMS.Data.Infrastructure;
using PagedList;
using TTCMS.Data.Entities;
using System.Data.Entity.Validation;
using System;

namespace TTCMS.Service
{
    public interface ITopicService
    {
        IEnumerable<Topic> GetList();
        IEnumerable<Topic> GetListTop(int limit);
        IEnumerable<Topic> GetListForActive();
        IEnumerable<Topic> GetParents(int skipId, int parentId);
        IEnumerable<Topic> GetChildren(int parentId);
        IPagedList<Topic> GetPagedList(Page page, string search);
        Topic GetById(int id);
        Topic Create(Topic model);
        void Update(Topic model);
        void Delete(int Id);
        void SaveChange();
    }
    public class TopicService : ITopicService
    {
        private readonly ITopicRepository _topicRepository;
        private readonly IUnitOfWork unitOfWork;

        public TopicService(ITopicRepository topicRepository, IUnitOfWork unitOfWork)
        {
            _topicRepository = topicRepository;
            this.unitOfWork = unitOfWork;
        }
        public Topic Create(Topic model)
        {
            _topicRepository.Add(model);
            return model;
        }

        public void Delete(int Id)
        {
            var topic = _topicRepository.GetById(Id);
            _topicRepository.Delete(topic);
        }
        public void Update(Topic model)
        {
            _topicRepository.Update(model);
        }

        public Topic GetById(int id)
        {
            var result = _topicRepository.GetById(id);
            return result;
        }

        public IEnumerable<Topic> GetList()
        {
            var result = _topicRepository.GetMany(x => x.DeletedAt == null);
            return result;
        }

        public IEnumerable<Topic> GetListForActive()
        {
            return _topicRepository.GetMany(g => g.Active == true && g.DeletedAt == null).OrderBy(x => x.CreatedDate);
        }
        public IEnumerable<Topic> GetParents(int skipId, int parentId = 0)
        {
            if (parentId != 0)
            {
                return _topicRepository.GetMany(x => x.Active && x.Id == parentId && x.Id != skipId && x.DeletedAt == null);
            }
            return _topicRepository.GetMany(x => x.Active && x.ParentTopicId == null && x.Id != skipId && x.DeletedAt == null);
        }

        public void SaveChange()
        {
               unitOfWork.Commit();
        }

        public IEnumerable<Topic> GetListTop(int limit)
        {
            var result = _topicRepository.GetAll().Take(limit);
            return result;
        }
        public IPagedList<Topic> GetPagedList(Page page, string search)
        {
            //if (search != "")
            //{
            //    return _topicRepository.GetPageDESC(page, x => x.TopicName.Contains(search) || x.TopicDescription.Contains(search), order => order.CreatedDate);
            //}
            return _topicRepository.GetPageDESC(page, x => true, order => order.CreatedDate);
        }

        public IEnumerable<Topic> GetChildren(int parentId = 0)
        {
            if (parentId != 0)
            {
                return _topicRepository.GetMany(x => x.Active && x.ParentTopicId == parentId && x.DeletedAt == null);
            }
            return _topicRepository.GetMany(x => x.Active && x.ParentTopicId != null && x.DeletedAt == null);
        }
    }
}
