﻿using System.Collections.Generic;
using System.Linq;
using System;
using TTCMS.Data.Entities;
using PagedList;
using TTCMS.Data.Repositories;
using TTCMS.Data.Infrastructure;

namespace TTCMS.Service
{

    public interface ISiteSettingService
    {
        SiteSetting GetById(int id);
        IEnumerable<SiteSetting> GetManyByParentId(int parentId);
        IEnumerable<SiteSetting> GetManyByParentId(int languageId, int parentId);
        SiteSetting GetByKey(SiteSettingKey key, int languageId = 0, int parentId = 0);
        IEnumerable<SiteSetting> GetManyByKey(SiteSettingKey key, int languageId = 0, int parentId = 0);
        SiteSetting Create(SiteSetting model);
        IEnumerable<SiteSetting> GetByLang(int langId);
        void Update(SiteSetting model);
        void Delete(SiteSetting model);
        void SaveChange();
    }

    public class SiteSettingService : ISiteSettingService
    {
        private readonly ISiteSettingRepository SiteSettingRepository;
        private readonly IUnitOfWork unitOfWork;

        public SiteSettingService(ISiteSettingRepository SiteSettingRepository, IUnitOfWork unitOfWork)
        {
            this.SiteSettingRepository = SiteSettingRepository;
            this.unitOfWork = unitOfWork;
        }

        public SiteSetting GetById(int id)
        {
            return SiteSettingRepository.Get(item => item.Id == id);
        }

        public SiteSetting Create(SiteSetting model)
        {
            var result = SiteSettingRepository.Add(model);
            SaveChange();
            return result;
        }

        public void Update(SiteSetting model)
        {
            SiteSettingRepository.Update(model);
            SaveChange();
        }

        public void Delete(SiteSetting model)
        {
            SiteSettingRepository.Delete(model);
            SaveChange();
        }

        public void SaveChange()
        {
            unitOfWork.Commit();
        }

        public SiteSetting GetByKey(SiteSettingKey key, int languageId = 0, int parentId = 0)
        {
            return SiteSettingRepository.Get(
                x => x.Key == key
                && (parentId == 0 || (parentId > 0 && x.Parent.Id == parentId))
                && (languageId == 0 || (languageId > 0 && x.Language.Id == languageId))
            );
        }

        public IEnumerable<SiteSetting> GetManyByKey(SiteSettingKey key, int languageId = 0, int parentId = 0)
        {
            return SiteSettingRepository.GetMany(
                    x => x.Key == key
                && (parentId == 0 || (parentId > 0 && x.Parent.Id == parentId))
                && (languageId == 0 || (languageId > 0 && x.Language.Id == languageId))
            );
        }

        public IEnumerable<SiteSetting> GetManyByParentId(int languageId, int parentId)
        {
            return SiteSettingRepository.GetMany(x => x.Language.Id == languageId && x.Parent.Id == parentId);
        }

        public IEnumerable<SiteSetting> GetByLang(int langId)
        {
            return SiteSettingRepository.GetAll().Where(x => x.Language?.Id == langId);
        }

        public IEnumerable<SiteSetting> GetManyByParentId(int parentId)
        {
            return SiteSettingRepository.GetMany(x => x.Parent.Id == parentId);
        }
    }
}
