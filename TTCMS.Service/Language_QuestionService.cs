﻿using System.Collections.Generic;
using System.Linq;
using TTCMS.Data.Entities;
using TTCMS.Data.Repositories;
using TTCMS.Data.Infrastructure;
using System;
using System.Globalization;

namespace TTCMS.Service
{

    public interface ILanguage_QuestionService
    {
        IEnumerable<Language_Question> GetList();
        Language_Question GetByQuestion(int questionId, string languageCode);
        IEnumerable<Language_Question> GetByQuestion(int questionId);
        IEnumerable<Language_Question> GetByKeyword(string keyword, string langCode);

        //IEnumerable<Language_Question> GetByRoute(string route);
        Language_Question GetById(int id);
        Language_Question GetById(string languageCode, int id);
        bool Check(string languageId, int Id);
        Language_Question Create(Language_Question model);
        void Update(Language_Question model);
        void Delete(int id);
        void Delete(Language_Question model);
        void SaveChange();
    }

    public class Language_QuestionService : ILanguage_QuestionService
    {
        private readonly ILanguage_QuestionRepository _language_QuestionRepository;
        private readonly IUnitOfWork unitOfWork;

        public Language_QuestionService(ILanguage_QuestionRepository language_QuestionRepository, IUnitOfWork unitOfWork)
        {
            this._language_QuestionRepository = language_QuestionRepository;
            this.unitOfWork = unitOfWork;
        }

        #region Language_QuestionService Members
        public IEnumerable<Language_Question> GetList()
        {
            var model = _language_QuestionRepository.GetAll();
            return model;
        }
        public Language_Question GetById(int id)
        {
            return _language_QuestionRepository.Get(x => x.Id == id);
        }

        public Language_Question GetById(string languageCode, int id)
        {
            var model = _language_QuestionRepository.Get(x => x.Language.Code == languageCode && x.Question.Id == id);
            return model;
        }
        public bool Check(string languageCode, int Id)
        {
            var language = GetList().SingleOrDefault(x => x.Language.Code == languageCode && x.Question.Id == Id);
            if (language != null)
            {
                return true;
            }
            return false;

        }
        public Language_Question Create(Language_Question model)
        {
            var result = _language_QuestionRepository.Add(model);
            return result;
        }
        public void Update(Language_Question model)
        {
            _language_QuestionRepository.Update(model);
        }
        public void Delete(int id)
        {
            var model = _language_QuestionRepository.GetMany(x => x.Question.Id == id);
            foreach (var lang in model)
            {
                _language_QuestionRepository.Delete(lang);
            }
        }

        public void Delete(Language_Question model)
        {
            _language_QuestionRepository.Delete(model);
        }

        public void SaveChange()
        {
            unitOfWork.Commit();
        }

        public IEnumerable<Language_Question> GetByQuestion(int questionId)
        {
            return _language_QuestionRepository.GetMany(item => item.Question.Id == questionId && item.Question.DeletedAt == null);
        }

        //public IEnumerable<Language_Question> GetByRoute(string route)
        //{
        //    return _language_QuestionRepository.GetAll().Where(l => l.Slug == route 
        //    && l.SinglePage.IsActive && l.Language.DeletedAt == null);
        //}

        public Language_Question GetByQuestion(int questionId, string languageCode)
        {
            if (languageCode != "")
            {
                return _language_QuestionRepository.Get(x =>
                    x.Question.Id == questionId
                    && x.Language.Code == languageCode
                );
            }
            return _language_QuestionRepository.Get(x => x.Question.Id == questionId);
        }

        public IEnumerable<Language_Question> GetByKeyword(string keyword, string langCode)
        {

            return _language_QuestionRepository.RunFullTextContainsQuery(keyword);
            //return _language_QuestionRepository.GetMany(x =>
            //    compareInfo.IndexOf(x.QuestionName, keyword, CompareOptions.IgnoreNonSpace) > -1
            //);
        }


        #endregion
    }
}
