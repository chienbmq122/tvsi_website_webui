﻿using System.Data.Entity.ModelConfiguration;
using TTCMS.Data.Entities;

namespace TTCMS.Data.Configurations
{
    class FAQConfiguration : EntityTypeConfiguration<FAQ>
    {
        public FAQConfiguration()
        {
            Property(g => g.Id).IsRequired();
        }
    }
}
