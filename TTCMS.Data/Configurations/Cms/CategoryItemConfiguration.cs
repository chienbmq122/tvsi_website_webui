﻿using System.Data.Entity.ModelConfiguration;
using TTCMS.Data.Entities;

namespace TTCMS.Data.Configurations
{
    public class CategoryItemConfiguration : EntityTypeConfiguration<CategoryItem>
    {
        public CategoryItemConfiguration()
        {
            Property(g => g.Name).IsRequired().HasMaxLength(250);
            Property(g => g.Description).HasMaxLength(500);
            Property(g => g.Slug).IsRequired().HasMaxLength(250);
            Property(g => g.CreatedDate).IsRequired();
        }
    }
}
