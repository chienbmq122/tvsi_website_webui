﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TTCMS.Data.Entities;

namespace TTCMS.Data.Configurations
{
    class Language_BannerConfiguration : EntityTypeConfiguration<Language_Banner>
    {
        public Language_BannerConfiguration()
        {
            Property(g => g.mediaId).IsRequired();
            Property(g => g.LanguageId).IsRequired();
            Property(g => g.BannerId).IsRequired();
            Property(g => g.RedirectURL).IsRequired();

        }
    }
}
