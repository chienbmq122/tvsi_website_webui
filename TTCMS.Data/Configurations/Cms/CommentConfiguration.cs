﻿using System.Data.Entity.ModelConfiguration;
using TTCMS.Data.Entities;

namespace TTCMS.Data.Configurations
{
    public class CommentConfiguration : EntityTypeConfiguration<Comment>
    {
        public CommentConfiguration()
        {
            Property(g => g.Id).IsRequired();
            Property(g => g.QuestionId).IsRequired();
            Property(g => g.ParentCommentId);
            Property(g => g.Content).IsRequired().HasMaxLength(4000);
            Property(g => g.AttachmentURL).HasMaxLength(4000);
            Property(g => g.CreatedBy).IsRequired().HasMaxLength(250);
            Property(g => g.Approved).IsRequired();
        }
    }
}
