﻿using System.Data.Entity.ModelConfiguration;
using TTCMS.Data.Entities;

namespace TTCMS.Data.Configurations
{
    public class Language_FAQConfiguration : EntityTypeConfiguration<Language_FAQ>
    {
        public Language_FAQConfiguration()
        {
            Property(g => g.LanguageId).IsRequired();
            Property(g => g.FAQId).IsRequired();
        }
    }
}
