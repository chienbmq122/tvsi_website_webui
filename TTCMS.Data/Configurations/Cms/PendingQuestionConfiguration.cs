﻿using System.Data.Entity.ModelConfiguration;
using TTCMS.Data.Entities;

namespace TTCMS.Data.Configurations
{
    public class PendingQuestionConfiguration : EntityTypeConfiguration<PendingQuestion>
    {
        public PendingQuestionConfiguration()
        {
            Property(g => g.Id).IsRequired();
            Property(g => g.PendingQuestionTitle).IsRequired().HasMaxLength(250);
            Property(g => g.Content).IsRequired().HasMaxLength(4000);
            Property(g => g.AttachmentURL).IsRequired().HasMaxLength(4000);
            Property(g => g.PhoneNumber).IsRequired().HasMaxLength(15);
            Property(g => g.AccountNumber).HasMaxLength(250);
        }
    }
}
