﻿using TTCMS.Data.Configurations;
using TTCMS.Data.Entities;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;

namespace TTCMS.Data
{
    public class TTCMSDbContext : IdentityDbContext<ApplicationUser>
    {
        public TTCMSDbContext() : base("TTCMSEntities") { }

        static TTCMSDbContext()
        {
            //Database.SetInitializer(new CreateDatabaseIfNotExists<TTCMSDbContext>());
        }
        public static TTCMSDbContext Create()
        {
            return new TTCMSDbContext();
        }

        #region CORE

        public DbSet<Function> Functions { get; set; }
        public DbSet<Language> Languages { get; set; }
        public DbSet<Permission> Permissions { get; set; }
        public DbSet<GAction> GActions { get; set; }
        public DbSet<Language_Function> Language_Functions { get; set; }
        public DbSet<Language_Role> Language_Roles { get; set; }
        public DbSet<Language_GAction> Language_GAction { get; set; }
        public DbSet<AccessStatistic> AccessStatistics { get; set; }
        public DbSet<ActivityLog> ActivityLogs { get; set; }
        public DbSet<SiteSetting> SiteSettings { get; set; }

        #endregion

        #region CMS

        public DbSet<Menu> Menus { get; set; }
        public DbSet<Media> Media { get; set; }
        public DbSet<FAQ_Media> FAQ_Media { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<CategoryItem> CategoryItems { get; set; }
        public DbSet<News> News { get; set; }
        public DbSet<SinglePage> SinglePages { get; set; }
        public DbSet<Language_SinglePage> Language_SinglePages { get; set; }
        public DbSet<Setting> Settings { get; set; }
        public DbSet<Language_Setting> Language_Settings { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Slide> Slides { get; set; }
        public DbSet<FAQ> fAQs { get; set; }
        public DbSet<Language_FAQ> Language_FAQs { get; set; }
        public DbSet<Banner> Banners { get; set; }
        public DbSet<Language_Banner> Language_Banners { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public virtual DbSet<PendingQuestion> PendingQuestions { get; set; }
        public virtual DbSet<Topic> Topics { get; set; }
        public virtual DbSet<Tag> Tag { get; set; }
        public virtual DbSet<Language_Topic> Language_Topic { get; set; }
        public virtual DbSet<Question> Questions { get; set; }
        public virtual DbSet<Language_Question> Language_Question { get; set; }
        public DbSet<QuestionAttachment> QuestionAttachments { get; set; }
        
        #endregion

        public virtual void Commit()
        {
            base.SaveChanges();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Configurations.Add(new FunctionConfiguration());
            modelBuilder.Configurations.Add(new LanguageConfiguration());
            modelBuilder.Configurations.Add(new GActionConfiguration());
            modelBuilder.Configurations.Add(new Language_RoleConfiguration());
            modelBuilder.Configurations.Add(new Language_FunctionConfiguration());
            modelBuilder.Configurations.Add(new Language_GActionConfiguration());
            modelBuilder.Configurations.Add(new AccessStatisticConfiguration());
            modelBuilder.Configurations.Add(new ActivityLogConfiguration());
            modelBuilder.Configurations.Add(new MenuConfiguration());
            modelBuilder.Configurations.Add(new CategoryConfiguration());
            modelBuilder.Configurations.Add(new CategoryItemConfiguration());
            modelBuilder.Configurations.Add(new SinglePageConfiguration());
            modelBuilder.Configurations.Add(new Language_SinglePageConfiguration());
            modelBuilder.Configurations.Add(new SettingConfiguration());
            modelBuilder.Configurations.Add(new Language_SettingConfiguration());
            modelBuilder.Configurations.Add(new UserConfiguration());
            modelBuilder.Configurations.Add(new SlideConfiguration());
            modelBuilder.Configurations.Add(new NewsConfiguration());
            modelBuilder.Configurations.Add(new FAQConfiguration());
            modelBuilder.Configurations.Add(new Language_FAQConfiguration());
            modelBuilder.Configurations.Add(new BannerConfiguration());
            modelBuilder.Configurations.Add(new Language_BannerConfiguration());

            modelBuilder.Entity<Topic>()
                .Property(e => e.CreatedBy)
                .IsRequired()
                .IsUnicode(false);

            modelBuilder.Entity<Topic>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<Topic>()
                .HasMany(e => e.Language_Topic)
                .WithRequired(e => e.Topic)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Topic>()
                .HasMany(e => e.SubTopics)
                .WithOptional(e => e.ParentTopic)
                .HasForeignKey(e => e.ParentTopicId);

            modelBuilder.Entity<Language_Topic>()
                .Property(e => e.TopicName)
                .IsRequired()
                .IsUnicode(true);

            modelBuilder.Entity<Language_Topic>()
                .Property(e => e.TopicDescription)
                .IsUnicode(true);

            modelBuilder.Entity<Language_Topic>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<Language_Topic>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<Language_Topic>()
                .Property(e => e.TopicId)
                .IsRequired();

            modelBuilder.Entity<Question>()
                .Property(e => e.Active)
                .IsRequired();

            modelBuilder.Entity<Question>()
                .Property(e => e.Latest)
                .IsRequired();

            modelBuilder.Entity<Question>()
               .Property(e => e.CreatedBy)
               .IsRequired()
               .IsUnicode(false);

            modelBuilder.Entity<Question>()
               .Property(e => e.UpdatedBy)
               .IsUnicode(false);

            modelBuilder.Entity<Question>()
               .HasMany(e => e.Language_Questions)
               .WithRequired(e => e.Question)
               .WillCascadeOnDelete(false);

            modelBuilder.Entity<Language_Question>()
                .Property(e => e.Content)
                .IsUnicode(true)
                .IsRequired();

            modelBuilder.Entity<Language_Question>()
                .Property(e => e.ContentBrief)
                .IsUnicode(true);

            modelBuilder.Entity<Language_Question>()
                .Property(e => e.QuestionName)
                .IsUnicode(true)
                .IsRequired();

            modelBuilder.Entity<Language_Question>()
                .Property(e => e.LanguageId)
                .IsRequired();

            modelBuilder.Entity<Language_Question>()
                .Property(e => e.CreatedBy)
                .IsRequired()
                .IsUnicode(false);

            modelBuilder.Entity<QuestionAttachment>()
                .Property(e => e.Id)
                .IsRequired();

            modelBuilder.Entity<QuestionAttachment>()
                .Property(e => e.LanguageId)
                .IsRequired();

            modelBuilder.Entity<QuestionAttachment>()
                .Property(e => e.QuestionId)
                .IsRequired();

            modelBuilder.Entity<QuestionAttachment>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<QuestionAttachment>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);
            modelBuilder.Entity<PendingQuestion>()
                .Property(e => e.PendingQuestionTitle)
                .IsUnicode(true);
            modelBuilder.Entity<PendingQuestion>()
                .Property(e => e.PhoneNumber)
                .IsRequired()
                .IsUnicode(true);
            modelBuilder.Entity<PendingQuestion>()
                .Property(e => e.Content)
                .IsRequired()
                .IsUnicode(true);
            modelBuilder.Entity<PendingQuestion>()
                .Property(e => e.CreatedDate)
                .IsRequired();

            modelBuilder.Entity<Comment>()
                .Property(e => e.Content)
                .IsRequired()
                .IsUnicode(true);

            modelBuilder.Entity<Comment>()
                .Property(e => e.CreatedBy)
                .IsRequired()
                .IsUnicode();

            modelBuilder.Entity<Tag>()
                .Property(e => e.TagName)
                .IsRequired()
                .IsUnicode();

            modelBuilder.Entity<Tag>()
                .Property(e => e.CreatedBy)
                .IsRequired()
                .IsUnicode();
        }
    }
}
