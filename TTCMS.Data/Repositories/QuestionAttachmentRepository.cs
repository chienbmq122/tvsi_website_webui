﻿using TTCMS.Data.Infrastructure;
using TTCMS.Data.Entities;

namespace TTCMS.Data.Repositories
{
    public class QuestionAttachmentRepository : RepositoryBase<QuestionAttachment>, IQuestionAttachmentRepository
    {
        public QuestionAttachmentRepository(IDbFactory dbFactory)
           : base(dbFactory)
        {
        }
    }
    public interface IQuestionAttachmentRepository : IRepository<QuestionAttachment>
    {
    }
}
