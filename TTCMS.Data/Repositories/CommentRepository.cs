
using TTCMS.Data.Infrastructure;
using TTCMS.Data.Entities;
namespace TTCMS.Data.Repositories
{
    public class CommentRepository : RepositoryBase<Comment>, ICommentRepository
    {
        public CommentRepository(IDbFactory dbFactory)
            : base(dbFactory)
        {
        }
    }
    public interface ICommentRepository : IRepository<Comment>
    {
    }
}