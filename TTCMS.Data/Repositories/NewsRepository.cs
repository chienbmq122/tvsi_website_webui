﻿
using TTCMS.Data.Infrastructure;
using TTCMS.Data.Entities;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System;

namespace TTCMS.Data.Repositories
{
    public class NewsRepository : RepositoryBase<News>, INewsRepository
        {
        public NewsRepository(IDbFactory dbFactory)
            : base(dbFactory)
            {
            }

        public IEnumerable<News> RunFullTextContainsQuery(string search, int languageId, int? pageIndex)
        {
            if (string.IsNullOrWhiteSpace(search))
            {
                return Enumerable.Empty<News>();
            }

            var searches = search.Split((char[])null, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < searches.Length; i++)
            {
                searches[i] = $"\"{searches[i]}\"";
            }
            var keywords = string.Join(" AND ", searches);

            var query = string.Format("SELECT " +
                "[News].[Id]," +
                "[News].[Title]," +
                "[News].[Content]," +
                "[News].[Description]," +
                "[News].[CategoryId]," +
                "[News].[LanguageId]," +
                "[News].[IsHot]," +
                "[News].[IsActive]," +
                "[News].[CreatedBy]," +
                "[News].[CreatedDate]," +
                "[News].[UpdatedBy]," +
                "[News].[UpdatedDate]" +
                " FROM [News] " +
                " WHERE CONTAINS([News].[QuestionName], @keywords) " +
                " AND [News].[LanguageId] = @languageId" +
                " ORDER BY [News].[CreatedDate]" +
                pageIndex != null ? "" : ""
                );

            return DbContext.Database.SqlQuery<News>(query, new SqlParameter("@keywords", keywords), new SqlParameter("@languageId", languageId));
        }
    }
    public interface INewsRepository : IRepository<News>
    {
        IEnumerable<News> RunFullTextContainsQuery(string search, int languageId, int? pageIndex);
    }
}