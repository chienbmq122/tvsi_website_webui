﻿

using TTCMS.Data.Infrastructure;
using TTCMS.Data.Entities;
namespace TTCMS.Data.Repositories
{
    public class SinglePageRepository : RepositoryBase<SinglePage>, ISinglePageRepository
    {
        public SinglePageRepository(IDbFactory dbFactory)
            : base(dbFactory)
        {
        }
    }
    public interface ISinglePageRepository : IRepository<SinglePage>
    {
    }
}