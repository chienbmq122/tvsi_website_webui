﻿using TTCMS.Data.Infrastructure;
using TTCMS.Data.Entities;

namespace TTCMS.Data.Repositories
{
    public class QuestionRepository : RepositoryBase<Question>, IQuestionRepository
    {
        public QuestionRepository(IDbFactory dbFactory)
           : base(dbFactory)
        {
        }
    }
    public interface IQuestionRepository : IRepository<Question>
    {
    }
}
