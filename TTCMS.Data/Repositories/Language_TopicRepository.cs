﻿
using TTCMS.Data.Infrastructure;
using TTCMS.Data.Entities;

namespace TTCMS.Data.Repositories
{
    public class Language_TopicRepository : RepositoryBase<Language_Topic>, ILanguage_TopicRepository
        {
        public Language_TopicRepository(IDbFactory dbFactory)
            : base(dbFactory)
            {
            }           
        }
    public interface ILanguage_TopicRepository : IRepository<Language_Topic>
    {
    }
}