﻿
using TTCMS.Data.Infrastructure;
using TTCMS.Data.Entities;
namespace TTCMS.Data.Repositories
{
    public class MediaRepository : RepositoryBase<Media>, IMediaRepository
    {
        public MediaRepository(IDbFactory dbFactory)
            : base(dbFactory)
        {
        }
    }
    public interface IMediaRepository : IRepository<Media>
    {
    }
}