﻿
using TTCMS.Data.Infrastructure;
using TTCMS.Data.Entities;
namespace TTCMS.Data.Repositories
{
    public class SettingRepository : RepositoryBase<Setting>, ISettingRepository
    {
        public SettingRepository(IDbFactory dbFactory)
            : base(dbFactory)
        {
        }
    }
    public interface ISettingRepository : IRepository<Setting>
    {
    }
}