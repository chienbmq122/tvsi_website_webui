﻿using System;
using System.Collections.Generic;
using System.Linq;
using TTCMS.Data.Infrastructure;
using TTCMS.Data.Entities;

namespace TTCMS.Data.Repositories
{
    public class Language_FAQRepository : RepositoryBase<Language_FAQ>, ILanguage_FAQRepository
    {
        public Language_FAQRepository(IDbFactory dbFactory)
           : base(dbFactory)
        {
        }
    }
    public interface ILanguage_FAQRepository : IRepository<Language_FAQ>
    {
    }
}
