﻿using TTCMS.Data.Infrastructure;
using TTCMS.Data.Entities;

namespace TTCMS.Data.Repositories
{
    public class TopicRepository : RepositoryBase<Topic>, ITopicRepository
    {
        public TopicRepository(IDbFactory dbFactory)
           : base(dbFactory)
        {
        }
    }
    public interface ITopicRepository : IRepository<Topic>
    {
    }
}
