﻿using TTCMS.Data.Infrastructure;
using TTCMS.Data.Entities;


namespace TTCMS.Data.Repositories
{
    class BannerRepository: RepositoryBase<Banner>, IBannerRepository
    {
        public BannerRepository(IDbFactory dbFactory)
          : base(dbFactory)
        {
        }
    }
    public interface IBannerRepository : IRepository<Banner>
    {
    }
}
