﻿
using TTCMS.Data.Infrastructure;
using TTCMS.Data.Entities;
using System.Collections.Generic;
using System.Linq.Expressions;
using System;
using System.Linq;
using System.Data.SqlClient;
using System.Reflection;


namespace TTCMS.Data.Repositories
{
    public class Language_QuestionRepository : RepositoryBase<Language_Question>, ILanguage_QuestionRepository
        {
        public Language_QuestionRepository(IDbFactory dbFactory)
            : base(dbFactory)
            {
            }

        public IEnumerable<Language_Question> RunFullTextContainsQuery(string search)
        {
            if (string.IsNullOrWhiteSpace(search))
            {
                return Enumerable.Empty<Language_Question>();
            }

            var searches = search.Split((char[])null, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < searches.Length; i++)
            {
                searches[i] = $"\"{searches[i]}\"";
            }
            var keywords = string.Join(" AND ", searches);

            var query = string.Format("SELECT " +
                "[Language_Question].[Id]," +
                "[Language_Question].[QuestionName]," +
                "[Language_Question].[Content]," +
                "[Language_Question].[ContentBrief]," +
                "[Language_Question].[QuestionId]," +
                "[Language_Question].[LanguageId]," +
                "[Language_Question].[CreatedBy]," +
                "[Language_Question].[CreatedDate]," +
                "[Language_Question].[UpdatedBy]," +
                "[Language_Question].[UpdatedDate]" +
                " FROM [Language_Question] " +
                " LEFT JOIN [Question] ON [Language_Question].[QuestionId] = [Question].[Id] " +
                " WHERE CONTAINS([Language_Question].[QuestionName], @keywords) " +
                " AND Question.Active = 1" +
                " ORDER BY [Question].[Priority] DESC, [Language_Question].[CreatedDate] ");

            return DbContext.Database.SqlQuery<Language_Question>(query, new SqlParameter("@keywords", keywords));
        }
    }
    public interface ILanguage_QuestionRepository : IRepository<Language_Question>
    {
        IEnumerable<Language_Question> RunFullTextContainsQuery(string search);

    }
}