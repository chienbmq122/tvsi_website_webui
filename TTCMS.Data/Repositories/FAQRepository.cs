﻿using TTCMS.Data.Infrastructure;
using TTCMS.Data.Entities;

namespace TTCMS.Data.Repositories
{
    public class FAQRepository : RepositoryBase<FAQ>, IFAQRepository
    {
        public FAQRepository(IDbFactory dbFactory)
           : base(dbFactory)
        {
        }
    }
    public interface IFAQRepository : IRepository<FAQ>
    {
    }
}
