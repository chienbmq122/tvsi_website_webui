﻿using TTCMS.Data.Infrastructure;
using TTCMS.Data.Entities;


namespace TTCMS.Data.Repositories
{
    class Language_BannerRepository : RepositoryBase<Language_Banner>, ILanguage_BannerRepository
    {
        public Language_BannerRepository(IDbFactory dbFactory)
         : base(dbFactory)
        {
        }
    }
    public interface ILanguage_BannerRepository : IRepository<Language_Banner>
    {
    }
}
