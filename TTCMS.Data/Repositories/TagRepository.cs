﻿using TTCMS.Data.Infrastructure;
using TTCMS.Data.Entities;

namespace TTCMS.Data.Repositories
{
    public class TagRepository : RepositoryBase<Tag>, ITagRepository
    {
        public TagRepository(IDbFactory dbFactory)
           : base(dbFactory)
        {
        }
    }
    public interface ITagRepository : IRepository<Tag>
    {
    }
}
