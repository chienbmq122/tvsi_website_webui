﻿
using TTCMS.Data.Infrastructure;
using TTCMS.Data.Entities;
namespace TTCMS.Data.Repositories
{
    public class SiteSettingRepository : RepositoryBase<SiteSetting>, ISiteSettingRepository
    {
        public SiteSettingRepository(IDbFactory dbFactory)
            : base(dbFactory)
        {
        }
    }
    public interface ISiteSettingRepository : IRepository<SiteSetting>
    {
    }
}