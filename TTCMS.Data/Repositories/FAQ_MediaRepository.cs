﻿
using TTCMS.Data.Infrastructure;
using TTCMS.Data.Entities;
namespace TTCMS.Data.Repositories
{
    public class FAQ_MediaRepository : RepositoryBase<FAQ_Media>, IFAQ_MediaRepository
    {
        public FAQ_MediaRepository(IDbFactory dbFactory)
            : base(dbFactory)
        {
        }
    }
    public interface IFAQ_MediaRepository : IRepository<FAQ_Media>
    {
    }
}