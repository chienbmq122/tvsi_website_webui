﻿
using TTCMS.Data.Infrastructure;
using TTCMS.Data.Entities;
namespace TTCMS.Data.Repositories
{
    public class CategoryItemRepository : RepositoryBase<CategoryItem>, ICategoryItemRepository
    {
        public CategoryItemRepository(IDbFactory dbFactory)
            : base(dbFactory)
        {
        }
    }
    public interface ICategoryItemRepository : IRepository<CategoryItem>
    {
    }
}