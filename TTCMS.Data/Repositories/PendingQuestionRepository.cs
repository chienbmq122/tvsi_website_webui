﻿using TTCMS.Data.Infrastructure;
using TTCMS.Data.Entities;

namespace TTCMS.Data.Repositories
{
    public class PendingQuestionRepository : RepositoryBase<PendingQuestion>, IPendingQuestionRepository
    {
        public PendingQuestionRepository(IDbFactory dbFactory)
           : base(dbFactory)
        {
        }
    }
    public interface IPendingQuestionRepository : IRepository<PendingQuestion>
    {
    }
}
