﻿
using TTCMS.Data.Infrastructure;
using TTCMS.Data.Entities;

namespace TTCMS.Data.Repositories
{
    public class NewsItemRepository : RepositoryBase<NewsItem>, INewsItemRepository
        {
        public NewsItemRepository(IDbFactory dbFactory)
            : base(dbFactory)
            {
            }
        }
    public interface INewsItemRepository : IRepository<NewsItem>
    {
    }
}