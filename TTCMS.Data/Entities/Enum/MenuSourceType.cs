﻿namespace TTCMS.Data.Entities
{
    public enum MenuSourceType
    {
        Category,
        Page,
        CustomLink
    }
}
