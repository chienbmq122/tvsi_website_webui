﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace TTCMS.Data.Entities.Enum
{
    public enum CategoryChildrenContentDisplayType
    {
        [Description("Categories")]
        Categories,
        [Description("News")]
        News
    }
}
