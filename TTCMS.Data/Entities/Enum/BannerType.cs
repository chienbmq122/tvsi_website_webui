﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTCMS.Data.Entities
{
    public enum BannerType
    {
        [Description("Header")]
        Header,
        [Description("Footer")]
        Footer,
        [Description("Right")]
        Right,
        [Description("Left")]
        Left,
        [Description("AppXtradeMobile")]
        AppXtradeMobile
    }
}
