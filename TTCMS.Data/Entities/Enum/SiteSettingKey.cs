﻿namespace TTCMS.Data.Entities
{
    public enum SiteSettingKey
    {
        Title, // 0
        ButtonText, // 1
        Description, // 2
        Url, // 3
        RedirectUrl, // 4
        AppStore = 15,
        GooglePlay = 16,
        HomeConfigSlider = 30,
        HomeConfigInvestmentForm = 31,
        HomeConfigInvestmentFormItem = 32,
        HomeConfigTradingTool = 33,
        HomeConfigAnalysisCenter = 34,
        HomeConfigAnalysisCenterItem = 35,
        HomeConfigFeaturedNews = 36,
        HomeConfigQuickAccessItem = 37,
    }
}
