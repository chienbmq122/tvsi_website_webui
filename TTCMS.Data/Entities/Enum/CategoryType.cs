﻿using System.ComponentModel;

namespace TTCMS.Data.Entities
{
    public enum CategoryType
    {
        [Description("News")]
        News,
        [Description("Product")]
        Product,
        [Description("Size")]
        Size,
        [Description("Color")]
        Color,
        [Description("District")]
        District,
    }
}
