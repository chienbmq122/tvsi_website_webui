﻿using System.ComponentModel;

namespace TTCMS.Data.Entities
{
    public enum MenuGroupType
    {
        [Description("Main Menu")]
        MainMenu,
        [Description("Header Top Menu")]
        TopMenu,
        [Description("Bot Menu")]
        BotMenu,
        [Description("Category Menu")]
        Category,
        [Description("Category Menu Mobile")]
        Category_Mobile,
        [Description("Page Menu")]
        Page_Menu,
        [Description("Header Menu")]
        HeaderMenu,
    }
}
