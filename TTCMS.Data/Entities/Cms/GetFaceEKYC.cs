namespace TTCMS.Data.Entities
{
    public class GetFaceEKYC
    {
        public string message { get; set; }

        public FaceEKYCHashDetail @object { get; set; }
    }
    public class FaceEKYCHashDetail
    {
        public string fileName { get; set; }

        public string tokenId { get; set; }

        public string description { get; set; }

        public string storageType { get; set; }

        public string title { get; set; }

        public string uploadedDate { get; set; }

        public string hash { get; set; }

        public string fileType { get; set; }
    }
    
}