namespace TTCMS.Data.Entities
{
    public class CategoryItem : BaseEntity
    {
        public CategoryItem()
        {

        }

        public string Name { get; set; }
        public string Description { get; set; }
        public string Keywords { get; set; }
        public string Slug { get; set; }
        public bool IsRedirect { get; set; }
        public bool IsInNewsList { get; set; }
        public string RedirectUrl { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public virtual Language Language { get; set; }
        public virtual Category Category { get; set; }

    }
}
