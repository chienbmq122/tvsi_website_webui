﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TTCMS.Data.Entities
{
    [Table("Province")]
    public class Province : BaseEntity
    {
        public string ProvinceID { get; set; }
        public string ProvinceName { get; set; }  
        public int Status { get; set; }  
    }

    public class ProvinceModel
    {
        public int ProvinceID { get; set; }
        public string ProvinceName { get; set; }
        public string ProvinceCode { get; set; }
    }
}