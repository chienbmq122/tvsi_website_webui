﻿using System.ComponentModel;

namespace TTCMS.Data.Entities
{
    public class Bank : BaseEntity
    {
        [Description("Mã ngân hàng")]
        public string BankNo { get; set; }
        [Description("Tên ngân hàng")] 
        public string ShortName { get; set; }
    }
}