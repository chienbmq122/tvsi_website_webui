namespace TTCMS.Data.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PendingQuestion")]
    public partial class PendingQuestion
    {
        public int Id { get; set; }

        [StringLength(250)]
        public string PendingQuestionTitle { get; set; }

        [Required]
        [StringLength(15)]
        public string PhoneNumber { get; set; }

        [StringLength(250)]
        public string AccountNumber { get; set; }
        public bool Seen { get; set; }

        [Required]
        [StringLength(4000)]
        public string Content { get; set; }

        [StringLength(4000)]
        public string AttachmentURL { get; set; }

        public DateTime CreatedDate { get; set; }
        public string FullName { get; set; }
    }
}

