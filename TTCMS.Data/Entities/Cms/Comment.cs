namespace TTCMS.Data.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Comment")]
    public partial class Comment
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Comment()
        {
            Replies = new HashSet<Comment>();
        }

        public int Id { get; set; }

        public int QuestionId { get; set; }

        public int? ParentCommentId { get; set; }

        [Required]
        [StringLength(4000)]
        public string Content { get; set; }

        [StringLength(4000)]
        public string AttachmentURL { get; set; }
        [StringLength(15)]
        public string PhoneNumber { get; set; }
        public bool AdminCreated { get; set; }

        [Required]
        [StringLength(250)]
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        [StringLength(50)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public bool Approved { get; set; }

        public virtual Question Question { get; set; }

        public virtual ICollection<Comment> Replies { get; set; }

        public virtual Comment ParentComment { get; set; }
    }
}
