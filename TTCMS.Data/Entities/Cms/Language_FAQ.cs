﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTCMS.Data.Entities
{
    public class Language_FAQ:BaseEntity
    {
        public int LanguageId { get; set; }
        public int FAQId { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
        public virtual FAQ faq { get; set; }
        public virtual Language Language { get; set; }
    }
}
