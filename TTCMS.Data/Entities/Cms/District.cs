namespace TTCMS.Data.Entities
{
    public class District
    {
        public int DistrictID { get; set; }

        public string DistrictCode { get; set; }

        public string DistrictName { get; set; }
    }
}