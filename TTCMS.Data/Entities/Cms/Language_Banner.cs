﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTCMS.Data.Entities
{
    public class Language_Banner : BaseEntity
    {
        public string RedirectURL { get; set; }
        public int BannerId { get; set; }
        public int LanguageId { get; set; }
        public int mediaId { get; set; }
        public int? Priority { get; set; }
        public virtual Media Image { get; set; }
        public virtual Banner Banner { get; set; }
        public virtual Language Language { get; set; }
    }
}
