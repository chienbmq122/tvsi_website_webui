using System.Collections.Generic;
using System.ComponentModel;

namespace TTCMS.Data.Entities
{
    public class SuccessListModel<T>
    {
        public SuccessListModel()
        {
            RetCode = "000";
        }

        [Description("Mã kết quả thực hiện")]
        public string RetCode { get; set; }

        [Description("Dữ liệu trả về")]
        public IEnumerable<T> RetData { get; set; } 
    }
}