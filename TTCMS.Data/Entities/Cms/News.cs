using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace TTCMS.Data.Entities
{
    public class News : BaseEntity
    {
        public News()
        {
        }

        public string Title { get; set; }
        public string Description { get; set; }
        public string Content { get; set; }
        public string Keyword { get; set; }
        public bool IsActive { get; set; }
        public bool IsHot { get; set; }
        public string ImageUrl { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public string DeletedBy { get; set; }
        public virtual Language Language { get; set; }
        public virtual Category Category { get; set; }
    }
}
