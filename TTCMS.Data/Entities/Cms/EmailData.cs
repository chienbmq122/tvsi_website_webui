namespace TTCMS.Data.Entities
{
    public class EmailData
    {
        public string FullName { get; set; }
        public string email { get; set; }
        public string url { get; set; }
        public string ConfirmCode { get; set; }
        public string ConfirmSms { get; set; }
        public string dr { get; set; }
        
        public  string DateOfBirth { get; set; }
        public  string CardID { get; set; }
        public  string IssueDate { get; set; }
        public  string CardIssue { get; set; }
        public  string TypeAccount { get; set; }
    }
}