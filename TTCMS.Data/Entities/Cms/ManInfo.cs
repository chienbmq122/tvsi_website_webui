using System;
using System.Collections;

namespace TTCMS.Data.Entities
{
    public class ManInfo : IComparable
    {
        private DateTime _ngay_tao;
        public ManInfo(string gia_tri, string ma_chi_nhanh)
        {
            this.gia_tri = gia_tri;
            this.ma_chi_nhanh = ma_chi_nhanh;
        }

        public string gia_tri { get; set; }

        public string ma_chi_nhanh { get; set; }
        public DateTime ngay_tao { get { return (this._ngay_tao); } }
        
        public int CompareTo(object obj)
        {
            if (obj is ManInfo)
            {
                ManInfo q = (ManInfo)obj;
                return this.ngay_tao.CompareTo(q.ngay_tao);
            }
            else
            {
                throw new ArgumentException("object is not a ManInfo");
            }
        }
    }
    
    public class ManDetail
    {
        public string manName { get; set; }
        public string postionName { get; set; }
        public string manNo { get; set; }
        public string manDate { get; set; }
        public string ManCardId { get; set; }
        public string ManCardDate { get; set; }
        public string ManCardIssue { get; set; }
    }

}