namespace TTCMS.Data.Entities
{
    public class Face
    {
        public Imgs imgs { get; set; }

        public string dataSign { get; set; }

        public string dataBase64 { get; set; }

        public string logID { get; set; }

        public string message { get; set; }

        public string server_version { get; set; }

        public FaceDetail @object { get; set; }

        public int statusCode { get; set; }

        public string challengeCode { get; set; }
    }
    public class Imgs
    {
        public string img { get; set; }
    }
    public class FaceDetail
    {
        public string liveness { get; set; }

        public string blur_face { get; set; }

        public string liveness_msg { get; set; }

        public string is_eye_open { get; set; }
    }
}