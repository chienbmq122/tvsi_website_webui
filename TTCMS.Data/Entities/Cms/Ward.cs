namespace TTCMS.Data.Entities
{
    public class Ward
    {
        public int WardID { get; set; }

        public string WardCode { get; set; }

        public string WardName { get; set; }
    }
}