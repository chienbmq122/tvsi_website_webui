﻿using System.ComponentModel;

namespace TTCMS.Data.Entities
{
    public class SubBranchBank : BaseEntity
    {
        [Description("Mã ngân hàng")]
        public string BankNo { get; set; }
        
        [Description("Mã chi nhánh")]
        public string BranchNo { get; set; }

        [Description("Tên ngân hàng")]
        public string BranchName { get; set; }
    }
}