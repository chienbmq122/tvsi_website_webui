namespace TTCMS.Data.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Topic")]
    public partial class Topic
    {
        public Topic()
        {
            Language_Topic = new HashSet<Language_Topic>();
            Questions = new HashSet<Question>();
            SubTopics = new HashSet<Topic>();
        }

        public int Id { get; set; }

        public bool QuickSearch { get; set; }

        public bool QuickAction { get; set; }

        public bool Active { get; set; }

        [Required]
        [StringLength(50)]
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        [StringLength(50)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }
        [StringLength(50)]
        public string DeletedBy { get; set; }

        public DateTime? DeletedAt { get; set; }

        public int? ParentTopicId { get; set; }
        public int Order { get; set; }        

        public virtual ICollection<Language_Topic> Language_Topic { get; set; }

        public virtual ICollection<Question> Questions { get; set; }

        public virtual ICollection<Topic> SubTopics { get; set; }

        public virtual Topic ParentTopic { get; set; }
        public virtual FAQ_Media Image { get; set; }
    }
}
