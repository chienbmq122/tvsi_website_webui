namespace TTCMS.Data.Entities
{
    public class SaleInfoModel
    {
        public string Email { get; set; }
        public string SaleName { get; set; }
    }
}