namespace TTCMS.Data.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Tags")]
    public partial class Tag
    {
        public Tag()
        {

        }
        public int Id { get; set; }
        [Required]
        [StringLength(100)]
        public string TagName { get; set; }
        public bool QuickSearch { get; set; }
        public int Priority { get; set; }
        [Required]
        [StringLength(50)]
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        [StringLength(50)]
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public virtual Language Language { get; set; }
    }
}
