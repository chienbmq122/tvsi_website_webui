namespace TTCMS.Data.Entities
{
    public class WardList
    {
        public int WardID { get; set; }

        public string WardCode { get; set; }

        public string WardName { get; set; }
    }
}