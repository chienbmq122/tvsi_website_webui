namespace TTCMS.Data.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Question")]
    public partial class Question
    {
        public Question()
        {
            Language_Questions = new HashSet<Language_Question>();
            QuestionAttachments = new HashSet<QuestionAttachment>();
        }

        public int Id { get; set; }

        [Required]
        public int TopicId { get; set; }

        public int NumberOfShared { get; set; }

        public int NumberOfLiked { get; set; }

        public bool Active { get; set; }
        public int Priority { get; set; }
        public bool Latest { get; set; }
        public bool IsWebsiteShown { get; set; }

        [Required]
        [StringLength(50)]
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        [StringLength(50)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }
        public string DeletedBy { get; set; }

        public DateTime? DeletedAt { get; set; }
        public virtual ICollection<Language_Question> Language_Questions { get; set; }
        public virtual ICollection<QuestionAttachment> QuestionAttachments { get; set; }
        public virtual Topic Topic { get; set; }
    }
}
