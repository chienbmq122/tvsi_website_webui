namespace TTCMS.Data.Entities
{
    public class GenerateSaleIDModel
    {
        public int WorkTimeID { get; set; }

        public string SaleID { get; set; }

        public string Count { get; set; }
    }
}