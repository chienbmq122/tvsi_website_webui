namespace TTCMS.Data.Entities
{
    public class OcrId
    {
        public OcrImage imgs { get; set; }

        public string dataSign { get; set; }

        public string dataBase64 { get; set; }

        public string logID { get; set; }

        public string server_version { get; set; }

        public string message { get; set; }

        public string statusCode { get; set; }

        public OcrDetail @object { get; set; }

        public string challengeCode { get; set; }
    }
    public class OcrImage
    {
        public string img_back { get; set; }

        public string img_front { get; set; }
    }
}