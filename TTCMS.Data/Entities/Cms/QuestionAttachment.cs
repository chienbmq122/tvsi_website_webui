namespace TTCMS.Data.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("QuestionAttachment")]
    public partial class QuestionAttachment
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public QuestionAttachment()
        {
        }

        public int Id { get; set; }

        public int QuestionId { get; set; }

        public int Platform { get; set; }
        public int Priority { get; set; }
        public int LanguageId { get; set; }

        [MaxLength]
        public string Description { get; set; }

        [Required]
        [StringLength(50)]
        public string CreatedBy { get; set; }
        
        public DateTime CreatedDate { get; set; }

        [StringLength(50)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }
        public string DeletedBy { get; set; }

        public DateTime? DeletedAt { get; set; }

        public virtual Question Question { get; set; }
        public virtual Language Language { get; set; }
        public virtual FAQ_Media Image { get; set; }
    }
}
