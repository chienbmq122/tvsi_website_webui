namespace TTCMS.Data.Entities
{
    public class FaceEKYC
    {
        public ImgFaces imgs { get; set; }

        public string dataSign { get; set; }

        public string dataBase64 { get; set; }

        public string logID { get; set; }

        public string message { get; set; }

        public string server_version { get; set; }

        public FaceEKYCDetail @object { get; set; }

        public int statusCode { get; set; }

        public string challengeCode { get; set; }
    }
    public class ImgFaces
    {
        public string img_face { get; set; }

        public string img_front { get; set; }
    }
    public class FaceEKYCDetail
    {
        public string msg { get; set; }

        public string result { get; set; }

        public double prob { get; set; }
    }
}