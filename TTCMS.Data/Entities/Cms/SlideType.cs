﻿using System.ComponentModel;

namespace TTCMS.Data.Entities
{
    public enum SlideType
    {
        [Description("Slide")]
        Slide,
        [Description("Quảng cáo")]
        QuangCao,
        [Description("Đối tác")]
        DoiTac,
    }
}
