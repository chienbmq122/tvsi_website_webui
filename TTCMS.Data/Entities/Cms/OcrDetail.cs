using System.Collections.Generic;

namespace TTCMS.Data.Entities
{
    public class OcrDetail
    {
        public string origin_location { get; set; }

        public double name_prob { get; set; }

        public string msg { get; set; }

        public string gender { get; set; }

        public string expire_warning { get; set; }

        public List<string> warning_msg { get; set; }

        public List<double> issue_date_probs { get; set; }

        public int back_type_id { get; set; }

        public string issue_date_warning { get; set; }

        public double valid_date_prob { get; set; }

        public string nation_policy { get; set; }

        public double origin_location_prob { get; set; }

        public string corner_warning { get; set; }

        public string valid_date { get; set; }

        public string issue_date { get; set; }

        public double id_fake_prob { get; set; }

        public string back_corner_warning { get; set; }

        public List<string> warning { get; set; }

        public string id { get; set; }

        public string back_expire_warning { get; set; }

        public string citizen_id_prob { get; set; }

        public string id_probs { get; set; }

        public string msg_back { get; set; }

        public double birth_day_prob { get; set; }

        public string issue_place { get; set; }

        public string id_fake_warning { get; set; }

        public string recent_location { get; set; }

        public int type_id { get; set; }

        public string card_type { get; set; }

        public string birth_day { get; set; }

        public string issue_date_prob { get; set; }

        public string citizen_id { get; set; }

        public string issue_place_prob { get; set; }

        public double recent_location_prob { get; set; }

        public string nationality { get; set; }

        public List<PostCode> post_code { get; set; }

        public string name { get; set; }

        public Tampering tampering { get; set; }

        public long? valuePlace { get; set; }
    }
    public class PostCode
    {
        public List<object> city { get; set; }

        public List<object> district { get; set; }

        public List<object> ward { get; set; }

        public string type { get; set; }
    }
    public class Tampering
    {
        public string is_legal { get; set; }

        public List<object> warning { get; set; }
    }
}