namespace TTCMS.Data.Entities
{
    public class CardLiveness
    {
        public ImagesHash imgs { get; set; }

        public string dataSign { get; set; }

        public string dataBase64 { get; set; }

        public string logID { get; set; }

        public string message { get; set; }

        public string server_version { get; set; }

        public int statusCode { get; set; }

        public LivenessDetail @object { get; set; }

        public string challengeCode { get; set; }
    }
    public class ImagesHash
    {
        public string img { get; set; }
    }
}