using System;
namespace TTCMS.Data.Entities
{
    public class NewsItem : BaseEntity
    {
        public NewsItem()
        {
        }
        public string Slug { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Content { get; set; }
        public string Keywords { get; set; }
        public bool IsActive { get; set; }
        public DateTime Published { get; set; }
        public int CreatedBy { get; set; }
        public short Status { get; set; }
        public virtual Language Language { get; set; }
        public virtual News News { get; set; }
    }
}
