namespace TTCMS.Data.Entities
{
    public class HashDetail
    {
        public string fileName { get; set; }

        public string tokenId { get; set; }

        public string description { get; set; }

        public string storageType { get; set; }

        public string title { get; set; }

        public string uploadedDate { get; set; }

        public string hash { get; set; }

        public string fileType { get; set; }
    }
}