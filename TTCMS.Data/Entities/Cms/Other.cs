namespace TTCMS.Data.Entities
{
    public class Other
    {
        public long? Category { get; set; }

        public string CategoryName { get; set; }
    }
}