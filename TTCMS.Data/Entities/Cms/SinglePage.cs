using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace TTCMS.Data.Entities
{
    public class SinglePage : BaseEntity
    {
        public SinglePage()
        {
            CreatedDate = DateTime.Now;
            Language_SinglePages = new HashSet<Language_SinglePage>();
        }
        public int Views { get; set; }
        public string Img_Thumbnail { get; set; }
        public string CssClass { set; get; }
        public bool IsActive { get; set; }
        public bool IsHot { get; set; }
        public bool IsHome { get; set; }
        public int Order { get; set; }
        public string CreatedById { get; set; }
        public string UpdatedById { get; set; }
        public virtual ApplicationUser CreatedBy { get; set; }
        public virtual ApplicationUser UpdatedBy { get; set; }
        public virtual Category Category { set; get; }
        public virtual ICollection<Language_SinglePage> Language_SinglePages { set; get; }

    }
}
