﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTCMS.Data.Entities
{
    public class SiteLanguage : BaseEntity
    {
        public SiteLanguage()
        {

        }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Icon { get; set; }
        public bool IsActived { get; set; }
        public bool IsDefault { get; set; }
    }
}
