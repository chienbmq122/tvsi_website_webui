using System.Collections.Generic;
namespace TTCMS.Data.Entities
{
    public class Category : BaseEntity
    {
        public Category()
        {
            News = new HashSet<News>();
        }

        public int? ParentID { get; set; }
        public string Icon { get; set; }
        public int Order { get; set; }
        public bool IsActive { get; set; }
        public CategoryLayout Layout { get; set; }
        public virtual Media Image { get; set; }
        public virtual ICollection<News> News { set; get; }
        public virtual ICollection<CategoryItem> CategoryItems { set; get; }
    }
}
