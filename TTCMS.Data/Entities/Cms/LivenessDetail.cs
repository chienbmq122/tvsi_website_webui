namespace TTCMS.Data.Entities
{
    public class LivenessDetail
    {
        public bool face_swapping { get; set; }

        public bool fake_print_photo { get; set; }

        public string liveness { get; set; }

        public bool fake_liveness { get; set; }

        public string liveness_msg { get; set; }
    }
}