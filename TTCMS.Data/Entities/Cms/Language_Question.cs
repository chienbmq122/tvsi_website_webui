namespace TTCMS.Data.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Language_Question
    {
        public int Id { get; set; }

        [Required]
        [StringLength(200)]
        public string QuestionName { get; set; }

        [Required]
        [MaxLength]
        public string Content { get; set; }

        [StringLength(200)]
        public string ContentBrief { get; set; }

        public int QuestionId { get; set; }

        public int LanguageId { get; set; }

        [Required]
        [StringLength(50)]
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        [StringLength(50)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public virtual Question Question { get; set; }
        public virtual Language Language { get; set; }
    }
}
