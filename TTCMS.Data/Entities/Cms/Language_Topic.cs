namespace TTCMS.Data.Entities
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public partial class Language_Topic
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string TopicName { get; set; }

        [StringLength(250)]
        public string TopicDescription { get; set; }

        [Required]
        [StringLength(50)]
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        [StringLength(50)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }
        public int TopicId { get; set; }
        public virtual Topic Topic { get; set; }
        public virtual Language Language { get; set; } 
    }
}
