﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTCMS.Data.Entities
{
    public class FAQ:BaseEntity
    {
        public bool IsActived { get; set; }
        public bool IsDeleted { get; set; }

    }
}
