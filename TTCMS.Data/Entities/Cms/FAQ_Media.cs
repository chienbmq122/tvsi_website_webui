﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TTCMS.Data.Entities
{
    public class FAQ_Media : BaseEntity
    {
        public FAQ_Media()
        {

        }

        public string Name { get; set; }
        public string Path { get; set; }

        [Index(IsUnique = true)]
        [StringLength(2000)]
        public string FullPath { get; set; }
    }
}
