namespace TTCMS.Data.Entities
{
    public class HashFile
    {
        public string message { get; set; }

        public HashDetail @object { get; set; }
    }
}