namespace TTCMS.Data.Entities
{
    public struct EKYCType
    {
        public const string HashFrontImage = "hashimagefront";
        public const string TokenID = "Token-id";
        public const string TokenKey = "Token-key";
        public const string Bearer = "Bearer";
        public const string MacAddress = "mac-address";
        public const string HearderValue = "application/json";
        public const string CardId = "idfront";
        public const string FrontId = "imagefront";
        public const string BackId = "imageback";
        public const string ImageFace = "image";
        public const string PathFaceEkyc = "path";
        public const string PathFileName = "path_image_face";
        public const string TokenId = "TokenId";
        public const string ClientSession = "ClientSession";
        public const string GenOTP = "GenOTP";
    }
}