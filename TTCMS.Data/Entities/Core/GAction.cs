using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace TTCMS.Data.Entities
{
    public class GAction
    {
        public GAction()
        {
            CreatedDate = DateTime.Now;
            Permissions = new HashSet<Permission>();
            Language_GActions = new HashSet<Language_GAction>();
        }
        public string Id { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<DateTime> UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public bool IsActived { get; set; }
        public bool IsDeleted { get; set; }
        public virtual ICollection<Permission> Permissions { set; get; }
        public virtual ICollection<Language_GAction> Language_GActions { set; get; }

    }
}
