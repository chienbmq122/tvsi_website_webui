using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TTCMS.Data.Entities
{
    public class SiteSetting
    {
        public SiteSetting()
        {
            
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public SiteSettingKey Key { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }
        public virtual SiteSetting Parent { get; set; }
        public virtual Language Language { get; set; }
    }
}
