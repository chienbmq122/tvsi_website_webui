namespace TTCMS.Data.Entities.WebsiteNews
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class TVSI_THONG_TIN_TVSI
    {
        [Key]
        public int thong_tin_tvsiid { get; set; }

        public int chuyen_muc_tin_tvsiid { get; set; }

        public int? nganhid { get; set; }

        public int? doanh_nghiepid { get; set; }

        public DateTime ngay_dang_tai { get; set; }

        [Required]
        [StringLength(255)]
        public string tieu_de { get; set; }

        [StringLength(500)]
        public string tom_tat { get; set; }

        [Column(TypeName = "ntext")]
        [Required]
        public string noi_dung { get; set; }

        public int tin_quan_trong { get; set; }

        public int ngon_nguid { get; set; }

        public int trang_thai { get; set; }

        public DateTime ngay_cap_nhat_cuoi { get; set; }

        [Required]
        [StringLength(50)]
        public string nguoi_cap_nhat_cuoi { get; set; }

        public int file_dang_tai { get; set; }

        [StringLength(255)]
        public string duong_dan_file_anh { get; set; }

        public int? thu_tu_hien_thi { get; set; }

        public int hien_thi_on_top { get; set; }

        public Guid rowguid { get; set; }
    }
}
