﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTCMS.Data.Entities
{
    public class Symbol
    {
        public string CODE { get; set; }
        public string COMPANYNAME { get; set; }
    }
}
