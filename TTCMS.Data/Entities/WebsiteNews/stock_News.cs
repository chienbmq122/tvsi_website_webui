namespace TTCMS.Data.Entities.WebsiteNews
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public class stock_News : BaseEntity
    {
        [Key]
        public int NewsID { get; set; }

        public int GroupID { get; set; }

        public int LanguageID { get; set; }

        [Required]
        [StringLength(256)]
        public string Title { get; set; }

        [StringLength(1024)]
        public string Description { get; set; }

        public string Content { get; set; }

        public DateTime Date { get; set; }

        [StringLength(256)]
        public string ImageUrl { get; set; }

        public bool IsHotNews { get; set; }

        [Required]
        [StringLength(256)]
        public string CreatedBy { get; set; }

        [Required]
        [StringLength(256)]
        public string LastModifiedBy { get; set; }

        public DateTime? LastModifiedDate { get; set; }
    }
}
